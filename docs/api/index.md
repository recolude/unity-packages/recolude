# API Reference

## Important Terminology

| Term | Definition |
|-|:-:|
| Subject | A game object in the scene to be recorded. All recordings come from observing a subject's actions. |
| Actor | A game object in the scene that "acts out" what was previously observed of a subject by some recorder. |
