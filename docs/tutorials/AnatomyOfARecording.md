# Anatomy of a Recording

## Definitions

* **Capture** - A capture is an arbitrary amount of data associated with a specific point in time.
* **Recording Hierarchy** - Due to the nature in which one recording can have any number of child recordings, and those child recordings can also have their own set of children and so on, a tree like structure emerges. A recording hierarchy refers to both the root of the tree and all descendants. 

## The Different Parts of a Recording 

A [Recording](xref:Recolude.Core.IRecording) consists of 7 parts: 

1. ID
2. Name
3. Metadata
4. Capture Collections
5. Child Recordings
6. Binaries
7. Binary References

### ID

The ID of a recording is meant to act as a unique identifier throughout the entire hierarchy of recordings. However, since this value can be supplied by any programmer, it's not always guaranteed to be unique. Using libraries provided by Recolude should guarantee uniqueness within the traversal of a recording hierarchy. However, two different recording hierarchies are not guaranteed to have completely separate IDs from one another.

IDs are not generally human readable.

### Name

The name of the recording is meant to be a human readable title to indicate what the data contained within refers too.

### Metadata

The [Metadata](xref:Recolude.Core.Metadata) of a recording is an arbitrary collection of different key value pairs of various types of data that can be used for a multitude of purposes.

To learn more about metadata, checkout the [Using Metadata Guide](UsingMetadata.md).

### Capture Collections

A capture collection consists of:

1. **Name** - A human readable title to represent the collection.
2. **Events**- An aggregation of events of a specific type ordered chronologically.
3. **Signature** - A unique identifier to let the serializer know how to handle the data.

Each recording can have as many or as little capture collections as it wants, of any capture type. If a developer chooses to introduce their own capture type, they must implement the [IEncoder](xref:Recolude.Core.IO.IEncoder`1) interface and register it with the RAPReader/Writer to properly encode the new type of data.


**Side Note**: Traditionally, a recording would have a static 4 capture collections:

1. Position ([Position Collection](xref:Recolude.Core.PositionCollection)) - meant to represent the recording's position in 3D space at a given point in time.
2. Rotation ([Euler Collection](xref:Recolude.Core.EulerRotationCollection)) - meant to represent the recording's rotation in 3D space at a given point in time.
3. Life Cycle ([Enum Collection](xref:Recolude.Core.EnumCollection)) - meant to keep up with the different [life cycle events](https://docs.unity3d.com/Manual/ExecutionOrder.html) that Unity Game Objects experience over the course of an application.
4. Custom Event ([Event Collection](xref:Recolude.Core.CustomEventCollection)) - A catch all for capturing program events considered important by developers, either for playback or analytical purposes.


### Child Recordings

An array of extra recordings which are meant to be considered the  children of the given recording. No elements within the array of children should ever be null, and attempting to serialize such a recording will throw an exception. 

Due to the nature of classes in C#, a recording, if one is careless, could theoretically end up having a reference to itself somewhere within it's descendants. This is bad! The recursive nature of the systems in place will break. Break hard! Don't do that!

### Binaries

The binaries found within a recording are just a collection of bytes. A [IBinary](xref:Recolude.Core.IBinary) consists of:

1. **Name** - A human readable title to represent the binary.
2. **Data** - A stream to read the binary data from.
3. **Size** - The number of bytes the binary contains.
4. **Metadata** - Extra metadata about the binary.

### Binary References

Recordings can also reference binaries that are not immediately present. A [Binary Reference](xref:Recolude.Core.BinaryReference) consists of:

1. **Name** - A human readable title to represent the binary.
2. **URI** - A location in which the binary is presumed to reside.
3. **Size** - The number of bytes the binary is expected to contain.
4. **Metadata** - Extra metadata about the binary.

## Hierarchy Strategies

Listed in this section are only suggestions/words of advice, and should not be taken as law. The flexible nature of how Recordings can be constructed is a double edge sword. Mixing and matching different strategies for structuring a recording within a single project can cause some heartache if not careful. 

### Traditional

The traditional method of structuring recordings is to flatten every object that gets rendered in 3D space onto the first layer of the recording hierarchy. Any recordings past the depth of 1 are not to be explicitly rendered in 3D space and serves to assist in configuring the recordings playback or to be data for analytics to be computed from. All data found on the root recording is considered "global".

### Scene Capture

Another method of structuring the hierarchy of a recording is to require that every recording within the tree corresponds to an object within 3D space that may or may not be rendered. This strategy mimics the scene hierarchy of Unity of which is composed of Game Objects. This can be overkill in some cases, but is especially useful for reconstructing a skeletal animation driven by IK, as seen in some VR rigs.