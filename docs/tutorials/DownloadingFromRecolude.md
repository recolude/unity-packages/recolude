# Downloading From Recolude

Much like [uploading to Recolude](Uploading%20To%20Recolude.html), the easiest way of getting started is by acquiring an instance of a [RecoludeConfig](xref:Recolude.API.RecoludeConfig). Once you have an instance to a config, you can call [LoadRecording](xref:Recolude.API.RecoludeConfig.LoadRecording*) to build yourself an [ILoadRecordingOperation](xref:Recolude.API.RecordingStorage.ILoadRecordingOperation). This is an abstract [command](https://sourcemaking.com/design_patterns/command) meant to encapsulate the HTTP download request, allowing developers to easily run the fetch asynchronously in something like a [Coroutine](https://docs.unity3d.com/Manual/Coroutines.html).

Once you have the command, you can then run it by calling it's [Run](xref:Recolude.API.RecordingStorage.IRecordingOperation.Run) method. It's advised you yield to this inside a coroutine as this is an asynchronous web request. You can check if anything went wrong by seeing if the [Error](xref:Recolude.API.RecordingStorage.IRecordingOperation.Error*) method returns anything. If it doesn't then congratulations, you just downloaded a recording from Recolude!

You can now view the recording you just pulled down by building a [playback](xref:Recolude.Core.Playback.PlaybackBehavior) from it. To learn more about this checkout the [Playback Quick Start Guide](ProgrammaticRecordingQuickStart.md).

## Example

```c#
using UnityEngine;
using Recolude.API;
using Recolude.Core.Playback;
using System.Collections;

public class DownloadExample : MonoBehaviour
{

    [SerializeField]
    private RecoludeConfig config;

    void Start()
    {
        StartCoroutine(ExampleDownload("c9bf45dc-0f47-4b0d-b9a5-fe4b87ce4096"));
    }

    IEnumerator ExampleDownload(string recordingID)
    {
        var req = config.LoadRecording(recordingID);
        yield return req.Run();

        // Log if an error occurred
        if (req.Error() != null)
        {
            Debug.LogError(req.Error());
            yield break;
        }
        // Play the recording we just downloaded
        var playback = PlaybackBehavior.Build(req.Recording()).Play();
    }

}
```
