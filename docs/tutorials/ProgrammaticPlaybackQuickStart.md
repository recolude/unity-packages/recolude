# Programmatic Playback Quick Start

_This tutorial requires Recolude.Core to be installed in the project._

The first step to playback is first having a recording to actually playback. Once you have reference to a recording you can use it to build a [PlaybackBehavior](xref:Recolude.Core.Playback.PlaybackBehavior) instance using the static [Build()](xref:Recolude.Core.Playback.PlaybackBehavior.Build*) function.

```c#
var myPlaybackBehavior = PlaybackBehavior.Build(myRecording);
```

## Playback Controllers

At the core of the playback system, all data is stored and used by [IPlaybackControllers](xref:Recolude.Core.Playback.IPlaybackController). The playback system ends up calling the various methods of the registered playback controllers throughout the systems lifetime. Recolude has created a Playback Controller per Capture collection type. For example, the [Position Collection](xref:Recolude.Core.PositionCollection) has a corresponding [Position Playback Controller](xref:Recolude.Core.Playback.CollectionControllers.PositionPlayback). While we've found this is to be a nice rule of thumb for building playback controllers, it's not a requirement.

To assist in building playback systems for continous datasets, we've created the abstract class [InterpolatePlayback](xref:Recolude.Core.Playback.CollectionControllers.InterpolatePlayback`1) in which you can extend. This class has one method in which you must implement called [Interpolate](<xref:Recolude.Core.Playback.CollectionControllers.InterpolatePlayback`1.Interpolate(`0,`0,System.Single)>). Once you implement that, the rest of the class can take care of playback for you. It's really that simple. Below is the source code for Recolude's position playback:

```c#
using UnityEngine;

namespace Recolude.Core.Playback.CollectionControllers
{
    public class PositionPlayback : InterpolatePlayback<Capture<Vector3>>
    {
        private Transform representation;

        public PositionPlayback(
            Transform representation,
            Capture<Vector3>[] captures
        ) : base(captures)
        {
            this.representation = representation;
        }

        public override void Interpolate(
            Capture<Vector3> start,
            Capture<Vector3> end,
            float progress
        )
        {
            var newPos = Vector3.Lerp(start.Value, end.Value, progress);
            if (representation.parent != null)
            {
                newPos.Scale(representation.parent.localScale);
            }
            representation.localPosition = newPos;
        }
    }
}
```

## Actor System

Sometimes multiple Playback controllers end up referencing a common resource. A common example of this would be a [Position Playback Controller](xref:Recolude.Core.Playback.CollectionControllers.PositionPlayback) and a [Rotation Playback Controller](xref:Recolude.Core.Playback.CollectionControllers.RotationPlayback) both referencing the same [Unity GameObject](https://docs.unity3d.com/ScriptReference/GameObject.html). Neither controller truly owns the game object, but it needs to be properly disposed of at the end of the playback.

A [Actor](xref:Recolude.Core.Playback.IActor) is a mapping of one or more playback controllers to some amount of shared resources. The playback system manages the actors and will call their [Cleanup](xref:Recolude.Core.Playback.IActor.CleanupResources) method when it's ready to dispose of it's controllers. Developers should extend the [IActor](xref:Recolude.Core.Playback.IActor) interface if they need certain resources to be cleaned up after playback. Developers can just use the [GameObjectActor](xref:Recolude.Core.Playback.GameObjectActor) if they only need to clean up game objects.

### Implementing Actor Builder

You can control what is used to represent any given recording by implementing the [IActorBuilder](xref:Recolude.Core.Playback.IActorBuilder) interface. The interface only has one function called [Build](<xref:Recolude.Core.Playback.IActorBuilder.Build(Recolude.Core.IRecording)>) which takes the root recording. The Build function is then expected to recursively traverse the root recording and build a flattened list of all actors that are to be used for playback.

```c#
using System.Collections.Generic;
using UnityEngine;

// Import the playback namespace for access to PlaybackBehavior
using Recolude.Core.Playback;

public class ActorBuilderExample : IActorBuilder
{
    // Creates an actor that uses a sphere to represent each child recording.
    public IActor[] Build(IRecording recording)
    {
        // Put all Game Objects under a single Game Object to easily move them
        // around the scene.
        var parent = new GameObject(recording.Name);

        var actors = new List<IActor>(recording.Recordings.Length);

        // Iterate through all the immediate children of the root recording
        foreach(var rec in recording.Recordings)
        {
            var sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            primitive.transform.SetParent(sphere);
            actors.Add(new GameObjectActor(rec, sphere));
        }

        // Add the parent as a actor as well
        actors.Add(new GameObjectActor(recording, parent));
        return actors.ToArray();
    }

}
```

You can also choose to add custom functions to be called when a custom event occurs by implementing the [IPlaybackCustomEventHandler](xref:Recolude.Core.Playback.IPlaybackCustomEventHandler) interface. This handler is used by the [Custom Event Playback Controller](xref:Recolude.Core.Playback.CollectionControllers.CustomEventPlayback) and get's called while time remains continous through the playback's lifetime. These event handlers can also be passed to the [Game Object Actor](xref:Recolude.Core.Playback.GameObjectActor)'s constructor and it will automatically construct a Event Playback Controller to pass the handler to.

```c#
using System.Collections;
using UnityEngine;

// Import the playback namespace for access to IPlaybackCustomEventHandler
using Recolude.Core.Playback;

public class EventPlaybackLogger : IPlaybackCustomEventHandler
{
    // This method satisfies the IPlaybackCustomEventHandler interface and
    // logs events that occur during playback.
    void OnCustomEvent(
        IRecording recording,
        TimeMovementDetails details,
        Capture<CustomEvent> customEvent
    )
    {
        Debug.LogFormat(
            "[{0:0.##}] {1} - {2}",
            details.CurrentTime,
            recording.Name,
            customEvent.Value.Name
        );
    }
}
```

### Predefined Actor Strategies

Recolude ships with a few predefined actor strategies developers might be able to use to get up and running quicker. All strategies can be found under the [Recolude.Core.Playback.ActorBuilderStrategies](xref:Recolude.Core.Playback.ActorBuilderStrategies) namespace.

- [AssetBundle Actor Builder](xref:Recolude.Core.Playback.ActorBuilderStrategies.AssetBundleActorBuilder) - attempts use the recording's name to look up a game object inside the asset bundle provided and use that as the actor representation in playback.
- [Dictionary Actor Builder](xref:Recolude.Core.Playback.ActorBuilderStrategies.DictionaryActorBuilder) - tries to match the recording's name to a key in the dictionary for loading actors.
- [Metadata Actor Builder](xref:Recolude.Core.Playback.ActorBuilderStrategies.MetadataActorBuilder) - Mimics what [Recolude's webplayer](https://app.recolude.com/) does to some degree.
- [Primitive Actor Builder](xref:Recolude.Core.Playback.ActorBuilderStrategies.PrimitiveActorBuilder) - Takes advantage of Unity's [Create Primitive](https://docs.unity3d.com/ScriptReference/GameObject.CreatePrimitive.html) and instantiates a primitive per child recording.
- [Resource Actor Builder](xref:Recolude.Core.Playback.ActorBuilderStrategies.ResourceActorBuilder) - attempts to look up the recorded subject's name inside a resources folder and use that as the actor representation in playback. WARNING: Unity recommends avoiding the use of the resources folder. Consider using the AssetBundleActorBuilder.

## Controlling Playback

With a reference to PlaybackBehavior you can call [Play](xref:Recolude.Core.Playback.PlaybackBehavior.Play*), [Pause](xref:Recolude.Core.Playback.PlaybackBehavior.Pause*), or [Stop](xref:Recolude.Core.Playback.PlaybackBehavior.Stop*) to control the playback. You can also change the speed the recording is being played back with [SetPlaybackSpeed](<xref:Recolude.Core.Playback.PlaybackBehavior.SetPlaybackSpeed(System.Single)>). We can set the time we are currently at in the playback of the recording using [SetTimeThroughPlayback](<xref:Recolude.Core.Playback.PlaybackBehavior.SetTimeThroughPlayback(System.Single)>).

## Example

```c#
using UnityEngine;

using Recolude.Core;
using Recolude.Core.Playback;

public class PlaybackExample : MonoBehaviour, IActorBuilder, IPlaybackCustomEventHandler
{

    /// <summary>
    /// A reference to the recording we want to playback. RecordingReference is
    /// a scriptable object that makes it easy for developers to point to any
    /// recording within the project. Because it's a Scriptable Object, changing
    /// what recording is referenced does make changes to the unity scene,
    /// assisting in simplifying source control.
    /// </summary>
    [SerializeField]
    RecordingReference recordingToPlayback;

    PlaybackBehavior playbackBehavior;

    /// <summary>
    /// The playback system calls this function *once* with the root recording
    /// </summary>
    /// <param name="recording">root recording to build actors for.</param>
    /// <returns>All actors to be used for playback.</returns>
    public IActor[] Build(IRecording recording)
    {
        var parent = new GameObject(recording.Name);
        IActor[] actors = new IActor[recording.Recordings.Length + 1];

        for (int i = 0; i < recording.Recordings.Length; i++)
        {
            var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.SetParent(parent.transform);
            actors[i] = new GameObjectActor(
                // The recording we want the actor to use for playback.
                recording.Recordings[i],

                // The game object we want to use to represent the recording
                // during playback
                cube,

                // Use this class (PlaybackExample) as the custom event handler
                this
            );
        }

        // This ends up using the empty game object for playback, but we pass 
        // this class to be used as a custom event handler for events on the 
        // root recording.
        actors[actors.Length - 1] = new GameObjectActor(recording, parent, this);

        return actors;
    }

    public void OnCustomEvent(
        IRecording recording,
        TimeMovementDetails details,
        Capture<CustomEvent> customEvent
    )
    {
        Debug.LogFormat(
            "[{0:0.##}] {1} - {2}",
            details.CurrentTime,
            recording.Name,
            customEvent.Value.Name
        );
    }

    // Start is called before the first frame update
    void Start()
    {
        playbackBehavior = PlaybackBehavior.Build(recordingToPlayback, this, true);
        playbackBehavior.Play();
    }

    private void OnGUI()
    {
        if (playbackBehavior.CurrentlyStopped())
        {
            if (GUILayout.Button("Play"))
            {
                playbackBehavior.Play();
            }
        }
        else if (playbackBehavior.CurrentlyPlaying())
        {
            if (GUILayout.Button("Pause"))
            {
                playbackBehavior.Pause();
            }
            if (GUILayout.Button("Stop"))
            {
                playbackBehavior.Stop();
            }
        }
        else if (playbackBehavior.CurrentlyPaused())
        {
            if (GUILayout.Button("Play"))
            {
                playbackBehavior.Play();
            }
            if (GUILayout.Button("Stop"))
            {
                playbackBehavior.Stop();
            }
        }
    }
}
```
