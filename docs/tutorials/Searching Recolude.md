# Searching Recolude

Much like [uploading to Recolude](Uploading%20To%20Recolude.html), the easiest way of getting started is by acquiring an instance of a [RecoludeConfig](xref:Recolude.API.RecoludeConfig). However, for this tutorial, any implementation of the [Config interface](xref:Recolude.API.Config) will do. The [RecordingService](xref:Recolude.API.RecordingService) you need to construct will use the configuration provided to make certain types of queries to Recolude. The query we're concerned about in this tutorial is [ListRecordings] (xref:Recolude.RecordingService.ListRecordings*). There are a bunch of optional parameters you can provide to specify what you exactly want returned from Recolude. These different parameters are grouped under the [ListRecordingsRequestParams](xref:Recolude.API.RecordingService.ListRecordingsRequestParams) object. 

Calling  [ListRecordings] (xref:Recolude.RecordingService.ListRecordings*) returns you a [command](https://sourcemaking.com/design_patterns/command) meant to encapsulate the HTTP request, allowing developers to easily run the fetch asynchronously in something like a [Coroutine](https://docs.unity3d.com/Manual/Coroutines.html). Once you have the command, you can then run it by calling it's [Run](xref:Recolude.RecordingService.ListRecordingsUnityWebRequest.Run) method. It's advised you yield to this inside a coroutine, as this is an aynchronous web request

## Order Options

There are different ways to order how the recordings get returned to you. You construct a sort by combining a category to sort on with an order. For example "duration desc" is sorting on the duration category in the descending order.

The different order categories are:

1. visibility
2. name
3. duration
4. subjects
5. custom_events
6. created_at

## Example

Given a config, this example will query Recolude for recordings, sorting them by duration ascending (shortest recording first). The results are then displayed to the player for them to view at their leasure.

```c#
using System.Collections;
using UnityEngine;
using Recolude;
using Recolude.Core.Playback;

public class SearchExample : MonoBehaviour
{

    [SerializeField]
    private RecoludeConfig config;

    /// <summary>
    /// This is just a bunch of recording summaries. No actuall recordings
    /// got downloaded, that will have to be done in a seperate request
    /// </summary>
    private V1Recording[] recordings;

    void Start()
    {
        StartCoroutine(Search());
    }

    IEnumerator Search()
    {
        var rs = new RecordingService(config);
        var params = new RecordingService.ListRecordingsRequestParams()
        {
            // specify this project specifically, because you can actually 
            // query from multiple projects if you'd like
            ProjectId = config.GetProjectID(),

            // Order our durations in ascending order, because we want the 
            // smallest time (fastest) to be presented first.
            Order_by = "duration asc",
        };
        var req = rs.ListRecordings(params);

        // Make the request
        yield return req.Run();

        // If our HTTP success has been populated, capture the recording 
        // summaries
        if (req.success != null)
        {
            recordings = req.success.recordings;
        }
        // If our fallback (failure) response has been populate
        else if (req.fallbackResponse != null)
        {
            Debug.LogError(req.fallbackResponse.message);
        }
    }

    IEnumerator View(string recordingID)
    {
        var req = config.LoadRecording(recordingID);
        yield return req.Run();

        // Log if an error occurred
        if (req.Error() != null)
        {
            Debug.LogError(req.Error());
            yield break;
        }
        // Play the recording we just downloaded
        var playback = PlaybackBehavior.Build(req.Recording());
        playback.Play();
    }

    void OnGUI()
    {
        if (recordings == null)
        {
            return;
        }
        // List all the recordings we received from the search
        foreach (var recording in recordings)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(recording.name);

            // Load and view the recording if selected
            if (GUILayout.Button("View"))
            {
                StartCoroutine(View(recording.id));
            }
            GUILayout.EndHorizontal();
        }
    }

}
```
