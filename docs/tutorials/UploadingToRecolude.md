# Uploading To Recolude

The quickest way to upload a recording to Recolude is by acquiring an instance of a [RecoludeConfig](xref:Recolude.API.RecoludeConfig). Once you have an instance to a config, you can call [SaveRecording](xref:Recolude.API.RecoludeConfig.SaveRecording*) to build yourself an [ISaveRecordingOperation](xref:Recolude.API.RecordingStorage.ISaveRecordingOperation). This is an abstract [command](https://sourcemaking.com/design_patterns/command) meant to encapsulate the upload request to be easily ran asynchronously in something like a [Coroutine](https://docs.unity3d.com/Manual/Coroutines.html).

Once you have the command, you can then run it by calling it's [Run](xref:Recolude.API.RecordingStorage.IRecordingOperation.Run) method. It's advised you yield to this inside a coroutine as this is an asynchronous web request. You can check if anything went wrong by seeing if the [Error](xref:Recolude.API.RecordingStorage.IRecordingOperation.Error*) method returns anything. If it doesn't then congratulations, you just uploaded to Recolude!

## Example

```c#
using UnityEngine;
using Recolude;
using Recolude.Core;
using System.Collections;

public class UploadExample : MonoBehaviour
{

    [SerializeField]
    private RecoludeConfig config;

    [SerializeField]
    private Recording recording;

    void Start()
    {
        StartCoroutine(ExampleUpload());
    }

    IEnumerator ExampleUpload()
    {
        var req = config.SaveRecording(recording);
        yield return req.Run();
        if (req.Error() == null)
        {
            Debug.LogFormat("Uploaded Recordings ID: {0}", req.RecordingID());
        }
    }

}
```
