# Using Metadata

[Metadata](xref:Recolude.Core.Metadata) objects (sometimes referred to as Metadata "Blocks") are sprinkled throughout the Recolude landscape to allow developers to easily capture important information about what's going on inside their application in a loose and fast manner. The [Metadata](xref:Recolude.Core.Metadata) class essentially acts as a wrapper around a [Dictionary](https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.dictionary-2?view=net-6.0)<[string](https://docs.microsoft.com/en-us/dotnet/api/system.string?view=net-6.0), [IProperty](xref:Recolude.Core.IProperty)>, providing extra utilities for storing and retrieving info.

## Using Properties

A metadata block can contain multiple different types of properties at once. Implemented data types a property can take on includes (both individual and array):

- string
- int32
- float32
- boolean
- byte
- [Vector2](https://docs.unity3d.com/ScriptReference/Vector2.html)
- [Vector3](https://docs.unity3d.com/ScriptReference/Vector3.html)
- [Metadata](xref:Recolude.Core.Metadata)
- [Time](https://docs.microsoft.com/en-us/dotnet/api/system.datetime?view=net-6.0)

### Implementing New Property Types

It's possible to extend the [IProperty](xref:Recolude.Core.IProperty) interface to allow the [Metadata](xref:Recolude.Core.Metadata) block to hold bespoke data types, but the [RAPWriter](xref:Recolude.Core.IO.RAPWriter) will drop these values upon serialization. All properties include a [Code](xref:Recolude.Core.IProperty.Code*) method used to denote it's data type when being serialized to RAP. Because this method returns a single byte, there is only 256 different property types that could ever be stored. The byte codes 0 - 199 are reserved for future Recolude implementation. Therefor, if you want to create a new Property, be sure have it return a code value between 200 and 255.

## Example Usage

```c#
// Store information about a collision
var metadata = new Metadata();
metadata["collided-with"] = new StringProperty("Wall");
metadata["collision-velocity"] = new FloatProperty(21.23f);
metadata["collision-normal"] = new Vector3Property(0.56f, -0.71f, 0.42f);
metadata["collision-position"] = new Vector3Property(17.15, 33.23, 81.51f);

// Use the collision data at a later time to construct FX during playback
var fxInstance = Instantiate<GameObject>(effectPrefab).transform;
fxInstance.position = metadata.AsVector3("collision-position");
fxInstance.localScale = Vector3.forward * metadata.AsFloat("collision-velocity");
fxInstance.LookAt(fxInstance.position + metadata.AsVector3("collision-normal"));
```
