# Configuring The Webplayer

You can set specific metadata key-value pairs inside of your recordings that will effect how it is played back in Recolude's webplayer.

## On a global basis

These specific attributes need to be added to the Recordings metadata in order to work. Adding these attributes to any specific subject will be ignored.

### recolude-bg-color

Color of the environment's background. Can be any RGB hex string.

```c#
recorder.SetMetaData(
    "recolude-bg-color",
    ColorUtility.ToHtmlStringRGB(Color.green)
);
```

### recolude-skybox

A URL to an image to be used as an equirectangular skybox for the playback.

```C#
recorder.SetMetaData("recolude-skybox", "https://i.imgur.com/GSKk2yB.png");
```

### recolude-skybox-xzy

A collection of image URLs to be used as the different sides of a cube map that will be used as the skybox. All sides must be specified for the skybox to render.

- recolude-skybox-front
- recolude-skybox-back
- recolude-skybox-up
- recolude-skybox-down
- recolude-skybox-left
- recolude-skybox-right

```C#
recorder.SetMetaData("recolude-skybox-front", "https://i.imgur.com/img1.png");
recorder.SetMetaData("recolude-skybox-back",  "https://i.imgur.com/img2.png");
recorder.SetMetaData("recolude-skybox-up",    "https://i.imgur.com/img3.png");
recorder.SetMetaData("recolude-skybox-down",  "https://i.imgur.com/img4.png");
recorder.SetMetaData("recolude-skybox-left",  "https://i.imgur.com/img5.png");
recorder.SetMetaData("recolude-skybox-right", "https://i.imgur.com/img6.png");
```

## On a per subject basis

Include any of these properties to customize a specific subject.

### recolude-scale

A 3 component vector that will scale the playback object in the webplayer on it's different axis.

```c#
subjectRecorder.SetMetaData(
    "recolude-scale",
    $"{myScale.x}, {myScale.y}, {myScale.z}"
);
```

### recolude-geom

The geometry that will be used to represent the actor in plaback. Currently supported geometries include:

* cube (default)
* sphere
* plane
* capsule
* vr-head

```C#
subjectRecorder.SetMetaData("recolude-geom", "capsule");
```

### recolude-color

Color of the actors geometry. Can be any RGB hex string.

```C#
subjectRecorder.SetMetaData(
    "recolude-color",
    ColorUtility.ToHtmlStringRGB(Color.red)
);
```

### recolude-offset

A 3 component vector that will offset the actor's geometry from it's origin.

```c#
subjectRecorder.SetMetaData(
    "recolude-offset",
    $"{myOffset.x}, {myOffset.y}, {myOffset.z}"
);
```

### recolude-ignore

A boolean indicating whether or not to include the subject in the playback.

```c#
subjectRecorder.SetMetaData("recolude-ignore", "true");
```

### recolude-fbx

A url that indicates where to pull a FBX file from.

```c#
subjectRecorder.SetMetaData("recolude-fbx", "https://someurl.com/my.fbx");
```