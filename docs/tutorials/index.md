# Tutorials

To learn about some of the fundamental aspects of how Recolude operates, checkout the guides:

* [Anatomy of a Recording](AnatomyOfARecording.md)
* [Using Metadata](UsingMetadata.md)

## Learning How to Create And Playback Recordings

Check out how to [create recordings](ProgrammaticRecordingQuickStart.md), and then how to [play them back](ProgrammaticRecordingQuickStart.md).

### Time

[Recolude's Time namespace](xref:Recolude.Core.Time) is responsible for controlling the time elapsed between updates to the recorder and playback functionality. Recolude defaults to using [BasicTimeProvider](xref:Recolude.Core.Time.BasicTimeProvider), which just uses [Unity3D's Time.time](https://docs.unity3d.com/ScriptReference/Time-time.html).

Games that have their own "time" independent of unity will want to implement the [ITimeProvider](xref:Recolude.Core.Time.ITimeProvider) to ensure the recording software captures the proper time values. An example of this would be a fighting game that operates at a locked 24 FPS for input, regardless of actual update lifecycle speed. 

## Learning Recolude

Quickly pick up the basics by checking out how to [upload](UploadingToRecolude.html) and [download](DownloadingFromRecolude.html) recordings from Recolude.

Once you become conformable with the basics, you can start to learn more about how [IRecordingStorage](xref:Recolude.API.RecordingStorage.IRecordingStorage) and [Config](xref:Recolude.API.Config) work. It's important to design your application around these abstract interfaces, as it allows you to easily upgrade/change your SDK version without any breaking changes. Going the extra step by designing your program around [IRecordingStorage](xref:Recolude.API.RecordingStorage.IRecordingStorage) allows you to easily add fallbacks, like [locally caching recordings](xref:Recolude.API.RecordingStorage.FailSafe.RecordingStorageFailSafe) when they fail to upload due to inevitable disturbances on a players home network.
