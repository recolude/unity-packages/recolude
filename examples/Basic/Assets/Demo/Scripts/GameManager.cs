﻿using Recolude.Analytics;
using Recolude.Analytics.Charting;
using Recolude.Analytics.Charting.Heatmap;
using Recolude.Analytics.Charting.MaterialStyleBuilder;
using Recolude.Core;
using Recolude.Core.Playback;
using Recolude.Core.Record;
using Recolude.Core.Util;
using Recolude.SceneSystem.Events;
using Recolude.SceneSystem.ScenePlayback;
using Recolude.SceneSystem.SceneRecording;
using UnityEngine;

namespace Recolude.Basic.Demo
{
    public class GameManager : MonoBehaviour
    {
        #region Configuration Variables

        [RequiredValue] [SerializeField] private GameObject cube;

        [SerializeField] private Vector3[] positions;

        [RequiredValue] [SerializeField] private RecordingSystem recordingSystem;

        [RequiredValue] [SerializeField] private PlaybackSystem playbackSystem;

        [RequiredValue] [SerializeField] private GameEvent<Vector3> cubeDeathEvent;

        [SerializeField] private GameObject deathEffect;

        [SerializeField] private GameObject collisionEffect;

        [SerializeField] private URPStyleBuilder styleBuilder;

        #endregion

        #region Runtime Variables

        private PlaybackBehavior playback;

        private Recorder currentRecorder;

        private Recording latestRecording;

        private GameObject[] objectsToShoot;

        #endregion

        private bool CurrentlyRecording => currentRecorder != null && currentRecorder.CurrentlyRecording();

        private bool CurrentlyPaused => currentRecorder != null && currentRecorder.CurrentlyPaused();

        private bool CurrentlyStopped => currentRecorder == null || currentRecorder.CurrentlyStopped();

        void Update()
        {
            if (!CurrentlyRecording)
            {
                return;
            }

            for (int i = objectsToShoot.Length - 1; i >= 0; i--)
            {
                if (objectsToShoot[i] == null || objectsToShoot[i].transform.position.y > -10)
                {
                    continue;
                }

                cubeDeathEvent.Raise(objectsToShoot[i].transform.position);

                Destroy(objectsToShoot[i]);
            }
        }

        private void ClearObjectsToTrack()
        {
            if (objectsToShoot == null)
            {
                return;
            }

            for (int i = objectsToShoot.Length - 1; i >= 0; i--)
            {
                Destroy(objectsToShoot[i]);
            }
        }

        void StartRecording()
        {
            SetUpObjectsToTrack();
            currentRecorder = recordingSystem.StartRecording();

            if (playback != null)
            {
                playback.Stop();
                Destroy(playback.gameObject);
            }
        }

        private void SetUpObjectsToTrack()
        {
            objectsToShoot = new GameObject[positions.Length];
            for (int i = 0; i < positions.Length; i++)
            {
                objectsToShoot[i] = Instantiate(cube);
                objectsToShoot[i].transform.position = positions[i];
            }
        }

        void FinishRecording()
        {
            latestRecording = recordingSystem.StopRecording();
            playback = playbackSystem.BuildPlayback(latestRecording);
            ClearObjectsToTrack();
            var analysis = new RecordingAnalysis(latestRecording);
            var heatmapData = analysis.TimeInRegionHeatmap(0.5f, 0, latestRecording.Duration);
            var heatmap = new HeatmapBuilder()
                .Data(heatmapData)
                .Theme(new CellTheme(heatmapData))
                .MaterialStyleBuilder(styleBuilder)
                .Render();
        }

        void OnGUI()
        {
            if (CurrentlyRecording)
            {
                OnGUI_CurrentlyRecording();
            }
            else if (CurrentlyPaused)
            {
                OnGUI_CurrentlyPaused();
            }
            else if (CurrentlyStopped)
            {
                OnGUI_CurrentlyStopped();
            }
        }

        void OnGUI_CurrentlyRecording()
        {
            if (GUI.Button(new Rect(10, 10, 120, 25), "Pause"))
            {
                currentRecorder.Pause();
            }

            if (GUI.Button(new Rect(10, 40, 120, 25), "Finish"))
            {
                FinishRecording();
            }
        }

        void OnGUI_CurrentlyPaused()
        {
            if (GUI.Button(new Rect(10, 10, 120, 25), "Resume"))
            {
                currentRecorder.Resume();
            }

            if (GUI.Button(new Rect(10, 40, 120, 25), "Finish"))
            {
                FinishRecording();
            }
        }

        void OnGUI_CurrentlyStopped()
        {
            if (GUI.Button(new Rect(10, 10, 120, 25), "Start Recording"))
            {
                StartRecording();
            }

            if (playback != null)
            {
                GUI.Box(new Rect(10, 50, 120, 250), "Playback");

                var playPauseButton = new Rect(15, 75, 110, 25);
                if (!playback.CurrentlyPlaying() && GUI.Button(playPauseButton, "Start"))
                {
                    if (playback.GetTimeThroughPlayback() >= playback.RecordingDuration() &&
                        playback.GetPlaybackSpeed() > 0)
                    {
                        playback.Play(0);
                    }
                    else if (playback.GetTimeThroughPlayback() <= 0 && playback.GetPlaybackSpeed() < 0)
                    {
                        playback.Play(playback.RecordingDuration());
                    }
                    else
                    {
                        playback.Play(0);
                    }
                }

                if (playback.CurrentlyPlaying() && GUI.Button(playPauseButton, "Pause"))
                {
                    playback.Pause();
                }

                GUI.Label(new Rect(55, 105, 100, 30), "Time");
                GUI.Label(new Rect(55, 125, 100, 30), playback.GetTimeThroughPlayback().ToString("0.00"));
                playback.SetTimeThroughPlayback(GUI.HorizontalSlider(new Rect(15, 150, 100, 30),
                    playback.GetTimeThroughPlayback(), 0.0F, playback.RecordingDuration()));

                GUI.Label(new Rect(20, 170, 100, 30), "Playback Speed");
                GUI.Label(new Rect(55, 190, 100, 30), playback.GetPlaybackSpeed().ToString("0.00"));
                playback.SetPlaybackSpeed(GUI.HorizontalSlider(new Rect(15, 215, 100, 30),
                    playback.GetPlaybackSpeed(), -8, 8));
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = new Color(1, 0, 0, 0.5f);
            foreach (var pos in positions)
            {
                Gizmos.DrawCube(pos, Vector3.one);
            }
        }
    }
}