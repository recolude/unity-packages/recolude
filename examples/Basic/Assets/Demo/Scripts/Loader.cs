using UnityEngine;

using System.IO;
using System.Collections.Generic;

using Recolude.Core;
using Recolude.Core.IO;
using Recolude.Core.Playback;

namespace Recolude.Basic.Demo
{
    public class Loader : MonoBehaviour, IActorBuilder
    {
        public IActor[] Build(IRecording recording)
        {
            var waves = new List<IActor>();
            foreach (var child in recording.Recordings)
            {
                waves.Add(new GameObjectActor(child, GameObject.CreatePrimitive(PrimitiveType.Sphere), null, ActorCleanupMethod.Destroy));
            }
            return waves.ToArray();
        }

        void Start()
        {
            string pathToRecording = "C:/dev/projects/recolude/frec/facial features.rap";
            using (var file = File.OpenRead(pathToRecording))
            {
                IRecording recording;
                using (var rapReader = new RAPReader(file))
                {
                    recording = rapReader.Recording();
                }
                var playback = PlaybackBehavior.Build(recording, this, false);
                playback.Play();
            }
        }
    }
}