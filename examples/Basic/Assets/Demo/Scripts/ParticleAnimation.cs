﻿using UnityEngine;

namespace Recolude.Basic.Demo
{
    public class ParticleAnimation : MonoBehaviour
    {
        [SerializeField] private GameObject animation;

        [SerializeField] private float lifetime;

        [SerializeField] private Vector3 rotation;
        
        public void Run()
        {
            Run(transform.position);
        }

        public void Run(Vector3 position)
        {
            var trans = transform;
            var effect = Instantiate(animation, position, trans.rotation);
            effect.transform.eulerAngles = rotation;
            
            Destroy(effect, lifetime);
        }
    }
}