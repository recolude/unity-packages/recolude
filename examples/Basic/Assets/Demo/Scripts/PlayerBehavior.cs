﻿using Recolude.SceneSystem.Events;
using UnityEngine;
using Recolude.SceneSystem.Properties.Reference;

namespace Recolude.Basic.Demo
{
    public class PlayerBehavior : MonoBehaviour
    {
        #region Configuration Variables

        [SerializeField] private Transform bulletSpawn;

        [SerializeField] private PropertyReference<float> bulletVelocity;

        [SerializeField] private PropertyReference<float> fireRate;

        [SerializeField] private PropertyReference<float> bulletLifetime;

        [SerializeField] private GameEvent fireWeaponEvent;

        [SerializeField] private GameObject bulletPrefab;

        #endregion

        private float nextFireTime;

        private bool CanFire => Time.time >= nextFireTime;

        private void Start()
        {
            nextFireTime = Time.time - (1f / fireRate.Value);
        }

        void Update()
        {
            Transform trans = transform;
            trans.Rotate(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"), 0);
            transform.eulerAngles = ClampedRotation(trans.eulerAngles);
            if (Input.GetKeyUp(KeyCode.Space))
            {
                Fire();
            }
        }

        private void Fire()
        {
            if (!CanFire)
            {
                return;
            }

            var bullet = Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);
            var rb = bullet.GetComponent<Rigidbody>();
            rb.velocity = transform.forward * bulletVelocity.Value;

            fireWeaponEvent.Raise();

            Destroy(bullet, bulletLifetime.Value);
            nextFireTime = Time.time + (1f / fireRate.Value);
        }

        private Vector3 ClampedRotation(Vector3 currentRotation)
        {
            var x = currentRotation.x;

            if (x < 90)
            {
                x = Mathf.Min(x, 60);
            }
            else if (x > 270)
            {
                x = Mathf.Max(x, 300);
            }

            return new Vector3(x, Mathf.Clamp(currentRotation.y, 10, 170), 0);
        }
    }
}