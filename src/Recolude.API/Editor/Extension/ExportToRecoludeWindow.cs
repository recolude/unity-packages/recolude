using UnityEngine;
using UnityEditor;

using System.Collections.Generic;

using Recolude.Core;

namespace Recolude.API.Editor.Extension
{

    /// <summary>
    /// Window for assisting in the exportation of recordings to recolude online service.
    /// </summary>
    public class ExportToRecoludeWindow : EditorWindow
    {
        List<IRecording> recordingsToExport;

        List<RecordingVisibility> recordingsToExportVisibility;

        RecoludeConfig recoludeConfig = null;

        public static ExportToRecoludeWindow Init(params IRecording[] recordingToExport)
        {
            ExportToRecoludeWindow window = (ExportToRecoludeWindow)GetWindow(typeof(ExportToRecoludeWindow));
            window.recordingsToExport = new List<IRecording>();
            window.recordingsToExport.AddRange(recordingToExport);
            window.recoludeConfig = null;
            window.Show();
            window.Repaint();
            return window;
        }

        public static ExportToRecoludeWindow Init(RecoludeConfig recoludeConfig)
        {
            ExportToRecoludeWindow window = (ExportToRecoludeWindow)GetWindow(typeof(ExportToRecoludeWindow));
            window.recordingsToExport = new List<IRecording>();
            window.recoludeConfig = recoludeConfig;
            window.Show();
            window.Repaint();
            return window;
        }

        [MenuItem("Window/Recolude/Upload Recordings")]
        public static ExportToRecoludeWindow Init()
        {
            ExportToRecoludeWindow window = (ExportToRecoludeWindow)GetWindow(typeof(ExportToRecoludeWindow));
            window.recordingsToExport = new List<IRecording>();
            window.recoludeConfig = null;
            window.Show();
            window.Repaint();
            return window;
        }

        void OnEnable()
        {
            titleContent = new GUIContent("Export To Recolude");
        }

        void RenderRecordingsToUploadList()
        {
            EditorGUILayout.LabelField("Recordings To Upload");
            if (recordingsToExport == null)
            {
                recordingsToExport = new List<IRecording>();
            }
            for (int i = recordingsToExport.Count - 1; i >= 0; i--)
            {
                EditorGUILayout.BeginHorizontal();
                // recordingsToExport[i] = EditorGUILayout.ObjectField(recordingsToExport[i], typeof(RecordingScriptableObject), false) as RecordingScriptableObject;
                if (GUILayout.Button("Remove"))
                {
                    recordingsToExport.RemoveAt(i);
                }
                EditorGUILayout.EndHorizontal();
            }
            if (GUILayout.Button("Add Recording"))
            {
                recordingsToExport.Add(null);
            }
        }



        void OnGUI()
        {
            EditorGUILayout.Space();
            recoludeConfig = EditorUtil.RenderAPIConfigControls(recoludeConfig);
            EditorGUILayout.Space();

            RenderRecordingsToUploadList();

            string error = Error();
            if (error != "")
            {
                EditorGUILayout.HelpBox(error, MessageType.Error);
                return;
            }

            EditorGUILayout.Space();
            if (GUILayout.Button("Export"))
            {
                Export(recordingsToExport);
            }
        }

        private void Export(List<IRecording> recordings)
        {
            if (recordings == null || recordings.Count == 0)
            {
                throw new System.Exception("Can't export null or empty recordings.");
            }
            if (recoludeConfig == null)
            {
                throw new System.Exception("Can not upload recordings without API configuration");
            }

            int numOfRecordingsToUpload = 0;
            foreach (var rec in recordings)
            {
                if (rec == null)
                {
                    continue;
                }
                numOfRecordingsToUpload++;
            }

            int numOfRecordingsUploaded = 0;
            try
            {
                foreach (var rec in recordings)
                {
                    if (rec == null)
                    {
                        continue;
                    }
                    EditorUtility.DisplayProgressBar(
                        "Uploading Recording",
                        string.Format("{0} out of {1} recordings uploaded", numOfRecordingsUploaded, numOfRecordingsToUpload),
                        numOfRecordingsUploaded / (float)(numOfRecordingsToUpload)
                    );

                    // var req = recoludeConfig.SaveRecording(rec);
                    // EditorUtil.RunCoroutine(req.Run());

                    var req = recoludeConfig.BuildUploadRecordingRequest(rec, RecordingVisibility.Public).SendWebRequest();
                    while(req.isDone == false ) {
                        continue;
                    }
                    Debug.Log(req.webRequest.url);
                    Debug.Log(req.webRequest.error);
                    Debug.Log(req.webRequest.responseCode);

                    numOfRecordingsUploaded++;
                }
            }
            catch (System.Exception e)
            {
                Debug.LogError(e.Message);
            }
            finally
            {
                EditorUtility.ClearProgressBar();
            }


        }

        private string Error()
        {
            if (recordingsToExport == null || recordingsToExport.Count == 0)
            {
                return "Please provide recordings to upload";
            }

            if (recoludeConfig == null)
            {
                return "Provide a recolude config";
            }

            return "";
        }

    }

}