namespace Recolude.API.Editor.Extension.Import
{
    public interface ILoadSelection
    {
        void Render();

        string Error();

        string SuggestedFolderName();

        void Import(string dir); 

    }

}