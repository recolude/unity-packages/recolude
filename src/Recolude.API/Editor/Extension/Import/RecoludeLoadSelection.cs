using UnityEngine;
using UnityEditor;
using System.IO;
using System.Globalization;
using System.Collections.Generic;

namespace Recolude.API.Editor.Extension.Import
{
    class RecoludeLoadSelection : ILoadSelection
    {

        List<Recording> recordings;

        RecoludeConfig config;

        public RecoludeLoadSelection(RecoludeConfig rec, List<Recording> recs)
        {
            config = rec;
            recordings = recs;
        }

        public string Error()
        {
            return "";
        }

        public void Import(string dir)
        {
            var recordingsSaved = 0;
            var importHalted = false;
            try
            {
                foreach (var rec in recordings)
                {
                    var downloadRequest = config.BuildDownloadRecordingRequest(rec.Id);
                    downloadRequest.SendWebRequest();

                    string progressbarMessage = string.Format("Currently downloading {0}. {1} out of {2} recordings saved", rec.Name, recordingsSaved, recordings.Count);

                    while (downloadRequest.isDone == false)
                    {
                        if (EditorUtility.DisplayCancelableProgressBar("Downloading Recordings", progressbarMessage, downloadRequest.downloadProgress))
                        {
                            EditorUtility.ClearProgressBar();
                            importHalted = true;
                            break;
                        };
                    }
                    if (importHalted)
                    {
                        break;
                    }

                    var dateString = rec.CreatedAt.ToUniversalTime().ToString("dd-MM-yy HH.mm.ss", DateTimeFormatInfo.InvariantInfo);
                    var fileName = string.Format("{0} {1}", string.IsNullOrEmpty(rec.Name) ? "[no name]" : rec.Name, dateString);
                    var rapBytes = downloadRequest.downloadHandler.data;

                    var rapfileName = AssetDatabase.GenerateUniqueAssetPath(Path.Combine(dir, fileName + ".rap"));
                    File.WriteAllBytes(rapfileName, rapBytes);


                    AssetDatabase.Refresh();
                    recordingsSaved++;
                }
            }
            catch (System.Exception e)
            {
                EditorUtility.ClearProgressBar();
                Debug.Log("Error: " + e.Message);
                throw e;
            }

            EditorUtility.ClearProgressBar();
        }

        public void Render()
        {
            EditorGUILayout.LabelField(string.Format("Import {0} recordings from recolude", recordings.Count));
        }

        public string SuggestedFolderName()
        {
            return "Recolude Downloads";
        }
    }

}