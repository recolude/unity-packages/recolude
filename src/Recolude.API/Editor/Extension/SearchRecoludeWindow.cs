using System.Collections;
using System.Collections.Generic;
using System.IO;
using Recolude.Core;
using Unity.EditorCoroutines.Editor;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

namespace Recolude.API.Editor.Extension
{

    /// <summary>
    /// Window for assisting in the search of recordings hosted on recolude's online service.
    /// </summary>
    public class SearchRecoludeWindow : EditorWindow
    {
        RecoludeConfig recoludeConfig = null;

        bool[] selectedRecordingsFromSearchResults;

        RecordingService.ListRecordingsUnityWebRequest listRecordingsUnityWebRequest;

        public static SearchRecoludeWindow Init(RecoludeConfig recoludeConfig)
        {
            SearchRecoludeWindow window = (SearchRecoludeWindow)GetWindow(typeof(SearchRecoludeWindow));
            window.recoludeConfig = recoludeConfig;
            window.Show();
            window.Repaint();
            window.position = Recolude.Core.Editor.Extension.EditorUtil.CenterWindowPosition(600, 400);
            return window;
        }

        [MenuItem("Window/Recolude/Search Recordings")]
        public static SearchRecoludeWindow Init()
        {
            SearchRecoludeWindow window = (SearchRecoludeWindow)GetWindow(typeof(SearchRecoludeWindow));
            window.recoludeConfig = null;
            window.Show();
            window.Repaint();
            window.position = Recolude.Core.Editor.Extension.EditorUtil.CenterWindowPosition(600, 400);
            return window;
        }

        void OnEnable()
        {
            titleContent = new GUIContent("Search Recolude");
        }


        private int NumberOfItemsSelected()
        {
            if (selectedRecordingsFromSearchResults == null || listRecordingsUnityWebRequest == null || listRecordingsUnityWebRequest.success == null)
            {
                return 0;
            }

            int total = 0;
            foreach (var selected in selectedRecordingsFromSearchResults)
            {
                if (selected)
                {
                    total++;
                }
            }
            return total;
        }

        private List<Recording> ItemsSelected()
        {
            var result = new List<Recording>();
            if (selectedRecordingsFromSearchResults == null || listRecordingsUnityWebRequest.success == null)
            {
                return result;
            }

            for (int i = 0; i < listRecordingsUnityWebRequest.success.Recordings.Length; i++)
            {
                if (selectedRecordingsFromSearchResults[i])
                {
                    result.Add(listRecordingsUnityWebRequest.success.Recordings[i]);
                }
            }
            return result;
        }

        Vector2 scroll;

        private void DisplaySearchResults(RecoludeConfig config, RecordingsResponse searchResultsToDisplay)
        {
            if (searchResultsToDisplay.Recordings.Length == 0)
            {
                GUILayout.Label("No results");
                return;
            }

            if (GUILayout.Button("Select All"))
            {
                for (int selectedIndex = 0; selectedIndex < selectedRecordingsFromSearchResults.Length; selectedIndex++)
                {
                    selectedRecordingsFromSearchResults[selectedIndex] = true;
                }
            }

            float widthOfACell = position.width / 4.0f;
            var widthLayoutOption = GUILayout.Width(widthOfACell);
            var lastItemWidthLayoutOption = GUILayout.Width((widthOfACell - 20) / 2f);
            var halfWidthLayoutOption = GUILayout.Width(widthOfACell / 2f);

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Name", widthLayoutOption);
            GUILayout.Label("Uploaded", widthLayoutOption);
            GUILayout.Label("Duration", halfWidthLayoutOption);
            GUILayout.Label("Subjects", halfWidthLayoutOption);
            EditorGUILayout.EndHorizontal();

            scroll = EditorGUILayout.BeginScrollView(scroll);
            int i = 0;
            foreach (var result in searchResultsToDisplay.Recordings)
            {
                EditorGUILayout.BeginHorizontal();
                selectedRecordingsFromSearchResults[i] = EditorGUILayout.ToggleLeft(
                    searchResultsToDisplay.Recordings[i].Name,
                    selectedRecordingsFromSearchResults[i],
                    widthLayoutOption
                );
                GUILayout.Label(result.Summary.CreatedAt.ToUniversalTime().ToString(), widthLayoutOption);
                GUILayout.Label(result.Summary.Duration.ToString("#.##"), halfWidthLayoutOption);
                GUILayout.Label(result.Summary.Subjects.ToString(), halfWidthLayoutOption);
                if (GUILayout.Button("View", lastItemWidthLayoutOption))
                {
                    this.StartCoroutine(ViewRecording(config, result));
                }
                
                if (GUILayout.Button("Download", lastItemWidthLayoutOption))
                {
                    this.StartCoroutine(SaveRecording(config, result));
                }
                EditorGUILayout.EndHorizontal();
                i++;
            }
            EditorGUILayout.EndScrollView();
        }

        bool interpretedRequest = false;

        private IEnumerator ExecuteSearch()
        {
            interpretedRequest = false;
            var rs = new RecordingService(recoludeConfig);
            listRecordingsUnityWebRequest = rs.ListRecordings(new RecordingService.ListRecordingsRequestParams()
            {
                ProjectId = recoludeConfig.GetProjectID(),
                Limit=100,
            });
            yield return listRecordingsUnityWebRequest.Run();
        }


        void OnGUI()
        {
            EditorGUILayout.Space();
            recoludeConfig = EditorUtil.RenderAPIConfigControls(recoludeConfig);
            EditorGUILayout.Space();

            if (listRecordingsUnityWebRequest != null)
            {
                // if (listRecordingsUnityWebRequest.UnderlyingRequest.error)
                // {
                //     EditorGUILayout.HelpBox("searching " + listRecordingsUnityWebRequest.UnderlyingRequest.url, MessageType.Info);
                // }
                if (listRecordingsUnityWebRequest.UnderlyingRequest.isDone == false)
                {
                    var dots = ".";
                    if (EditorApplication.timeSinceStartup % 2 == 0)
                    {
                        dots = "..";
                    }
                    else if (EditorApplication.timeSinceStartup % 3 == 0)
                    {
                        dots = "...";
                    }
                    EditorGUILayout.HelpBox("searching" + dots, MessageType.Info);
                }

                if (listRecordingsUnityWebRequest.UnderlyingRequest.isNetworkError)
                {
                    EditorGUILayout.HelpBox("network error!", MessageType.Error);
                }

                if (interpretedRequest == false && listRecordingsUnityWebRequest.UnderlyingRequest.isDone)
                {
                    interpretedRequest = true;
                    listRecordingsUnityWebRequest.Interpret(listRecordingsUnityWebRequest.UnderlyingRequest);
                    selectedRecordingsFromSearchResults = new bool[listRecordingsUnityWebRequest.success.Recordings.Length];
                }
            }


            string error = Error();
            if (error != "")
            {
                EditorGUILayout.HelpBox(error, MessageType.Error);
                return;
            }

            if (GUILayout.Button("Search"))
            {
                this.StartCoroutine(ExecuteSearch());
            }

            EditorGUILayout.Space();
            if (listRecordingsUnityWebRequest != null && listRecordingsUnityWebRequest.success != null)
            {
                DisplaySearchResults(recoludeConfig, listRecordingsUnityWebRequest.success);
            }

            if (listRecordingsUnityWebRequest != null && listRecordingsUnityWebRequest.fallbackResponse != null)
            {
                EditorGUILayout.HelpBox(listRecordingsUnityWebRequest.fallbackResponse.Message, MessageType.Error);
            }

            if (NumberOfItemsSelected() > 0)
            {
                EditorGUILayout.BeginHorizontal();

                if (
                    GUILayout.Button("Delete") &&
                    EditorUtility.DisplayDialog(
                        "Delete Recordings From Recolude",
                        $"Are you sure you want to permanently delete {NumberOfItemsSelected()} recordings from recolude?",
                        "Delete",
                        "Cancel"))
                {
                    this.StartCoroutine(DeleteSelectedRecordings());
                }

                EditorGUILayout.EndHorizontal();
            }
        }

        private IEnumerator ViewRecording(RecoludeConfig config, Recording recordingToView)
        {
            var rec = config.LoadRecording(recordingToView.Id);
            yield return rec.Run();

            if (string.IsNullOrEmpty(rec.Error()) == false)
            {
                Debug.LogErrorFormat("Error fetching recording: {0}", rec.Error());
                yield break;
            }

            Recolude.Core.Editor.Extension.PlaybackWindow.
                Init().
                SetRecordingForPlayback(rec.Recording());
        }
        
        private IEnumerator SaveRecording(RecoludeConfig config, Recording recordingToView)
        {
            var rec = config.BuildDownloadRecordingRequest(recordingToView.Id);
            yield return rec.SendWebRequest();

            if (rec.result != UnityWebRequest.Result.Success)
            {
                Debug.LogErrorFormat("Error fetching recording: {0}", rec.error);
                yield break;
            }
            
            var path = EditorUtility.SaveFilePanel(
                "Save Recording",
                "",
                recordingToView.Name + ".rap",
                "rap");

            File.WriteAllBytes(path, rec.downloadHandler.data);
            AssetDatabase.Refresh();
        }

        private IEnumerator DeleteSelectedRecordings()
        {
            var recordingsToDelete = ItemsSelected();
            var recordingsDeleted = 0;
            string errorOccurred = null;
            foreach (var rec in recordingsToDelete)
            {
                if (EditorUtility.DisplayCancelableProgressBar(
                    "Deleting Recording",
                    string.Format("Currently deleting {0}. {1} out of {2} recordings deleted", rec.Name, recordingsDeleted, recordingsToDelete.Count),
                    recordingsDeleted / (float)(recordingsToDelete.Count)
                ))
                {
                    EditorUtility.ClearProgressBar();
                    break;
                };

                var req = recoludeConfig.DeleteRecording(rec.Id);
                yield return req.Run();
                if (req.Error() != null)
                {
                    errorOccurred = req.Error();
                    break;
                }
                recordingsDeleted++;
            }

            EditorUtility.ClearProgressBar();
            if (errorOccurred == null)
            {
                this.StartCoroutine(ExecuteSearch());
            }
            else
            {
                Debug.LogError(errorOccurred);
            }
        }

        private string Error()
        {
            if (recoludeConfig == null)
            {
                return "Provide a recolude config";
            }
            return "";
        }

    }

}