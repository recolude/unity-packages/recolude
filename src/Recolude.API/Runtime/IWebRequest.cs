using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

namespace Recolude.API
{
    public interface IWebRequest
    {

        UnityWebRequest UnderlyingRequest { get; }

        IEnumerator Run();
    }

}
