﻿using UnityEngine;
using Recolude.Core.Record;
using Recolude.Core;
using Recolude.Core.Properties;

namespace Recolude.API.Playback
{
    /// <summary>
    /// Helper class for quickly configuring how a subject should be displayed 
    /// in the Recolude webplayer.
    /// </summary>
    public class SubjectPlaybackConfigBuilder
    {
        private readonly SubjectRepresentation representation;

        private readonly Vector3 scale;

        private readonly Color color;

        public SubjectPlaybackConfigBuilder()
        {
            representation = SubjectRepresentation.Cube;
            scale = Vector3.one;
            color = UnityEngine.Color.white;
        }

        public SubjectPlaybackConfigBuilder(SubjectRepresentation representation, Vector3 scale, Color color)
        {
            this.representation = representation;
            this.scale = scale;
            this.color = color;
        }

        public SubjectPlaybackConfigBuilder Color(Color color)
        {
            return new SubjectPlaybackConfigBuilder(representation, scale, color);
        }

        public SubjectPlaybackConfigBuilder Color(float r, float g, float b)
        {
            return new SubjectPlaybackConfigBuilder(representation, scale, new Color(r, g, b));
        }

        public SubjectPlaybackConfigBuilder Color(string rgbHex)
        {
            if (ColorUtility.TryParseHtmlString(rgbHex, out Color color))
            {
                return new SubjectPlaybackConfigBuilder(representation, scale, color);
            }
            throw new System.ArgumentException(string.Format("Invalid Hex Color: ", rgbHex));
        }

        public SubjectPlaybackConfigBuilder Scale(Vector3 scale)
        {
            return new SubjectPlaybackConfigBuilder(representation, scale, color);
        }

        public SubjectPlaybackConfigBuilder Geometry(SubjectRepresentation representation)
        {
            return new SubjectPlaybackConfigBuilder(representation, scale, color);
        }

        public Metadata ToMetadata()
        {
            var metadata = new Metadata();
            metadata["recolude-geom"] = new Property<string>(representation.ToRecoludeValue());
            metadata["recolude-scale"] = new Vector3Property(scale);
            metadata["recolude-color"] = new Property<string>($"#{ColorUtility.ToHtmlStringRGB(color)}");
            return metadata;
        }

        public void Apply(Recorder subject)
        {
            var metadata = ToMetadata();
            foreach (var entry in metadata)
            {
                subject.SetMetaData(entry.Key, entry.Value);
            }
        }

    }

}