namespace Recolude.API.Playback
{
    /// <summary>
    /// How the subject will be represented in the web player
    /// </summary>
    public enum SubjectRepresentation
    {
        Cube,
        Sphere,
        Head,
        Capsule,
    }

    internal static class SubjectRepresentationExtensionMethods
    {
        public static string ToRecoludeValue(this SubjectRepresentation representation)
        {
            switch (representation)
            {
                case SubjectRepresentation.Head:
                    return "vr-head";
                case SubjectRepresentation.Sphere:
                    return "sphere";
                case SubjectRepresentation.Capsule:
                    return "capsule";
                default:
                    return "cube";
            }
        }
    }
}