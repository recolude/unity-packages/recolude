using UnityEngine;
using System.IO;
using System.Text;
using Recolude.Core.IO;
using Recolude.Core;

namespace Recolude.API.RecordingStorage.FailSafe
{

    public class LogFailedSaveInstruction : CustomYieldInstruction
    {
        IRecording recording;

        bool finished;

        public LogFailedSaveInstruction(IRecording recording, IBinaryStorage fallback, int incrementEntry, FailSafeOutline outlineReference)
        {
            this.finished = false;
            this.recording = recording;

            FailSafeOutline loadedOutline = outlineReference;

            if (outlineReference == null)
            {
                if (fallback.Exists("failsafe.json"))
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        using (var failsafeFile = fallback.Read("failsafe.json"))
                        {
                            failsafeFile.CopyTo(memoryStream);
                        }
                        loadedOutline = JsonUtility.FromJson<FailSafeOutline>(Encoding.UTF8.GetString(memoryStream.ToArray()));
                    }
                }
            }

            if (loadedOutline == null)
            {
                loadedOutline = new FailSafeOutline();
            }

            var recEntry = loadedOutline.GetLargestFailedSaveRecEntry() + 1;
            if (incrementEntry != -1)
            {
                recEntry = incrementEntry;
            }

            loadedOutline.AppendFailedSave(recEntry);

            using (var str = new MemoryStream(Encoding.UTF8.GetBytes(JsonUtility.ToJson(loadedOutline))))
            {
                fallback.Write(str, "failsafe.json");
            }

            using (MemoryStream mem = new MemoryStream())
            {
                using (var rapWriter = new RAPWriter(mem, true))
                {
                    rapWriter.Write(recording);
                }
                mem.Seek(0, SeekOrigin.Begin);
                fallback.Write(mem, recEntry.ToString());
            }
            this.finished = true;
        }

        public override bool keepWaiting
        {
            get
            {
                return !finished;
            }
        }

    }

}
