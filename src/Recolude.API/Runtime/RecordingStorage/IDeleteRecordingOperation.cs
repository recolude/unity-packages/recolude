namespace Recolude.API.RecordingStorage
{

    /// <summary>
    /// An operation that when ran will attempt to delete a recording.
    /// </summary>
    public interface IDeleteRecordingOperation : IRecordingOperation
    {

    }

}