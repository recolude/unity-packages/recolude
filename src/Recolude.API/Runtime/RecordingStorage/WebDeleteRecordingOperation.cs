using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.IO;
using Recolude.Core;
using Recolude.Core.IO;

namespace Recolude.API.RecordingStorage
{
    public class WebDeleteRecordingOperation : IDeleteRecordingOperation
    {

        UnityWebRequest webRequest;

        bool finished;

        string error;

        public WebDeleteRecordingOperation(UnityWebRequest webRequest)
        {
            this.webRequest = webRequest;
            this.finished = false;
            this.error = null;
        }

        public string Error()
        {
            return error;
        }

        public bool Finished()
        {
            return finished;
        }


        public IEnumerator Run()
        {
            yield return webRequest.SendWebRequest();
            this.finished = true;

            if (webRequest.isNetworkError || webRequest.isHttpError)
            {
                this.error = webRequest.error;
                yield break;
            }

        }
    }

}