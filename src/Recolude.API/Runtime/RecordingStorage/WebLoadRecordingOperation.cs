using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.IO;
using Recolude.Core;
using Recolude.Core.IO;

namespace Recolude.API.RecordingStorage
{
    public class WebLoadRecordingOperation : ILoadRecordingOperation
    {
        private IRecording recording;

        private readonly IEncoder<ICaptureCollection<ICapture>>[] encoders;

        private readonly UnityWebRequest webRequest;

        private bool finished;

        private string error;

        public WebLoadRecordingOperation(UnityWebRequest webRequest,
            IEncoder<ICaptureCollection<ICapture>>[] encoders = null)
        {
            this.webRequest = webRequest;
            this.encoders = encoders;
            recording = null;
            finished = false;
            error = null;
        }

        public string Error()
        {
            return error;
        }

        public bool Finished()
        {
            return finished;
        }

        public IRecording Recording()
        {
            return recording;
        }

        private RAPReader Reader(MemoryStream stream)
        {
            if (encoders != null)
            {
                return new RAPReader(stream, encoders, false);
            }

            return new RAPReader(stream);
        }

        public IEnumerator Run()
        {
            yield return webRequest.SendWebRequest();
            this.finished = true;

            if (webRequest.result != UnityWebRequest.Result.Success)
            {
                this.error = webRequest.error;
                yield break;
            }

            if (webRequest.downloadHandler.data.Length == 0)
            {
                this.error = "File download was empty";
                yield break;
            }

            using (var memStr = new MemoryStream(webRequest.downloadHandler.data))
            {
                using (var rapReader = Reader(memStr))
                {
                    recording = rapReader.Recording();
                }
            }
        }
    }
}