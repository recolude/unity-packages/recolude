using System;
using UnityEngine.Networking;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Recolude.Core.IO;

namespace Recolude.API.RecordingStorage
{
    public class WebSaveRecordingOperation : ISaveRecordingOperation
    {
        #region Stragegy 1 - Upload something already serialize

        private readonly UnityWebRequest webRequest;

        #endregion

        #region Stategy 2 - Defer serialization to IEnumerator

        private readonly SerializationCommandBuilder command;

        private readonly WWWForm form;

        #endregion

        private string recordingID;

        private bool finished;

        private string error;

        public WebSaveRecordingOperation(UnityWebRequest webRequest)
        {
            this.webRequest = webRequest ?? throw new ArgumentNullException(nameof(webRequest));

            recordingID = null;
            finished = false;
            error = null;
        }

        public WebSaveRecordingOperation(UnityWebRequest webRequest, WWWForm form, SerializationCommandBuilder command)
        {
            this.webRequest = webRequest ?? throw new ArgumentNullException(nameof(webRequest));
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            this.form = form ?? throw new ArgumentNullException(nameof(form));

            recordingID = null;
            finished = false;
            error = null;
        }

        public string Error()
        {
            return error;
        }

        public bool Finished()
        {
            return finished;
        }

        public string RecordingID()
        {
            return recordingID;
        }

        private static void SetupPost(UnityWebRequest request, WWWForm formData)
        {
            byte[] data = null;
            if (formData != null)
            {
                data = formData.data;
                if (data.Length == 0)
                    data = null;
            }
            request.uploadHandler = new UploadHandlerRaw(data);
            request.downloadHandler = new DownloadHandlerBuffer();
            if (formData == null)
                return;
            foreach (KeyValuePair<string, string> header in formData.headers)
                request.SetRequestHeader(header.Key, header.Value);
        }
        
        public IEnumerator Run()
        {
            var requestToRun = webRequest;

            if (form != null)
            {
                using MemoryStream fs = new MemoryStream();
                yield return command.WithStream(fs).KeepStreamOpen().RunAsCoroutine();
                var fileData = fs.GetBuffer();
                // Debug.Log(fileData.Length);
                form.AddBinaryData("recording", fileData, "myFile.rap", "multipart/form-data");
                SetupPost(requestToRun, form);
            }

            yield return requestToRun.SendWebRequest();
            finished = true;
            if (requestToRun.result != UnityWebRequest.Result.Success)
            {
                error = requestToRun.error;
                Debug.LogError(
                    $"Error Saving Recording to Web: [{requestToRun.result}] '{requestToRun.downloadHandler.text}'");
                yield break;
            }

            var summary =
                Newtonsoft.Json.JsonConvert.DeserializeObject<RecordingSummary>(requestToRun.downloadHandler.text);
            recordingID = summary.Id;
        }
    }
}