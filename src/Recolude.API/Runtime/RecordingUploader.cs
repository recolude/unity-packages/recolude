using UnityEngine;
using System.Collections;

namespace Recolude.API
{

    /// <summary>
    /// For Internal use by Recolude only. A hack for allowing anything to run 
    /// coroutines.
    /// </summary>
    public class RecordingUploader : MonoBehaviour
    {
        private static RecordingUploader instance;

        public static RecordingUploader GetInstance()
        {
            if (instance == null)
            {
                instance = new GameObject("Recording Uploader").AddComponent<RecordingUploader>();
                DontDestroyOnLoad(instance.gameObject);
            }
            return instance;
        }

        public void RunRoutines(params IEnumerator[] ops)
        {
            StartCoroutine(RunOpsAsCoroutine(ops));
        }

        private IEnumerator RunOpsAsCoroutine(IEnumerator[] ops)
        {
            foreach (var op in ops)
            {
                yield return op;
            }
        }

    }

}