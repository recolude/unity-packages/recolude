
using UnityEngine;
using UnityEditor;
using Unity.EditorCoroutines.Editor;

using System.Collections.Generic;
using System.Collections;

using Recolude.API;
using Recolude.API.Editor.Extension;


namespace Recolude.API.Editor.Extension
{

    /// <summary>
    /// Window for assisting in the search of heatmaps hosted on recolude's online service.
    /// </summary>
    public class HeatmapWindow : EditorWindow
    {
        RecoludeConfig recoludeConfig = null;


        public static HeatmapWindow Init(RecoludeConfig recoludeConfig)
        {
            HeatmapWindow window = (HeatmapWindow)GetWindow(typeof(HeatmapWindow));
            window.recoludeConfig = recoludeConfig;
            window.Show();
            window.Repaint();
            window.position = Recolude.Core.Editor.Extension.EditorUtil.CenterWindowPosition(600, 400);
            return window;
        }

        [MenuItem("Window/Recolude/Heatmaps")]
        public static HeatmapWindow Init()
        {
            HeatmapWindow window = (HeatmapWindow)GetWindow(typeof(HeatmapWindow));
            window.recoludeConfig = null;
            window.Show();
            window.Repaint();
            window.position = Recolude.Core.Editor.Extension.EditorUtil.CenterWindowPosition(600, 400);
            return window;
        }

        void OnEnable()
        {
            titleContent = new GUIContent("Recolude Heatmaps");
        }


        private void DisplaySearchResults(RecoludeConfig config, RecordingsResponse searchResultsToDisplay)
        {
            if (searchResultsToDisplay.Recordings.Length == 0)
            {
                GUILayout.Label("No results");
                return;
            }

            float widthOfACell = position.width / 4.0f;
            var widthLayoutOption = GUILayout.Width(widthOfACell);
            var lastItemWidthLayoutOption = GUILayout.Width(widthOfACell - 20);
            var halfWidthLayoutOption = GUILayout.Width(widthOfACell / 2f);

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Name", widthLayoutOption);
            GUILayout.Label("Uploaded", widthLayoutOption);
            GUILayout.Label("Duration", halfWidthLayoutOption);
            GUILayout.Label("Subjects", halfWidthLayoutOption);
            EditorGUILayout.EndHorizontal();

            int i = 0;
            foreach (var result in searchResultsToDisplay.Recordings)
            {
                EditorGUILayout.BeginHorizontal();
                GUILayout.Label(result.Summary.CreatedAt.ToUniversalTime().ToString(), widthLayoutOption);
                GUILayout.Label(result.Summary.Duration.ToString("#.##"), halfWidthLayoutOption);
                GUILayout.Label(result.Summary.Subjects.ToString(), halfWidthLayoutOption);
                if (GUILayout.Button("View", lastItemWidthLayoutOption))
                {
                    this.StartCoroutine(ViewRecording(config, result));
                }
                EditorGUILayout.EndHorizontal();
                i++;
            }
        }

        bool interpretedRequest = false;

        private IEnumerator ExecuteSearch()
        {
            interpretedRequest = false;

            var projectService = new ProjectService(recoludeConfig);
            var projectReq = projectService.GetProject(new ProjectService.GetProjectRequestParams()
            {
                ProjectId = recoludeConfig.GetProjectID(),
                OrganizationId = recoludeConfig.GetOrganizationID()
            });
            yield return projectReq.Run();

            if (projectReq.UnderlyingRequest.responseCode != 200)
            {
                Debug.LogError(projectReq.UnderlyingRequest.responseCode);
                Debug.LogError(projectReq.fallbackResponse.Message);
                Debug.LogError(projectReq.fallbackResponse.Error);
                yield break;
            }

            foreach (var id in projectReq.success.Project.Analytics.ContinuousMapIds)
            {
                Debug.Log(id);
            }

            interpretedRequest = true;

            // var analyticService = new AnalyticsService(recoludeConfig);
            // analyticService.Get
            // yield return listRecordingsUnityWebRequest.Run();
        }


        void OnGUI()
        {
            EditorGUILayout.Space();
            recoludeConfig = EditorUtil.RenderAPIConfigControls(recoludeConfig);
            EditorGUILayout.Space();

            // if (listRecordingsUnityWebRequest != null)
            // {
            //     // if (listRecordingsUnityWebRequest.UnderlyingRequest.error)
            //     // {
            //     //     EditorGUILayout.HelpBox("searching " + listRecordingsUnityWebRequest.UnderlyingRequest.url, MessageType.Info);
            //     // }
            //     if (listRecordingsUnityWebRequest.UnderlyingRequest.isDone == false)
            //     {
            //         var dots = ".";
            //         if (EditorApplication.timeSinceStartup % 2 == 0)
            //         {
            //             dots = "..";
            //         }
            //         else if (EditorApplication.timeSinceStartup % 3 == 0)
            //         {
            //             dots = "...";
            //         }
            //         EditorGUILayout.HelpBox("searching" + dots, MessageType.Info);
            //     }

            //     if (listRecordingsUnityWebRequest.UnderlyingRequest.isNetworkError)
            //     {
            //         EditorGUILayout.HelpBox("network error!", MessageType.Error);
            //     }

            //     if (interpretedRequest == false && listRecordingsUnityWebRequest.UnderlyingRequest.isDone)
            //     {
            //         interpretedRequest = true;
            //         listRecordingsUnityWebRequest.Interpret(listRecordingsUnityWebRequest.UnderlyingRequest);
            //     }
            // }


            string error = Error();
            if (error != "")
            {
                EditorGUILayout.HelpBox(error, MessageType.Error);
                return;
            }

            if (GUILayout.Button("Search"))
            {
                this.StartCoroutine(ExecuteSearch());
            }

            EditorGUILayout.Space();
            // if (listRecordingsUnityWebRequest != null && listRecordingsUnityWebRequest.success != null)
            // {
            //     DisplaySearchResults(recoludeConfig, listRecordingsUnityWebRequest.success);
            // }

            // if (listRecordingsUnityWebRequest != null && listRecordingsUnityWebRequest.fallbackResponse != null)
            // {
            //     EditorGUILayout.HelpBox(listRecordingsUnityWebRequest.fallbackResponse.Message, MessageType.Error);
            // }

        }

        private IEnumerator ViewRecording(RecoludeConfig config, Recording recordingToView)
        {
            var rec = config.LoadRecording(recordingToView.Id);
            yield return rec.Run();

            if (string.IsNullOrEmpty(rec.Error()) == false)
            {
                Debug.LogErrorFormat("Error fetching recording: {0}", rec.Error());
                yield break;
            }

            Recolude.Core.Editor.Extension.PlaybackWindow.
                Init().
                SetRecordingForPlayback(rec.Recording());
        }
        private string Error()
        {
            if (recoludeConfig == null)
            {
                return "Provide a recolude config";
            }
            return "";
        }

    }

}