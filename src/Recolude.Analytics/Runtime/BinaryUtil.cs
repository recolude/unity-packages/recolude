using System;
using System.IO;
using System.Text;

namespace Recolude.Analytics
{
    public static class BinaryUtil
    {
        static internal uint ReadUint32(Stream stream)
        {
            byte[] bytes = new byte[4];
            stream.Read(bytes, 0, 4);

            if (BitConverter.IsLittleEndian == false)
                Array.Reverse(bytes);

            return BitConverter.ToUInt32(bytes, 0);
        }


        static internal ulong ReadUvarint(Stream r)
        {
            ulong x = 0;
            uint s = 0;
            int i = 0;
            while (true)
            {
                var b = (byte)r.ReadByte();

                if (b < 0x80)
                {
                    if (i > 9 || i == 9 && b > 1)
                    {
                        throw new System.Exception("binary: varint overflows a 64-bit integer");
                    }
                    return x | (ulong)(b) << (int)s;
                }
                x |= (ulong)(b & 0x7f) << (int)s;
                s += 7;
                i++;
            }
        }

        static internal long ReadVarint(Stream r)
        {
            var ux = ReadUvarint(r);
            var x = (long)(ux >> 1);
            if ((ux & 1) != 0)
            {
                x = ~x;
            }
            return x;
        }

        static internal string ReadString(Stream stream)
        {
            ulong strLen = ReadUvarint(stream);
            byte[] strBuffer = new byte[strLen];
            stream.Read(strBuffer, 0, strBuffer.Length);
            return Encoding.UTF8.GetString(strBuffer, 0, strBuffer.Length);
        }

        public static float ReadFloat16(Stream stream, float min, float max)
        {
            var buf = new byte[2]{
                (byte)stream.ReadByte(),
                (byte)stream.ReadByte()
            };

            var length = max - min;
            var curValue = min + (length / 2.0f);
            var increment = length / 4.0f;

            for (var byteIndex = 0; byteIndex < buf.Length; byteIndex++)
            {
                for (var bitIndex = 0; bitIndex < 8; bitIndex++)
                {
                    if (((buf[byteIndex] >> bitIndex) & 1) == 1)
                    {
                        curValue += increment;
                    }
                    else
                    {
                        curValue -= increment;
                    }
                    increment /= 2.0f;
                }
            }

            return curValue;
        }

    }

}