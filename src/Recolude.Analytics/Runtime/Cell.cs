using UnityEngine;

namespace Recolude.Analytics
{
    public class Cell
    {
        Vector3 position;

        float value;

        public Cell(Vector3 position, float value)
        {
            this.position = position;
            this.value = value;
        }

        public Vector3 Position { get => position; set => position = value; }
        public float Value { get => value; set => this.value = value; }

        public override bool Equals(object obj)
        {
            return obj is Cell cell &&
                   position.Equals(cell.position) &&
                   value == cell.value;
        }

        public override int GetHashCode()
        {
            int hashCode = -1974622876;
            hashCode = hashCode * -1521134295 + position.GetHashCode();
            hashCode = hashCode * -1521134295 + value.GetHashCode();
            return hashCode;
        }
    }

}