﻿using UnityEngine;


namespace Recolude.Analytics
{
    public class CellBehavior : MonoBehaviour
    {
        private float min;

        private float max;

        private Cell cell;

        private Material mat;

        HeatmapBehavior heatmap;

        private BoxCollider boxCollider;

        public void SetHeatmap(HeatmapBehavior heatmap)
        {
            this.heatmap = heatmap;
        }

        public static CellBehavior Build(Cell cell, float size, Material mat, float min, float max)
        {
            var cellObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cellObj.isStatic = true;
            cellObj.transform.position = cell.Position;
            cellObj.transform.localScale = size * Vector3.one;
            cellObj.GetComponent<MeshRenderer>().material = mat;
            cellObj.transform.name = cell.Value.ToString("0.00");
            var behavior = cellObj.AddComponent<CellBehavior>();
            behavior.cell = cell;
            behavior.mat = mat;
            behavior.min = min;
            behavior.max = max;
            behavior.boxCollider = cellObj.GetComponent<BoxCollider>();
            return behavior;
        }

        void OnMouseDown()
        {
            if (heatmap == null)
            {
                Debug.LogError("Heatmap cell clicked but no heatmap parent is set");
                return;
            }
            heatmap.CellClicked(cell);
        }

        /// <summary>
        /// Set's how visible the cell is. 0 being invisible, 2 being
        /// overridden to be opaque.
        /// </summary>
        /// <param name="value">
        /// Value between 0 and 2, 1 being normal.
        /// </param>
        public void SetStrength(float value)
        {
            var color = mat.color;
            var dist = max - min;

            // There's really nothing we can do other than scale linearly
            if (dist == 0)
            {
                color.a = value / 2f;
            }
            else
            {
                var perc = (cell.Value - min) / dist;

                if (value < 1)
                {
                    color.a = perc * value;
                }
                else
                {
                    color.a = perc + ((1f - perc) * (value - 1f));
                }
            }
            mat.color = color;

            var shouldHaveCollider = color.a > .01f;
            if (boxCollider.enabled != shouldHaveCollider)
            {
                boxCollider.enabled = shouldHaveCollider;
            }
        }
    }

}