﻿using System;
using System.Collections;
using UnityEngine;
using Recolude.API;
using Recolude.Analytics.Charting;

namespace Recolude.Analytics
{

    public class ChartRenderRequest : IRenderRequest
    {
        RenderError error;

        bool completed;

        GameObject chart;

        AnalyticsService.GetGraphExecutionUnityWebRequest graphRequest;

        public ChartRenderRequest(AnalyticsService.GetGraphExecutionUnityWebRequest graphRequest)
        {
            this.completed = false;
            this.graphRequest = graphRequest;
        }

        public bool Completed()
        {
            return completed;
        }

        public RenderError Error()
        {
            return error;
        }

        public GameObject RenderedObject()
        {
            return chart;
        }

        public IEnumerator Run()
        {
            yield return graphRequest.Run();
            completed = true;
            if (graphRequest.UnderlyingRequest.responseCode != 200)
            {
                error = RenderError.FailedWebRequest(graphRequest.UnderlyingRequest);
                yield break;
            }

            if (graphRequest.success == null)
            {
                error = new RenderError("Response code was 200 but contained no data");
                yield break;
            }

            RenderDataset(GraphToDataset(graphRequest.success), graphRequest.success);
        }

        private static DataSet GraphToDataset(Graph graph)
        {
            var entries = new Entry[graph.Result.Data.Length];
            var columns = new string[graph.Result.Dimensions.Length];
            for (int entryIndex = 0; entryIndex < graph.Result.Data.Length; entryIndex++)
            {
                var entry = new Entry();
                for (int dimensionIndex = 0; dimensionIndex < graph.Result.Dimensions.Length; dimensionIndex++)
                {
                    columns[dimensionIndex] = graph.Result.Dimensions[dimensionIndex].Name;
                    var format = graph.Result.Dimensions[dimensionIndex].Format;
                    IComparable value = graph.Result.Data[entryIndex][dimensionIndex];
                    if (format.ToLower() == "number")
                    {
                        if (float.TryParse(graph.Result.Data[entryIndex][dimensionIndex], out float parsed))
                        {
                            value = parsed;
                        }
                    }
                    entry = entry.Set(graph.Result.Dimensions[dimensionIndex].Name, value);
                }
                entries[entryIndex] = entry;
            }

            var currentDataset = new DataSet(graph.Name, entries, columns);
            for (int dimensionIndex = 0; dimensionIndex < graph.Result.Dimensions.Length; dimensionIndex++)
            {
                var dimensionName = graph.Result.Dimensions[dimensionIndex].Name;
                if (dimensionName != "week")
                {
                    continue;
                }
                currentDataset = currentDataset
                    .Map((entry) =>
                    {
                        var parsedDate = DateTime.Parse(entry.String(dimensionName));
                        return entry.Set("day", parsedDate.DayOfWeek.ToString());
                    })
                    .Sum(
                        graph.Result.Dimensions[dimensionIndex + 1].Name, 
                        graph.Result.Dimensions[graph.Result.Dimensions.Length - 1].Name);
            }

            Debug.Log(currentDataset.ToString());

            return currentDataset;
        }

        private void RenderDataset(DataSet dataset, Graph graph)
        {
            string sumOn = graph.Result.Dimensions[graph.Result.Dimensions.Length - 1].Name;

            switch (graph.RenderType.ToLower())
            {
                case "pie":
                    string legend = graph.Result.Dimensions[graph.Result.Dimensions.Length - 2].Name;
                    chart = dataset
                        .PieChart(sumOn)
                        .Legend(legend)
                        .Render();
                    break;

                case "bar":
                    var barChart = dataset
                        .BarChart()
                        .YAxis(sumOn);

                    switch (dataset.Columns().Length)
                    {
                        case 2:
                            barChart = barChart
                                .XAxis(dataset.Columns()[0]);
                            break;

                        case 3:
                            barChart = barChart
                                .XAxis(dataset.Columns()[0])
                                .GroupOn(dataset.Columns()[1]);
                            break;

                    }

                    chart = barChart.Render();
                    break;

                default:
                    error = new RenderError($"Unimplemented graph render type: {graph.RenderType}");
                    break;
            }
        }
    }

}