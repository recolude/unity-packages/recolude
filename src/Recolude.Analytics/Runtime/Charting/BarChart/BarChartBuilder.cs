﻿using System.Collections.Generic;
using UnityEngine;

namespace Recolude.Analytics.Charting.BarChart
{
    public class BarChartBuilder
    {

        private class Bar
        {
            float value;

            MaterialStyle color;

            public Bar(float value, MaterialStyle color)
            {
                this.value = value;
                this.color = color;
            }

            internal float Value()
            {
                return value;

            }

            internal MaterialStyle MaterialStyle()
            {
                return color;
            }
        }

        private class BarGroup
        {
            string xLabel;

            Bar[] bars;

            public BarGroup(string xLabel, Bar[] bars)
            {
                this.xLabel = xLabel;
                this.bars = bars;
            }

            internal Bar[] Bars()
            {
                return bars;
            }

            internal string Label()
            {
                return xLabel;
            }
        }

        private class BarChart
        {
            List<BarGroup> groups;

            Dictionary<string, MaterialStyle> groupLegend;

            public BarChart(List<BarGroup> groups, Dictionary<string, MaterialStyle> groupLegend)
            {
                this.groups = groups;
                this.groupLegend = groupLegend;
            }

            public Dictionary<string, MaterialStyle> GroupLegend { get => groupLegend; set => groupLegend = value; }
            public List<BarGroup> Groups { get => groups; set => groups = value; }
        }

        DataSet data;

        string xKey;

        string yKey;

        string groupOn;

        int numYTics;

        ITheme<Entry> theme;

        IMaterialStyleBuilder matBuilder;

        ITextBuilder textBuilder;

        public BarChartBuilder(DataSet data, ITextBuilder textBuilder, IMaterialStyleBuilder matBuilder, ITheme<Entry> theme)
        {
            this.textBuilder = textBuilder;
            this.data = data;
            this.matBuilder = matBuilder;
            this.numYTics = 5;
            this.theme = theme;
        }

        public BarChartBuilder XAxis(string key)
        {
            xKey = key;
            return this;
        }

        public BarChartBuilder YAxis(string key)
        {
            yKey = key;
            return this;
        }

        public BarChartBuilder GroupOn(string key)
        {
            groupOn = key;
            return this;
        }

        public BarChartBuilder YTics(int tics)
        {
            numYTics = tics;
            return this;
        }

        private float RoundY(int numTicks, float highestYVal)
        {
            return highestYVal;
        }

        public float MaxVal()
        {
            var curMax = Mathf.NegativeInfinity;
            foreach (var entry in data.Entries())
            {
                var val = entry.Float(yKey);
                if (val > curMax)
                {
                    curMax = val;
                }
            }
            return curMax;
        }

        private BarChart BuildGroups()
        {
            var groups = new List<BarGroup>();

            if (string.IsNullOrWhiteSpace(groupOn))
            {
                foreach (var entry in data.Entries())
                {
                    var mat = theme.Material(entry);
                    groups.Add(new BarGroup(
                        entry.String(xKey),
                        new Bar[]
                        {
                            new Bar(
                                entry.Float(yKey),
                                mat
                            )
                        }
                    ));
                }
                return new BarChart(groups, null);
            }

            string curLabel = null;
            var curBars = new List<Bar>();
            Dictionary<string, MaterialStyle> groupToMat = new Dictionary<string, MaterialStyle>();
            foreach (var entry in data.Entries())
            {
                var label = entry.String(xKey);
                if (curLabel != label)
                {
                    if (curBars.Count > 0)
                    {
                        groups.Add(new BarGroup(curLabel, curBars.ToArray()));
                        curBars.Clear();
                    }
                    curLabel = label;
                }

                var group = entry.String(groupOn);
                MaterialStyle style = null;
                if (groupToMat.TryGetValue(group, out MaterialStyle outStyle))
                {
                    style = outStyle;
                }
                else
                {
                    style = theme.Material(entry);
                    groupToMat[group] = style;
                }

                curBars.Add(new Bar(entry.Float(yKey), style));
            }

            groups.Add(new BarGroup(curLabel, curBars.ToArray()));

            return new BarChart(groups, groupToMat);
        }

        private GameObject RenderTitle(Vector2 pos)
        {
            var title = textBuilder.Build(TextPivot.MiddleCenter, data.Name());
            title.gameObject.name = "Title";
            title.transform.position = new Vector3(pos.x, pos.y, 0);
            return title;
        }

        private GameObject Legend(Dictionary<string, MaterialStyle> legend)
        {
            var returnObj = new GameObject("Legend");
            const float keySize = .25f;
            const float padding = 0.2f;
            int i = 0;
            foreach (var item in legend)
            {
                var keyPos = new Vector3(padding + (keySize / 2f), -padding - ((keySize + padding) * i), 0);
                var keyTextPos = new Vector3(keyPos.x + keySize, keyPos.y, keyPos.z);

                var key = GameObject.CreatePrimitive(PrimitiveType.Cube);
                key.transform.SetParent(returnObj.transform);
                key.GetComponent<MeshRenderer>().material = matBuilder.Build(item.Value);
                key.transform.localScale = Vector3.one * keySize;
                key.transform.position = keyPos;

                var keyText = textBuilder.Build(TextPivot.MiddleLeft, item.Key);
                keyText.transform.SetParent(returnObj.transform);
                keyText.transform.position = keyTextPos;
                i++;
            }
            return returnObj;
        }

        public GameObject Render()
        {
            if (string.IsNullOrWhiteSpace(xKey))
            {
                throw new System.InvalidOperationException("x-axis has not been set");
            }

            BarChart chart = BuildGroups();

            const int numYTics = 5;
            const float graphWidth = 5f;
            const float graphHeight = 3f;
            const float groupSpacing = 0.1f;
            float totalGroupSpacing = groupSpacing * chart.Groups.Count;

            float groupWidth = (graphWidth / chart.Groups.Count) - groupSpacing;
            float maxVal = MaxVal();
            const float barDepth = .15f;
            const float xTicLength = 0.25f;
            const float xTicWidth = 0.05f;
            const float lineWidth = 0.1f;
            Material lineMaterial = matBuilder.Build(new MaterialStyle(Color.black));

            float highestYVal = Mathf.NegativeInfinity;
            Vector3 bottomLeft = new Vector3(-lineWidth / 2f, -lineWidth / 2f, 0);
            Vector3 topLeft = bottomLeft;
            Vector3 bottomRight = bottomLeft;

            GameObject graph = new GameObject(data.Name());
            for (int groupIndex = 0; groupIndex < chart.Groups.Count; groupIndex++)
            {
                var group = chart.Groups[groupIndex];
                var barWidth = groupWidth / group.Bars().Length;
                var xTicPos = (groupIndex * groupWidth) + (groupWidth / 2f) + (groupSpacing * groupIndex);

                for (int entryIndex = 0; entryIndex < group.Bars().Length; entryIndex++)
                {
                    Bar entry = group.Bars()[entryIndex];

                    // Create bar
                    var entryObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    entryObj.transform.SetParent(graph.transform);
                    entryObj.name = entry.Value().ToString();
                    entryObj.GetComponent<MeshRenderer>().material = matBuilder.Build(entry.MaterialStyle());

                    // Arrange bar  
                    var height = (entry.Value() / maxVal) * graphHeight;
                    var xPos = (entryIndex * barWidth) - (groupWidth / 2f) + xTicPos + (barWidth / 2f);
                    entryObj.transform.localScale = new Vector3(barWidth, height, barDepth);
                    entryObj.transform.localPosition += (Vector3.right * xPos) + (Vector3.up * (height / 2f));
                    if (highestYVal < height)
                    {
                        highestYVal = height;
                    }
                }

                // Build X Tic
                var xTic = Util.BuildLine(
                    xTicWidth,
                    lineMaterial,
                    new Vector3(xTicPos, bottomLeft.y, 0),
                    new Vector3(xTicPos, bottomLeft.y - xTicLength, 0)
                );
                xTic.transform.SetParent(graph.transform);

                // Build X Tic Text
                var text = textBuilder.Build(TextPivot.MiddleLeft, group.Label());
                text.transform.position = new Vector3(xTicPos - .2f, bottomLeft.y - (xTicLength * 2), 0);
                text.transform.SetParent(xTic.transform);
                text.transform.Rotate(Vector3.forward, -30);
            }

            topLeft.y = graphHeight + (xTicWidth / 2f);
            bottomRight.x = graphWidth;

            // Build Y Tic
            float increment = maxVal / (numYTics - 1);
            for (int i = 0; i < numYTics; i++)
            {
                float tickHight = i * (graphHeight / (float)(numYTics - 1));
                Util.BuildLine(
                    xTicWidth,
                    lineMaterial,
                    new Vector3(bottomLeft.x, tickHight, 0),
                    new Vector3(bottomLeft.x - xTicLength, tickHight, 0)
                ).transform.SetParent(graph.transform);
                var text = textBuilder.Build(TextPivot.MiddleRight, (increment * i).ToString("0.00"));
                text.transform.SetParent(graph.transform);
                text.transform.position = new Vector3(bottomLeft.x - xTicLength - .2f, tickHight, 0);
            }

            // Conditionally Build Legend
            if (string.IsNullOrWhiteSpace(groupOn) == false)
            {
                var legend = Legend(chart.GroupLegend);
                legend.transform.position = new Vector3(bottomRight.x + .2f, topLeft.y);
                legend.transform.SetParent(graph.transform);
            }

            RenderTitle(
                new Vector2(
                    graphWidth / 2f,
                    graphHeight + 0.5f
                )
            ).transform.SetParent(graph.transform);

            Util.BuildLine(lineWidth, lineMaterial, topLeft, bottomLeft, bottomRight).transform.SetParent(graph.transform);
            return graph;
        }
    }
}
