using UnityEngine;
using UnityEngine.UI;

namespace Recolude.Analytics.Charting
{
    public class CanvasTextBuilder : ITextBuilder
    {
        readonly Font font;

        public CanvasTextBuilder()
        {
            font = Resources.GetBuiltinResource<Font>("Arial.ttf");
        }

        public CanvasTextBuilder(Font font)
        {
            this.font = font;
        }

        public CanvasTextBuilder Font(Font font)
        {
            return new CanvasTextBuilder(font);
        }

        private Vector2 Pivot(TextPivot pivot)
        {
            switch (pivot)
            {
                case TextPivot.MiddleLeft:
                    return new Vector2(0, 0.5f);

                case TextPivot.MiddleCenter:
                    return new Vector2(0.5f, 0.5f);

                case TextPivot.MiddleRight:
                    return new Vector2(1, 0.5f);

                default:
                    throw new System.Exception($"Unimplemented text pivot: {pivot}");
            }
        }

        private GameObject BuildCanvas(TextPivot pivot)
        {
            GameObject canvasObj = new GameObject("canvas", typeof(RectTransform));
            var canvas = canvasObj.AddComponent<Canvas>();
            canvas.renderMode = RenderMode.WorldSpace;
            canvas.gameObject.AddComponent<CanvasScaler>().dynamicPixelsPerUnit = 300;
            var canvasTransform = canvas.GetComponent<RectTransform>();
            var canvasRect = canvasTransform.rect;
            canvasTransform.sizeDelta = new Vector2(10, .5f);
            canvasTransform.pivot = Pivot(pivot);
            return canvasObj;
        }

        public GameObject Build(TextPivot pivot, string displayText)
        {
            GameObject canvasObj = BuildCanvas(pivot);

            GameObject textObj = new GameObject("text", typeof(RectTransform));
            textObj.transform.SetParent(canvasObj.transform);

            var text = textObj.AddComponent<Text>();
            switch (pivot)
            {
                case TextPivot.MiddleLeft:
                    text.alignment = TextAnchor.MiddleLeft;
                    break;

                case TextPivot.MiddleCenter:
                    text.alignment = TextAnchor.MiddleCenter;
                    break;

                case TextPivot.MiddleRight:
                    text.alignment = TextAnchor.MiddleRight;
                    break;

                default:
                    throw new System.Exception($"Unimplemented text pivot: {pivot}");
            }
            text.text = displayText;
            text.resizeTextForBestFit = true;
            text.resizeTextMinSize = 0;
            text.font = font;

            var textTransform = text.GetComponent<RectTransform>();
            textTransform.anchorMin = new Vector2(0, 0);
            textTransform.anchorMax = new Vector2(1, 1);
            textTransform.pivot = new Vector2(0.5f, 0.5f);

            textTransform.anchoredPosition = new Vector3(0, 0, 0);
            textTransform.SetLeft(0);
            textTransform.SetRight(0);
            textTransform.SetTop(0);
            textTransform.SetBottom(0);

            return canvasObj;
        }
    }
}