using UnityEngine;

namespace Recolude.Analytics.Charting
{
    public class CellTheme : ITheme<Cell>
    {
        public enum ColorInterpolation
        {
            Linear,
            Logarithmic
        }

        float minValue;

        float maxValue;

        ColorBlender gradients;

        ColorInterpolation colorInterpolation;

        public CellTheme() : this(0, 1, ColorBlender.HeatMap(), ColorInterpolation.Logarithmic)
        {
        }

        public CellTheme(HeatmapData data) : this(data.MinCellValue, data.MaxCellValue, ColorBlender.HeatMap(), ColorInterpolation.Logarithmic)
        {
        }

        public CellTheme(HeatmapData data, ColorInterpolation colorInterpolation) : this(data.MinCellValue, data.MaxCellValue, ColorBlender.HeatMap(), colorInterpolation)
        {
        }

        public CellTheme(float minValue, float maxValue, ColorBlender gradient, ColorInterpolation colorInterpolation)
        {
            this.minValue = minValue;
            this.maxValue = maxValue;
            this.gradients = gradient;
        }

        public MaterialStyle Material(Cell cell)
        {
            switch (colorInterpolation)
            {
                case ColorInterpolation.Linear:
                    return LinearMaterial(cell);

                case ColorInterpolation.Logarithmic:
                    return LogarithmicMaterial(cell);

                default:
                    throw new System.NotImplementedException("No implementation for color interpolation: " + colorInterpolation);
            }
        }

        private MaterialStyle LinearMaterial(Cell cell)
        {
            float dist = maxValue - minValue;
            float calmpedVal = Mathf.Clamp(cell.Value, minValue, maxValue);
            float scaledVal = dist == 0 ? 0 : (calmpedVal - minValue) / dist;
            Color color = gradients.Sample(scaledVal);
            color.a = Mathf.Pow((float)System.Math.E, scaledVal - 1f) - 0.1f;
            return new MaterialStyle(color);
        }

        private MaterialStyle LogarithmicMaterial(Cell cell)
        {
            var diff = Mathf.Log10(maxValue - minValue + 1);

            if (diff == 0)
            {
                return new MaterialStyle(gradients.Sample(0));
            }

            float scaledVal = Mathf.Log10(cell.Value - minValue + 1) / diff;
            Color color = gradients.Sample(scaledVal);
            color.a = Mathf.Pow((float)System.Math.E, scaledVal - 1f) - 0.1f;
            return new MaterialStyle(color);
        }

    }
}
