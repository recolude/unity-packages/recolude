using UnityEngine;

namespace Recolude.Analytics.Charting
{
    public class ColorBlender
    {

        private readonly Color[] colors;

        private readonly float divisor;

        public ColorBlender(Color[] colors)
        {
            if (colors == null)
            {
                throw new System.ArgumentNullException("Colors can not be null");
            }
            if (colors.Length < 2)
            {
                throw new System.ArgumentException("Atleast 2 colors are required to make a gradiant");
            }
            this.colors = colors;
            this.divisor = 1f / colors.Length;
        }

        public static ColorBlender HeatMap()
        {
            return new ColorBlender(
                new Color[] {
                    Color.blue,
                    Color.cyan,
                    Color.green,
                    Color.yellow,
                    Color.red
                }
            );
        }

        public Color Sample(float value)
        {
            if (value <= 0)
            {
                return colors[0];
            }

            if (value >= 1)
            {
                return colors[colors.Length - 1];
            }

            float startValue = Mathf.Floor(value * colors.Length);
            int minIndex = Mathf.FloorToInt(value * (colors.Length - 1));
            int nextIndex = minIndex + 1;
            Color minColor = colors[minIndex];
            Color maxColor = colors[nextIndex];

            float scaledVal = (value - (startValue / colors.Length)) / divisor;
            float scaledValInv = 1f - scaledVal;
            return new Color(
                (minColor.r * scaledValInv) + (maxColor.r * scaledVal),
                (minColor.g * scaledValInv) + (maxColor.g * scaledVal),
                (minColor.b * scaledValInv) + (maxColor.b * scaledVal)
            );
        }

    }

}