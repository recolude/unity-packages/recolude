using Recolude.Analytics.Charting.BarChart;
using Recolude.Analytics.Charting.PieChart;
using Recolude.Analytics.Charting.Sort;
using Recolude.Analytics.Charting.MaterialStyleBuilder;
using System;
using System.Collections.Generic;
using System.Text;

namespace Recolude.Analytics.Charting
{
    public class DataSet
    {
        string name;

        Entry[] data;

        string[] columns;

        public DataSet(string name, Entry[] data, string[] columns)
        {
            this.name = name;
            this.data = data;
            this.columns = columns;
        }

        public DataSet(string name, Entry[] data)
        {
            this.name = name;
            this.data = data;
            Dictionary<string, bool> columnsFound = new Dictionary<string, bool>();
            foreach (var entry in data)
            {
                foreach (var item in entry.Data())
                {
                    if (item.Key == null)
                    {
                        continue;
                    }
                    columnsFound[item.Key] = true;
                }
            }

            columns = new string[columnsFound.Count];
            int i = 0;
            foreach (var item in columnsFound)
            {
                columns[i] = item.Key;
                i++;
            }
        }

        public string Name()
        {
            return this.name;
        }

        public Entry[] Entries()
        {
            return this.data;
        }

        public string[] Columns()
        {
            return columns;
        }


        public DataSet Map(Func<Entry, Entry> mappingFunction)
        {
            Entry[] newData = new Entry[this.data.Length];
            for (int i = 0; i < data.Length; i++)
            {
                newData[i] = mappingFunction(this.data[i]);
            }
            return new DataSet(name, newData);
        }

        public DataSet Sum(string groupByKey, string keyToSum)
        {
            Dictionary<IComparable, float> results = new Dictionary<IComparable, float>();

            foreach (var entry in this.Entries())
            {
                var key = entry.Value(groupByKey);
                if (results.ContainsKey(key) == false)
                {
                    results.Add(key, 0);
                }
                results[key] = results[key] + entry.Float(keyToSum);
            }

            var resultingEntries = new Entry[results.Count];
            var i = 0;
            foreach (var keyval in results)
            {
                resultingEntries[i] = new Entry()
                    .Set(groupByKey, keyval.Key)
                    .Set(keyToSum, keyval.Value);
                i++;
            }

            return new DataSet(name, resultingEntries);
        }

        public DataSet Sort(ISort sort)
        {
            return new DataSet(name, sort.Sort(data), columns);
        }

        public DataSet SortAscending(string key)
        {
            return Sort(new Ascending(key));
        }

        public DataSet SortDescending(string key)
        {
            return Sort(new Descending(key));
        }

        public BarChartBuilder BarChart()
        {
            return new BarChartBuilder(this, new CanvasTextBuilder(), new BuiltinMatStyleBuilder(BuiltinMatStyleBuilder.MaterialType.Unlit), EntryTheme.BarChart());
        }

        public PieChartBuilder PieChart(string sumOn)
        {
            return new PieChartBuilder(
                this,
                new CanvasTextBuilder(),
                new BuiltinMatStyleBuilder(BuiltinMatStyleBuilder.MaterialType.Unlit),
                EntryTheme.PieChart(),
                sumOn
            );
        }

        public override string ToString()
        {
            StringBuilder stringOut = new StringBuilder();
            stringOut.Append("|");
            foreach (var column in columns)
            {
                stringOut.Append(" ");
                if (column == null)
                {
                    stringOut.Append("(null)");
                }
                else
                {
                    stringOut.Append(column);
                }
                stringOut.Append(" |");
            }
            stringOut.Append("\n");


            foreach (var entry in data)
            {
                stringOut.Append("|");
                foreach (var column in columns)
                {
                    stringOut.Append(" ");
                    if (column != null && entry.Contains(column))
                    {
                        stringOut.Append(entry.Value(column));
                    }
                    else
                    {
                        stringOut.Append("N/A");
                    }
                    stringOut.Append(" |");
                }
                stringOut.Append("\n");
            }

            return stringOut.ToString();
        }
    }
}