using System.Collections.Generic;
using System;

namespace Recolude.Analytics.Charting
{
    public class Entry
    {
        Dictionary<string, IComparable> data;

        public Entry()
        {
            this.data = new Dictionary<string, IComparable>();
        }

        public Entry(Dictionary<string, IComparable> data)
        {
            this.data = data;
        }

        public Entry Set(string key, IComparable value)
        {
            this.data[key] = value;
            return this;
        }

        public Dictionary<string, IComparable> Data()
        {
            return data;
        }

        public int Count
        {
            get
            {
                return data.Count;
            }
        }

        public bool Contains(string key)
        {
            return this.data.ContainsKey(key);
        }

        public IComparable Value(string key)
        {
            return data[key];
        }

        public string String(string key)
        {
            return data[key].ToString();
        }

        public float Float(string key)
        {
            var val = data[key];
            if (val is float)
            {
                return (float)val;
            }
            if (val is int)
            {
                return (int)val;
            }
            throw new System.Exception(string.Format("Data[{0}] is not int or float: {1}", key, val));
        }

        public override bool Equals(object obj)
        {
            return obj is Entry entry &&
                   EqualityComparer<Dictionary<string, IComparable>>.Default.Equals(data, entry.data);
        }

        public override int GetHashCode()
        {
            return 1768953197 + EqualityComparer<Dictionary<string, IComparable>>.Default.GetHashCode(data);
        }
    }
}