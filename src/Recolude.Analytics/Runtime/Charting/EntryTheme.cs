using UnityEngine;
using System.Collections.Generic;

namespace Recolude.Analytics.Charting
{
    public class EntryTheme : ITheme<Entry>
    {
        Color[] colors;

        Dictionary<Entry, MaterialStyle> mappings;

        public EntryTheme() : this(new Color[] { Color.red, Color.green, Color.blue, Color.yellow, Color.cyan, Color.magenta })
        {
        }

        public EntryTheme(Color[] colors)
        {
            this.colors = colors;
            mappings = new Dictionary<Entry, MaterialStyle>();
        }

        private static Color To01(float r, float g, float b)
        {
            return new Color(r / 255f, g / 255f, b / 255f);
        }

        public static EntryTheme BarChart()
        {
            return new EntryTheme(new Color[] {
                To01(215, 1, 90), // #d7015a
                To01(255, 177, 78), // #ffb14e
                To01(250, 135, 117), // #fa8775
                To01(234, 95, 148), // #ea5f94
                To01(205, 52, 181), // #cd34b5
                To01(157, 2, 215), // #9d02d7
                To01(0, 0, 255), // #0000ff
            });
        }

        public static EntryTheme PieChart()
        {
            return new EntryTheme(new Color[] {
                To01(46, 0, 19), // #2e0013
                To01(92, 0, 38), // #5c0026
                To01(138, 0, 57), // #8a0039
                To01(184, 0, 77), // #b8004d
                To01(230, 1, 96), // #e60160
                To01(253, 24, 119), // #fd1877
                To01(254, 70, 146), // #fe4692
                To01(254, 116, 173), // #fe74ad
                To01(254, 162, 200), // #fea2c8
                To01(254, 208, 227), // #fed0e3
            });
        }

        public MaterialStyle Material(Entry entry)
        {
            if (mappings.ContainsKey(entry) == false)
            {
                mappings[entry] = new MaterialStyle(colors[mappings.Count % colors.Length]);
            }
            return mappings[entry];
        }

    }
}
