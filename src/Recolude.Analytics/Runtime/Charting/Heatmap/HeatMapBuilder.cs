using UnityEngine;

namespace Recolude.Analytics.Charting.Heatmap
{

    public class HeatmapBuilder
    {
        HeatmapData data;

        ITheme<Cell> theme;

        IMaterialStyleBuilder matBuilder;

        public HeatmapBuilder()
        {

        }

        public HeatmapBuilder(HeatmapData data, ITheme<Cell> theme, IMaterialStyleBuilder matBuilder)
        {
            this.data = data;
            this.theme = theme;
            this.matBuilder = matBuilder;
        }

        public HeatmapBuilder Data(HeatmapData data)
        {
            return new HeatmapBuilder(data, theme, matBuilder);
        }

        public HeatmapBuilder Theme(ITheme<Cell> theme)
        {
            return new HeatmapBuilder(data, theme, matBuilder);
        }

        public HeatmapBuilder MaterialStyleBuilder(IMaterialStyleBuilder matBuilder)
        {
            return new HeatmapBuilder(data, theme, matBuilder);
        }

        public HeatmapBehavior Render()
        {
            GameObject graph = new GameObject("Heatmap");

            var builtCells = new CellBehavior[data.Cells.Length];

            for (int i = 0; i < data.Cells.Length; i++)
            {
                builtCells[i] = CellBehavior.Build(
                    data.Cells[i],
                    data.CellSize,
                    matBuilder.Build(theme.Material(data.Cells[i])),
                    data.MinCellValue,
                    data.MaxCellValue
                );
                builtCells[i].transform.SetParent(graph.transform);
            }

            return HeatmapBehavior.Build(graph, builtCells);
        }
    }
}