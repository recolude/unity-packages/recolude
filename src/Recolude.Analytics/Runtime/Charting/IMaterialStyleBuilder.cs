using UnityEngine;

namespace Recolude.Analytics.Charting
{
    public interface IMaterialStyleBuilder
    {
        Material Build(MaterialStyle style);
    }
}