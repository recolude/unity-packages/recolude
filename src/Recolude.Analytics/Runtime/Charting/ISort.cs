namespace Recolude.Analytics.Charting
{
    public interface ISort
    {
        Entry[] Sort(Entry[] data);
    }
}