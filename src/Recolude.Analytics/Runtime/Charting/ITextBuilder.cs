using UnityEngine;

namespace Recolude.Analytics.Charting
{
    public interface ITextBuilder
    {
        GameObject Build(TextPivot pivot, string text);
    }
}