namespace Recolude.Analytics.Charting
{
    public interface ITheme<T>
    {
        MaterialStyle Material(T data);
    }
}