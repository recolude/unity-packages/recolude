using UnityEngine;

namespace Recolude.Analytics.Charting
{
    public class MaterialStyle
    {
        Color color;

        public MaterialStyle(Color color)
        {
            this.color = color;
        }

        public Color Color()
        {
            return color;
        }

    }
}