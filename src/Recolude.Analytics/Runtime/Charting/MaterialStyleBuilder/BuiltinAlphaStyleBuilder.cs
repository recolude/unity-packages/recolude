using UnityEngine;

namespace Recolude.Analytics.Charting.MaterialStyleBuilder
{
    public class BuiltinAlphaStyleBuilder : IMaterialStyleBuilder
    {
        float alpha;

        public BuiltinAlphaStyleBuilder(float alpha)
        {
            this.alpha = alpha;
        }

        private static Shader standardShaderInstance;

        private static Shader StandardShader()
        {
            if (standardShaderInstance == null)
            {
                standardShaderInstance = Shader.Find("Unlit/Color");
            }
            return standardShaderInstance;
        }

        public Material Build(MaterialStyle style)
        {
            Material mat = new Material(StandardShader());

            // https://github.com/Unity-Technologies/UnityCsReference/blob/master/Editor/Mono/Inspector/StandardShaderGUI.cs
            //mat.SetOverrideTag("RenderType", "Transparent");
            //mat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
            //mat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            //mat.SetInt("_ZWrite", 0);
            //mat.DisableKeyword("_ALPHATEST_ON");
            //mat.DisableKeyword("_ALPHABLEND_ON");
            //mat.EnableKeyword("_ALPHAPREMULTIPLY_ON");
            //mat.renderQueue = (int)UnityEngine.Rendering.RenderQueue.Transparent;

            mat.color = style.Color();
            return mat;
        }

    }
}