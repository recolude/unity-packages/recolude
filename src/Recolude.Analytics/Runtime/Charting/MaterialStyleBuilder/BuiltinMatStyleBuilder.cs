using UnityEngine;

namespace Recolude.Analytics.Charting.MaterialStyleBuilder
{
    public class BuiltinMatStyleBuilder : IMaterialStyleBuilder
    {
        public enum MaterialType
        {
            Standard,
            Unlit,
            Metalic
        }

        MaterialType materialType;

        public BuiltinMatStyleBuilder(MaterialType materialType)
        {
            this.materialType = materialType;
        }

        public Material Build(MaterialStyle style)
        {
            Material mat = new Material(MaterialTypeToShaderName(materialType));
            mat.color = style.Color();
            return mat;
        }

        private static Shader MaterialTypeToShaderName(MaterialType type)
        {
            switch (type)
            {
                case MaterialType.Standard:
                case MaterialType.Metalic:
                    return Shader.Find("Standard");

                case MaterialType.Unlit:
                    return Shader.Find("Unlit/Color");
            }
            throw new System.ArgumentException("Unrecognized material type: " + type.ToString());
        }
    }
}