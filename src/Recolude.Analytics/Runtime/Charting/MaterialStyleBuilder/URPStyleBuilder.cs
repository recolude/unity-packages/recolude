using UnityEngine;

namespace Recolude.Analytics.Charting.MaterialStyleBuilder
{
    [CreateAssetMenu(fileName = "URP Style Builder", menuName = "Recolude/Charting/URP Style Builder", order = 1)]
    public class URPStyleBuilder : ScriptableObject, IMaterialStyleBuilder
    {

        [SerializeField]
        Material materialToUse;

        public Material Build(MaterialStyle style)
        {
            Material mat = new Material(materialToUse);
            mat.color = style.Color();
            return mat;
        }

    }
}