using UnityEngine;

namespace Recolude.Analytics.Charting.PieChart
{
    public class PieChartBuilder
    {
        readonly DataSet data;

        readonly ITheme<Entry> theme;

        readonly IMaterialStyleBuilder matBuilder;

        readonly ITextBuilder textBuilder;

        readonly string sumOn;

        readonly string legend;

        public PieChartBuilder(DataSet data, ITextBuilder textBuilder, IMaterialStyleBuilder matBuilder, ITheme<Entry> theme, string sumOn, string legend)
        {
            this.data = data;
            this.textBuilder = textBuilder;
            this.matBuilder = matBuilder;
            this.theme = theme;
            this.sumOn = sumOn;
            this.legend = legend;
        }

        public PieChartBuilder(DataSet data, ITextBuilder textBuilder, IMaterialStyleBuilder matBuilder, ITheme<Entry> theme, string sumOn) : this(data, textBuilder, matBuilder, theme, sumOn, null)
        {
        }

        public PieChartBuilder Legend(string legend)
        {
            return new PieChartBuilder(data, textBuilder, matBuilder, theme, sumOn, legend);
        }

        GameObject GenerateWedge(float offset, float size, float wedgeRadius, Material mat)
        {
            const int verticesOnEdge = 20;
            float depth = wedgeRadius / 4f;
            float halfDepth = depth / 2f;

            var wedge = new GameObject(size.ToString());

            Vector3[] vertices = new Vector3[(verticesOnEdge + 1) * 2];

            // Two centers of pi wedge
            vertices[0] = new Vector3(0, 0, halfDepth);
            vertices[verticesOnEdge + 1] = new Vector3(0, 0, -halfDepth);

            float increment = (360f * size * Mathf.Deg2Rad) / (verticesOnEdge - 1);
            float angleOffset = 360f * offset * Mathf.Deg2Rad;
            for (int i = 0; i < verticesOnEdge; i++)
            {
                var cos = Mathf.Cos((i * increment) + angleOffset) * wedgeRadius;
                var sin = Mathf.Sin((i * increment) + angleOffset) * wedgeRadius;
                vertices[1 + i] = new Vector3(cos, sin, halfDepth);
                vertices[verticesOnEdge + 2 + i] = new Vector3(cos, sin, -halfDepth);
            }

            int[] triIndices = new int[(verticesOnEdge - 1) * 3 * 4]; // * 3 <-- tris; * 4 <-- front poly, back poly, two polys on edge
            for (int i = 1; i < verticesOnEdge; i++)
            {
                var front = (i - 1) * 3;
                var back = front + ((verticesOnEdge - 1) * 3);
                var edge = back + ((verticesOnEdge - 1) * 3);
                var edge2 = edge + ((verticesOnEdge - 1) * 3);

                triIndices[front] = 0;
                triIndices[front + 1] = i;
                triIndices[front + 2] = i + 1;

                triIndices[back + 2] = verticesOnEdge + 1;
                triIndices[back + 1] = verticesOnEdge + 1 + i;
                triIndices[back] = verticesOnEdge + 1 + i + 1;

                triIndices[edge] = i;
                triIndices[edge + 1] = verticesOnEdge + 1 + i;
                triIndices[edge + 2] = i + 1;

                triIndices[edge2] = verticesOnEdge + 1 + i;
                triIndices[edge2 + 1] = verticesOnEdge + 1 + i + 1;
                triIndices[edge2 + 2] = i + 1;
            }

            var mesh = new Mesh();
            mesh.vertices = vertices;
            mesh.triangles = triIndices;
            mesh.RecalculateNormals();
            mesh.UploadMeshData(true);

            wedge.AddComponent<MeshFilter>().mesh = mesh;
            wedge.AddComponent<MeshRenderer>().material = mat;

            return wedge;
        }

        private GameObject RenderTitle()
        {
            var title = textBuilder.Build(TextPivot.MiddleCenter, data.Name());
            title.gameObject.name = "Title";
            title.transform.position = new Vector3(0, 1.5f, 0);
            return title;
        }

        public GameObject Render()
        {
            if (textBuilder == null)
            {
                throw new System.InvalidOperationException("can not render graph with null text builder");
            }

            GameObject graph = new GameObject(data.Name());

            RenderTitle().transform.SetParent(graph.transform);

            float[] values = new float[data.Entries().Length];
            float sum = 0;
            for (int i = 0; i < data.Entries().Length; i++)
            {
                values[i] = data.Entries()[i].Float(sumOn);
                sum += values[i];
            }

            var offset = 0f;
            const float keySize = .25f;
            const float padding = 0.1f;
            for (int i = 0; i < data.Entries().Length; i++)
            {
                var normalized = values[i] / sum;
                var mat = matBuilder.Build(theme.Material(data.Entries()[i]));

                GenerateWedge(offset, normalized, 1f, mat).transform.SetParent(graph.transform);

                var keyPos = new Vector3(1 + padding + (keySize / 2f), 1 - padding - ((keySize + padding) * i), 0);
                var keyTextPos = new Vector3(keyPos.x + keySize, keyPos.y, keyPos.z);

                var key = GameObject.CreatePrimitive(PrimitiveType.Cube);
                key.transform.SetParent(graph.transform);
                key.GetComponent<MeshRenderer>().material = mat;
                key.transform.localScale = Vector3.one * keySize;
                key.transform.position = keyPos;

                string keyTextDisplay = null;
                if (string.IsNullOrWhiteSpace(legend))
                {
                    keyTextDisplay = data.Entries()[i].Float(sumOn).ToString("0.00");
                }
                else
                {
                    keyTextDisplay = data.Entries()[i].Value(legend).ToString();
                }

                var keyText = textBuilder.Build(TextPivot.MiddleLeft, keyTextDisplay);
                keyText.transform.SetParent(graph.transform);
                keyText.transform.position = keyTextPos;

                offset += normalized;
            }

            return graph;
        }
    }
}
