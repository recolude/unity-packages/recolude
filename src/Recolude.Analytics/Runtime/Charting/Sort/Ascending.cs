namespace Recolude.Analytics.Charting.Sort
{
    public class Ascending : ISort
    {
        string key;

        public Ascending(string key)
        {
            this.key = key;
        }

        public Entry[] Sort(Entry[] data)
        {
            var sortedData = new System.Collections.Generic.List<Entry>(data);
            sortedData.Sort((x, y) => x.Value(key).CompareTo(y.Value(key)));
            return sortedData.ToArray();
        }
    }
}