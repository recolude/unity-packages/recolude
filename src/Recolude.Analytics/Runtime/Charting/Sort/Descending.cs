namespace Recolude.Analytics.Charting.Sort
{
    public class Descending : ISort
    {
        string key;

        public Descending(string key)
        {
            this.key = key;
        }

        public Entry[] Sort(Entry[] data)
        {
            var sortedData = new System.Collections.Generic.List<Entry>(data);
            sortedData.Sort((x, y) => y.Value(key).CompareTo(x.Value(key)));
            return sortedData.ToArray();
        }
    }
}