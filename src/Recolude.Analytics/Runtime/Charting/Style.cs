using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Recolude.Analytics.Charting
{
    public class Style
    {
        Color color;

        public Style(Color color)
        {
            this.color = color;
        }

        public Color Color()
        {
            return this.color;
        }
    }
}
