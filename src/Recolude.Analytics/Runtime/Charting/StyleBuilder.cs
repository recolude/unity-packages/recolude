using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Recolude.Analytics.Charting
{
    public class StyleBuilder
    {
        Color color;

        public StyleBuilder()
        {
            color = UnityEngine.Color.blue;
        }

        public StyleBuilder(Color color)
        {
            color = this.color;
        }

        public StyleBuilder Color(Color color)
        {
            return new StyleBuilder(color);
        }

        public Style Style()
        {
            return new Style(color);
        }
    }
}
