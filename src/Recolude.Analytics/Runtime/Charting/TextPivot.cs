namespace Recolude.Analytics.Charting
{
    public enum TextPivot
    {
        MiddleLeft,
        MiddleRight,
        MiddleCenter
    }
}