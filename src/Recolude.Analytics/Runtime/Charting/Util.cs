using Recolude.Analytics.Charting.BarChart;
using Recolude.Analytics.Charting.Sort;
using Recolude.Analytics.Charting.MaterialStyleBuilder;
using UnityEngine;

namespace Recolude.Analytics.Charting
{
    public static class Util
    {

        public static LineRenderer BuildLine(float width, Material lineMaterial, params Vector3[] points)
        {
            GameObject line = new GameObject("line");
            LineRenderer lineRenderer = line.AddComponent<LineRenderer>();
            lineRenderer.positionCount = points.Length;
            lineRenderer.SetPositions(points);
            lineRenderer.material = lineMaterial;
            lineRenderer.startWidth = width;
            lineRenderer.endWidth = width;
            lineRenderer.useWorldSpace = false;
            return lineRenderer;
        }

    }
}