﻿using UnityEngine;

namespace Recolude.Analytics
{
    public class HeatmapBehavior : MonoBehaviour
    {
        CellBehavior[] cells;

        Cell currentlySelectedCell;

        public static HeatmapBehavior Build(GameObject gameObject, CellBehavior[] cells)
        {
            var heatmapBehavior = gameObject.AddComponent<HeatmapBehavior>();
            heatmapBehavior.cells = cells;
            foreach (var cell in cells)
            {
                cell.SetHeatmap(heatmapBehavior);
            }
            return heatmapBehavior;
        }

        public void SetStrength(float val)
        {
            var cleanedVal = Mathf.Clamp(val, 0f, 2f);
            foreach (var cell in cells)
            {
                cell.SetStrength(cleanedVal);
            }
        }

        internal void CellClicked(Cell cell)
        {
            currentlySelectedCell = cell;
        }

        public Cell CurrentlySelectedCell()
        {
            return currentlySelectedCell;
        }
    }
}