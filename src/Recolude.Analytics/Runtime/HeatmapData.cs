﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace Recolude.Analytics
{
    public class HeatmapData
    {
        
        float cellSize;

        Cell[] cells;

        float min;

        float max;

        public HeatmapData(float cellSize, Cell[] cells, float min, float max)
        {
            this.cellSize = cellSize;
            this.cells = cells;
            this.min = min;
            this.max = max;
        }

        public float CellSize { get => cellSize; set => cellSize = value; }
        public Cell[] Cells { get => cells; set => cells = value; }

        public float MinCellValue { get => min; }

        public float MaxCellValue { get => max; }

        public static HeatmapData Build(Stream streamIn)
        {
            using (BinaryReader reader = new BinaryReader(streamIn))
            {
                int version = reader.ReadByte();
                if (version != 1)
                {
                    throw new System.Exception(string.Format("Unrecognized heatmap version format: {0}", version));
                }

                float cellSize = reader.ReadSingle();
                int numberOfCells = (int)BinaryUtil.ReadUvarint(streamIn);
                if (numberOfCells == 0)
                {
                    return new HeatmapData(cellSize, new Cell[0], 0, 0);
                }

                float min = reader.ReadSingle();
                float max = reader.ReadSingle();
                float left = reader.ReadSingle();
                float bottom = reader.ReadSingle();
                float back = reader.ReadSingle();

                var cells = new Cell[numberOfCells];
                for (int i = 0; i < numberOfCells; i++)
                {
                    var cellValue = BinaryUtil.ReadFloat16(streamIn, min, max);
                    cells[i] = new Cell(
                        new Vector3(
                            ((int)BinaryUtil.ReadUvarint(streamIn)) * cellSize + left,
                            ((int)BinaryUtil.ReadUvarint(streamIn)) * cellSize + bottom,
                            ((int)BinaryUtil.ReadUvarint(streamIn)) * cellSize + back
                        ),
                        cellValue
                    );
                }
                return new HeatmapData(cellSize, cells, min, max);
            }
        }

        public override bool Equals(object obj)
        {
            return obj is HeatmapData heatmap &&
                   CellSize == heatmap.CellSize &&
                   EqualityComparer<Cell[]>.Default.Equals(Cells, heatmap.Cells);
        }

        public override int GetHashCode()
        {
            int hashCode = 480580542;
            hashCode = hashCode * -1521134295 + CellSize.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<Cell[]>.Default.GetHashCode(Cells);
            return hashCode;
        }
    }

}