﻿using System;
using System.Collections;
using UnityEngine;
using Recolude.API;
using Recolude.Analytics.Charting;
using System.IO;
using Recolude.Analytics.Charting.Heatmap;
using Recolude.Analytics.Charting.MaterialStyleBuilder;

namespace Recolude.Analytics
{

    public class HeatmapRenderRequest : IRenderRequest
    {
        RenderError error;

        bool completed;

        HeatmapBehavior heatmap;

        IWebRequest heatmapRequest;

        public HeatmapRenderRequest(AnalyticsService.GetDiscreteMapExecutionUnityWebRequest discreteMap)
        {
            heatmapRequest = discreteMap;
            completed = false;
        }

        public HeatmapRenderRequest(AnalyticsService.GetContinuousMapExecutionUnityWebRequest continuousMap)
        {
            heatmapRequest = continuousMap;
            completed = false;
        }

        public bool Completed()
        {
            return completed;
        }

        public RenderError Error()
        {
            return error;
        }

        public GameObject RenderedObject()
        {
            return heatmap.gameObject;
        }

        public HeatmapBehavior RenderedHeatmap()
        {
            return heatmap;
        }

        public IEnumerator Run()
        {
            Debug.Log("About to run");
            yield return heatmapRequest.Run();
            Debug.Log(heatmapRequest.UnderlyingRequest.responseCode);
            completed = true;

            if (heatmapRequest.UnderlyingRequest.responseCode != 200)
            {
                error = RenderError.FailedWebRequest(heatmapRequest.UnderlyingRequest);
                yield break;
            }

            byte[] heatmapRaw = heatmapRequest.UnderlyingRequest.downloadHandler.data;
            HeatmapData heatmapData = Recolude.Analytics.HeatmapData.Build(new MemoryStream(heatmapRaw));
            heatmap = new HeatmapBuilder()
                .Data(heatmapData)
                .Theme(new CellTheme(heatmapData))
                .MaterialStyleBuilder(new BuiltinAlphaStyleBuilder(0.65f))
                .Render();
        }
    }

}