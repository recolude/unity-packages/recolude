﻿using System.Collections;
using UnityEngine;

namespace Recolude.Analytics
{

    /// <summary>
    /// A render request reaches out to Recolude's servers, collects necessary
    /// data pertaining to the query, and displays it inside the application.
    /// </summary>
    public interface IRenderRequest
    {
        /// <summary>
        /// Any error that occurred while attempting to complete the request.
        /// </summary>
        /// <returns>An error, if one occurred. Null otherwise.</returns>
        RenderError Error();

        /// <summary>
        /// Runs the request as a coroutine.
        /// </summary>
        /// <returns>A coroutine to be waited upon.</returns>
        IEnumerator Run();

        /// <summary>
        /// Whether or not the request has finished.
        /// </summary>
        /// <returns>true if the request has completed.</returns>
        bool Completed();

        /// <summary>
        /// The rendered data, if no errors occurred.
        /// </summary>
        /// <returns>The rendered data, null if errors occurred.</returns>
        GameObject RenderedObject();
    }

}