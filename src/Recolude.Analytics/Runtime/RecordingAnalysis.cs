using System.Collections.Generic;
using Recolude.Core;
using UnityEngine;

namespace Recolude.Analytics
{
    public class RecordingAnalysis
    {

        IRecording recording;

        public RecordingAnalysis(IRecording recording)
        {
            this.recording = recording;
        }

        private static List<IRecording> FlattenHierarchy(IRecording recording)
        {
            var temp = new List<IRecording>();
            foreach (var child in recording.Recordings)
            {
                temp.AddRange(FlattenHierarchy(child));
            }
            temp.Add(recording);
            return temp;
        }

        private static Vector3 DiscritizePosIntoCell(float cubeSize, Vector3 pos)
        {
            var mult = 1.0f / cubeSize;
            return new Vector3(
              Mathf.Round(pos.x * mult) / mult,
              Mathf.Round(pos.y * mult) / mult,
              Mathf.Round(pos.z * mult) / mult
            );
        }


        struct CaptureStart
        {
            int leftIndex;
            int rightIndex;

            public int LeftIndex { get => leftIndex; set => leftIndex = value; }
            public int RightIndex { get => rightIndex; set => rightIndex = value; }
        }

        private static void add(
            Dictionary<float, Dictionary<float, Dictionary<float, float>>> temp,
            Vector3 pos,
            float val
        )
        {
            if (!temp.ContainsKey(pos.x))
            {
                temp[pos.x] = new Dictionary<float, Dictionary<float, float>>();
            }

            if (!temp[pos.x].ContainsKey(pos.y))
            {
                temp[pos.x][pos.y] = new Dictionary<float, float>();
            }

            Dictionary<float, float> xy = temp[pos.x][pos.y];

            if (!xy.ContainsKey(pos.z))
            {
                xy[pos.z] = val;
            }
            else
            {
                xy[pos.z] = xy[pos.z] + val;
            }
        }

        private static int GetAllCubes(
            float cubeSize,
            Vector3 a,
            Vector3 b,
            Vector3[] runningData,
            int index
        )
        {
            if (a == b)
            {
                runningData[index] = a;
                return index + 1;
            }

            var mid = new Vector3(
              (b.x + a.x) / 2.0f,
              (b.y + a.y) / 2.0f,
              (b.z + a.z) / 2.0f
            );
            mid = DiscritizePosIntoCell(cubeSize, mid);

            if (a == mid || mid == b)
            {
                runningData[index] = a;
                runningData[index + 1] = b;
                return index + 2;
            }

            var newIndex = GetAllCubes(cubeSize, a, mid, runningData, index);
            // runningData.pop(); // Don't want any overlap with right side (mid)
            return GetAllCubes(cubeSize, mid, b, runningData, newIndex - 1); // Don't want any overlap with right side, so start back one (mid)
        }


        private static Vector3[] interpolateAndAddEverythingCache;

        private static void InterpolateAndAddEverything(
            Dictionary<float, Dictionary<float, Dictionary<float, float>>> temp,
            float cubeSize,
            Vector3 a,
            Vector3 b,
            float delta
        )
        {
            if (
              float.IsNaN(a.x) ||
              float.IsNaN(a.y) ||
              float.IsNaN(a.z) ||
              float.IsNaN(b.x) ||
              float.IsNaN(b.y) ||
              float.IsNaN(b.z)
            )
            {
                return;
            }
            if (interpolateAndAddEverythingCache == null)
            {
                // I swear to god if we ever  get a recording that has a straight line longer than these many units...
                // TODO: Eli figure out a good warning
                interpolateAndAddEverythingCache = new Vector3[10000];
                for (var i = 0; i < interpolateAndAddEverythingCache.Length; i++)
                {
                    interpolateAndAddEverythingCache[i] = new Vector3();
                }
            }
            var cubesWritten = GetAllCubes(
              cubeSize,
              a,
              b,
              interpolateAndAddEverythingCache,
              0
            );

            var deltaPortion = delta / cubesWritten;
            for (var i = 0; i < cubesWritten; i++)
            {
                add(temp, interpolateAndAddEverythingCache[i], deltaPortion);
            }
        }


        private static CaptureStart GetStartCapturePrecisely(Capture<Vector3>[] captures, float startTime, float endTime)
        {
            // No valid captures fall within the timeslice, we need to ignore this
            // recording in it's entirety during heatmap calcs
            if (captures[0].Time > endTime)
            {
                throw new System.Exception("This should have already been checked (start)");
            }

            for (var i = 0; i < captures.Length; i++)
            {
                if (captures[i].Time > startTime)
                {
                    return new CaptureStart
                    {
                        LeftIndex = i - 1, // notice if i == 0, left is -1, a cute math thing
                        RightIndex = i,
                    };
                }
            }

            throw new System.Exception("no captures occur after start");
        }

        private static CaptureStart GetEndCapturePrecisely(Capture<Vector3>[] captures, float startTime, float endTime)
        {
            // No valid captures fall within the timeslice, we need to ignore this
            // recording in it's entirety during heatmap calcs
            if (captures[captures.Length - 1].Time < startTime)
            {
                throw new System.Exception("This should have already been checked (end)");
            }

            // Look for the first capture that occurs before the end cut
            for (int i = captures.Length - 1; i >= 0; i--)
            {
                if (captures[i].Time < endTime)
                {
                    return new CaptureStart
                    {
                        LeftIndex = i,
                        RightIndex = i == captures.Length - 1 ? -1 : i + 1,
                    };
                }
            }

            throw new System.Exception("no captures occur before end");
        }

        private static Capture<Vector3> GetPreciseCapture(Capture<Vector3>[] captures, CaptureStart indices, float cut)
        {
            if (indices.RightIndex == -1)
            {
                return new VectorCapture(
                     captures[indices.LeftIndex].Time,
                     captures[indices.LeftIndex].Value
                );
            }

            if (indices.LeftIndex == -1)
            {
                return new VectorCapture(
                    captures[indices.RightIndex].Time,
                    captures[indices.RightIndex].Value
                );
            }

            var leftCapture = captures[indices.LeftIndex];
            var rightCapture = captures[indices.RightIndex];

            var duration = rightCapture.Time - leftCapture.Time;
            var timeThroughCapture = cut - leftCapture.Time;
            var percent = timeThroughCapture / duration;

            var pos = ((rightCapture.Value - leftCapture.Value) * percent) + leftCapture.Value;

            return new VectorCapture(cut, pos);
        }

        public HeatmapData TimeInRegionHeatmap(float cubeSize, float startTime, float endTime)
        {
            var temp = new Dictionary<float, Dictionary<float, Dictionary<float, float>>>();
            var summary = FlattenHierarchy(recording);
            for (int subjectIndex = 0; subjectIndex < summary.Count; subjectIndex++)
            {
                IRecording subject = summary[subjectIndex];
                var positionCollection = subject.PositionCollection();
                if (positionCollection == null)
                {
                    continue;
                }
                Capture<Vector3>[] positions = subject.PositionCollection().Captures;
                if (positions.Length < 2)
                {
                    continue;
                }

                // This recording should be completely ignored (See: Case 8)
                if (positions[positions.Length - 1].Time <= startTime)
                {
                    continue;
                }

                // This recording should be completely ignored (See: Case 7)
                if (positions[0].Time >= endTime)
                {
                    continue;
                }

                var startIndices = GetStartCapturePrecisely(
                  positions,
                  startTime,
                  endTime
                );

                var startDetails = GetPreciseCapture(
                  positions,
                  startIndices,
                  startTime
                );

                var endIndices = GetEndCapturePrecisely(
                  positions,
                  startTime,
                  endTime
                );

                var endDetails = GetPreciseCapture(positions, endIndices, endTime);

                var forLoopStart = -1;
                if (positions[startIndices.RightIndex].Time < endTime)
                {
                    forLoopStart = startIndices.RightIndex;
                }

                var forLoopEnd = -1;
                if (positions[endIndices.LeftIndex].Time > startTime)
                {
                    forLoopEnd = endIndices.LeftIndex;
                }

                // DiscritizePosIntoCell(cubeSize, posCapture.Position())

                // Time slice takes place between two captures (See: Case 1)
                if (forLoopStart == -1 || forLoopEnd == -1)
                {
                    InterpolateAndAddEverything(
                      temp,
                      cubeSize,
                      DiscritizePosIntoCell(cubeSize, startDetails.Value),
                      DiscritizePosIntoCell(cubeSize, endDetails.Value),
                      endDetails.Time - startDetails.Time
                    );
                    continue;
                }

                // Add up trailing Start
                if (startIndices.LeftIndex != -1)
                {
                    InterpolateAndAddEverything(
                      temp,
                      cubeSize,
                      DiscritizePosIntoCell(cubeSize, startDetails.Value),
                      DiscritizePosIntoCell(
                        cubeSize,
                        positions[startIndices.RightIndex].Value
                      ),
                      positions[startIndices.RightIndex].Time - startDetails.Time
                    );
                }

                // Add up trailing End
                if (endIndices.RightIndex != -1)
                {
                    InterpolateAndAddEverything(
                      temp,
                      cubeSize,
                      DiscritizePosIntoCell(
                        cubeSize,
                        positions[endIndices.LeftIndex].Value
                      ),
                      DiscritizePosIntoCell(cubeSize, endDetails.Value),
                      endDetails.Time - positions[endIndices.LeftIndex].Time
                    );
                }

                // Fill in middle captures
                if (forLoopStart != forLoopEnd)
                {
                    for (
                      var posIndex = forLoopStart + 1;
                      posIndex <= forLoopEnd;
                      posIndex++
                    )
                    {
                        InterpolateAndAddEverything(
                          temp,
                          cubeSize,
                          DiscritizePosIntoCell(cubeSize, positions[posIndex - 1].Value),
                          DiscritizePosIntoCell(cubeSize, positions[posIndex].Value),
                          positions[posIndex].Time - positions[posIndex - 1].Time
                        );
                    }
                }
            }

            float max = Mathf.NegativeInfinity;
            float min = Mathf.Infinity;
            foreach (KeyValuePair<float, Dictionary<float, Dictionary<float, float>>> xy in temp)
            {
                foreach (KeyValuePair<float, Dictionary<float, float>> zCol in xy.Value)
                {
                    foreach (KeyValuePair<float, float> val in zCol.Value)
                    {
                        if (val.Value > max)
                        {
                            max = val.Value;
                        }
                        if (val.Value < min)
                        {
                            min = val.Value;
                        }
                    }
                }
            }

            List<Cell> cells = new List<Cell>();
            foreach (KeyValuePair<float, Dictionary<float, Dictionary<float, float>>> xy in temp)
            {
                foreach (KeyValuePair<float, Dictionary<float, float>> zCol in xy.Value)
                {
                    foreach (KeyValuePair<float, float> val in zCol.Value)
                    {
                        cells.Add(new Cell(new Vector3(xy.Key, zCol.Key, val.Key), val.Value));
                    }
                }
            }

            return new HeatmapData(cubeSize, cells.ToArray(), min, max);
        }

    }
}