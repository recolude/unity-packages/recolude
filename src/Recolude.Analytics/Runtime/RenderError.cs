﻿using UnityEngine.Networking;

namespace Recolude.Analytics
{
    public class RenderError
    {
        private readonly string message;

        public RenderError(string message)
        {
            this.message = message;
        }

        public string Message { get => message; }

        public static RenderError FailedWebRequest(UnityWebRequest request)
        {
            var code = request.responseCode;
            if (request.downloadHandler == null)
            {
                return new RenderError($"Failed Web Request: [{code}] - (No Download Handler)");
            }

            return new RenderError($"Failed Web Request: [{code}] - '{request.downloadHandler.text}'");
        }
    }

}