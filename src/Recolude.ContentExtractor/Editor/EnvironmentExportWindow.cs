﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


/*
{
    name: "main scene",
    imposters: [
        {
            name: "trees",
            transforms: [
                {
                    position: {},
                    rotation: {},
                    scale: {}
                }
            ]
        }
    ],
    clones: [
        {
            name: "rock",
            geometry: "url to recolude"
            transform: {}
        }
    ],
    terrain: [

    ],
    skybox: {

    }
}
*/

namespace Recolude.ContentExtractor.Environment.Editor
{

    public class EnvironmentExportWindow : EditorWindow
    {
        List<InstanceSelection> instanceSelectionsToExport;
        List<bool> instanceSelectionsToExportFoldout;

        [MenuItem("Window/Recolude/Export/Environment")]
        public static EnvironmentExportWindow Init()
        {
            EnvironmentExportWindow window = (EnvironmentExportWindow)GetWindow(typeof(EnvironmentExportWindow));
            window.Show();
            window.Repaint();
            if (window.instanceSelectionsToExport == null)
            {
                window.instanceSelectionsToExport = new List<InstanceSelection>();
                window.instanceSelectionsToExportFoldout = new List<bool>();
            }
            return window;
        }

        void OnEnable()
        {
            titleContent = new GUIContent("Environment Builder");
        }


        [MenuItem("GameObject/Recolude/Add To Scene Export", priority = 30)]
        private static void AddToExport()
        {
            if (Selection.gameObjects.Length == 0)
            {
                return;
            }

            string name = "New Instance Selection";

            if (Selection.gameObjects.Length == 1)
            {
                name = Selection.gameObjects[0].name;
            }
            else
            {
                bool allSameName = true;
                for (int i = 1; i < Selection.gameObjects.Length; i++)
                {
                    if (string.Equals(Selection.gameObjects[i - 1].name, Selection.gameObjects[i].name) == false)
                    {
                        allSameName = false;
                        break;
                    }
                }
                if (allSameName)
                {
                    name = Selection.gameObjects[0].name;
                }
            }

            var inst = Init();
            inst.instanceSelectionsToExport.Add(new InstanceSelection(name, Selection.gameObjects));
            inst.instanceSelectionsToExportFoldout.Add(false);

            // The reason we have to clear immediatly is because without this, 
            // this specific function Will get called once per selected object.
            // So if we have 30 selected objects, we will end up calling this 
            // function 30 times, adding a total of 1200 objects.
            Selection.objects = null;
        }

        [MenuItem("GameObject/Recolude/Add To Scene Export", true)]
        private static bool AddToExportValidation()
        {
            if (Selection.activeObject == null)
            {
                return false;
            }
            return Selection.activeObject.GetType() == typeof(GameObject);
        }

        void Reset()
        {
            instanceSelectionsToExport = new List<InstanceSelection>();
            instanceSelectionsToExportFoldout = new List<bool>();
        }

        Vector2 scrollPos;

        string environmentName;

        void OnGUI()
        {
            environmentName = EditorGUILayout.TextField("Environment Name", environmentName);

            scrollPos =
                EditorGUILayout.BeginScrollView(scrollPos);
            if (instanceSelectionsToExport == null)
            {
                instanceSelectionsToExport = new List<InstanceSelection>();
                instanceSelectionsToExportFoldout = new List<bool>();
            }

            for (int selectionIndex = instanceSelectionsToExport.Count - 1; selectionIndex >= 0; selectionIndex--)
            {
                instanceSelectionsToExportFoldout[selectionIndex] = EditorGUILayout.BeginFoldoutHeaderGroup(instanceSelectionsToExportFoldout[selectionIndex], instanceSelectionsToExport[selectionIndex].Name());
                EditorGUI.indentLevel++;

                if (instanceSelectionsToExportFoldout[selectionIndex])
                {
                    instanceSelectionsToExport[selectionIndex].SetName(EditorGUILayout.TextField("Name", instanceSelectionsToExport[selectionIndex].Name()));
                    for (int instanceIndex = instanceSelectionsToExport[selectionIndex].Instances().Count - 1; instanceIndex >= 0; instanceIndex--)
                    {
                        EditorGUILayout.BeginHorizontal();
                        instanceSelectionsToExport[selectionIndex].Instances()[instanceIndex] = EditorGUILayout.ObjectField(instanceSelectionsToExport[selectionIndex].Instances()[instanceIndex], typeof(GameObject), true) as GameObject;
                        if (GUILayout.Button("Remove"))
                        {
                            instanceSelectionsToExport[selectionIndex].Instances().RemoveAt(instanceIndex);
                        }
                        EditorGUILayout.EndHorizontal();
                    }

                    if (Selection.gameObjects.Length > 0 && GUILayout.Button("Add Selected GameObjects"))
                    {
                        instanceSelectionsToExport[selectionIndex].AddObjects(Selection.gameObjects);
                    }
                    EditorGUI.indentLevel--;
                }
                EditorGUILayout.EndFoldoutHeaderGroup();
            }

            EditorGUILayout.EndScrollView();

            if (GUILayout.Button("Add Instance Selection"))
            {
                instanceSelectionsToExport.Add(new InstanceSelection("New Instance Selection", new GameObject[0]));
                instanceSelectionsToExportFoldout.Add(true);
            }

            if (GUILayout.Button("Remove All Instance Selections"))
            {
                Reset();
            }

            if (GUILayout.Button("Export"))
            {
                var env = new Export(environmentName, instanceSelectionsToExport.ToArray());
                string path = EditorUtility.SaveFilePanel("Export Environment", "", environmentName + ".json", "json");
                if (string.IsNullOrWhiteSpace(path))
                {
                    return;
                }
                using (StreamWriter writer = File.CreateText(path))
                {
                    env.WriteJSON(writer);
                }
                EditorUtility.RevealInFinder(path);
            }

        }
    }

}