﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Recolude.ContentExtractor.Editor
{

    public class TerrainExportWindow : EditorWindow
    {

        [SerializeField]
        Terrain terrainToExport;

        [SerializeField]
        Vector2Int start;

        [SerializeField]
        Vector2Int end;

        [MenuItem("Window/Recolude/Export/Terrain")]
        public static TerrainExportWindow Init()
        {
            TerrainExportWindow window = (TerrainExportWindow)GetWindow(typeof(TerrainExportWindow));
            window.Show();
            window.Repaint();
            return window;
        }

        private static void ExportTerrainTextures(TerrainData terrainData)
        {
            int i = 0;
            foreach (var tex in terrainData.terrainLayers)
            {
                string outName = string.Format("{0} - {1}.png", i++, tex.name);
                File.WriteAllBytes(outName, MipmapTextureAndStripAlpha(MipmapTextureAndStripAlpha(tex.diffuseTexture)).EncodeToPNG());
            }
        }

        private static Texture2D MipmapTextureAndStripAlpha(Texture2D tex)
        {
            if (tex == null)
            {
                throw new System.ArgumentNullException(nameof(tex));
            }
            var outTex = new Texture2D(tex.width / 2, tex.height / 2);
            outTex.name = tex.name;
            for (int x = 0; x < outTex.width; x++)
            {
                for (int y = 0; y < outTex.height; y++)
                {
                    var a = tex.GetPixel(x * 2, y * 2);
                    var b = tex.GetPixel((x * 2) + 1, y * 2);
                    var c = tex.GetPixel(x * 2, (y * 2) + 1);
                    var d = tex.GetPixel((x * 2) + 1, (y * 2) + 1);
                    var avg = new Color(
                        (a.r + b.r + c.r + d.r) / 4f,
                        (a.g + b.g + c.g + d.g) / 4f,
                        (a.b + b.b + c.b + d.b) / 4f
                    );
                    outTex.SetPixel(x, y, avg);
                }
            }

            outTex.Apply();
            return outTex;
        }

        Texture2D GetHeightmap(Texture2D alphamapTex)
        {
            if (alphamapTex == null)
            {
                throw new System.ArgumentNullException(nameof(alphamapTex));
            }

            var outTex = new Texture2D(alphamapTex.width, alphamapTex.height);
            for (int x = 0; x < alphamapTex.width; x++)
            {
                for (int y = 0; y < alphamapTex.height; y++)
                {
                    var alphamapPixel = alphamapTex.GetPixel(x, y);
                    outTex.SetPixel(x, y, new Color(alphamapPixel.r, alphamapPixel.r, alphamapPixel.r));
                }
            }
            outTex.Apply();
            return outTex;
        }

        static Texture2D BuildSplatTexture(TerrainData terrainData, Vector2Int start, Vector2Int end)
        {
            Texture2D outTex = new Texture2D(end.x - start.x, end.y - start.y);
            float[,,] maps = terrainData.GetAlphamaps(0, 0, terrainData.alphamapWidth, terrainData.alphamapHeight);

            for (int x = start.x; x < end.x; x++)
            {
                for (int y = start.y; y < end.y; y++)
                {
                    float r = 0;
                    float g = 0;
                    float b = 0;
                    for (int texIndex = 0; texIndex < terrainData.terrainLayers.Length; texIndex++)
                    {
                        var pix = terrainData.terrainLayers[texIndex].diffuseTexture.GetPixel(0, 0);
                        r += pix.r * maps[x, y, texIndex];
                        g += pix.g * maps[x, y, texIndex];
                        b += pix.b * maps[x, y, texIndex];
                    }
                    outTex.SetPixel(x - start.x, y - start.y, new Color(r, g, b));

                }
            }
            outTex.Apply();
            return outTex;
        }

        static Texture2D CropTexture(TerrainData terrainData, Texture2D inTex, Vector2Int start, Vector2Int end)
        {
            Texture2D outTex = new Texture2D(end.x - start.x, end.y - start.y);

            for (int x = start.x; x < end.x; x++)
            {
                for (int y = start.y; y < end.y; y++)
                {
                    outTex.SetPixel(outTex.width - (x - start.x) - 1, y - start.y, inTex.GetPixel(y, x));
                }
            }
            outTex.Apply();
            return outTex;
        }

        static void WriteHeightmapToRaw(FileStream fs, Terrain terrain, Vector2Int start, Vector2Int end)
        {
            var td = terrain.terrainData;
            float[,] rawHeights = td.GetHeights(0, 0, td.heightmapResolution, td.heightmapResolution);
            using (var bw = new BinaryWriter(fs))
            {
                // Vector3Int middle = new Vector2Int();

                // Write Vector3 pos
                // TODO: Haven't tested this for terrain "centered" at 0,0,0
                // Because origin moves when we change dimensions of terrain, 
                // we have to do weird stuff.
                //      s + ((e-s)/2) - m
                bw.Write((td.heightmapScale.x * (((end.x - start.x) / 2.0f) + start.x)) + terrain.transform.position.x);
                bw.Write(terrain.transform.position.y);
                bw.Write((td.heightmapScale.z * (((end.y - start.y) / 2.0f) + start.y)) + terrain.transform.position.z);
                // bw.Write((td.heightmapScale.z * start.y));
                // bw.Write((td.heightmapScale.z * start.y));

                Debug.Log(terrain.transform.position.x + (td.size.x / 2f));
                Debug.Log(terrain.transform.position.z + (td.size.z / 2f));

                // Write height
                bw.Write(td.heightmapScale.y);
                bw.Write(td.heightmapScale.x * ((end.x - start.x)));
                bw.Write(td.heightmapScale.z * ((end.y - start.y)));

                // Write Img Dimensions
                bw.Write(end.x - start.x);
                bw.Write(end.y - start.y);

                for (int x = start.x; x < end.x; x++)
                {
                    for (int y = start.y; y < end.y; y++)
                    {
                        bw.Write((System.UInt16)(rawHeights[x, y] * 65535));
                    }
                }
            }
        }

        static Texture2D GetHeightmap(TerrainData terrainData, Vector2Int start, Vector2Int end)
        {
            Texture2D hm = new Texture2D(end.x - start.x, end.y - start.y);
            float[,] rawHeights = terrainData.GetHeights(0, 0, terrainData.heightmapResolution, terrainData.heightmapResolution);
            for (int x = start.x; x < end.x; x++)
            {
                for (int y = start.y; y < end.y; y++)
                {
                    hm.SetPixel(x - start.x, y - start.y, new Color(rawHeights[x, y], rawHeights[x, y], rawHeights[x, y]));
                }
            }
            hm.Apply();
            return hm;
        }

        [SerializeField]
        Texture2D sampleTerrain;

        void OnEnable()
        {
            SceneView.duringSceneGui += OnSceneGUI;
            Debug.Log("Enabled");
        }

        void OnDisable()
        {
            SceneView.duringSceneGui -= OnSceneGUI;
        }


        void OnSceneGUI(SceneView sceneView)
        {
            if (terrainToExport == null)
            {
                return;
            }
            var td = terrainToExport.terrainData;

            var bottomLeft = new Vector2(
                terrainToExport.transform.position.z + (td.heightmapScale.z * start.y),
                terrainToExport.transform.position.x + (td.heightmapScale.x * start.x)
            );

            var topRight = new Vector2(
               terrainToExport.transform.position.z + (td.heightmapScale.z * start.y) + (td.heightmapScale.z * (end.y - start.y)),
               terrainToExport.transform.position.x + (td.heightmapScale.x * start.x) + (td.heightmapScale.x * (end.x - start.x))
           );

            var bottomRight = new Vector2(
                terrainToExport.transform.position.z + (td.heightmapScale.z * start.y) + (td.heightmapScale.z * (end.y - start.y)),
                terrainToExport.transform.position.x + (td.heightmapScale.x * start.x)
            );

            var topLeft = new Vector2(
                terrainToExport.transform.position.z + (td.heightmapScale.z * start.y),
                terrainToExport.transform.position.x + (td.heightmapScale.x * start.x) + (td.heightmapScale.x * (end.x - start.x))
            );

            Handles.color = Color.gray;
            Handles.DrawLine(new Vector3(-250, 0, -250), new Vector3(-250, 500, -250));
            Handles.DrawLine(new Vector3(250, 0, 250), new Vector3(250, 500, 250));
            Handles.DrawLine(new Vector3(-250, 0, 250), new Vector3(-250, 500, 250));
            Handles.DrawLine(new Vector3(250, 0, -250), new Vector3(250, 500, -250));

            Handles.color = Color.yellow;
            Handles.DrawLine(new Vector3(bottomLeft.x, 0, bottomLeft.y), new Vector3(bottomLeft.x, 500, bottomLeft.y));
            Handles.color = Color.green;
            Handles.DrawLine(new Vector3(topRight.x, 0, topRight.y), new Vector3(topRight.x, 500, topRight.y));
            Handles.color = Color.red;
            Handles.DrawLine(new Vector3(bottomRight.x, 0, bottomRight.y), new Vector3(bottomRight.x, 500, bottomRight.y));
            Handles.color = Color.blue;
            Handles.DrawLine(new Vector3(topLeft.x, 0, topLeft.y), new Vector3(topLeft.x, 500, topLeft.y));
        }

        void OnGUI()
        {
            Terrain newTerrainToExport = EditorGUILayout.ObjectField(terrainToExport, typeof(Terrain), true) as Terrain;
            if (newTerrainToExport != terrainToExport)
            {
                terrainToExport = newTerrainToExport;
                start = Vector2Int.zero;
                // end = new Vector2Int((int)terrainToExport.terrainData.size.x, (int)terrainToExport.terrainData.size.z);
                end = new Vector2Int(terrainToExport.terrainData.heightmapResolution, terrainToExport.terrainData.heightmapResolution);
                sampleTerrain = GetHeightmap(terrainToExport.terrainData, start, end);
            }

            if (terrainToExport == null)
            {
                return;
            }

            var td = terrainToExport.terrainData;

            var newStart = EditorGUILayout.Vector2IntField("Start", start);
            // newStart.x = Mathf.Clamp(newStart.x, 0, (int)terrainToExport.terrainData.size.x);
            // newStart.y = Mathf.Clamp(newStart.y, 0, (int)terrainToExport.terrainData.size.z);
            newStart.x = Mathf.Clamp(newStart.x, 0, end.x);
            newStart.y = Mathf.Clamp(newStart.y, 0, end.y);
            if (newStart != start)
            {
                start = newStart;
                sampleTerrain = GetHeightmap(td, start, end);
            }

            var newEnd = EditorGUILayout.Vector2IntField("End", end);
            newEnd.x = Mathf.Clamp(newEnd.x, start.x, (int)td.heightmapResolution);
            newEnd.y = Mathf.Clamp(newEnd.y, start.y, (int)td.heightmapResolution);
            if (newEnd != end)
            {
                end = newEnd;
                sampleTerrain = GetHeightmap(td, start, end);
            }

            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Export Height Map"))
            {
                using (FileStream writer = File.Create("heightmap.raw"))
                {
                    WriteHeightmapToRaw(writer, terrainToExport, start, end);
                }
            }

            if (GUILayout.Button("Export Splat Map"))
            {
                var scaledStart = new Vector2Int(
                    Mathf.RoundToInt(((float)start.x / td.heightmapResolution) * td.alphamapWidth),
                    Mathf.RoundToInt(((float)start.y / td.heightmapResolution) * td.alphamapHeight)
                );
                var scaledEnd = new Vector2Int(
                    Mathf.RoundToInt(((float)end.x / td.heightmapResolution) * td.alphamapWidth),
                    Mathf.RoundToInt(((float)end.y / td.heightmapResolution) * td.alphamapHeight)
                );
                File.WriteAllBytes("splat.png", BuildSplatTexture(td, scaledStart, scaledEnd).EncodeToPNG());
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Export Alpha Maps"))
            {
                var scaledStart = new Vector2Int(
                    Mathf.RoundToInt(((float)start.x / td.heightmapResolution) * td.alphamapWidth),
                    Mathf.RoundToInt(((float)start.y / td.heightmapResolution) * td.alphamapHeight)
                );
                var scaledEnd = new Vector2Int(
                    Mathf.RoundToInt(((float)end.x / td.heightmapResolution) * td.alphamapWidth),
                    Mathf.RoundToInt(((float)end.y / td.heightmapResolution) * td.alphamapHeight)
                );
                int i = 0;
                foreach (var map in td.alphamapTextures)
                {
                    File.WriteAllBytes(string.Format("Alphamap {0}.png", i++), CropTexture(td, map, scaledStart, scaledEnd).EncodeToPNG());
                }
            }

            if (GUILayout.Button("Export Terrain Layers"))
            {
                ExportTerrainTextures(td);
            }
            EditorGUILayout.EndHorizontal();

            EditorGUI.DrawPreviewTexture(new Rect(25, 175, 400, 400), sampleTerrain);
        }
    }
}