using System.IO;

namespace Recolude.ContentExtractor.Environment
{

    public class Export
    {

        string name;

        InstanceSelection[] selections;

        public ExportIssue[] Issues()
        {
            return null;
        }

        public Export(string name, InstanceSelection[] selections)
        {
            this.name = name;
            this.selections = selections;
        }

        public string Name()
        {
            return name;
        }

        public void WriteJSON(StreamWriter writer)
        {
            writer.Write("{ \"name\": \"");
            writer.Write(name);
            writer.Write("\", \"instanceSelections\": [");

            for (int i = 0; i < selections.Length; i++)
            {
                var selection = selections[i];
                if (selection == null)
                {
                    continue;
                }
                selection.WriteJSON(writer);
                if (i < selections.Length - 1)
                {
                    writer.Write(", ");
                }
            }

            writer.Write("] }");
        }
    }
}

