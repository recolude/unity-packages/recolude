using System;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace Recolude.ContentExtractor.Environment
{

    public class InstanceSelection
    {

        string name;

        List<GameObject> instances;

        public InstanceSelection(string name, GameObject[] instances)
        {
            if (name == null)
            {
                throw new System.ArgumentNullException(nameof(name));
            }

            if (instances == null)
            {
                throw new System.ArgumentNullException(nameof(instances));
            }

            this.name = name;
            this.instances = new List<GameObject>(instances);
        }

        public List<GameObject> Instances()
        {
            return this.instances;
        }

        public string Name()
        {
            return name;
        }

        public void SetName(string v)
        {
            if (v == null)
            {
                throw new System.ArgumentNullException(v);
            }
            name = v;
        }

        private void RemoveNullEntries()
        {
            for (int i = instances.Count - 1; i >= 0; i--)
            {
                if (instances[i] == null)
                {
                    instances.RemoveAt(i);
                }
            }
        }

        public void WriteJSON(StreamWriter writer)
        {
            RemoveNullEntries();

            writer.Write("{ \"name\": \"");
            writer.Write(name);
            writer.Write("\", \"transforms\": [");

            for (int i = 0; i < instances.Count; i++)
            {
                var obj = instances[i];
                var pos = obj.transform.position;
                var rot = obj.transform.rotation.eulerAngles;
                var scale = obj.transform.localScale;

                writer.Write("{");
                writer.Write(string.Format("\"position\": {{ \"x\": {0}, \"y\": {1}, \"z\": {2} }}, ", pos.x, pos.y, pos.z));
                writer.Write(string.Format("\"rotation\": {{ \"x\": {0}, \"y\": {1}, \"z\": {2} }}, ", rot.x, rot.y, rot.z));
                writer.Write(string.Format("\"scale\": {{ \"x\": {0}, \"y\": {1}, \"z\": {2} }} ", scale.x, scale.y, scale.z));
                writer.Write("}");

                if (i < instances.Count - 1)
                {
                    writer.Write(", ");
                }
            }

            writer.Write("] }");
        }

        public void AddObjects(GameObject[] gameObjects)
        {
            instances.AddRange(gameObjects);
        }
    }

}