using UnityEngine;
using UnityEditor;


/// <summary>
/// All classes for extending the unity editor. You shouldn't care about this unless you want to change how Recolude.Core works.
/// </summary>
namespace Recolude.Core.Editor.Extension
{

    /// <summary>
    /// A Utility class for common functions for extending the editor.
    /// </summary>
    public static class EditorUtil
    {

        public static Rect CenterWindowPosition(int windowWidth, int windowHeight)
        {
            return new Rect(
                (Screen.currentResolution.width / 2) - (windowWidth / 2),
                (Screen.currentResolution.height / 2) - (windowHeight / 2),
                windowWidth,
                windowHeight
            );
        }

        public static void RenderMultipleRecordingsInfo(IRecording[] recordings)
        {
            if (recordings == null)
            {
                throw new System.Exception("Can't render null recordings!");
            }

            EditorGUILayout.LabelField("Number Of Recordings", recordings.Length.ToString());
            EditorGUILayout.LabelField("Total Duration", string.Format("{0:0.00} seconds", Duration(recordings)));
            EditorGUILayout.LabelField("Total Subjects", NumberSubjects(recordings).ToString());
            EditorGUILayout.LabelField("Total Custom Events", NumberCustomEvents(recordings).ToString());

        }

        public static void RenderSingleRecordingInfo(IRecording recording)
        {
            if (recording == null)
            {
                throw new System.Exception("Can't render a null recording!");
            }

            EditorGUILayout.LabelField("Name", recording.Name.ToString());
            EditorGUILayout.LabelField("Duration", string.Format("{0:0.00} seconds", Duration(recording)));
            EditorGUILayout.LabelField("Subjects", NumberSubjects(recording).ToString());
            EditorGUILayout.LabelField("Custom Events", NumberCustomEvents(recording).ToString());
        }

        public static int NumberCustomEvents(params IRecording[] recordings)
        {
            if (recordings == null)
            {
                return 0;
            }

            int sum = 0;
            foreach (var recording in recordings)
            {
                var customEvents = recording.GetCollection<Capture<CustomEvent>>();
                sum += customEvents == null ? 0 : customEvents.Captures.Length;
                sum += NumberCustomEvents(recording.Recordings);
            }
            return sum;
        }

        private static int NumberSubjects(params IRecording[] recordings)
        {
            if (recordings == null)
            {
                return 0;
            }

            int sum = 0;
            foreach (var recording in recordings)
            {
                sum += recording.Recordings == null ? 0 : recording.Recordings.Length;
            }
            return sum;
        }

        private static float Duration(params IRecording[] recordings)
        {
            if (recordings == null)
            {
                return 0;
            }

            float sum = 0;
            foreach (var recording in recordings)
            {
                sum += recording.Duration;
            }
            return sum;
        }

    }

}