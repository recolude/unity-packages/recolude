using UnityEngine;


#if UNITY_2020_2_OR_NEWER
using UnityEditor.AssetImporters;
#else
using UnityEditor.Experimental.AssetImporters;
#endif

using System.IO;
using Recolude.Core.IO;

namespace Recolude.Core.Editor.Extension.FileFormats
{
    [ScriptedImporterAttribute(1, "rap")]
    public class RapImporter : ScriptedImporter
    {

        [SerializeField]
        private bool importAnimations;

        void GenerateAnimationClips(IRecording recording, AssetImportContext ctx)
        {
            // // Assets must be assigned a unique identifier string consistent across imports
            ctx.AddObjectToAsset(recording.Name, RecordingAnimationClipUtil.RecordingToClip(recording, true));
            foreach (var child in recording.Recordings)
            {
                GenerateAnimationClips(child, ctx);
            }
        }

        public override void OnImportAsset(AssetImportContext ctx)
        {
            if (ctx == null)
            {
                return;
            }
            // // 'cube' is a a GameObject and will be automatically converted into a Prefab
            // // (Only the 'Main Asset' is eligible to become a Prefab.)
            // ctx.AddObjectToAsset("main obj", cube);
            // ctx.SetMainObject(cube);

            using (var assetData = File.OpenRead(ctx.assetPath))
            using (var reader = new RAPReader(assetData))
            {
                var recording = reader.Recording();
                // GenerateAnimationClips(recording, ctx);
                ctx.AddObjectToAsset(recording.Name, RecordingAsset.Build(recording));

                if (importAnimations)
                {
                    foreach (var child in recording.Recordings)
                    {
                        GenerateAnimationClips(child, ctx);
                    }
                }

                // ctx.SetMainObject(recording)
            }
        }
    }
}