
// using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEditor;

#if UNITY_2020_2_OR_NEWER
using UnityEditor.AssetImporters;
#else
using UnityEditor.Experimental.AssetImporters;
#endif


namespace Recolude.Core.Editor.Extension.FileFormats
{
    [CustomEditor(typeof(RapImporter))]
    public class RapImporterEditor : ScriptedImporterEditor
    {
        public override void OnInspectorGUI()
        {
            var importer = serializedObject.targetObject as RapImporter;
            var colorShift = new GUIContent("Color Shift");
            // EditorGUILayout.LabelField("Hello");
            EditorGUILayout.PropertyField(serializedObject.FindProperty("importAnimations"),
                    new GUIContent("Import Animations", "Whether or not to build animation clips from subjects"));
            // var prop = serializedObject.FindProperty("m_ColorShift");
            // EditorGUILayout.PropertyField(prop, colorShift);
            base.ApplyRevertGUI();
        }
    }
}