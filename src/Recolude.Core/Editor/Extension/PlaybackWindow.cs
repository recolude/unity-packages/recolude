﻿using UnityEngine;
using UnityEditor;

using System.Collections.Generic;

using Recolude.Core.Playback;
using Recolude.Core.Playback.ActorBuilderStrategies;

namespace Recolude.Core.Editor.Extension
{

    /// <summary>
    /// A window for manual playback of saved recordings. Access through Window/Recolude/Playback
    /// </summary>
    [InitializeOnLoad]
    public class PlaybackWindow : EditorWindow
    {
        class EditorActorBuilder : IActorBuilder
        {
            public Dictionary<string, GameObject> mapping;

            public EditorActorBuilder()
            {
                mapping = new Dictionary<string, GameObject>();
            }

            public IActor[] Build(IRecording rootRecording)
            {
                List<IActor> actors = new List<IActor>();

                foreach (var child in rootRecording.Recordings)
                {
                    if (mapping.ContainsKey(child.ID) && mapping[child.ID] != null)
                    {
                        actors.Add(new GameObjectActor(child, Instantiate(mapping[child.ID]), ActorCleanupMethod.DestroyImmediate));
                    }
                }

                return actors.ToArray();
            }
        }

        private EditorActorBuilder actorBuilder;

        private EditorActorBuilder ActorBuilderReference()
        {
            if (actorBuilder == null)
            {
                actorBuilder = new EditorActorBuilder();
            }
            return actorBuilder;
        }

        private IRecording loadedRecording = null;

        /// <summary>
        /// Keep for the InitializeOnLoad attribute
        /// </summary>
        static PlaybackWindow()
        {
            EditorApplication.playModeStateChanged += LogPlayModeState;
        }

        [MenuItem("Window/Recolude/Playback")]
        public static PlaybackWindow Init()
        {
            PlaybackWindow window = (PlaybackWindow)GetWindow(typeof(PlaybackWindow));
            window.Show();
            window.Repaint();

            return window;
        }

        private static void LogPlayModeState(PlayModeStateChange state)
        {
            if (playbackService != null)
            {
                DestroyImmediate(playbackService.gameObject);
            }
        }

        public void SetRecordingForPlayback(IRecording recording)
        {
            if (recording == null)
            {
                throw new System.ArgumentNullException(nameof(recording), "Can not set a null recording for playback");
            }
            if (playbackService != null)
            {
                playbackService.Stop();
                DestroyImmediate(playbackService.gameObject);
            }
            loadedRecording = recording;
            actorRepresentationsToUse = new GameObject[loadedRecording.Recordings.Length];
            Repaint();
        }

        public void Awake()
        {
            actorBuilder = new EditorActorBuilder();
        }

        void OnGUI()
        {
            recordingColor = new Color(1f, 1f, 1f, .5f);

            titleContent = new GUIContent("Playback");

            if (loadedRecording == null)
            {
                RecordingSelection(3, 3);
            }
            else
            {
                int currentHeight = 3;

                currentHeight += LoadedRecordingDetails(3, currentHeight);

                if (playbackService == null)
                {
                    currentHeight += RecordingPlaybackOptions(3, currentHeight);
                    currentHeight += ControlPlaybackOptions(3, currentHeight);
                    currentHeight += RecordingSelection(3, currentHeight);
                }
                else
                {
                    ControlPlaybackOptions(3, currentHeight);
                }
            }
        }

        private int RecordingSelection(int x, int y)
        {
            var last = loadedRecording;
            // loadedRecording = (RecordingScriptableObject)EditorGUI.ObjectField(new Rect(x, y, position.width - (x * 2), 17), "Recording To Playback", loadedRecording, typeof(RecordingScriptableObject), false);
            if (last != loadedRecording)
            {
                actorRepresentationsToUse = new GameObject[loadedRecording.Recordings.Length];
            }
            return 30;
        }

        void Update()
        {
            if (playbackService != null)
            {
                playbackService.UpdateState();
                Repaint();
            }
        }

        Color recordingColor = new Color(1f, 1f, 1f, 1f);

        static PlaybackBehavior playbackService = null;

        private int ControlPlaybackOptions(int x, int y)
        {
            Rect dimensions = new Rect(x, y, position.width - (x * 2), 110);

            GUI.BeginGroup(dimensions);

            if (playbackService == null)
            {
                if (GUI.Button(new Rect(0, 0, dimensions.width, 40), "Play"))
                {
                    IActorBuilder builder = null;
                    if (useCustomActors)
                    {
                        builder = ActorBuilderReference();
                    }
                    else
                    {
                        builder = new PrimitiveActorBuilder(PrimitiveType.Cube, ActorCleanupMethod.DestroyImmediate);
                    }
                    playbackService = PlaybackBehavior.Build(
                        loadedRecording,
                        builder,
                        loop,
                        new Time.RealTimeSinceStartupProvider()
                    );
                    playbackService.Play();
                }

                if (GUI.Button(new Rect(0, 50, dimensions.width, 40), "Play Backwards"))
                {
                    IActorBuilder builder = null;
                    if (useCustomActors)
                    {
                        builder = ActorBuilderReference();
                    }
                    else
                    {
                        builder = new PrimitiveActorBuilder(PrimitiveType.Cube, ActorCleanupMethod.DestroyImmediate);
                    }
                    playbackService = PlaybackBehavior.Build(loadedRecording, builder, loop, new Time.RealTimeSinceStartupProvider());
                    playbackService.PlayBackwards();
                }

                GUI.EndGroup();
                return 100;
            }

            if (GUI.Button(new Rect(dimensions.width / 2 + 5, 0, dimensions.width / 2 - 10, 30), "Stop"))
            {
                playbackService.Stop();
                DestroyImmediate(playbackService.gameObject);
            }

            if (playbackService.CurrentlyPlaying() && GUI.Button(new Rect(5, 0, dimensions.width / 2 - 10, 30), "Pause"))
            {
                playbackService.Pause();
            }

            if (playbackService.CurrentlyPaused() && GUI.Button(new Rect(5, 0, dimensions.width / 2 - 10, 30), "Resume"))
            {
                playbackService.Play();
            }

            playbackService.SetPlaybackSpeed(EditorGUI.Slider(new Rect(3, 35, dimensions.width - 6, 20), "Playback Speed", playbackService.GetPlaybackSpeed(), -8, 8));

            float curTime = playbackService.GetTimeThroughPlayback();
            float newTime = EditorGUI.Slider(new Rect(3, 55, dimensions.width - 6, 20), "Current Time", curTime, 0, loadedRecording.Duration);

            if (newTime != curTime)
            {
                playbackService.SetTimeThroughPlayback(newTime);
            }

            GUI.EndGroup();
            return 70;
        }

        private bool useCustomActors = false;

        private bool loop = true;

        GameObject[] actorRepresentationsToUse;

        Vector2 actorSetScrollPos;

        private int RecordingPlaybackOptions(int x, int y)
        {
            if (loadedRecording == null)
            {
                return 0;
            }

            int height = 60;
            if (useCustomActors)
            {
                height = (int)Mathf.Min((loadedRecording.Recordings.Length * 20) + height, position.height - y - 80);
            }

            Rect dimensions = new Rect(x, y, position.width - (x * 2), height);
            GUI.BeginGroup(dimensions);
            loop = EditorGUILayout.Toggle("Loop", loop);
            useCustomActors = EditorGUILayout.Toggle("Set Custom Actors", useCustomActors);

            int spaceTakenUp = 0;
            if (useCustomActors)
            {
                actorSetScrollPos = GUI.BeginScrollView(new Rect(0, 40, position.width - (x * 2), height - 50), actorSetScrollPos, new Rect(0, 0, position.width - (x * 10), 20 * loadedRecording.Recordings.Length));
                for (int actorIndex = 0; actorIndex < loadedRecording.Recordings.Length; actorIndex++)
                {
                    actorRepresentationsToUse[actorIndex] = (GameObject)EditorGUI.ObjectField(
                        new Rect(3, 20 * actorIndex, position.width - 30, 17),
                        loadedRecording.Recordings[actorIndex].Name,
                        actorRepresentationsToUse[actorIndex],
                        typeof(GameObject),
                        false
                    );

                    if (ActorBuilderReference().mapping.ContainsKey(loadedRecording.Recordings[actorIndex].ID))
                    {
                        ActorBuilderReference().mapping[loadedRecording.Recordings[actorIndex].ID] = actorRepresentationsToUse[actorIndex];
                    }
                    else
                    {
                        ActorBuilderReference().mapping.Add(loadedRecording.Recordings[actorIndex].ID, actorRepresentationsToUse[actorIndex]);
                    }
                    spaceTakenUp += 20;
                }
                GUI.EndScrollView();
            }

            GUI.EndGroup();
            return height;
        }

        void OnDestroy()
        {
            if (playbackService != null)
            {
                playbackService.Stop();
                DestroyImmediate(playbackService.gameObject);
            }
        }

        private int LoadedRecordingDetails(int x, int y)
        {
            if (loadedRecording == null)
            {
                return 0;
            }

            Rect dimensions = new Rect(x, y, position.width - (x * 2), 80);

            GUI.BeginGroup(dimensions);

            var titleStyle = new GUIStyle(GUI.skin.label)
            {
                alignment = TextAnchor.UpperCenter,
                fontStyle = FontStyle.Bold,
                fontSize = 18,
            };

            var centerStyle = new GUIStyle(GUI.skin.label)
            {
                alignment = TextAnchor.UpperCenter,
            };

            EditorGUI.DrawRect(new Rect(0, 0, dimensions.width, dimensions.height), recordingColor);
            EditorGUI.LabelField(new Rect(3, 0, dimensions.width - 6, 30), loadedRecording.Name, titleStyle);
            EditorGUI.LabelField(new Rect(3, 19, dimensions.width - 6, 30), loadedRecording.Duration.ToString("##.## 'seconds'"), centerStyle);
            EditorGUI.LabelField(new Rect(3, 53, dimensions.width - 6, 30), "Actors: " + loadedRecording.Recordings.Length.ToString());
            EditorGUI.LabelField(new Rect(3 + (dimensions.width / 2), 53, dimensions.width / 2 - 6, 30), "Events: " + EditorUtil.NumberCustomEvents(loadedRecording).ToString());

            GUI.EndGroup();
            return 90;
        }

    }

}