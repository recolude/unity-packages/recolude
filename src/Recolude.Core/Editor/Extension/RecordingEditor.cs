using UnityEngine;
using UnityEditor;

using System.Text;
using System.IO;

using Recolude.Core.IO;
using Recolude.Core.Util;

namespace Recolude.Core.Editor.Extension
{

    [CustomEditor(typeof(IRecording))]
    [CanEditMultipleObjects]
    public class RecordingEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            if (targets == null)
            {
                return;
            }

            if (targets.Length == 1)
            {
                RenderSingleRecordingView((IRecording)target);
            }
            else if (targets.Length > 1)
            {
                IRecording[] recordings = new IRecording[targets.Length];
                for (int i = 0; i < targets.Length; i++)
                {
                    recordings[i] = (IRecording)targets[i];
                }
                RenderMultipleRecordingsView(recordings);
            }
        }

        public void RenderSingleRecordingView(IRecording recording)
        {
            EditorUtil
                .RenderSingleRecordingInfo(recording);

            if (GUILayout.Button("View Playback"))
            {
                PlaybackWindow
                    .Init()
                    .SetRecordingForPlayback(recording);
            }

            if (GUILayout.Button("Export As JSON"))
            {
                string path = EditorUtility.SaveFilePanel("Export Recording As JSON", "", recording.Name + ".json", "json");
                System.IO.File.WriteAllText(path, JSON.ToJSON(recording));
                EditorUtility.RevealInFinder(path);
            }

            if (GUILayout.Button("Export As CSV"))
            {
                ExportCSVWindow.Init(recording);
            }

            if (GUILayout.Button("Export As RAP"))
            {
                string path = EditorUtility.SaveFilePanel("Export Recording As RAP", "", recording.Name + ".rap", "rap");
                using (FileStream fs = File.Create(path))
                {
                    using (var rapWriter = new RAPWriter(fs))
                    {
                        rapWriter.Write(recording);
                    }
                }
                EditorUtility.RevealInFinder(path);
            }

            if (GUILayout.Button("Export As Unity Animation Clip"))
            {
                ExportAnimationClipWindow.Init(recording);
            }

            if (recording.Metadata != null && recording.Metadata.Count > 0)
            {
                EditorGUILayout.LabelField("Metadata:", new GUIStyle(GUI.skin.label)
                {
                    fontStyle = FontStyle.Bold,
                });
                foreach (var keyvaluePair in recording.Metadata)
                {
                    EditorGUILayout.LabelField(keyvaluePair.Key, keyvaluePair.Value.ToString());
                }
            }
        }

        public void RenderMultipleRecordingsView(IRecording[] recordings)
        {
            EditorUtil
               .RenderMultipleRecordingsInfo(recordings);

            if (GUILayout.Button("Export All As JSON"))
            {
                string path = EditorUtility.SaveFilePanel("Export Recordings As JSON", "", "", "json");

                StringBuilder json = new StringBuilder("[");
                for (var i = 0; i < recordings.Length; i++)
                {
                    json.Append(JSON.ToJSON(recordings[i]));
                    if (i < recordings.Length - 1)
                    {
                        json.Append(",");
                    }
                }

                json.Append("]");

                System.IO.File.WriteAllText(path, json.ToString());
            }

            if (GUILayout.Button("Export All As CSV"))
            {
                ExportCSVWindow.Init(recordings);
            }

        }

    }

}