﻿using Recolude.Core.Util;
using UnityEditor;
using UnityEngine;

namespace Recolude.Core.Editor.Extension.Util
{
    [CustomPropertyDrawer(typeof(HorizontalLineAttribute))]
    public class HorizontalLineDecoratorDrawer : DecoratorDrawer
    {
        private HorizontalLineAttribute Line => (HorizontalLineAttribute)attribute;
        
        public override float GetHeight()
        {
            return EditorGUIUtility.singleLineHeight + Line.Height;
        }

        public override void OnGUI(Rect position)
        {
            Rect rect = EditorGUI.IndentedRect(position);
            rect.y += EditorGUIUtility.singleLineHeight / 3f;

            var line = Line;
            rect.height = line.Height;
            EditorGUI.DrawRect(rect, line.Color);
        }

        public static void DrawLine(Rect position, float height, Color color)
        {
            Rect rect = EditorGUI.IndentedRect(position);
            rect.y += EditorGUIUtility.singleLineHeight / 3f;
            rect.height = height;
            EditorGUI.DrawRect(rect, color);
        } 
    }
}