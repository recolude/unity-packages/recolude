﻿using Recolude.Core.Util;
using UnityEditor;
using UnityEngine;

namespace Recolude.Core.Editor.Extension.Util
{
    [CustomPropertyDrawer(typeof(OverrideName), true)]
    public class OverridableValuePropertyDrawer : PropertyDrawer
    {
        const string OverrideFieldName = "overrideValue";
        const string ValueFieldName = "value";

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var propertyAttribute = this.attribute as OverrideName;
            EditorGUI.BeginProperty(position, label, property);

            var shouldOverrideProperty = property.FindPropertyRelative(OverrideFieldName);

            var boolPos = position;
            boolPos.height = EditorGUIUtility.singleLineHeight;
            EditorGUI.PropertyField(boolPos, shouldOverrideProperty,
                new GUIContent($"{propertyAttribute.overrideKeyword} {propertyAttribute.label}"));

            if (shouldOverrideProperty.boolValue)
            {
                EditorGUI.indentLevel++;
                var valueProperty = property.FindPropertyRelative(ValueFieldName);
                var valuePosition = position;
                valuePosition.y += EditorGUIUtility.singleLineHeight;
                valuePosition.height = EditorGUIUtility.singleLineHeight;
                EditorGUI.PropertyField(valuePosition, valueProperty, new GUIContent(propertyAttribute.label));
                EditorGUI.indentLevel--;
            }

            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var shouldOverrideProperty = property.FindPropertyRelative(OverrideFieldName);
            if (shouldOverrideProperty.boolValue)
            {
                return (EditorGUIUtility.singleLineHeight * 2);
            }

            // return base.GetPropertyHeight(property, label);
            return EditorGUIUtility.singleLineHeight;
        }
    }
}