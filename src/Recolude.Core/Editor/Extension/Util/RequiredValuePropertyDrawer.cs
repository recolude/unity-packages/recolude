﻿using Recolude.Core.Util;
using UnityEditor;
using UnityEngine;

namespace Recolude.Core.Editor.Extension.Util
{
    [CustomPropertyDrawer(typeof(RequiredValue), useForChildren: true)]
    public class RequiredValuePropertyDrawer : PropertyDrawer
    {
        private const int MessagePadding = 10;

        private bool Null(SerializedProperty property)
        {
            return property.propertyType switch
            {
                SerializedPropertyType.ObjectReference => property.objectReferenceValue == null,
                SerializedPropertyType.String => string.IsNullOrEmpty(property.stringValue),
                _ => false
            };
        }


        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            var fieldPos = position;
            if (Null(property))
            {
                var boxPos = position;

                fieldPos.y += EditorGUIUtility.singleLineHeight + MessagePadding;
                fieldPos.height = EditorGUIUtility.singleLineHeight;

                boxPos.height = EditorGUIUtility.singleLineHeight + MessagePadding;

                EditorGUI.HelpBox(boxPos, $"{label.text} should be set to ensure component operates as desired",
                    MessageType.Error);
            }

            EditorGUI.PropertyField(fieldPos, property, label);


            EditorGUI.EndProperty();
        }


        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (Null(property))
            {
                return (EditorGUIUtility.singleLineHeight * 2) + MessagePadding;
            }

            return EditorGUIUtility.singleLineHeight;
        }
    }
}