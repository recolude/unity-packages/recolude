﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

namespace Recolude.Core.Editor
{

    public static class RecordingAnimationClipUtil
    {

        // /// <summary>
        // /// Saves the recording as an asset to the asset folder of the project.  If no name is provided and the recording's set name is blank, the file will be named "Unamed".
        // /// </summary>
        // /// <param name="recording">The recording we want to save to the assets folder.</param>
        // /// <param name="name">Name of the asset file.</param>
        // /// <param name="path">Where in the project for the asset to be saved.</param>
        // /// <remarks>Will append a number to the end of the name if another asset already uses the name passed in.</remarks>
        // static public void SaveToAssets(IRecording recording, string name, string path)
        // {
        //     if (string.IsNullOrEmpty(path))
        //     {
        //         path = "Assets";
        //     }

        //     var nameToUse = name;
        //     if (string.IsNullOrEmpty(nameToUse))
        //     {
        //         nameToUse = recording.Name;
        //     }

        //     string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(Path.Combine(path, string.Format("{0}.asset", string.IsNullOrEmpty(nameToUse) ? "Unamed" : nameToUse)));
        //     AssetDatabase.CreateAsset((RecordingScriptableObject)recording, assetPathAndName);
        //     // AssetDatabase.SaveAssets();
        // }

        // /// <summary>
        // /// Saves the recording as an asset to the root of the asset folder of the project. If no name is provided and the set name for the recording is blank, the file will be named "Unamed".
        // /// </summary>
        // /// <param name="recording">The recording we want to save to the assets folder.</param>
        // /// <param name="name">Name of the asset file.</param>
        // /// <remarks>Will append a number to the end of the name if another asset already uses the name passed in.</remarks>
        // static public void SaveToAssets(IRecording recording, string name)
        // {
        //     SaveToAssets(recording, name, "");
        // }

        // /// <summary>
        // /// Saves the recording as an asset to the root of the asset folder of the project. If no name is provided and the set name for the recording is blank, the file will be named "Unamed".
        // /// </summary>
        // /// <param name="recording">The recording we want to save to the assets folder.</param>
        // /// <remarks>Will append a number to the end of the name if another asset already uses the name passed in.</remarks>
        // static public void SaveToAssets(RecordingScriptableObject recording)
        // {
        //     SaveToAssets(recording, recording.Name, "");
        // }

        private static void ApplyPositionCollection(AnimationClip clip, bool smooth, IRecording recording)
        {
            var collection = recording.PositionCollection();
            if (collection == null)
            {
                return;
            }

            var capturedPositions = collection.Captures;
            if (capturedPositions.Length == 0)
            {
                return;
            }

            var startingPos = capturedPositions[0].Value;
            var endingPos = capturedPositions[capturedPositions.Length - 1].Value;

            AnimationCurve translateX = AnimationCurve.Linear(0, startingPos.x, recording.Duration, endingPos.x);
            AnimationCurve translateY = AnimationCurve.Linear(0, startingPos.y, recording.Duration, endingPos.y);
            AnimationCurve translateZ = AnimationCurve.Linear(0, startingPos.z, recording.Duration, endingPos.z);

            foreach (var positionCapture in capturedPositions)
            {
                var adjustedEndtime = positionCapture.Time - recording.StartTime;
                translateX.AddKey(adjustedEndtime, positionCapture.Value.x);
                translateY.AddKey(adjustedEndtime, positionCapture.Value.y);
                translateZ.AddKey(adjustedEndtime, positionCapture.Value.z);
            }

            if (smooth == false)
            {
                for (int i = 0; i < capturedPositions.Length; i++)
                {
                    AnimationUtility.SetKeyLeftTangentMode(translateX, i, AnimationUtility.TangentMode.Linear);
                    AnimationUtility.SetKeyLeftTangentMode(translateY, i, AnimationUtility.TangentMode.Linear);
                    AnimationUtility.SetKeyLeftTangentMode(translateZ, i, AnimationUtility.TangentMode.Linear);
                    AnimationUtility.SetKeyRightTangentMode(translateX, i, AnimationUtility.TangentMode.Linear);
                    AnimationUtility.SetKeyRightTangentMode(translateY, i, AnimationUtility.TangentMode.Linear);
                    AnimationUtility.SetKeyRightTangentMode(translateZ, i, AnimationUtility.TangentMode.Linear);
                }
            }

            clip.SetCurve("", typeof(Transform), "m_LocalPosition.x", translateX);
            clip.SetCurve("", typeof(Transform), "m_LocalPosition.y", translateY);
            clip.SetCurve("", typeof(Transform), "m_LocalPosition.z", translateZ);
        }

        private static void ApplyRotationCollection(AnimationClip clip, bool smooth, IRecording recording)
        {
            var collection = recording.RotationCollection();
            if (collection == null)
            {
                return;
            }

            var capturedRotations = collection.Captures;
            if (capturedRotations.Length == 0)
            {
                return;
            }

            var startingRot = Quaternion.Euler(capturedRotations[0].Value);
            var endingRot = Quaternion.Euler(capturedRotations[capturedRotations.Length - 1].Value);

            AnimationCurve rotateX = AnimationCurve.Linear(0, startingRot.x, recording.Duration, endingRot.x);
            AnimationCurve rotateY = AnimationCurve.Linear(0, startingRot.y, recording.Duration, endingRot.y);
            AnimationCurve rotateZ = AnimationCurve.Linear(0, startingRot.z, recording.Duration, endingRot.z);
            AnimationCurve rotateW = AnimationCurve.Linear(0, startingRot.w, recording.Duration, endingRot.w);

            foreach (var rotationCapture in capturedRotations)
            {
                var adjustedEndtime = rotationCapture.Time - recording.StartTime;
                var q = Quaternion.Euler(rotationCapture.Value);
                rotateX.AddKey(adjustedEndtime, q.x);
                rotateY.AddKey(adjustedEndtime, q.y);
                rotateZ.AddKey(adjustedEndtime, q.z);
                rotateW.AddKey(adjustedEndtime, q.w);
            }

            if (smooth == false)
            {
                for (int i = 0; i < capturedRotations.Length; i++)
                {
                    AnimationUtility.SetKeyLeftTangentMode(rotateX, i, AnimationUtility.TangentMode.Linear);
                    AnimationUtility.SetKeyLeftTangentMode(rotateY, i, AnimationUtility.TangentMode.Linear);
                    AnimationUtility.SetKeyLeftTangentMode(rotateZ, i, AnimationUtility.TangentMode.Linear);
                    AnimationUtility.SetKeyLeftTangentMode(rotateW, i, AnimationUtility.TangentMode.Linear);

                    AnimationUtility.SetKeyRightTangentMode(rotateX, i, AnimationUtility.TangentMode.Linear);
                    AnimationUtility.SetKeyRightTangentMode(rotateY, i, AnimationUtility.TangentMode.Linear);
                    AnimationUtility.SetKeyRightTangentMode(rotateZ, i, AnimationUtility.TangentMode.Linear);
                    AnimationUtility.SetKeyRightTangentMode(rotateW, i, AnimationUtility.TangentMode.Linear);
                }
            }

            clip.SetCurve("", typeof(Transform), "m_LocalRotation.x", rotateX);
            clip.SetCurve("", typeof(Transform), "m_LocalRotation.y", rotateY);
            clip.SetCurve("", typeof(Transform), "m_LocalRotation.z", rotateZ);
            clip.SetCurve("", typeof(Transform), "m_LocalRotation.w", rotateW);
            clip.EnsureQuaternionContinuity();
        }

        private static void ApplyCustomEventCollection(AnimationClip clip, IRecording recording)
        {
            var collection = recording.CustomEventCollection();
            if (collection == null)
            {
                return;
            }

            var capturedEvents = collection.Captures;
            if (capturedEvents.Length == 0)
            {
                return;
            }

            var animationEvents = new List<AnimationEvent>();
            for (int i = 0; i < capturedEvents.Length; i++)
            {
                // If it was a non-dictionary event
                if (capturedEvents[i].Value.Contents.Count == 1 && capturedEvents[i].Value.Contents.IsString("value"))
                {
                    animationEvents.Add(new AnimationEvent
                    {
                        time = capturedEvents[i].Time - recording.StartTime,
                        functionName = capturedEvents[i].Value.Name,
                        stringParameter = capturedEvents[i].Value.Contents.AsString("value"),
                    });
                }
                else
                {
                    // Create an event per value in dictionary
                    foreach (var keyVal in capturedEvents[i].Value.Contents)
                    {
                        animationEvents.Add(new AnimationEvent
                        {
                            time = capturedEvents[i].Time - recording.StartTime,
                            functionName = string.Format("{0}-{1}", capturedEvents[i].Value.Name, keyVal.Key),
                            stringParameter = keyVal.Value.ToString(),
                        });
                    }
                }
            }
            AnimationUtility.SetAnimationEvents(clip, animationEvents.ToArray());
        }

        public static AnimationClip RecordingToClip(IRecording recording, bool smooth)
        {
            var clip = new AnimationClip
            {
                name = recording.Name
            };

            ApplyPositionCollection(clip, smooth, recording);
            ApplyRotationCollection(clip, smooth, recording);
            ApplyCustomEventCollection(clip, recording);

            return clip;
        }

        /// <summary>
        /// Builds an animation clip per subject in the recording.
        /// 
        /// <b>NOTE:</b> This is an editor-only functionality due to the nature of <a href="https://docs.unity3d.com/ScriptReference/AnimationClip.SetCurve.html">AnimationClips.SetCurve</a>.
        /// </summary>
        /// <param name="recording">The recording we want to build animation clips from.</param>
        /// <param name="smooth">Whether or not there will be a smooth transition from one keyframe to the next. Will cause the actor to be in locations and rotations that where never recorded and will look akward in long standing positions and roations.</param>
        /// <returns>Animation Clips that represent actor movement</returns>
        public static AnimationClip[] GetAnimationClips(IRecording recording, bool smooth)
        {
            AnimationClip[] clips = new AnimationClip[recording.Recordings.Length];
            for (int subjectIndex = 0; subjectIndex < recording.Recordings.Length; subjectIndex++)
            {
                var subj = recording.Recordings[subjectIndex];
                clips[subjectIndex] = RecordingToClip(subj, smooth);
            }
            return clips;
        }

    }

}