﻿using System.Collections;
using Recolude.Core.Util;
using UnityEngine;

namespace Recolude.Core.AssetManagement
{
    public abstract class AssetCollection<T> : ScriptableObject, IEnumerable where T : Object
    {
        class AssetEnumerator : IEnumerator
        {
            private readonly AssetCollection<T> collection;

            int position;

            public AssetEnumerator(AssetCollection<T> collection)
            {
                this.collection = collection;
                position = -1;
            }

            public bool MoveNext()
            {
                position++;
                return (position < collection.assets.Length);
            }

            public void Reset()
            {
                position = -1;
            }

            public object Current => collection.assets[position];
        }

        [ExtendableScriptableObject] [SerializeField]
        protected T[] assets;

        public T Random()
        {
            return assets.Length == 0 ? null : assets[UnityEngine.Random.Range(0, assets.Length)];
        }

        public IEnumerator GetEnumerator()
        {
            return new AssetEnumerator(this);
        }
    }
}