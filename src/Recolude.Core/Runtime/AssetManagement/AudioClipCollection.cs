﻿using UnityEngine;

namespace Recolude.Core.AssetManagement
{
    [CreateAssetMenu(fileName = "Audio Clip Collection", menuName = "Recolude/Collections/Audio Clip", order = 1)]
    public class AudioClipCollection : AssetCollection<AudioClip>
    {
    }
}