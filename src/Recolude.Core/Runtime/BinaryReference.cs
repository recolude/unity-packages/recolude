using System;
using System.Collections.Generic;

namespace Recolude.Core
{
    public class BinaryReference
    {
        string name;

        string uri;

        ulong size;

        Metadata metadata;

        public BinaryReference(string name, string uri, ulong size, Metadata metadata)
        {
            this.name = name;
            this.uri = uri;
            this.size = size;
            this.metadata = metadata;
        }

        public string Name { get => name; }
        public string Uri { get => uri; }
        public ulong Size { get => size; }
        public Metadata Metadata { get => metadata; }

        public override bool Equals(object obj)
        {
            return obj is BinaryReference reference &&
                   name == reference.name &&
                   uri == reference.uri &&
                   size == reference.size &&
                   EqualityComparer<Metadata>.Default.Equals(metadata, reference.metadata);
        }

        public override int GetHashCode()
        {
            int hashCode = -1484443265;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(name);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(uri);
            hashCode = hashCode * -1521134295 + size.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<Metadata>.Default.GetHashCode(metadata);
            return hashCode;
        }
    }
}