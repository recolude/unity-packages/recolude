using UnityEngine;

namespace Recolude.Core
{

    /// <summary>
    /// Encompasses a single piece of data generated at a certian point in time during a 
    /// recording. Meant to be immutable.
    /// </summary>
    public abstract class Capture<T> : ICapture
    {
        [SerializeField]
        T value;

        [SerializeField]
        float time;

        public Capture(float time, T value)
        {
            this.time = time;
            this.value = value;
        }

        /// <summary>
        /// The time the event was captured in the recording.
        /// </summary>
        public float Time { get { return time; } }

        public T Value { get { return value; } }

        /// <summary>
        /// Builds a JSON string that represents the Capture.
        /// </summary>
        /// <returns>A JSON string.</returns>
        public abstract string ToJSON();

        /// <summary>
        /// Builds a string that represents a single row in a csv file that contains this object's data.
        /// </summary>
        /// <returns>A row of csv data as a string.</returns>
        public abstract string ToCSV();

        /// <summary>
        /// Builds a new capture object that has a modified capture time. The original capture object is left unchanged.
        /// </summary>
        /// <param name="newTime">The new time the capture occurred.</param>
        /// <returns>A new capture object with the capture time set to the value passed in.</returns>
        public abstract ICapture SetTime(float time);

        public abstract Capture<T> SetValue(T val);

    }

}