namespace Recolude.Core
{
    public abstract class CaptureCollection<T> : ICaptureCollection<T> where T : ICapture
    {
        string name;

        protected T[] captures;

        int lastIndex;

        public CaptureCollection(string name, T[] captures)
        {
            if (captures == null)
            {
                throw new System.ArgumentNullException("A collection can not have null captures array");
            }
            this.name = name;
            this.captures = captures;
            this.lastIndex = captures.Length > 0 ? captures.Length - 1 : -1;
        }

        public T this[int i]
        {
            get { return captures[i]; }
        }

        /// <summary>
        /// Shorthand for getting the time of the first capture in the 
        /// collection.
        /// </summary>
        public float StartTime => captures[0].Time;

        /// <summary>
        /// Shorthand for getting the time of the last capture in the 
        /// collection.
        /// </summary>
        public float EndTime => captures[lastIndex].Time;

        public float Duration => captures[lastIndex].Time - captures[0].Time;

        public int Length => captures.Length;

        public T[] Captures => captures;

        public string Name => name;

        public abstract string Signature { get; }
    }
}