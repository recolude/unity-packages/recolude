namespace Recolude.Core
{
    public static class CollectionNames
    {
        public const string Position = "Position";
        
        public const string Rotation = "Rotation";

        public const string Scale = "Scale";

        public const string CustomEvent = "Custom Event";

        public const string LifeCycle = "Life Cycle";
    }

}