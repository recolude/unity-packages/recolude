using UnityEngine;
using System.Collections.Generic;

namespace Recolude.Core
{
    public class CustomEvent
    {
        private readonly string name;

        private readonly Metadata content;

        public CustomEvent(string name, Metadata content)
        {
            this.name = name;
            this.content = content;
        }

        public CustomEvent(string name, Dictionary<string, string> content)
        {
            this.name = name;
            this.content = new Metadata(content);
        }
        
        public CustomEvent(string name)
        {
            this.name = name;
            content = new Metadata();
        }

        public string Name => name;

        public Metadata Contents => content;
    }
}