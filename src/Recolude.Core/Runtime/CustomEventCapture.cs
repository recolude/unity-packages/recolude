﻿using Recolude.Core.Util;
using System.Collections.Generic;
using System.Text;

namespace Recolude.Core
{
    /// <summary>
    /// An event that acts as a key value pair with an associated timestamp.
    /// </summary>
    [System.Serializable]
    public class CustomEventCapture : Capture<CustomEvent>
    {

        public CustomEventCapture(float time, CustomEvent eventData) : base(time, eventData)
        {
        }

        /// <summary>
        /// Create a new custom event capture. Initializes a dictionary with a single entry that is "value" => contents
        /// </summary>
        /// <param name="time">The time the event occurred in the recording.</param>
        /// <param name="name">The name of the event.</param>
        /// <param name="contents">The details of the event that occurred.</param>
        public CustomEventCapture(float time, string name, string contents) : this(time, new CustomEvent(name, new Dictionary<string, string> { { "value", contents } }))
        {
        }

        /// <summary>
        /// Create a new custom event capture.
        /// </summary>
        /// <param name="time">The time the event occurred in the recording.</param>
        /// <param name="name">The name of the event.</param>
        /// <param name="contents">The details of the event that occurred.</param>
        public CustomEventCapture(float time, string name, Dictionary<string, string> contentData) : this(time, new CustomEvent(name, contentData))
        {
        }
        
        
        public CustomEventCapture(float time, string name, Metadata metadata) : this(time, new CustomEvent(name, metadata))
        {
        }
        
        public CustomEventCapture(float time, string name) : this(time, new CustomEvent(name))
        {
        }

        /// <summary>
        /// The details of the event
        /// </summary>
        public Metadata Contents => Value.Contents;

        /// <summary>
        /// The name of the event
        /// </summary>
        public string Name => Value.Name;

        /// <summary>
        /// Creates a new CustomEventCapture with the same name and details but with a modified time, leaving the original event unchanged.
        /// </summary>
        /// <param name="newTime">The new time the event occurred in the recording.</param>
        /// <returns>A entirely new capture that occurred with the time passed in.</returns>
        public override ICapture SetTime(float newTime)
        {
            return new CustomEventCapture(newTime, Value);
        }
        
        public override Capture<CustomEvent> SetValue(CustomEvent val)
        {
            return new CustomEventCapture(Time, val);
        }

        /// <summary>
        /// Builds a JSON string that represents the CustomEventCapture object.
        /// </summary>
        /// <returns>A JSON string.</returns>
        public override string ToJSON()
        {
            StringBuilder builder = new StringBuilder("{");
            builder.AppendFormat(" \"Time\": {0}, \"Name\": \"{1}\", \"Contents\": [", Time, FormattingUtil.JavaScriptStringEncode(Value.Name));

            int entriesWritten = 0;
            foreach (var keyVal in Value.Contents)
            {
                builder.AppendFormat(
                    "{{\"Key\": \"{0}\", \"Value\": \"{1}\"}}",
                    FormattingUtil.JavaScriptStringEncode(keyVal.Key),
                    FormattingUtil.JavaScriptStringEncode(keyVal.Value.ToString())
                );

                if (entriesWritten < Value.Contents.Count - 1)
                {
                    builder.AppendFormat(", ");
                }

                entriesWritten++;
            }

            builder.Append("]}");
            return builder.ToString();
        }

        /// <summary>
        /// Builds a string that represents a single row in a csv file that contains this object's data.
        /// </summary>
        /// <returns>A row of csv data as a string.</returns>
        public override string ToCSV()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var keyVal in Value.Contents)
            {
                sb.AppendFormat(
                    "{0}, {1}, {2}\n",
                    Time,
                    FormattingUtil.StringToCSVCell(keyVal.Key),
                    FormattingUtil.StringToCSVCell(keyVal.Value.ToString())
                );
            }
            return sb.ToString();
        }

    }
}