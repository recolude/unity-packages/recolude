namespace Recolude.Core
{
    public class CustomEventCollection : CaptureCollection<Capture<CustomEvent>>
    {
        public CustomEventCollection(string name, Capture<CustomEvent>[] captures) : base(name, captures)
        {
        }

        public CustomEventCollection(Capture<CustomEvent>[] captures) : base(CollectionNames.CustomEvent, captures)
        {
        }

        public override string Signature => "recolude.event";
    }
}