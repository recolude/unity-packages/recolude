namespace Recolude.Core
{
    public class EnumCollection : CaptureCollection<Capture<int>>, IEnumCollection
    {
        string[] members;

        public EnumCollection(string name, string[] members, Capture<int>[] captures) : base(name, captures)
        {
            this.members = members;
        }

        public override string Signature => "recolude.enum";

        public string[] EnumMembers => members;
    }
}