using UnityEngine;

namespace Recolude.Core
{
    public class EulerRotationCollection : CaptureCollection<Capture<Vector3>>
    {

        public EulerRotationCollection(Capture<Vector3>[] captures) : base(CollectionNames.Rotation, captures)
        {
        }

        public EulerRotationCollection(string name, Capture<Vector3>[] captures) : base(name, captures)
        {
        }

        public override string Signature => "recolude.euler";
    }
}