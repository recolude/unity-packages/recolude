using System.IO;

namespace Recolude.Core
{
    public interface IBinary
    {
        /// <summary>
        /// Name to represent the binary
        /// </summary>
        /// <returns></returns>
        string Name();

        /// <summary>
        /// The actual binary data.
        /// </summary>
        /// <returns>A stream that can be read from to aquire the binary data.</returns>
        Stream Data();

        /// <summary>
        /// Length in bytes of the binary data.
        /// </summary>
        /// <returns>Size of the binary</returns>
        long Size();

        /// <summary>
        /// Metadata associated with the binary.
        /// </summary>
        /// <returns>Metadata associated with the binary.</returns>
        Metadata Metadata();
    }
}