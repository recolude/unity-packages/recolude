namespace Recolude.Core
{
    public interface ICapture
    {
        float Time { get; }

        ICapture SetTime(float time);

        string ToJSON();
    }
}