namespace Recolude.Core
{
    public interface ICaptureCollection<out T> where T : ICapture
    {
        string Name { get; }

        T[] Captures { get; }

        string Signature { get; }
    }
}