using UnityEngine;

namespace Recolude.Core
{

    public static class ICaptureExtensions
    {
        /// <summary>
        /// Figure out if the captured event occurs within the time slice provided. The range is inclusive [start, end]. If the range is provided backwards such that the start of the timeslice occurs after the end (start > end), then the values are automatically flipped and the function continues.
        /// </summary>
        /// <param name="start">The start of the timeslice.</param>
        /// <param name="end">The end of the timeslice.</param>
        /// <returns>Whether or not the event occurs within the timeslice.</returns>
        public static bool FallsWithin(this ICapture capture, float start, float end)
        {
            if (end < start)
            {
                return FallsWithin(capture, end, start);
            }
            return capture.Time >= start && capture.Time <= end;
        }

        /// <summary>
        /// Figure out if the captured event occurs within the time slice provided. The range is inclusive [timeSlice.x, timeSlice.y]. If the range is provided backwards such that the start of the timeslice occurs after the end (timeSlice.x > timeSlice.y), then the values are automatically flipped and the function continues.
        /// </summary>
        /// <param name="timeSlice">The timeslice where the x component is the start and the y component is the end.</param>
        /// <returns>Whether or not the event occurs within the timeslice</returns>
        public static bool FallsWithin(this ICapture capture, Vector2 timeSlice)
        {
            return capture.FallsWithin(timeSlice.x, timeSlice.y);
        }
    }

}