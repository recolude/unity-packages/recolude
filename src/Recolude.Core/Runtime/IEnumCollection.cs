namespace Recolude.Core
{
    public interface IEnumCollection : ICaptureCollection<Capture<int>>
    {
        string[] EnumMembers { get; }
    }

}