using System;
using System.IO;
using System.Text;

namespace Recolude.Core.IO
{
    public class BinaryUtilReader : IDisposable
    {
        Stream stream;

        BinaryReader reader;


        public BinaryUtilReader(Stream stream, bool leaveOpen)
        {
            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            this.stream = stream;
            reader = new BinaryReader(this.stream, Encoding.UTF8, leaveOpen);
        }

        public BinaryUtilReader(Stream stream) : this(stream, false)
        {
        }

        public BinaryUtilReader(byte[] data) : this(new MemoryStream(data), false)
        {
        }

        public void Dispose()
        {
            reader.Dispose();
        }

        public string[] StringArray()
        {
            var numStrings = UVarInt();
            var outStrings = new string[numStrings];

            for (int i = 0; i < numStrings; i++)
            {
                outStrings[i] = String();
            }

            return outStrings;
        }

        public string String()
        {
            var size = UVarInt();
            var stringData = reader.ReadBytes(size);
            return Encoding.UTF8.GetString(stringData, 0, stringData.Length);
        }

        public int UVarInt()
        {
            return RAPBinary.ReadUVarInt(reader.BaseStream);
        }

        public byte Byte()
        {
            return reader.ReadByte();
        }

        public int[] UVarIntArray()
        {
            int arraySize = UVarInt();
            int[] output = new int[arraySize];
            for (int i = 0; i < arraySize; i++)
            {
                output[i] = UVarInt();
            }

            return output;
        }

        public float Float16BST(float min, float max)
        {
            var buf = new byte[2]
            {
                reader.ReadByte(),
                reader.ReadByte()
            };
            return RAPBinary.FromFloat16BST(buf, min, max);
        }

        public byte[] BytesArray()
        {
            return reader.ReadBytes(UVarInt());
        }

        public byte[] BytesArray(int length)
        {
            return reader.ReadBytes(length);
        }

        public int Int32()
        {
            return reader.ReadInt32();
        }

        public float Float32()
        {
            return reader.ReadSingle();
        }

        public double Float64()
        {
            return reader.ReadDouble();
        }

        public long Int64()
        {
            return reader.ReadInt64();
        }
    }
}