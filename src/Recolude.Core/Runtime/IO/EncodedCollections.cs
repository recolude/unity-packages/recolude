
namespace Recolude.Core.IO
{

    /// <summary>
    /// All information association with encoded an array of capture 
    /// collections.
    /// </summary>
    public class EncodedCollections
    {
        byte[] header;

        byte[][] encodedCollectionsData;

        public EncodedCollections(byte[] header, byte[][] encodedCollectionsData)
        {
            this.header = header;
            this.encodedCollectionsData = encodedCollectionsData;
        }

        /// <summary>
        /// Summary data produced from encoding that specific array of capture 
        /// collections.
        /// </summary>
        /// <value>Serialized summary data.</value>
        public byte[] Header { get => header; set => header = value; }

        /// <summary>
        /// All serialized capture collection data.
        /// </summary>
        /// <value>All serialized capture collection data.</value>
        public byte[][] EncodedCollectionsData { get => encodedCollectionsData; set => encodedCollectionsData = value; }
    }

}