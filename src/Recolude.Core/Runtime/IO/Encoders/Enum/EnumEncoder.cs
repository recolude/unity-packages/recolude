using System;
using System.IO;
using System.Collections.Generic;

namespace Recolude.Core.IO.Encoders.Enum
{

    /// <summary>
    /// An encoder responsible for converting enum capture data to and from 
    /// binary.
    /// </summary>
    public class EnumEncoder : IEncoder<IEnumCollection>
    {
        readonly IEnumFactory[] enumFactories;

        public EnumEncoder(params IEnumFactory[] enumFactories)
        {
            this.enumFactories = enumFactories ?? Array.Empty<IEnumFactory>();
        }

        public EnumEncoder()
        {
            this.enumFactories = new IEnumFactory[] {
                new LifeCycleFactory()
            };
        }

        public bool Accepts(ICaptureCollection<ICapture> captureCollection)
        {
            return captureCollection.Signature == "recolude.enum";
        }

        public IEnumCollection Decode(string name, byte[] header, byte[] body, float[] times)
        {
            string[] allEnumMembers;
            using (var binaryHeadReader = new BinaryUtilReader(header))
            {
                allEnumMembers = binaryHeadReader.StringArray();
            }

            using (var rapReader = new BinaryUtilReader(body))
            {
                var enumMemberIndexes = rapReader.UVarIntArray();
                string[] enumMembers = new string[enumMemberIndexes.Length];

                for (int i = 0; i < enumMemberIndexes.Length; i++)
                {
                    enumMembers[i] = allEnumMembers[enumMemberIndexes[i]];
                }

                IntCapture[] enumCaptures = new IntCapture[times.Length];
                for (int i = 0; i < times.Length; i++)
                {
                    enumCaptures[i] = new IntCapture(times[i], rapReader.UVarInt());
                }

                foreach (var factory in enumFactories)
                {
                    if (factory.Matches(allEnumMembers))
                    {
                        return factory.Build(name, enumCaptures);
                    }
                }

                return new EnumCollection(name, enumMembers, enumCaptures);
            }
        }

        public EncodedCollections Encode(ICaptureCollection<ICapture>[] captureCollections)
        {
            var enumMemberMapping = new Dictionary<string, int>();
            var streamDataBuffers = new MemoryStream[captureCollections.Length];

            for (int bufferIndex = 0; bufferIndex < captureCollections.Length; bufferIndex++)
            {
                var stream = captureCollections[bufferIndex];
                var enmstr = stream as IEnumCollection;
                if (enmstr == null)
                {
                    throw new System.Exception("can not encode non-enum collection");
                }

                // Build mapping from enum member to index in header
                var indexMapping = new int[enmstr.EnumMembers.Length];
                for (int i = 0; i < enmstr.EnumMembers.Length; i++)
                {
                    var member = enmstr.EnumMembers[i];
                    if (enumMemberMapping.TryGetValue(member, out int val))
                    {
                        indexMapping[i] = val;
                    }
                    else
                    {
                        indexMapping[i] = enumMemberMapping.Count;
                        enumMemberMapping[member] = enumMemberMapping.Count;
                    }
                }

                streamDataBuffers[bufferIndex] = new MemoryStream();
                using (var rapWriter = new RAPWriter(streamDataBuffers[bufferIndex], false))
                {
                    rapWriter.WriteAsUVarInt(indexMapping);
                    foreach (var capture in enmstr.Captures)
                    {
                        rapWriter.WriteAsUVarInt(capture.Value);
                    }
                }
            }

            // Build header
            var headerMembers = new string[enumMemberMapping.Count];
            foreach (var item in enumMemberMapping)
            {
                headerMembers[item.Value] = item.Key;
            }

            var streamData = new byte[captureCollections.Length][];
            for (int i = 0; i < captureCollections.Length; i++)
            {
                streamData[i] = streamDataBuffers[i].ToArray();
            }

            using (var header = new MemoryStream())
            {
                using (var writer = new RAPWriter(header, false))
                {
                    writer.Write(headerMembers);
                }

                return new EncodedCollections(header.ToArray(), streamData);
            }
        }

        public string Signature()
        {
            return "recolude.enum";
        }

        public int Version()
        {
            return 0;
        }
    }
}