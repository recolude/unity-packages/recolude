namespace Recolude.Core.IO.Encoders.Enum
{
    /// <summary>
    /// Factory for identifying and producing more specific enums from a random
    /// collection of arbitrary enum data.
    /// </summary>
    public interface IEnumFactory
    {
        /// <summary>
        /// Determines whether or not this specific instance of the enum 
        /// factory should be used given the provided enum members
        /// </summary>
        /// <param name="enumMembers">Enum members to evaluate.</param>
        /// <returns>
        /// Whether or not this factory should be used.
        /// </returns>
        bool Matches(string[] enumMembers);

        /// <summary>
        /// Builds an enum collection from the provided data.
        /// </summary>
        /// <param name="name">Name of the enum collection.</param>
        /// <param name="captures">The captures of the enum collection.</param>
        /// <returns>An enum collection based on the data provided.</returns>
        IEnumCollection Build(string name, Capture<int>[] captures);

    }
}