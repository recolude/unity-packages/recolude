namespace Recolude.Core.IO.Encoders.Enum
{
    /// <summary>
    /// Responsible for identifying lifecycle collections from arbitrary enum 
    /// members, and reconstructing them from the provided data.
    /// </summary>
    public class LifeCycleFactory : IEnumFactory
    {
        public IEnumCollection Build(string name, Capture<int>[] captures)
        {
            var remapped = new Capture<UnityLifeCycleEvent>[captures.Length];
            for (int i = 0; i < captures.Length; i++)
            {
                remapped[i] = new UnityLifeCycleEventCapture(captures[i].Time, (UnityLifeCycleEvent)captures[i].Value);
            }
            return new LifeCycleCollection(name, remapped);
        }

        public bool Matches(string[] enumMembers)
        {
            if (enumMembers == null)
            {
                throw new System.ArgumentNullException(nameof(enumMembers));
            }

            if (enumMembers.Length != 4)
            {
                return false;
            }

            if (enumMembers[0] != "START")
            {
                return false;
            }

            if (enumMembers[1] != "ENABLE")
            {
                return false;
            }

            if (enumMembers[2] != "DISABLE")
            {
                return false;
            }

            if (enumMembers[3] != "DESTROY")
            {
                return false;
            }

            return true;
        }

    }

}