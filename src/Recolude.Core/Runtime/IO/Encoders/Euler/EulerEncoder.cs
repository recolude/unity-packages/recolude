using UnityEngine;
using System.IO;

namespace Recolude.Core.IO.Encoders.Euler
{
    public class EulerEncoder : IEncoder<ICaptureCollection<Capture<Vector3>>>
    {
        readonly StorageTechnique storageTechnique;

        public EulerEncoder()
        {
            this.storageTechnique = StorageTechnique.Raw16;
        }

        public EulerEncoder(StorageTechnique storageTechnique)
        {
            this.storageTechnique = storageTechnique;
        }

        float WrapEulerAngle(float angle)
        {
            return angle - (360 * Mathf.Floor(angle / 360));
        }


        public int Version()
        {
            return 0;
        }

        public string Signature()
        {
            return "recolude.euler";
        }

        private EulerRotationCollection DecodeRaw64(string name, BinaryUtilReader binaryReader, float[] times)
        {
            VectorCapture[] captures = new VectorCapture[times.Length];
            for (int i = 0; i < times.Length; i++)
            {
                captures[i] = new VectorCapture(
                    times[i],
                    new Vector3(
                        (float)binaryReader.Float64(),
                        (float)binaryReader.Float64(),
                        (float)binaryReader.Float64()
                    )
                );
            }

            return new EulerRotationCollection(name, captures);
        }

        private EulerRotationCollection DecodeRaw32(string name, BinaryUtilReader binaryReader, float[] times)
        {
            VectorCapture[] captures = new VectorCapture[times.Length];
            for (int i = 0; i < times.Length; i++)
            {
                captures[i] = new VectorCapture(
                    times[i],
                    new Vector3(
                        binaryReader.Float32(),
                        binaryReader.Float32(),
                        binaryReader.Float32()
                    )
                );
            }

            return new EulerRotationCollection(name, captures);
        }

        private EulerRotationCollection DecodeRaw16(string name, BinaryUtilReader binaryReader, float[] times)
        {
            var captures = new VectorCapture[times.Length];

            if (times.Length == 1)
            {
                captures[0] = new VectorCapture(
                    times[0],
                    new Vector3(
                        binaryReader.Float32(),
                        binaryReader.Float32(),
                        binaryReader.Float32()
                    )
                );
                return new EulerRotationCollection(name, captures);
            }

            for (int i = 0; i < times.Length; i++)
            {
                captures[i] = new VectorCapture(
                    times[i],
                    new Vector3(
                        binaryReader.Float16BST(0, 360),
                        binaryReader.Float16BST(0, 360),
                        binaryReader.Float16BST(0, 360)
                    )
                );
            }

            return new EulerRotationCollection(name, captures);
        }

        public ICaptureCollection<Capture<Vector3>> Decode(string name, byte[] header, byte[] body, float[] times)
        {
            using (var binaryReader = new BinaryUtilReader(body))
            {
                var typeByte = (StorageTechnique)binaryReader.Byte();
                switch (typeByte)
                {
                    case StorageTechnique.Raw64:
                        return DecodeRaw64(name, binaryReader, times);

                    case StorageTechnique.Raw32:
                        return DecodeRaw32(name, binaryReader, times);

                    case StorageTechnique.Raw16:
                        return DecodeRaw16(name, binaryReader, times);

                    default:
                        throw new System.Exception($"Unimplemented rotational encoding technique: {typeByte}");
                }
            }
        }

        public bool Accepts(ICaptureCollection<ICapture> captureCollection)
        {
            return captureCollection.Signature == "recolude.euler";
        }

        /*
        func encodeRaw16(captures []euler.Capture) []byte {
            buffer2Byes := make([]byte, 2)
            for _, capture := range captures {
                binaryutil.UnsignedFloatBSTToBytes(wrapEulerAngle(capture.EulerZXY().X()), 0, 360, buffer2Byes)
                streamData.Write(buffer2Byes)

                binaryutil.UnsignedFloatBSTToBytes(wrapEulerAngle(capture.EulerZXY().Y()), 0, 360, buffer2Byes)
                streamData.Write(buffer2Byes)

                binaryutil.UnsignedFloatBSTToBytes(wrapEulerAngle(capture.EulerZXY().Z()), 0, 360, buffer2Byes)
                streamData.Write(buffer2Byes)
            }
            return streamData.Bytes()
        }
        */

        public void EncodeRaw16(ICaptureCollection<Capture<Vector3>> captureCollection, BinaryWriter writer)
        {
            var captures = captureCollection.Captures;
            if (captures.Length == 0)
            {
                return;
            }

            if (captures.Length == 1)
            {
                writer.Write(captures[0].Value.x);
                writer.Write(captures[0].Value.y);
                writer.Write(captures[0].Value.z);
                return;
            }

            var buffer2Bytes = new byte[2];
            foreach (var capture in captures)
            {
                RAPBinary.ToFloat16BST(buffer2Bytes, 0, 360, WrapEulerAngle(capture.Value.x));
                writer.Write(buffer2Bytes);
                RAPBinary.ToFloat16BST(buffer2Bytes, 0, 360, WrapEulerAngle(capture.Value.y));
                writer.Write(buffer2Bytes);
                RAPBinary.ToFloat16BST(buffer2Bytes, 0, 360, WrapEulerAngle(capture.Value.z));
                writer.Write(buffer2Bytes);
            }
        }

        private void EncodeRaw64(ICaptureCollection<Capture<Vector3>> captureCollection, BinaryWriter writer)
        {
            foreach (var capture in captureCollection.Captures)
            {
                writer.Write((double)capture.Value.x);
                writer.Write((double)capture.Value.y);
                writer.Write((double)capture.Value.z);
            }
        }

        private void EncodeRaw32(ICaptureCollection<Capture<Vector3>> captureCollection, BinaryWriter writer)
        {
            foreach (var capture in captureCollection.Captures)
            {
                writer.Write(capture.Value.x);
                writer.Write(capture.Value.y);
                writer.Write(capture.Value.z);
            }
        }

        private byte[] Encode(ICaptureCollection<Capture<Vector3>> captureCollection)
        {
            if (captureCollection == null)
            {
                throw new System.ArgumentNullException(nameof(captureCollection));
            }

            using (var memoryBuffer = new MemoryStream())
            {
                using (var writer = new BinaryWriter(memoryBuffer))
                {
                    writer.Write((byte)storageTechnique);
                    switch (storageTechnique)
                    {
                        case StorageTechnique.Raw64:
                            EncodeRaw64(captureCollection, writer);
                            break;

                        case StorageTechnique.Raw32:
                            EncodeRaw32(captureCollection, writer);
                            break;

                        case StorageTechnique.Raw16:
                            EncodeRaw16(captureCollection, writer);
                            break;

                        default:
                            throw new System.Exception(
                                $"unimplemented position encoding technique: {storageTechnique}");
                    }
                }

                return memoryBuffer.ToArray();
            }
        }

        public EncodedCollections Encode(ICaptureCollection<ICapture>[] captureCollections)
        {
            var allCaptureData = new byte[captureCollections.Length][];
            for (int i = 0; i < captureCollections.Length; i++)
            {
                allCaptureData[i] = Encode(captureCollections[i] as ICaptureCollection<Capture<Vector3>>);
            }

            return new EncodedCollections(new byte[0], allCaptureData);
        }
    }
}