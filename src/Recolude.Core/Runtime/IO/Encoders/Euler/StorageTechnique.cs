﻿namespace Recolude.Core.IO.Encoders.Euler
{
    public enum StorageTechnique
    {
        /// <summary>
        /// Raw64 encodes all values at fullest precision, costing 192 bits 
        /// per capture.
        /// </summary>
        Raw64 = 0,

        /// <summary>
        /// Raw32 encodes all values at 32bit precision, costing 96 bits 
        /// per capture.
        /// </summary>
        Raw32 = 1,

        /// <summary>
        /// Raw16 stores all values at 16 bit precision, costiting 48 bits 
        /// per capture.
        /// </summary>
        Raw16 = 2
    }
}