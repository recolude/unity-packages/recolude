using System.IO;
using System.Collections.Generic;
using Recolude.Core.IO.PropertyWriters;

namespace Recolude.Core.IO.Encoders.Event
{
    public class EventEncoder : IEncoder<ICaptureCollection<Capture<CustomEvent>>>
    {
        public int Version()
        {
            return 0;
        }

        public string Signature()
        {
            return "recolude.event";
        }

        public ICaptureCollection<Capture<CustomEvent>> Decode(string name, byte[] header, byte[] body, float[] times)
        {
            string[] headerNames;
            string[] metadataKeyNames;

            using (var binaryHeadReader = new BinaryUtilReader(header))
            {
                headerNames = binaryHeadReader.StringArray();
                metadataKeyNames = binaryHeadReader.StringArray();
            }

            var captures = new CustomEventCapture[times.Length];

            using (var rapReader = new BinaryUtilReader(body))
            {
                for (int i = 0; i < times.Length; i++)
                {
                    var eventNameIndex = rapReader.UVarInt();
                    var metadataIndeces = rapReader.UVarIntArray();

                    var block = new Metadata();
                    for (int metadataIndex = 0; metadataIndex < metadataIndeces.Length; metadataIndex++)
                    {
                        block[metadataKeyNames[metadataIndeces[metadataIndex]]] = UnpackagerV2.ReadProperty(rapReader);
                    }
                    captures[i] = new CustomEventCapture(times[i], new CustomEvent(headerNames[eventNameIndex], block));
                }
            }

            return new CustomEventCollection(name, captures);
        }

        public bool Accepts(ICaptureCollection<ICapture> captureCollection)
        {
            return captureCollection.Signature == "recolude.event";
        }

        public EncodedCollections Encode(ICaptureCollection<ICapture>[] captureCollections)
        {
            var eventNamesSet = new Dictionary<string, int>();
            var eventKeysSet = new Dictionary<string, int>();
            var streamDataBuffers = new MemoryStream[captureCollections.Length];

            for (int bufferIndex = 0; bufferIndex < captureCollections.Length; bufferIndex++)
            {
                var stream = captureCollections[bufferIndex] as ICaptureCollection<Capture<CustomEvent>>;
                if (stream == null)
                {
                    throw new System.Exception("Can not encode collection that's not of type CustomEvent");
                }

                streamDataBuffers[bufferIndex] = new MemoryStream();
                using (var streamWriter = new RAPWriter(streamDataBuffers[bufferIndex]))
                {
                    foreach (var eventCapture in stream.Captures)
                    {
                        // Write Name Of Event
                        if (eventNamesSet.ContainsKey(eventCapture.Value.Name) == false)
                        {
                            eventNamesSet[eventCapture.Value.Name] = eventNamesSet.Count;
                        }
                        streamWriter.WriteAsUVarInt(eventNamesSet[eventCapture.Value.Name]);

                        var allKeyIndexes = new int[eventCapture.Value.Contents.Count];
                        var allValueDataBuffer = new MemoryStream();
                        using (allValueDataBuffer)
                        {
                            using (var allValueWriter = new BinaryWriter(allValueDataBuffer, new System.Text.UTF8Encoding(false, true), false))
                            {
                                var keyCount = 0;
                                foreach (var keyVal in eventCapture.Value.Contents)
                                {
                                    if (eventKeysSet.ContainsKey(keyVal.Key) == false)
                                    {
                                        eventKeysSet[keyVal.Key] = eventKeysSet.Count;
                                    }
                                    allKeyIndexes[keyCount] = eventKeysSet[keyVal.Key];
                                    PropertyWriterCollection.Instance.Write(allValueWriter, keyVal.Value);
                                    keyCount++;
                                }
                            }
                        }
                        streamWriter.WriteAsUVarInt(allKeyIndexes);
                        streamWriter.WriteWithoutSize(allValueDataBuffer.ToArray());
                    }
                }
            }

            var streamData = new byte[captureCollections.Length][];
            for (int i = 0; i < streamDataBuffers.Length; i++)
            {
                streamData[i] = streamDataBuffers[i].ToArray();
            }

            var allNames = new string[eventNamesSet.Count];
            foreach (var keyVal in eventNamesSet)
            {
                allNames[keyVal.Value] = keyVal.Key;
            }

            var allKeys = new string[eventKeysSet.Count];
            foreach (var keyVal in eventKeysSet)
            {
                allKeys[keyVal.Value] = keyVal.Key;
            }

            var header = new MemoryStream();
            using (var writer = new RAPWriter(header))
            {
                writer.Write(allNames);
                writer.Write(allKeys);
            }

            return new EncodedCollections(header.ToArray(), streamData);
        }
    }
}