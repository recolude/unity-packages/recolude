using UnityEngine;
using System.IO;
using System;

namespace Recolude.Core.IO.Encoders.Position
{
    public abstract class PositionEncoder<T> : IEncoder<ICaptureCollection<Capture<T>>>
    {
        private Vector3 F32(T val) => new Vector3(Xf32(val), Yf32(val), Zf32(val));

        private float Xf32(T val) => (float)Xf64(val);

        private float Yf32(T val) => (float)Yf64(val);

        private float Zf32(T val) => (float)Zf64(val);

        protected abstract double Xf64(T val);

        protected abstract double Yf64(T val);

        protected abstract double Zf64(T val);

        protected abstract CaptureCollection<Capture<T>> NewCollection(string name, Capture<T>[] captures);

        protected abstract Capture<T> NewCapture(float time, float x, float y, float z);

        protected abstract Capture<T> NewCapture(float time, double x, double y, double z);

        /// <summary>
        /// ORDER MATTERS: topBit bit shift 2 | rightBit bit shift 1 | forwardBit
        /// </summary>
        enum OctCell
        {
            TopRightForward = 0,
            TopRightBackward = 1,
            TopLeftForward = 2,
            TopLeftBackward = 3,
            BottomRightForward = 4,
            BottomRightBackward = 5,
            BottomLeftForward = 6,
            BottomLeftBackward = 7,
        }

        private readonly StorageTechnique storageTechnique;

        protected PositionEncoder(StorageTechnique storageTechnique)
        {
            this.storageTechnique = storageTechnique;
        }

        protected PositionEncoder() : this(StorageTechnique.Oct24)
        {
        }

        public int Version()
        {
            return 0;
        }

        public string Signature()
        {
            return "recolude.position";
        }

        Vector3 OctCellsToVec3(Vector3 min, Vector3 max, OctCell[] cells)
        {
            var center = (min + max) / 2.0f;
            var crossSection = max - min;
            var incrementX = crossSection.x / 4.0f;
            var incrementY = crossSection.y / 4.0f;
            var incrementZ = crossSection.z / 4.0f;


            for (var cellIndex = 0; cellIndex < cells.Length; cellIndex++)
            {
                var newY = center.y - incrementY;

                if (((int)cells[cellIndex] & 0b100) == 0)
                {
                    newY = center.y + incrementY;
                }

                var newX = center.x - incrementX;

                if (((int)cells[cellIndex] & 0b010) == 0)
                {
                    newX = center.x + incrementX;
                }

                var newZ = center.z - incrementZ;

                if (((int)cells[cellIndex] & 0b001) == 0)
                {
                    newZ = center.z + incrementZ;
                }

                center = new Vector3(newX, newY, newZ);

                incrementX /= 2.0f;
                incrementY /= 2.0f;
                incrementZ /= 2.0f;
            }

            return center;
        }

        void Vec3ToOctCells(Vector3 v, Vector3 min, Vector3 max, OctCell[] cells)
        {
            var center = (min + max) / 2.0f;
            var crossSection = max - min;
            var incrementX = crossSection.x / 4.0f;
            var incrementY = crossSection.y / 4.0f;
            var incrementZ = crossSection.z / 4.0f;

            for (var cellIndex = 0; cellIndex < cells.Length; cellIndex++)
            {
                var topBit = 0;
                var newY = center.y + incrementY;
                if (v.y < center.y)
                {
                    topBit = 1;
                    newY = center.y - incrementY;
                }

                var rightBit = 0;
                var newX = center.x + incrementX;
                if (v.x < center.x)
                {
                    rightBit = 1;
                    newX = center.x - incrementX;
                }

                var forwardBit = 0;
                var newZ = center.z + incrementZ;
                if (v.z < center.z)
                {
                    forwardBit = 1;
                    newZ = center.z - incrementZ;
                }

                cells[cellIndex] = (OctCell)(topBit << 2 | rightBit << 1 | forwardBit);

                center = new Vector3(newX, newY, newZ);
                incrementX /= 2.0f;
                incrementY /= 2.0f;
                incrementZ /= 2.0f;
            }
        }

        private CaptureCollection<Capture<T>> DecodeRaw64(string name, BinaryReader binaryReader, float[] times)
        {
            Capture<T>[] captures = new Capture<T>[times.Length];
            for (int i = 0; i < times.Length; i++)
            {
                captures[i] = NewCapture(
                    times[i],
                    binaryReader.ReadDouble(),
                    binaryReader.ReadDouble(),
                    binaryReader.ReadDouble()
                );
            }

            return NewCollection(name, captures);
        }

        private CaptureCollection<Capture<T>> DecodeRaw32(string name, BinaryReader binaryReader, float[] times)
        {
            Capture<T>[] captures = new Capture<T>[times.Length];
            for (int i = 0; i < times.Length; i++)
            {
                captures[i] = NewCapture(
                    times[i],
                    binaryReader.ReadSingle(),
                    binaryReader.ReadSingle(),
                    binaryReader.ReadSingle()
                );
            }

            return NewCollection(name, captures);
        }

        void BytesToOctCells24(OctCell[] cells, byte[] buffer)
        {
            // This method is better for compression
            for (var i = 0; i < 8; i++)
            {
                cells[i] = (OctCell)((((byte)buffer[0] >> i) & 0b1) | ((((byte)buffer[1] >> i) & 0b1) << 1) |
                                     ((((byte)buffer[2] >> i) & 0b1) << 2));
            }
        }

        void OctCellsToBytes24(OctCell[] cells, byte[] buffer)
        {
            buffer[0] = 0;
            buffer[1] = 0;
            buffer[2] = 0;

            // This method is better for compression
            for (var i = 0; i < 8; i++)
            {
                buffer[0] |= (byte)(((byte)cells[i] & 0b1) << i);
                buffer[2] |= (byte)((((byte)cells[i] & 0b100) >> 2) << i);
                buffer[1] |= (byte)((((byte)cells[i] & 0b10) >> 1) << i);
            }
        }

        void OctCellsToBytes48(OctCell[] cells, byte[] buffer)
        {
            buffer[0] = 0;
            buffer[1] = 0;
            buffer[2] = 0;
            buffer[3] = 0;
            buffer[4] = 0;
            buffer[5] = 0;

            // This method is better for compression
            for (var i = 0; i < 8; i++)
            {
                buffer[0] |= (byte)(((byte)cells[i] & 0b1) << i);
                buffer[1] |= (byte)((((byte)cells[i] & 0b10) >> 1) << i);
                buffer[2] |= (byte)((((byte)cells[i] & 0b100) >> 2) << i);
            }

            for (var i = 0; i < 8; i++)
            {
                buffer[3] |= (byte)(((byte)cells[i + 8] & 0b1) << i);
                buffer[4] |= (byte)((((byte)cells[i + 8] & 0b10) >> 1) << i);
                buffer[5] |= (byte)((((byte)cells[i + 8] & 0b100) >> 2) << i);
            }
        }

        void BytesToOctCells48(OctCell[] cells, byte[] buffer)
        {
            // This method is better for compression
            for (var i = 0; i < 8; i++)
            {
                cells[i] = (OctCell)(((buffer[0] >> i) & 0b1) | (((buffer[1] >> i) & 0b1) << 1) |
                                     (((buffer[2] >> i) & 0b1) << 2));
            }

            for (var i = 0; i < 8; i++)
            {
                cells[i + 8] = (OctCell)(((buffer[3] >> i) & 0b1) | (((buffer[4] >> i) & 0b1) << 1) |
                                         (((buffer[5] >> i) & 0b1) << 2));
            }
        }

        private ICaptureCollection<Capture<T>> DecodeOct24(string name, BinaryReader binaryReader, float[] times)
        {
            return DecodeOct(name, binaryReader, times, 8, 3, BytesToOctCells24);
        }

        private ICaptureCollection<Capture<T>> DecodeOct48(string name, BinaryReader binaryReader, float[] times)
        {
            return DecodeOct(name, binaryReader, times, 16, 6, BytesToOctCells48);
        }

        private ICaptureCollection<Capture<T>> DecodeOct(string name, BinaryReader binaryReader, float[] times,
            int size,
            int byteBufferSize, Action<OctCell[], byte[]> converter)
        {
            Capture<T>[] captures = new Capture<T>[times.Length];

            if (captures.Length == 0)
            {
                return NewCollection(name, captures);
            }

            if (captures.Length == 1)
            {
                captures[0] = NewCapture(
                    times[0],
                    binaryReader.ReadSingle(),
                    binaryReader.ReadSingle(),
                    binaryReader.ReadSingle()
                );
                return NewCollection(name, captures);
            }

            var min = new Vector3(
                binaryReader.ReadSingle(),
                binaryReader.ReadSingle(),
                binaryReader.ReadSingle()
            );

            var max = new Vector3(
                binaryReader.ReadSingle(),
                binaryReader.ReadSingle(),
                binaryReader.ReadSingle()
            );

            var starting = new Vector3(
                binaryReader.ReadSingle(),
                binaryReader.ReadSingle(),
                binaryReader.ReadSingle()
            );

            captures[0] = NewCapture(times[0], starting.x, starting.y, starting.z);

            var currentPosition = starting;
            var octCells = new OctCell[size];

            for (int i = 1; i < times.Length; i++)
            {
                converter(octCells, binaryReader.ReadBytes(byteBufferSize));
                currentPosition += OctCellsToVec3(min, max, octCells);
                captures[i] = NewCapture(times[i], currentPosition.x, currentPosition.y, currentPosition.z);
            }

            return NewCollection(name, captures);
        }

        public ICaptureCollection<Capture<T>> Decode(string name, byte[] header, byte[] body, float[] times)
        {
            using (MemoryStream memStream = new MemoryStream(body))
            using (var binaryReader = new BinaryReader(memStream))
            {
                var typeByte = (StorageTechnique)binaryReader.ReadByte();
                return typeByte switch
                {
                    StorageTechnique.Raw64 => DecodeRaw64(name, binaryReader, times),
                    StorageTechnique.Raw32 => DecodeRaw32(name, binaryReader, times),
                    StorageTechnique.Oct48 => DecodeOct48(name, binaryReader, times),
                    StorageTechnique.Oct24 => DecodeOct24(name, binaryReader, times),
                    _ => throw new Exception($"Unimplemented positional encoding technique: {typeByte}")
                };
            }
        }

        public bool Accepts(ICaptureCollection<ICapture> captureCollection)
        {
            return captureCollection.Signature == "recolude.position";
        }

        private void EncodeOct(
            ICaptureCollection<Capture<T>> captureCollection,
            BinaryWriter writer,
            int octBufferSize,
            int byteBufferSize,
            Action<OctCell[], byte[]> converter
        )
        {
            switch (captureCollection.Captures.Length)
            {
                case 0:
                    return;
                case 1:
                    writer.Write(Xf32(captureCollection.Captures[0].Value));
                    writer.Write(Yf32(captureCollection.Captures[0].Value));
                    writer.Write(Zf32(captureCollection.Captures[0].Value));
                    return;
            }

            var min = new Vector3(Mathf.Infinity, Mathf.Infinity, Mathf.Infinity);
            var max = new Vector3(Mathf.NegativeInfinity, Mathf.NegativeInfinity, Mathf.NegativeInfinity);
            for (int i = 1; i < captureCollection.Captures.Length; i++)
            {
                var dist = F32(captureCollection.Captures[i].Value) - F32(captureCollection.Captures[i - 1].Value);
                min.x = Mathf.Min(min.x, dist.x);
                min.y = Mathf.Min(min.y, dist.y);
                min.z = Mathf.Min(min.z, dist.z);

                max.x = Mathf.Max(max.x, dist.x);
                max.y = Mathf.Max(max.y, dist.y);
                max.z = Mathf.Max(max.z, dist.z);
            }

            writer.Write(min.x);
            writer.Write(min.y);
            writer.Write(min.z);
            writer.Write(max.x);
            writer.Write(max.y);
            writer.Write(max.z);

            writer.Write(Xf32(captureCollection.Captures[0].Value));
            writer.Write(Yf32(captureCollection.Captures[0].Value));
            writer.Write(Zf32(captureCollection.Captures[0].Value));

            var octBuffer = new OctCell[octBufferSize];
            var octByteBuffer = new byte[byteBufferSize];
            var quantizedPosition = F32(captureCollection.Captures[0].Value);
            for (int i = 1; i < captureCollection.Captures.Length; i++)
            {
                var dir = F32(captureCollection.Captures[i].Value) - quantizedPosition;
                Vec3ToOctCells(dir, min, max, octBuffer);
                converter(octBuffer, octByteBuffer);
                writer.Write(octByteBuffer);

                // Read back quantized position to fix drifting
                quantizedPosition += OctCellsToVec3(min, max, octBuffer);
            }
        }

        private void EncodeOct24(ICaptureCollection<Capture<T>> captureCollection, BinaryWriter writer)
        {
            EncodeOct(captureCollection, writer, 8, 3, OctCellsToBytes24);
        }

        private void EncodeOct48(ICaptureCollection<Capture<T>> captureCollection, BinaryWriter writer)
        {
            EncodeOct(captureCollection, writer, 16, 6, OctCellsToBytes48);
        }

        private void EncodeRaw64(ICaptureCollection<Capture<T>> captureCollection, BinaryWriter writer)
        {
            foreach (var capture in captureCollection.Captures)
            {
                writer.Write(Xf64(capture.Value));
                writer.Write(Yf64(capture.Value));
                writer.Write(Zf64(capture.Value));
            }
        }

        private void EncodeRaw32(ICaptureCollection<Capture<T>> captureCollection, BinaryWriter writer)
        {
            foreach (var capture in captureCollection.Captures)
            {
                writer.Write(Xf32(capture.Value));
                writer.Write(Yf32(capture.Value));
                writer.Write(Zf32(capture.Value));
            }
        }

        private byte[] Encode(ICaptureCollection<Capture<T>> captureCollection)
        {
            if (captureCollection == null)
            {
                throw new System.ArgumentNullException(nameof(captureCollection));
            }

            using (var memoryBuffer = new MemoryStream())
            {
                using (var writer = new BinaryWriter(memoryBuffer))
                {
                    writer.Write((byte)storageTechnique);
                    switch (storageTechnique)
                    {
                        case StorageTechnique.Raw64:
                            EncodeRaw64(captureCollection, writer);
                            break;

                        case StorageTechnique.Raw32:
                            EncodeRaw32(captureCollection, writer);
                            break;

                        case StorageTechnique.Oct24:
                            EncodeOct24(captureCollection, writer);
                            break;

                        case StorageTechnique.Oct48:
                            EncodeOct48(captureCollection, writer);
                            break;

                        default:
                            throw new System.Exception(
                                $"unimplemented position encoding technique: {storageTechnique}");
                    }
                }

                return memoryBuffer.ToArray();
            }
        }

        public EncodedCollections Encode(ICaptureCollection<ICapture>[] captureCollections)
        {
            var allCaptureData = new byte[captureCollections.Length][];
            for (var i = 0; i < captureCollections.Length; i++)
            {
                allCaptureData[i] = Encode(captureCollections[i] as ICaptureCollection<Capture<T>>);
            }

            return new EncodedCollections(Array.Empty<byte>(), allCaptureData);
        }
    }
}