﻿namespace Recolude.Core.IO.Encoders.Position
{
    public enum StorageTechnique
    {
        /// <summary>
        /// Raw64 encodes all values at fullest precision, costing 256 bits per
        /// capture
        /// </summary>
        Raw64 = 0,

        /// <summary>
        /// Raw32 encodes all values at 32bit precision, costing 128 bits per
        /// capture
        /// </summary>
        Raw32 = 1,

        /// <summary>
        /// Oct48 stores all values in a oct tree of depth 16, costing 64 bits per
        /// capture (time is stored in 16 bits)
        /// </summary>
        Oct48 = 2,

        /// <summary>
        /// Oct24 stores all values in a oct tree of depth 8, costing 40 bits per
        /// capture (time is stored in 16 bits)
        /// </summary>
        Oct24 = 3,
    }
}