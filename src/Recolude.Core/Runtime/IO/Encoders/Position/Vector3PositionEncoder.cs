﻿using UnityEngine;

namespace Recolude.Core.IO.Encoders.Position
{
    public class Vector3PositionEncoder : PositionEncoder<Vector3>
    {
        public Vector3PositionEncoder(StorageTechnique storageTechnique) : base(storageTechnique)
        {
        }

        public Vector3PositionEncoder() : base(StorageTechnique.Oct24)
        {
        }

        protected override CaptureCollection<Capture<Vector3>> NewCollection(string name, Capture<Vector3>[] captures)
        {
            return new PositionCollection(name, captures);
        }

        protected override Capture<Vector3> NewCapture(float time, float x, float y, float z)
        {
            return new VectorCapture(time, new Vector3(x, y, z));
        }
        
        protected override Capture<Vector3> NewCapture(float time, double x, double y, double z)
        {
            return new VectorCapture(time, new Vector3((float)x, (float)y, (float)z));
        }

        protected override double Xf64(Vector3 val)
        {
            return val.x;
        }

        protected override double Yf64(Vector3 val)
        {
            return val.y;
        }

        protected override double Zf64(Vector3 val)
        {
            return val.z;
        }
    }
}