namespace Recolude.Core.IO
{
    /// <summary>
    /// Encoders are used by RapWriters and RapReaders for serializing and 
    /// deserializing Recording data. Implement this interface to serialize
    /// custom data types. 
    /// </summary>
    /// <typeparam name="T">
    /// The capture collection this encoder is meant to serialize/deserialize.
    /// </typeparam>
    public interface IEncoder<out T>
    {
        /// <summary>
        /// When serializing a recording, this value gets written along 
        /// with the encoder's signature. When the recording is being 
        /// deserialized, the version found in the recording is compared 
        /// against the encoder registered to the RapReader. If the version
        /// found in the recording is greater than the registered encoder, an
        /// exception will get thrown.
        /// 
        /// The value must be non-negative.
        /// </summary>
        /// <returns>Version of encoder.</returns>
        int Version();

        /// <summary>
        /// When serializing a recording, this value get's written to denote 
        /// what capture collections this encoder is designed to handle. The 
        /// naming for a encoder's signature follows the scheme:
        ///     [company name].[capture collection name]
        /// 
        /// For example:
        ///     recolude.position
        ///     recolude.enum
        ///     recolude.euler
        ///     recolude.event
        /// </summary>
        /// <returns>The signature of the recording.</returns>
        string Signature();

        /// <summary>
        /// Evaluates whether or not the encoder can serialize the provided 
        /// capture collection.
        /// </summary>
        /// <param name="captureCollection">
        /// The collection to evaluate whether or not the encoder can handle it.
        /// </param>
        /// <returns>
        /// Whether or not the encoder can handle this specific collection type.
        /// </returns>
        bool Accepts(ICaptureCollection<ICapture> captureCollection);

        /// <summary>
        /// Produces a capture collection by deserializing the data provided.
        /// </summary>
        /// <param name="name">Name of the capture stream to create.</param>
        /// <param name="header">Header data for all capture collections.</param>
        /// <param name="body">
        /// The data specific to the collection to deserialize.
        /// </param>
        /// <param name="times">
        /// The time stamps for the captures in the collection.
        /// </param>
        /// <returns>The deserialized collection.</returns>
        T Decode(string name, byte[] header, byte[] body, float[] times);

        /// <summary>
        /// Encodes all collections provided.
        /// </summary>
        /// <param name="captureCollections">The collections to encode.</param>
        /// <returns>The encoded data.</returns>
        EncodedCollections Encode(ICaptureCollection<ICapture>[] captureCollections);
    }
}