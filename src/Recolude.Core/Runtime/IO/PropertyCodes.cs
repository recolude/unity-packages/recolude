﻿namespace Recolude.Core.IO
{
    public static class PropertyCodes
    {
        public const byte String = 0;
        public const byte Int32 = 1;
        public const byte Float32 = 2;
        public const byte BoolTrue = 3;
        public const byte BoolFalse = 4;
        public const byte Byte = 5;
        public const byte Vector2 = 6;
        public const byte Vector3 = 7;
        public const byte Metadata = 11;
        public const byte Time = 12;
        
        public const byte StringArray = 13;
        public const byte Int32Array = 14;
        public const byte Float32Array = 15;
        public const byte BoolArray = 16;
        public const byte ByteArray = 18;
        public const byte Vector2Array = 19;
        public const byte Vector3Array = 20;
        public const byte MetadataArray = 24;
        public const byte TimeArray = 25;
    }
}