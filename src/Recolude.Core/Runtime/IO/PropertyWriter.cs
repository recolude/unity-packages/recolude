﻿using System.IO;

namespace Recolude.Core.IO
{
    public interface IPropertyWriter<in T>
    {
        public byte Code(T property);

        public void Write(BinaryWriter writer, T property);

        public string AsJsonValue(T property);
    }

    public  abstract class PropertyWriter<T> : IPropertyWriter<T>
    {
        public abstract byte Code(T property);

        protected abstract void WriteProperty(BinaryWriter writer, T property);

        public void Write(BinaryWriter writer, T property)
        {
            writer.Write(Code(property));
            WriteProperty(writer, property);
        }

        public abstract string AsJsonValue(T property);
    }
}