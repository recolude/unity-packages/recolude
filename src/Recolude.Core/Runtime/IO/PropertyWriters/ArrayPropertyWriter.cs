﻿using System.IO;

namespace Recolude.Core.IO.PropertyWriters
{
    public abstract class ArrayPropertyWriter<T> : PropertyWriter<T[]>
    {
        protected override void WriteProperty(BinaryWriter writer, T[] property)
        {
            RAPBinary.WriteUVarInt(writer.BaseStream, property.Length);
            WriteArrayData(writer, property);
        }

        protected abstract void WriteArrayData(BinaryWriter writer, T[] property);
    }
}