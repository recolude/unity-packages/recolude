﻿using System.IO;

namespace Recolude.Core.IO.PropertyWriters
{
    public class BoolArrayPropertyWriter : ArrayPropertyWriter<bool>
    {
        public override byte Code(bool[] property)
        {
            return PropertyCodes.BoolArray;
        }

        public override string AsJsonValue(bool[] property)
        {
            return Util.AsJsonValue(property);
        }

        protected override void WriteArrayData(BinaryWriter writer, bool[] property)
        {
            foreach (var b in property)
            {
                writer.BaseStream.WriteByte((byte)(b ? 1 : 0));
            }
        }
    }
}