﻿using System.IO;

namespace Recolude.Core.IO.PropertyWriters
{
    public class BoolPropertyWriter : PropertyWriter<bool>
    {
        public override byte Code(bool property)
        {
            return property ? PropertyCodes.BoolTrue : PropertyCodes.BoolFalse;
        }

        protected override void WriteProperty(BinaryWriter writer, bool property)
        {
            // Left intentionally blank
        }

        public override string AsJsonValue(bool property)
        {
            return property ? "true" : "false";
        }
    }
}