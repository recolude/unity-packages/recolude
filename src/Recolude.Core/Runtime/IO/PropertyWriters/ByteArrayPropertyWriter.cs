﻿using System.IO;

namespace Recolude.Core.IO.PropertyWriters
{
    public class ByteArrayPropertyWriter: ArrayPropertyWriter<byte>
    {
        public override byte Code(byte[] property)
        {
            return PropertyCodes.ByteArray;
        }

        public override string AsJsonValue(byte[] property)
        {
            return Util.AsJsonValue(property);
        }

        protected override void WriteArrayData(BinaryWriter writer, byte[] property)
        {
            foreach (var b in property)
            {
                writer.BaseStream.WriteByte(b);
            }
        }
    }
}