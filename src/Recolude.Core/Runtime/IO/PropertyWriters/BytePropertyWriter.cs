﻿using System.IO;

namespace Recolude.Core.IO.PropertyWriters
{
    public class BytePropertyWriter: PropertyWriter<byte>
    {
        public override byte Code(byte property)
        {
            return PropertyCodes.Byte;
        }

        protected override void WriteProperty(BinaryWriter writer, byte property)
        {
            writer.Write(property);
        }

        public override string AsJsonValue(byte property)
        {
            return property.ToString();
        }
    }
}