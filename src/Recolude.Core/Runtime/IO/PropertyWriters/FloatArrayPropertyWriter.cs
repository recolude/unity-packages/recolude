﻿using System.IO;

namespace Recolude.Core.IO.PropertyWriters
{
    public class FloatArrayPropertyWriter : ArrayPropertyWriter<float>
    {
        public override byte Code(float[] property)
        {
            return PropertyCodes.Float32Array;
        }

        public override string AsJsonValue(float[] property)
        {
            return property.ToString();
        }

        protected override void WriteArrayData(BinaryWriter writer, float[] property)
        {
            foreach (var f in property)
            {
                writer.Write(f);
            }
        }
    }
}