﻿using System.IO;

namespace Recolude.Core.IO.PropertyWriters
{
    public class FloatPropertyWriter: PropertyWriter<float>
    {
        public override byte Code(float property)
        {
            return PropertyCodes.Float32;
        }

        protected override void WriteProperty(BinaryWriter writer, float property)
        {
            writer.Write(property);
        }
        
        public override string AsJsonValue(float property)
        {
            return property.ToString();
        }
    }
}