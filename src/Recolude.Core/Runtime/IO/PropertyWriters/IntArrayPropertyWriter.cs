﻿using System.IO;

namespace Recolude.Core.IO.PropertyWriters
{
    public class IntArrayPropertyWriter : ArrayPropertyWriter<int>
    {
        public override byte Code(int[] property)
        {
            return PropertyCodes.Int32Array;
        }

        public override string AsJsonValue(int[] property)
        {
            return Util.AsJsonValue(property);
        }

        protected override void WriteArrayData(BinaryWriter writer, int[] property)
        {
            foreach (var i in property)
            {
                writer.Write(i);
            }
        }
    }
}