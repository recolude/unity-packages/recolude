﻿using System;
using System.IO;

namespace Recolude.Core.IO.PropertyWriters
{
    public class IntPropertyWriter : PropertyWriter<Int32>
    {
        public override byte Code(int property)
        {
            return PropertyCodes.Int32;
        }

        protected override void WriteProperty(BinaryWriter writer, int property)
        {
            writer.Write(property);
        }

        public override string AsJsonValue(int property)
        {
            return property.ToString();
        }
    }
}