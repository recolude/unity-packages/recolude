﻿using System.IO;

namespace Recolude.Core.IO.PropertyWriters
{
    public class MetadataArrayPropertyWriter : ArrayPropertyWriter<Metadata>
    {
        public override byte Code(Metadata[] property)
        {
            return PropertyCodes.MetadataArray;
        }

        public override string AsJsonValue(Metadata[] property)
        {
            return Util.AsJsonValue(property, metadata => PropertyWriterCollection.Instance.JsonValue(metadata));
        }

        protected override void WriteArrayData(BinaryWriter writer, Metadata[] property)
        {
            foreach (var md in property)
            {
                md.Write(writer);
            }
        }
    }
}