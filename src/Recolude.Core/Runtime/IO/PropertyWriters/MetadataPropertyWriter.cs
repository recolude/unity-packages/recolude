﻿using System.IO;
using System.Text;
using Recolude.Core.Util;

namespace Recolude.Core.IO.PropertyWriters
{
    public class MetadataPropertyWriter : PropertyWriter<Metadata>
    {
        public override byte Code(Metadata property)
        {
            return PropertyCodes.Metadata;
        }

        protected override void WriteProperty(BinaryWriter writer, Metadata property)
        {
            property.Write(writer);
        }

        public override string AsJsonValue(Metadata property)
        {
            StringBuilder json = new StringBuilder("{");

            if (property != null)
            {
                foreach (var keypair in property)
                {
                    json.AppendFormat(
                        "\"{0}\": {1},",
                        FormattingUtil.JavaScriptStringEncode(keypair.Key),
                        PropertyWriterCollection.Instance.AsJsonValue(keypair.Value)
                    );
                }

                if (property.Count > 0)
                {
                    json.Remove(json.Length - 1, 1);
                }
            }

            json.Append("}");

            return json.ToString();
        }
    }
}