﻿using System.IO;
using System.Text;

namespace Recolude.Core.IO.PropertyWriters
{
    public class StringArrayPropertyWriter : ArrayPropertyWriter<string>
    {
        public override byte Code(string[] property)
        {
            return PropertyCodes.StringArray;
        }

        public override string AsJsonValue(string[] property)
        {
            return Util.AsJsonValue(property);
        }

        protected override void WriteArrayData(BinaryWriter writer, string[] property)
        {
            foreach (var str in property)
            {
                RAPBinary.WriteUVarInt(writer.BaseStream, str.Length);
                writer.Write(Encoding.UTF8.GetBytes(str));
            }
        }
    }
}