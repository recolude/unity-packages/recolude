﻿using System.IO;
using System.Text;
using Recolude.Core.Properties;

namespace Recolude.Core.IO.PropertyWriters
{
    public class StringPropertyWriter : PropertyWriter<string>
    {
        public override byte Code(string property)
        {
            return PropertyCodes.String;
        }

        protected override void WriteProperty(BinaryWriter writer, string property)
        {
            var bytes = Encoding.UTF8.GetBytes(property);
            RAPBinary.WriteUVarInt(writer.BaseStream, bytes.Length);
            writer.Write(bytes);
        }

        public override string AsJsonValue(string property)
        {
            return PropertyJsonFormatting.String(property);
        }
    }
}