﻿using System;
using System.IO;

namespace Recolude.Core.IO.PropertyWriters
{
    public class TimeArrayPropertyWriter : ArrayPropertyWriter<DateTime>
    {
        private static readonly DateTime epochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);


        public override byte Code(DateTime[] property)
        {
            return PropertyCodes.TimeArray;
        }

        public override string AsJsonValue(DateTime[] property)
        {
            return Util.AsJsonValue(property, Util.AsJsonValue);
        }

        protected override void WriteArrayData(BinaryWriter writer, DateTime[] property)
        {
            foreach (var v in property)
            {
                writer.Write((v - epochStart).Ticks / 10);
            }
        }
    }
}