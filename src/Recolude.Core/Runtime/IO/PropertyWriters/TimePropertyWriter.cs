﻿using System;
using System.IO;

namespace Recolude.Core.IO.PropertyWriters
{
    public class TimePropertyWriter : PropertyWriter<DateTime>
    {
        private static readonly DateTime epochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public override byte Code(DateTime property)
        {
            return PropertyCodes.Time;
        }

        protected override void WriteProperty(BinaryWriter writer, DateTime property)
        {
            writer.Write((property - epochStart).Ticks / 10);
        }

        public override string AsJsonValue(DateTime property)
        {
            return Util.AsJsonValue(property);
        }
    }
}