﻿using System;
using System.Text;
using UnityEngine;

namespace Recolude.Core.IO.PropertyWriters
{
    public static class Util
    {
        public static string AsJsonValue(Vector3 v)
        {
            return $"{{ \"x\": {v.x}, \"y\": {v.y}, \"z\": {v.z} }}";
        }

        public static string AsJsonValue(Vector2 v)
        {
            return $"{{ \"x\": {v.x}, \"y\": {v.y} }}";
        }

        
        public static string AsJsonValue(DateTime v)
        {
            return ((DateTimeOffset)v).ToUnixTimeSeconds().ToString();
        }

        public static string AsJsonValue<T>(T[] array)
        {
            var builder = new StringBuilder("[");
            for (int i = 0; i < array.Length; i++)
            {
                builder.Append(array[i]);
                if (i < array.Length - 1)
                {
                    builder.Append(",");
                }
            }
            builder.Append("]");
            return builder.ToString();
        }
        
        public static string AsJsonValue<T>(T[] array, Func<T, string> formatter)
        {
            var builder = new StringBuilder("[");
            for (int i = 0; i < array.Length; i++)
            {
                builder.Append(formatter.Invoke(array[i]));
                if (i < array.Length - 1)
                {
                    builder.Append(",");
                }
            }
            builder.Append("]");
            return builder.ToString();
        }
    }
}