﻿using System.IO;
using UnityEngine;

namespace Recolude.Core.IO.PropertyWriters
{
    public class Vector2ArrayPropertyWriter : ArrayPropertyWriter<Vector2>
    {
        public override byte Code(Vector2[] property)
        {
            return PropertyCodes.Vector2Array;
        }

        public override string AsJsonValue(Vector2[] property)
        {
            return Util.AsJsonValue(property, Util.AsJsonValue);
        }

        protected override void WriteArrayData(BinaryWriter writer, Vector2[] property)
        {
            foreach (var v in property)
            {
                writer.Write((double)v.x);
                writer.Write((double)v.y);
            }
        }
    }
}