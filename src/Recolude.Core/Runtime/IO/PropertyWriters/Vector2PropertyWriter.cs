﻿using System.IO;
using UnityEngine;

namespace Recolude.Core.IO.PropertyWriters
{
    public class Vector2PropertyWriter : PropertyWriter<Vector2>
    {
        public override byte Code(Vector2 property)
        {
            return PropertyCodes.Vector2;
        }

        protected override void WriteProperty(BinaryWriter writer, Vector2 property)
        {
            writer.Write((double)property.x);
            writer.Write((double)property.y);
        }

        public override string AsJsonValue(Vector2 property)
        {
            return Util.AsJsonValue(property);
        }
    }
}