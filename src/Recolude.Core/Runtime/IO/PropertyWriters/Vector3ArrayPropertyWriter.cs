﻿using System.IO;
using UnityEngine;

namespace Recolude.Core.IO.PropertyWriters
{
    public class Vector3ArrayPropertyWriter : ArrayPropertyWriter<Vector3>
    {
        public override byte Code(Vector3[] property)
        {
            return PropertyCodes.Vector3Array;
        }

        public override string AsJsonValue(Vector3[] property)
        {
            return Util.AsJsonValue(property, Util.AsJsonValue);
        }

        protected override void WriteArrayData(BinaryWriter writer, Vector3[] property)
        {
            foreach (var v in property)
            {
                writer.Write((double)v.x);
                writer.Write((double)v.y);
                writer.Write((double)v.z);
            }
        }
    }
}