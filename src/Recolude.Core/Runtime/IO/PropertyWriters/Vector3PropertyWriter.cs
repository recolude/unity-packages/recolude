﻿using System.IO;
using UnityEngine;

namespace Recolude.Core.IO.PropertyWriters
{
    public class Vector3PropertyWriter : PropertyWriter<Vector3>
    {
        public override byte Code(Vector3 property)
        {
            return PropertyCodes.Vector3;
        }

        protected override void WriteProperty(BinaryWriter writer, Vector3 property)
        {
            writer.Write((double)property.x);
            writer.Write((double)property.y);
            writer.Write((double)property.z);
        }

        public override string AsJsonValue(Vector3 property)
        {
            return Util.AsJsonValue(property);
        }
    }
}