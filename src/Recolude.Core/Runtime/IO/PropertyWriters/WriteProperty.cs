﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Recolude.Core.IO.PropertyWriters
{
    public class PropertyWriterCollection
    {
        private static PropertyWriterCollection instance;
        public static PropertyWriterCollection Instance => instance ??= new PropertyWriterCollection();

        private readonly Dictionary<Type, object> propertyWriters;

        private PropertyWriterCollection()
        {
            propertyWriters = new Dictionary<Type, object>();
            AddPropertyWriter(new StringPropertyWriter());
            AddPropertyWriter(new IntPropertyWriter());
            AddPropertyWriter(new FloatPropertyWriter());
            AddPropertyWriter(new BoolPropertyWriter());
            AddPropertyWriter(new BytePropertyWriter());
            AddPropertyWriter(new Vector2PropertyWriter());
            AddPropertyWriter(new Vector3PropertyWriter());
            AddPropertyWriter(new TimePropertyWriter());
            AddPropertyWriter(new MetadataPropertyWriter());

            AddPropertyWriter(new StringArrayPropertyWriter());
            AddPropertyWriter(new IntArrayPropertyWriter());
            AddPropertyWriter(new FloatArrayPropertyWriter());
            AddPropertyWriter(new BoolArrayPropertyWriter());
            AddPropertyWriter(new ByteArrayPropertyWriter());
            AddPropertyWriter(new Vector2ArrayPropertyWriter());
            AddPropertyWriter(new Vector3ArrayPropertyWriter());
            AddPropertyWriter(new TimeArrayPropertyWriter());
            AddPropertyWriter(new MetadataArrayPropertyWriter());
        }

        public void AddPropertyWriter<T>(IPropertyWriter<T> writer)
        {
            propertyWriters[typeof(T)] = writer;
        }

        public void WriteValue<T>(BinaryWriter writer, T val)
        {
            if (!(propertyWriters[typeof(T)] is IPropertyWriter<T> propWriter))
            {
                throw new InvalidOperationException($"no registered property writer for type {typeof(T)}");
            }

            propWriter.Write(writer, val);
        }

        public string JsonValue<T>(T val)
        {
            if (!(propertyWriters[typeof(T)] is IPropertyWriter<T> propWriter))
            {
                throw new InvalidOperationException($"no registered property writer for type {typeof(T)}");
            }

            return propWriter.AsJsonValue(val);
        }

        public void Write<T>(BinaryWriter writer, IProperty<T> prop)
        {
            WriteValue(writer, prop.Value);
        }

        public void Write<T>(BinaryWriter writer, T val) where T : IProperty
        {
            switch (val)
            {
                case IProperty<string> p:
                    WriteValue(writer, p.Value);
                    break;
                case IProperty<int> p:
                    WriteValue(writer, p.Value);
                    break;
                case IProperty<float> p:
                    WriteValue(writer, p.Value);
                    break;
                case IProperty<byte> p:
                    WriteValue(writer, p.Value);
                    break;
                case IProperty<bool> p:
                    WriteValue(writer, p.Value);
                    break;
                case IProperty<Vector2> p:
                    WriteValue(writer, p.Value);
                    break;
                case IProperty<Vector3> p:
                    WriteValue(writer, p.Value);
                    break;
                case IProperty<Metadata> p:
                    WriteValue(writer, p.Value);
                    break;
                case IProperty<DateTime> p:
                    WriteValue(writer, p.Value);
                    break;

                case IProperty<string[]> p:
                    WriteValue(writer, p.Value);
                    break;
                case IProperty<int[]> p:
                    WriteValue(writer, p.Value);
                    break;
                case IProperty<float[]> p:
                    WriteValue(writer, p.Value);
                    break;
                case IProperty<byte[]> p:
                    WriteValue(writer, p.Value);
                    break;
                case IProperty<bool[]> p:
                    WriteValue(writer, p.Value);
                    break;
                case IProperty<Vector2[]> p:
                    WriteValue(writer, p.Value);
                    break;
                case IProperty<Vector3[]> p:
                    WriteValue(writer, p.Value);
                    break;
                case IProperty<Metadata[]> p:
                    WriteValue(writer, p.Value);
                    break;
                case IProperty<DateTime[]> p:
                    WriteValue(writer, p.Value);
                    break;
            }
        }

        public string AsJsonValue<T>(T val) where T : IProperty
        {
            switch (val)
            {
                case IProperty<string> t1: return JsonValue(t1.Value);
                case IProperty<int> t1: return JsonValue(t1.Value);
                case IProperty<float> t1: return JsonValue(t1.Value);
                case IProperty<byte> t1: return JsonValue(t1.Value);
                case IProperty<bool> t1: return JsonValue(t1.Value);
                case IProperty<Vector2> t1: return JsonValue(t1.Value);
                case IProperty<Vector3> t1: return JsonValue(t1.Value);
                case IProperty<DateTime> t1: return JsonValue(t1.Value);
                case IProperty<Metadata> t1: return JsonValue(t1.Value);

                case IProperty<string[]> t1: return JsonValue(t1.Value);
                case IProperty<int[]> t1: return JsonValue(t1.Value);
                case IProperty<float[]> t1: return JsonValue(t1.Value);
                case IProperty<byte[]> t1: return JsonValue(t1.Value);
                case IProperty<bool[]> t1: return JsonValue(t1.Value);
                case IProperty<Vector2[]> t1: return JsonValue(t1.Value);
                case IProperty<Vector3[]> t1: return JsonValue(t1.Value);
                case IProperty<DateTime[]> t1: return JsonValue(t1.Value);
                case IProperty<Metadata[]> t1: return JsonValue(t1.Value);
            }


            throw new InvalidOperationException($"No json encoder registered for type: {typeof(T)}");
        }
    }
}