using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;

using Recolude.Core.IO.Encoders.Enum;
using Recolude.Core.IO.Encoders.Euler;
using Recolude.Core.IO.Encoders.Event;
using Recolude.Core.IO.Encoders.Position;

namespace Recolude.Core.IO
{
    public class RAPReader : IDisposable
    {
        private readonly Stream stream;

        private readonly bool leaveOpen;

        private readonly Dictionary<string, IEncoder<ICaptureCollection<ICapture>>> registeredEncoders;

        public RAPReader(Stream stream, IEncoder<ICaptureCollection<ICapture>>[] encoders, bool leaveOpen)
        {
            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            if (encoders == null)
            {
                throw new ArgumentNullException(nameof(encoders));
            }

            this.leaveOpen = leaveOpen;
            this.stream = stream;

            registeredEncoders = new Dictionary<string, IEncoder<ICaptureCollection<ICapture>>>();
            foreach (var encoder in encoders)
            {
                registeredEncoders[encoder.Signature()] = encoder;
            }
        }

        public RAPReader(Stream stream) : this(
            stream,
            new IEncoder<ICaptureCollection<ICapture>>[] {
                new Vector3PositionEncoder(),
                new EulerEncoder(),
                new EventEncoder(),
                new EnumEncoder(),
            },
            false
        )
        { }

        public RAPReader(Stream stream, bool leaveOpen) : this(
            stream,
            new IEncoder<ICaptureCollection<ICapture>>[] {
                new Vector3PositionEncoder(),
                new EulerEncoder(),
                new EventEncoder(),
                new EnumEncoder(),
            },
            leaveOpen
        )
        { }

        /// <summary>
        /// Reads from a stream of data and build an recording that can be used for playback.
        /// </summary>
        /// <param name="stream">The stream to be read from for building Recording objects.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Thrown when the stream is null or empty.</exception>
        /// <exception cref="System.SystemException">Thrown when the stream contains data in an unexpected format.</exception>
        public IRecording Recording()
        {
            if (stream == null || stream.Length == 0)
            {
                throw new ArgumentException("Nothing to unpackage, stream is empty");
            }

            int fileVersion = stream.ReadByte();
            switch (fileVersion)
            {
                case -1:
                    throw new SystemException("End of stream prematurely! Not enough data to build recordings from");

                case 1:
                    #if RAPV1
                    return UnpackagerV1.UnpackageV1(stream);
                    #else
                    throw new System.Exception("Recording file format saved as version 1, but RAPV1 is not a found as a compiler directive. Please add RAPV1 as a compiler directive and make sure protobuf exists within the project.");
                    #endif
                case 2:
                    return UnpackageV2(stream);

                default:
                    throw new SystemException($"Unsupported File Version: {fileVersion}");
            }
        }

        private Recording UnpackageV2(Stream stream)
        {
            bool compressed = false;
            IEncoder<ICaptureCollection<ICapture>>[] encoders = null;
            using (var reader = new BinaryUtilReader(stream, true))
            {
                encoders = ReadEncoders(reader);
                var compressionByte = reader.Byte();
                switch (compressionByte)
                {
                    case 0:
                        compressed = false;
                        break;
                    case 1:
                        compressed = true;
                        break;
                    default:
                        throw new System.Exception(string.Format("unrecognized compression byte: {0}", compressionByte));
                }
            }

            using (var resetOfStream = compressed ? new DeflateStream(stream, CompressionMode.Decompress, true) : stream)
            {
                using (var binReader = new BinaryUtilReader(resetOfStream, true))
                {
                    var encoderHeaders = new byte[encoders.Length][];
                    for (int i = 0; i < encoders.Length; i++)
                    {
                        encoderHeaders[i] = binReader.BytesArray();
                    }
                    var metadataKeys = binReader.StringArray();
                    return UnpackagerV2.RecursiveBuildRecordingsV2(binReader, metadataKeys, encoders, encoderHeaders);
                }
            }
        }

        private IEncoder<ICaptureCollection<ICapture>>[] ReadEncoders(BinaryUtilReader binaryReader)
        {
            var encoderSignatureData = binaryReader.StringArray();
            var encoders = new IEncoder<ICaptureCollection<ICapture>>[encoderSignatureData.Length];
            for (var i = 0; i < encoderSignatureData.Length; i++)
            {
                var encoderVersion = binaryReader.UVarInt();

                if (registeredEncoders.TryGetValue(encoderSignatureData[i], out var encoder))
                {
                    if (encoderVersion > encoder.Version())
                    {
                        throw new System.Exception($"unrecognized {encoderSignatureData[i]} version: {encoderVersion}");
                    }
                    encoders[i] = encoder;
                }
            }
            return encoders;
        }

        public void Dispose()
        {
            if (!leaveOpen)
            {
                stream.Dispose();
            }
        }
    }
}