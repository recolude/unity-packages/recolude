using System;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Collections.Generic;
using UnityEngine;
using Recolude.Core.IO.Encoders.Enum;
using Recolude.Core.IO.Encoders.Euler;
using Recolude.Core.IO.Encoders.Event;
using Recolude.Core.IO.Encoders.Position;
using Recolude.Core.IO.PropertyWriters;

namespace Recolude.Core.IO
{
    /// <summary>
    /// A writer for writing Recordings to a stream in the RAP format. 
    /// </summary>
    public class RAPWriter : IDisposable
    {
        private class EncoderCollectionMapping
        {
            IEncoder<ICaptureCollection<ICapture>> encoder;

            List<ICaptureCollection<ICapture>> captureCollections;

            List<int> collectionOrder;

            public IEncoder<ICaptureCollection<ICapture>> Encoder
            {
                get => encoder;
                set => encoder = value;
            }

            public List<ICaptureCollection<ICapture>> CaptureCollections
            {
                get => captureCollections;
                set => captureCollections = value;
            }

            public List<int> CollectionOrder
            {
                get => collectionOrder;
                set => collectionOrder = value;
            }

            public EncoderCollectionMapping(IEncoder<ICaptureCollection<ICapture>> encoder)
            {
                this.encoder = encoder;
                this.captureCollections = new List<ICaptureCollection<ICapture>>();
                this.collectionOrder = new List<int>();
            }

            internal void AddCollection(int index, ICaptureCollection<ICapture> collection)
            {
                this.collectionOrder.Add(index);
                this.captureCollections.Add(collection);
            }

            internal int Size()
            {
                return captureCollections.Count;
            }

            internal bool Empty()
            {
                return Size() == 0;
            }

            internal void Merge(EncoderCollectionMapping childMap)
            {
                captureCollections.AddRange(childMap.captureCollections);
                collectionOrder.AddRange(childMap.collectionOrder);
            }
        }

        private class CollectionsEvaluation
        {
            List<EncoderCollectionMapping> collectionMappings;

            int offset;

            public CollectionsEvaluation(List<EncoderCollectionMapping> collectionMappings, int offset)
            {
                this.collectionMappings = collectionMappings;
                this.offset = offset;
            }

            public int Offset
            {
                get => offset;
                set => offset = value;
            }

            public List<EncoderCollectionMapping> CollectionMappings
            {
                get => collectionMappings;
                set => collectionMappings = value;
            }
        }

        private readonly BinaryWriter writer;

        private DeflateStream compressStream;

        private BinaryWriter compressWriter;

        private bool leaveOpen;

        private BinaryWriter CurrentWriter => compressStream != null ? compressWriter : writer;

        readonly IEncoder<ICaptureCollection<ICapture>>[] encoders;

        public RAPWriter(Stream stream, IEncoder<ICaptureCollection<ICapture>>[] encoders, bool leaveOpen)
        {
            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream), "Can't write to a null stream!");
            }

            if (!stream.CanWrite)
            {
                throw new ArgumentException("Can't write to an unwritable stream", nameof(stream));
            }

            this.encoders = encoders ?? throw new ArgumentNullException(nameof(encoders));

            this.leaveOpen = leaveOpen;
            writer = new BinaryWriter(stream, new UTF8Encoding(false, true), leaveOpen);
        }

        /// <summary>
        /// Creates a new writer for RAP files with the default encoders of 
        /// Position, Euler, Event, And Enum. The stream will automatically be
        /// closed when this writer is disposed of.
        /// </summary>
        /// <param name="stream">The stream to write the recordings too.</param>
        public RAPWriter(Stream stream) : this(
            stream,
            new IEncoder<ICaptureCollection<ICapture>>[]
            {
                new Vector3PositionEncoder(),
                new EulerEncoder(),
                new EventEncoder(),
                new EnumEncoder()
            },
            false
        )
        {
        }

        /// <summary>
        /// Creates a new writer for RAP files with the default encoders of 
        /// Position, Euler, Event, And Enum.
        /// </summary>
        /// <param name="stream">The stream to write the recordings too.</param>
        /// <param name="leaveOpen">
        /// Whether or not to leave open the stream after this writer is 
        /// disposed of.
        /// </param>
        public RAPWriter(Stream stream, bool leaveOpen) : this(
            stream,
            new IEncoder<ICaptureCollection<ICapture>>[]
            {
                new Vector3PositionEncoder(),
                new EulerEncoder(),
                new EventEncoder(),
                new EnumEncoder()
            },
            leaveOpen
        )
        {
        }

        public void Dispose()
        {
            if (compressWriter != null)
            {
                compressWriter.Dispose();
                compressStream.Dispose();
            }

            writer.Dispose();
        }

        private CollectionsEvaluation EvaluateCollections(IRecording recording, int offset)
        {
            var mappings = new List<EncoderCollectionMapping>();
            var streamsSatisfied = new bool[recording.CaptureCollections.Length];
            for (int collectionIndex = 0; collectionIndex < recording.CaptureCollections.Length; collectionIndex++)
            {
                if (recording.CaptureCollections[collectionIndex] == null)
                {
                    throw new ArgumentException("can not serialize recording with null capture collections");
                }

                streamsSatisfied[collectionIndex] = false;
            }

            for (int encoderIndex = 0; encoderIndex < encoders.Length; encoderIndex++)
            {
                var encoder = encoders[encoderIndex];
                var mapping = new EncoderCollectionMapping(encoder);

                for (int collectionIndex = 0; collectionIndex < recording.CaptureCollections.Length; collectionIndex++)
                {
                    var collection = recording.CaptureCollections[collectionIndex];
                    if (streamsSatisfied[collectionIndex] == false && encoder.Accepts(collection))
                    {
                        streamsSatisfied[collectionIndex] = true;
                        mapping.AddCollection(collectionIndex + offset, collection);
                    }
                }

                if (!mapping.Empty())
                {
                    mappings.Add(mapping);
                }
            }

            // Check for unhandled streams
            for (int i = 0; i < recording.CaptureCollections.Length; i++)
            {
                if (streamsSatisfied[i] == false)
                {
                    throw new InvalidOperationException(
                        $"no encoder registered to handle stream: {recording.CaptureCollections[i].Signature}");
                }
            }

            // TODO: ELI, I think this initial offset calculation might be 
            // wrong, need to check in cases where a encoder satifies multiple
            // different capture collection types, instead of the traditional 
            // 1:1 use case.
            //
            // Also, gotta be sure to try it in Golang version as well
            var curOffset = offset + recording.CaptureCollections.Length;
            foreach (var child in recording.Recordings)
            {
                var childEval = EvaluateCollections(child, curOffset);
                curOffset = childEval.Offset;

                foreach (var childMap in childEval.CollectionMappings)
                {
                    var found = false;
                    for (int i = 0; i < mappings.Count; i++)
                    {
                        var ourMap = mappings[i];
                        if (ourMap.Encoder.Signature() == childMap.Encoder.Signature())
                        {
                            ourMap.Merge(childMap);
                            found = true;
                        }
                    }

                    if (!found)
                    {
                        mappings.Add(childMap);
                    }
                }
            }

            return new CollectionsEvaluation(mappings, curOffset);
        }

        private void AccumulateMetadataKeys(IRecording recording, Dictionary<string, int> keyMappingToIndex)
        {
            var keyCount = keyMappingToIndex.Count;

            foreach (var keyVal in recording.Metadata)
            {
                if (keyVal.Key == null)
                {
                    throw new ArgumentException(
                        $"Can not serialize null metadata property key on recording: '{recording.Name}'"
                    );
                }

                if (keyVal.Value == null)
                {
                    throw new ArgumentException(
                        $"Can not serialize null metadata property value: '{keyVal.Key}' on recording: '{recording.Name}'"
                    );
                }

                if (keyMappingToIndex.ContainsKey(keyVal.Key) == false)
                {
                    keyMappingToIndex[keyVal.Key] = keyCount;
                    keyCount++;
                }
            }

            foreach (var reference in recording.BinaryReferences)
            {
                foreach (var keyVal in reference.Metadata)
                {
                    if (keyVal.Key == null)
                    {
                        throw new ArgumentException(
                            $"Can not serialize null metadata property key on recording '{recording.Name}' on binary reference '{reference.Name}'"
                        );
                    }

                    if (keyVal.Value == null)
                    {
                        throw new ArgumentException(
                            $"Can not serialize null metadata property value: '{keyVal.Key}' on recording '{recording.Name}' on binary reference '{reference.Name}'"
                        );
                    }

                    if (keyMappingToIndex.ContainsKey(keyVal.Key) == false)
                    {
                        keyMappingToIndex[keyVal.Key] = keyCount;
                        keyCount++;
                    }
                }
            }

            foreach (var reference in recording.Binaries)
            {
                foreach (var keyVal in reference.Metadata())
                {
                    if (keyVal.Key == null)
                    {
                        throw new ArgumentException(
                            $"Can not serialize null metadata property key on recording '{recording.Name}' on binary '{reference.Name()}'"
                        );
                    }

                    if (keyVal.Value == null)
                    {
                        throw new ArgumentException(
                            $"Can not serialize null metadata property value: '{keyVal.Key}' on recording '{recording.Name}' on binary'{reference.Name()}'"
                        );
                    }

                    if (keyMappingToIndex.ContainsKey(keyVal.Key) == false)
                    {
                        keyMappingToIndex[keyVal.Key] = keyCount;
                        keyCount++;
                    }
                }
            }

            foreach (var child in recording.Recordings)
            {
                AccumulateMetadataKeys(child, keyMappingToIndex);
            }
        }

        private void WriteEncoders(List<EncoderCollectionMapping> encodersToWrite)
        {
            string[] encoderSignatures = new string[encodersToWrite.Count];
            int[] encoderVersions = new int[encodersToWrite.Count];

            for (int i = 0; i < encodersToWrite.Count; i++)
            {
                encoderSignatures[i] = encodersToWrite[i].Encoder.Signature();
                encoderVersions[i] = encodersToWrite[i].Encoder.Version();
            }

            Write(encoderSignatures);
            foreach (var val in encoderVersions)
            {
                WriteAsUVarInt(val);
            }
        }

        private int NumberCaptureCollectionsRecursive(IRecording recording)
        {
            if (recording == null)
            {
                throw new ArgumentNullException(nameof(recording));
            }

            int total = recording.CaptureCollections.Length;
            foreach (var child in recording.Recordings)
            {
                total += NumberCaptureCollectionsRecursive(child);
            }

            return total;
        }

        public void Write(IRecording recording)
        {
            Write(recording, System.IO.Compression.CompressionLevel.Fastest, TimeStorageTechnique.BST16);
        }

        public void Write(IRecording recording, TimeStorageTechnique timeStorageTechnique)
        {
            Write(recording, System.IO.Compression.CompressionLevel.Fastest, timeStorageTechnique);
        }

        public void Write(IRecording recording, System.IO.Compression.CompressionLevel compressionLevel)
        {
            Write(recording, compressionLevel, TimeStorageTechnique.BST16);
        }

        public void Write(IRecording recording, System.IO.Compression.CompressionLevel compressionLevel,
            TimeStorageTechnique timeStorageTechnique)
        {
            if (recording == null)
            {
                throw new ArgumentNullException(nameof(recording));
            }

            var encoderMappings = EvaluateCollections(recording, 0).CollectionMappings;

            // Write File Version
            CurrentWriter.Write((byte)2);

            // Write Encoders
            WriteEncoders(encoderMappings);

            // Write compression flag.
            CurrentWriter.Write(
                (byte)(compressionLevel == System.IO.Compression.CompressionLevel.NoCompression ? 0 : 1));

            if (compressionLevel != System.IO.Compression.CompressionLevel.NoCompression)
            {
                compressStream = new DeflateStream(writer.BaseStream, compressionLevel, true);
                compressWriter = new BinaryWriter(compressStream, new System.Text.UTF8Encoding(false, true));
            }

            var totalNumberOfCaptureCollections = NumberCaptureCollectionsRecursive(recording);
            var encodingBlocks = new byte[totalNumberOfCaptureCollections][];
            var streamIndexToEncoderUsedIndex = new int[totalNumberOfCaptureCollections];

            // Write Encoder headers
            for (int encoderIndex = 0; encoderIndex < encoderMappings.Count; encoderIndex++)
            {
                var val = encoderMappings[encoderIndex];
                var encoderResults = val.Encoder.Encode(val.CaptureCollections.ToArray());

                for (int i = 0; i < val.CollectionOrder.Count; i++)
                {
                    var order = val.CollectionOrder[i];
                    encodingBlocks[order] = encoderResults.EncodedCollectionsData[i];
                    streamIndexToEncoderUsedIndex[order] = encoderIndex;
                }

                Write(encoderResults.Header);
            }

            // Write Metadata Keys
            var keyMappingToIndex = new Dictionary<string, int>();
            AccumulateMetadataKeys(recording, keyMappingToIndex);
            var allKeys = new string[keyMappingToIndex.Count];
            foreach (var keyval in keyMappingToIndex)
            {
                allKeys[keyval.Value] = keyval.Key;
            }

            Write(allKeys);

            // Write All Recordings
            Write(recording, keyMappingToIndex, encodingBlocks, streamIndexToEncoderUsedIndex, 0, timeStorageTechnique);
        }

        private int Write(IRecording recording, Dictionary<string, int> keyMappingToIndex, byte[][] encodingBlocks,
            int[] streamIndexToEncoderUsedIndex, int offset, TimeStorageTechnique tech)
        {
            Write(recording.ID);
            Write(recording.Name);

            try
            {
                Write(recording.Metadata, keyMappingToIndex);
            }
            catch (Exception e)
            {
                Debug.Log($"{recording.Name}[{recording.ID}]");
                throw new SystemException($"{recording.Name}[{recording.ID}]: Unable to serialize invalid metadata", e);
            }

            // Capture Collections
            WriteAsUVarInt(recording.CaptureCollections.Length);
            for (int streamIndex = 0; streamIndex < recording.CaptureCollections.Length; streamIndex++)
            {
                WriteAsUVarInt(streamIndexToEncoderUsedIndex[offset + streamIndex]);
                Write(recording.CaptureCollections[streamIndex].Name);
                WriteTimes(recording.CaptureCollections[streamIndex], tech);
                Write(encodingBlocks[offset + streamIndex]);
            }

            // Binary References
            WriteAsUVarInt(recording.BinaryReferences.Length);
            foreach (var binRef in recording.BinaryReferences)
            {
                Write(binRef.Name);
                Write(binRef.Uri);
                WriteAsUVarInt((long)binRef.Size);
                Write(binRef.Metadata, keyMappingToIndex);
            }

            // Binaries
            WriteAsUVarInt(recording.Binaries.Length);
            foreach (var bin in recording.Binaries)
            {
                Write(bin.Name());
                WriteAsUVarInt((long)bin.Size());
                Write(bin.Metadata(), keyMappingToIndex);
                Write(bin.Data());
            }

            // Recordings
            WriteAsUVarInt(recording.Recordings.Length);
            var newOffset = offset + recording.CaptureCollections.Length;
            foreach (var child in recording.Recordings)
            {
                newOffset = Write(child, keyMappingToIndex, encodingBlocks, streamIndexToEncoderUsedIndex, newOffset,
                    tech);
            }

            return newOffset;
        }

        private void WriteTimesRaw32(ICaptureCollection<ICapture> captures)
        {
            foreach (var capture in captures.Captures)
            {
                CurrentWriter.Write(capture.Time);
            }
        }

        private void WriteTimesRaw64(ICaptureCollection<ICapture> captures)
        {
            foreach (var capture in captures.Captures)
            {
                CurrentWriter.Write((double)capture.Time);
            }
        }

        private void WriteTimesRawBST16(ICaptureCollection<ICapture> captures)
        {
            if (captures.Captures.Length == 0)
            {
                return;
            }

            CurrentWriter.Write(captures.Captures[0].Time);

            if (captures.Captures.Length == 1)
            {
                return;
            }

            var maxTimeDifference = captures.Captures[1].Time - captures.Captures[0].Time;
            for (int i = 2; i < captures.Captures.Length; i++)
            {
                maxTimeDifference = Mathf.Max(maxTimeDifference,
                    captures.Captures[i].Time - captures.Captures[i - 1].Time);
            }

            CurrentWriter.Write(maxTimeDifference);

            var totalledQuantizedDuration = captures.Captures[0].Time;
            var buffer2Byes = new byte[2];
            for (int i = 1; i < captures.Captures.Length; i++)
            {
                var duration = captures.Captures[i].Time - totalledQuantizedDuration;
                RAPBinary.ToFloat16BST(buffer2Byes, 0, maxTimeDifference, duration);
                CurrentWriter.Write(buffer2Byes);
                totalledQuantizedDuration += RAPBinary.FromFloat16BST(buffer2Byes, 0, maxTimeDifference);
            }
        }

        private void WriteTimes(ICaptureCollection<ICapture> captures, TimeStorageTechnique storageTechnique)
        {
            CurrentWriter.Write((byte)storageTechnique);
            WriteAsUVarInt(captures.Captures.Length);
            switch (storageTechnique)
            {
                case TimeStorageTechnique.Raw64:
                    WriteTimesRaw64(captures);
                    break;

                case TimeStorageTechnique.Raw32:
                    WriteTimesRaw32(captures);
                    break;

                case TimeStorageTechnique.BST16:
                    WriteTimesRawBST16(captures);
                    break;

                default:
                    throw new ArgumentException($"Unimplemented timme storage technique: {storageTechnique}");
            }
        }

        private void Write(Metadata block, Dictionary<string, int> keyMappingToIndex)
        {
            if (block == null)
            {
                throw new ArgumentNullException(nameof(block));
            }

            var metadataIndices = new int[block.Count];

            using (var metadataValuesBuffer = new MemoryStream())
            {
                using (BinaryWriter writer = new BinaryWriter(metadataValuesBuffer, new UTF8Encoding(false, true)))
                {
                    int i = 0;
                    foreach (var keyVal in block)
                    {
                        metadataIndices[i] = keyMappingToIndex[keyVal.Key];
                        if (keyVal.Value == null)
                        {
                            throw new ArgumentException(
                                $"Can not write metadata entry '{keyVal.Key}' ['{metadataIndices[i]}'] with null property.");
                        }

                        PropertyWriterCollection.Instance.Write(writer, keyVal.Value);
                        i++;
                    }
                }

                WriteAsUVarInt(metadataIndices);
                CurrentWriter.Write(metadataValuesBuffer.ToArray());
            }
        }

        public void Write(Stream stream)
        {
            stream.CopyTo(CurrentWriter.BaseStream);
        }

        public void WriteWithoutSize(byte[] data)
        {
            CurrentWriter.Write(data);
        }

        public void Write(string str)
        {
            var bytes = Encoding.UTF8.GetBytes(str);
            RAPBinary.WriteUVarInt(CurrentWriter.BaseStream, bytes.Length);
            CurrentWriter.Write(bytes);
        }

        public void Write(string[] strArr)
        {
            RAPBinary.WriteUVarInt(CurrentWriter.BaseStream, strArr.Length);
            foreach (var str in strArr)
            {
                Write(str);
            }
        }

        private void Write(byte[] data)
        {
            WriteAsUVarInt(data.Length);
            CurrentWriter.Write(data);
        }

        public void WriteAsUVarInt(int[] intArr)
        {
            WriteAsUVarInt(intArr.Length);
            foreach (var val in intArr)
            {
                WriteAsUVarInt(val);
            }
        }

        public void WriteAsUVarInt(long val)
        {
            if (val < 0)
            {
                throw new ArgumentException("can not write negative val as uvar int");
            }

            var runningVal = val;
            while (runningVal >= 0x80)
            {
                CurrentWriter.Write((byte)(runningVal | 0x80));
                runningVal >>= 7;
            }

            CurrentWriter.Write((byte)runningVal);
        }
    }
}