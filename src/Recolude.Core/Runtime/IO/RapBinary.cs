using System.IO;

namespace Recolude.Core.IO
{

    /// <summary>
    /// General Utility functions for dealing with RAP data seen within 
    /// different encoders, as well as the file format itself.
    /// </summary>
    public static class RAPBinary
    {
        const int MaxVarintLen16 = 3;

        const int MaxVarintLen32 = 5;

        const int MaxVarintLen64 = 10;

        const string OverflowError = "binary: varint overflows a 64-bit integer";

        public static float FromFloat16BST(byte[] buffer, float min, float max)
        {
            var length = max - min;
            var curValue = min + (length / 2.0f);
            var increment = length / 4.0f;

            for (var byteIndex = 0; byteIndex < buffer.Length; byteIndex++)
            {
                for (var bitIndex = 0; bitIndex < 8; bitIndex++)
                {
                    if (((buffer[byteIndex] >> bitIndex) & 1) == 1)
                    {
                        curValue += increment;
                    }
                    else
                    {
                        curValue -= increment;
                    }
                    increment /= 2.0f;
                }
            }
            return curValue;
        }

        public static void ToFloat16BST(byte[] buffer, float min, float max, float value)
        {
            var duration = max - min;
            var curValue = min + (duration / 2.0f);
            var increment = duration / 4.0f;

            for (var byteIndex = 0; byteIndex < buffer.Length; byteIndex++)
            {
                // Clear whatever byte might be there
                buffer[byteIndex] = 0;
                for (var bitIndex = 0; bitIndex < 8; bitIndex++)
                {
                    if (value < curValue)
                    {
                        curValue -= increment;
                    }
                    else
                    {
                        buffer[byteIndex] = (byte)(buffer[byteIndex] | (1 << bitIndex));
                        curValue += increment;
                    }
                    increment /= 2.0f;
                }
            }
        }

        public static int ReadUVarInt(Stream reader)
        {
            var x = 0;
            var s = 0;

            for (var i = 0; i < MaxVarintLen64; i++)
            {
                var b = reader.ReadByte();
                if (b == -1)
                {
                    throw new System.Exception("End of stream reached");
                }
                if (b < 0x80)
                {
                    if (i == 9 && b > 1)
                    {
                        throw new System.Exception(OverflowError);
                    }
                    return x | (b << s);
                }
                x |= (b & 0x7f) << s;
                s += 7;
            }

            throw new System.Exception(OverflowError);
        }

        public static void WriteUVarInt(Stream writer, int value)
        {
            if (value < 0)
            {
                throw new System.ArgumentException("attempting to write a negative number as unsigned");
            }
            WriteUVarInt(writer, (uint)value);
        }

        public static void WriteUVarInt(BinaryWriter writer, int value)
        {
            if (value < 0)
            {
                throw new System.ArgumentException("attempting to write a negative number as unsigned");
            }
            WriteUVarInt(writer, (uint)value);
        }

        public static void WriteUVarInt(Stream writer, uint value)
        {
            var x = value;
            while (x >= 0x80)
            {
                writer.WriteByte((byte)(x | 0x80));
                x >>= 7;
            }
            writer.WriteByte((byte)x);
        }

        public static void WriteUVarInt(BinaryWriter writer, uint value)
        {
            var x = value;
            while (x >= 0x80)
            {
                writer.Write((byte)(x | 0x80));
                x >>= 7;
            }
            writer.Write((byte)x);
        }

        // public static void WriteUVarInt(BinaryWriter writer, int value)
        // {
        //     if (value < 0)
        //     {
        //         throw new System.ArgumentException("attempting to write a negative number as unsigned");
        //     }
        //     var x = value;
        //     while (x >= 0x80)
        //     {
        //         writer.Write((byte)(x | 0x80));
        //         x >>= 7;
        //     }
        //     writer.Write((byte)x);
        // }
    }
}
