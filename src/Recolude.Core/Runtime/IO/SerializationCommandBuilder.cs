﻿using System;
using System.Collections;
using System.IO;
using System.IO.Compression;
using Recolude.Core.IO.Encoders.Enum;
using Recolude.Core.IO.Encoders.Euler;
using Recolude.Core.IO.Encoders.Event;
using Recolude.Core.IO.Encoders.Position;

namespace Recolude.Core.IO
{
    /// <summary>
    /// A helper class for quickly configuring all options pertaining to how a
    /// recording can be serialized.
    /// </summary>
    public class SerializationCommandBuilder
    {
        private readonly IRecording recording;

        private readonly Stream stream;

        private readonly bool leaveStreamOpen;

        private readonly IEncoder<ICaptureCollection<ICapture>>[] encoders;

        private readonly TimeStorageTechnique timeStorageTechnique;

        private readonly CompressionLevel compressionLevel;

        public SerializationCommandBuilder(
            IRecording recording,
            Stream stream = null,
            bool leaveStreamOpen = false,
            IEncoder<ICaptureCollection<ICapture>>[] encoders = null,
            TimeStorageTechnique timeStorageTechnique = TimeStorageTechnique.BST16,
            CompressionLevel compressionLevel = CompressionLevel.Fastest
        )
        {
            this.recording = recording;

            this.stream = stream;
            this.leaveStreamOpen = leaveStreamOpen;

            this.encoders = encoders ?? Array.Empty<IEncoder<ICaptureCollection<ICapture>>>();
            this.timeStorageTechnique = timeStorageTechnique;
            this.compressionLevel = compressionLevel;
        }

        public void Run()
        {
            using (var rapWriter = new RAPWriter(stream, encoders, leaveStreamOpen))
            {
                rapWriter.Write(recording, compressionLevel, timeStorageTechnique);
            }
        }

        public IEnumerator RunAsCoroutine()
        {
            yield return new SerializeRecordingInstruction(this);
        }

        public SerializationCommandBuilder CloseStream()
        {
            return new SerializationCommandBuilder(
                recording,
                stream,
                false,
                encoders,
                timeStorageTechnique,
                compressionLevel
            );
        }

        public SerializationCommandBuilder KeepStreamOpen()
        {
            return new SerializationCommandBuilder(
                recording,
                stream,
                true,
                encoders,
                timeStorageTechnique,
                compressionLevel
            );
        }

        public SerializationCommandBuilder WithTimeStorageTechnique(TimeStorageTechnique technique)
        {
            return new SerializationCommandBuilder(
                recording,
                stream,
                leaveStreamOpen,
                encoders,
                technique,
                compressionLevel
            );
        }

        public SerializationCommandBuilder WithCompressionLevel(CompressionLevel level)
        {
            return new SerializationCommandBuilder(
                recording,
                stream,
                leaveStreamOpen,
                encoders,
                timeStorageTechnique,
                level
            );
        }

        public SerializationCommandBuilder WithStream(Stream stream)
        {
            return new SerializationCommandBuilder(
                recording,
                stream,
                leaveStreamOpen,
                encoders,
                timeStorageTechnique,
                compressionLevel
            );
        }


        public SerializationCommandBuilder WithEncoders(IEncoder<ICaptureCollection<ICapture>>[] encoders)
        {
            return new SerializationCommandBuilder(
                recording,
                stream,
                leaveStreamOpen,
                encoders,
                timeStorageTechnique,
                compressionLevel
            );
        }

        public SerializationCommandBuilder WithDefaultEncoders()
        {
            return new SerializationCommandBuilder(
                recording,
                stream,
                leaveStreamOpen,
                new IEncoder<ICaptureCollection<ICapture>>[]
                {
                    new Vector3PositionEncoder(),
                    new EulerEncoder(),
                    new EventEncoder(),
                    new EnumEncoder()
                },
                timeStorageTechnique,
                compressionLevel
            );
        }

        public SerializationCommandBuilder AddEncoder(IEncoder<ICaptureCollection<ICapture>> encoder)
        {
            var newEncoders = new IEncoder<ICaptureCollection<ICapture>>[encoders.Length + 1];
            for (int i = 0; i < encoders.Length; i++)
            {
                newEncoders[i] = encoders[i];
            }

            newEncoders[encoders.Length] = encoder;

            return new SerializationCommandBuilder(
                recording,
                stream,
                leaveStreamOpen,
                newEncoders,
                timeStorageTechnique,
                compressionLevel
            );
        }

        public SerializationCommandBuilder AddEncoders(IEncoder<ICaptureCollection<ICapture>>[] encodersToAdd)
        {
            var newEncoders = new IEncoder<ICaptureCollection<ICapture>>[encoders.Length + encodersToAdd.Length];
            for (int i = 0; i < encoders.Length; i++)
            {
                newEncoders[i] = encoders[i];
            }

            for (int i = 0; i < encodersToAdd.Length; i++)
            {
                newEncoders[encoders.Length + i] = encodersToAdd[i];
            }

            return new SerializationCommandBuilder(
                recording,
                stream,
                leaveStreamOpen,
                newEncoders,
                timeStorageTechnique,
                compressionLevel
            );
        }
    }
}