﻿using System;
using System.Threading;
using UnityEngine;

namespace Recolude.Core.IO
{
    public class SerializeRecordingInstruction : CustomYieldInstruction
    {
        private readonly SerializationCommandBuilder command;
        private readonly Thread thread;

        public SerializeRecordingInstruction(SerializationCommandBuilder command)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            thread = new Thread(SerializeRecordingThread);
            thread.Start();
        }

        private void SerializeRecordingThread()
        {
            command.Run();
        }

        public override bool keepWaiting => thread.ThreadState == ThreadState.Running;
    }
}