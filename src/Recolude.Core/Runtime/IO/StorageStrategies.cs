﻿using UnityEngine;

namespace Recolude.Core.IO
{
    public static class StorageStrategies
    {
        /// <summary>
        /// Based on the platform we're running on, pick between RAM or the
        /// file system for saving data too.
        /// </summary>
        /// <returns>A Binary Storage medium suited for the current platform.</returns>
        public static IBinaryStorage PlatformSpecific()
        {
            if (Application.platform == RuntimePlatform.WebGLPlayer)
            {
                return new InMemoryBinaryStorage();
            }

            return new FSBinaryStorage();
        }
    }
}