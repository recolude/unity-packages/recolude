using System.IO;
using System;

namespace Recolude.Core.IO
{
    internal static class TimeBlockUtil
    {
        private static float[] DecodeRaw64(BinaryUtilReader reader, int length)
        {
            var outTimes = new float[length];

            for (int i = 0; i < outTimes.Length; i++)
            {
                outTimes[i] = (float)reader.Float64();
            }

            return outTimes;
        }

        private static float[] DecodeRaw32(BinaryUtilReader reader, int length)
        {
            var outTimes = new float[length];

            for (int i = 0; i < outTimes.Length; i++)
            {
                outTimes[i] = reader.Float32();
            }

            return outTimes;
        }

        private static float[] DecodeBST16(BinaryUtilReader reader, int length)
        {
            if (length == 0)
            {
                return new float[0];
            }

            var startTime = reader.Float32();

            var captures = new float[length];
            captures[0] = startTime;

            if (length == 1)
            {
                return captures;
            }

            var maxTimeDifference = reader.Float32();
            var currentTime = startTime;
            for (int i = 1; i < captures.Length; i++)
            {
                var timeIncrement = reader.Float16BST(0, maxTimeDifference);
                currentTime += timeIncrement;
                captures[i] = currentTime;
            }
            return captures;
        }

        internal static float[] DecodeTimes(BinaryUtilReader reader)
        {
            var typeByte = (TimeStorageTechnique)reader.Byte();
            var numTimes = reader.UVarInt();

            switch (typeByte)
            {
                case TimeStorageTechnique.Raw64:
                    return DecodeRaw64(reader, numTimes);

                case TimeStorageTechnique.Raw32:
                    return DecodeRaw32(reader, numTimes);

                case TimeStorageTechnique.BST16:
                    return DecodeBST16(reader, numTimes);

            }
            throw new Exception($"Unimplemented time storage technique: {typeByte}");
        }
    }
}