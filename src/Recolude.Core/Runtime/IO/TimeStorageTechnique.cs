namespace Recolude.Core.IO
{
    public enum TimeStorageTechnique
    {
        Raw64 = 0,

        // Raw32 encodes time with 32 bit precision
        Raw32 = 1,

        BST16 = 2,
    }
}