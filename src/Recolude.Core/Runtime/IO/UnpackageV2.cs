using Recolude.Core.Record;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System;
using UnityEngine;
using Recolude.Core.IO.Encoders.Enum;
using Recolude.Core.IO.Encoders.Euler;
using Recolude.Core.IO.Encoders.Event;
using Recolude.Core.IO.Encoders.Position;
using Recolude.Core.Properties;

namespace Recolude.Core.IO
{
    internal static class UnpackagerV2
    {
        internal static IProperty ReadProperty(BinaryUtilReader reader)
        {
            byte propType = reader.Byte();
            switch (propType)
            {
                case PropertyCodes.String: // string property
                    return new Property<string>(reader.String());

                case PropertyCodes.Int32: // int32 property
                    return new IntProperty(reader.Int32());

                case PropertyCodes.Float32: // float 32
                    return new FloatProperty(reader.Float32());

                case PropertyCodes.BoolTrue:
                    return new Property<bool>(true);

                case PropertyCodes.BoolFalse:
                    return new Property<bool>(false);

                case PropertyCodes.Byte:
                    return new Property<byte>(reader.Byte());

                case PropertyCodes.Vector2:
                    return new Vector2Property(new Vector2((float)reader.Float64(), (float)reader.Float64()));

                case PropertyCodes.Vector3:
                    return new Vector3Property(new Vector3((float)reader.Float64(), (float)reader.Float64(),
                        (float)reader.Float64()));

                case PropertyCodes.Metadata:
                    return new MetadataProperty(Metadata.Read(reader));

                case PropertyCodes.Time:
                    long microseconds = reader.Int64();
                    DateTime epochTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    return new TimeProperty(epochTime.AddTicks(microseconds * 10));

                // String array property
                case PropertyCodes.StringArray:
                    return new ArrayProperty<string>(reader.StringArray());

                // 14 - 13 = 1 (int32)
                case PropertyCodes.Int32Array:
                    int[] intData = new int[reader.UVarInt()];
                    for (int i = 0; i < intData.Length; i++)
                    {
                        intData[i] = reader.Int32();
                    }

                    return new ArrayProperty<int>(intData);

                // 15 - 13 = 2 (float)
                case PropertyCodes.Float32Array:
                    float[] floatData = new float[reader.UVarInt()];
                    for (int i = 0; i < floatData.Length; i++)
                    {
                        floatData[i] = reader.Float32();
                    }

                    return new ArrayProperty<float>(floatData);

                // 16 - 13 = 3 (bool)
                case PropertyCodes.BoolArray:
                    bool[] boolData = new bool[reader.UVarInt()];
                    for (int i = 0; i < boolData.Length; i++)
                    {
                        boolData[i] = reader.Byte() == 1;
                    }

                    return new ArrayProperty<bool>(boolData);

                // 18 - 13 = 5 (byte)
                case PropertyCodes.ByteArray:
                    return new ArrayProperty<byte>(reader.BytesArray());

                // 19 - 13 = 6 (Vector2)
                case PropertyCodes.Vector2Array:
                    Vector2[] vector2Data = new Vector2[reader.UVarInt()];
                    for (int i = 0; i < vector2Data.Length; i++)
                    {
                        vector2Data[i] = new Vector2((float)reader.Float64(), (float)reader.Float64());
                    }

                    return new ArrayProperty<Vector2>(vector2Data);

                // 20 - 13 = 7 (Vector3)
                case PropertyCodes.Vector3Array:
                    Vector3[] vector3Data = new Vector3[reader.UVarInt()];
                    for (int i = 0; i < vector3Data.Length; i++)
                    {
                        vector3Data[i] = new Vector3((float)reader.Float64(), (float)reader.Float64(),
                            (float)reader.Float64());
                    }

                    return new ArrayProperty<Vector3>(vector3Data);

                // 24 - 13 = 11 (Metadata)
                case PropertyCodes.MetadataArray:
                    Metadata[] metadataData = new Metadata[reader.UVarInt()];
                    for (int i = 0; i < metadataData.Length; i++)
                    {
                        metadataData[i] = Metadata.Read(reader);
                    }

                    return new ArrayProperty<Metadata>(metadataData);

                // 25 - 13 = 12 (Datetime)
                case PropertyCodes.TimeArray:
                    DateTime[] timeData = new DateTime[reader.UVarInt()];
                    for (int i = 0; i < timeData.Length; i++)
                    {
                        long microSeconds = reader.Int64();
                        DateTime epochTimeEntry = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                        timeData[i] = epochTimeEntry.AddTicks(microSeconds * 10);
                    }

                    return new ArrayProperty<DateTime>(timeData);
            }

            throw new System.Exception($"unknown property type: {propType}");
        }

        internal static Recording RecursiveBuildRecordingsV2(BinaryUtilReader reader, string[] metadataKeys,
            IEncoder<ICaptureCollection<ICapture>>[] encoders, byte[][] headers)
        {
            string recordingID = reader.String();
            string recordingName = reader.String();
            Metadata recordingMetadata = Metadata.Read(reader, metadataKeys);

            var numStreams = reader.UVarInt();
            var allStreams = new List<ICaptureCollection<ICapture>>();
            for (int streamIndex = 0; streamIndex < numStreams; streamIndex++)
            {
                var encoderIndex = reader.UVarInt();
                var streamName = reader.String();
                var times = TimeBlockUtil.DecodeTimes(reader);
                var streamBody = reader.BytesArray();
                allStreams.Add(encoders[encoderIndex].Decode(streamName, headers[encoderIndex], streamBody, times));
            }

            // Read Binary References
            var binaryReferences = new BinaryReference[reader.UVarInt()];
            for (int i = 0; i < binaryReferences.Length; i++)
            {
                binaryReferences[i] = new BinaryReference(
                    reader.String(),
                    reader.String(),
                    (ulong)reader.UVarInt(),
                    Metadata.Read(reader, metadataKeys)
                );
            }

            // Read Binaries
            var binaries = new IBinary[reader.UVarInt()];
            for (int i = 0; i < binaries.Length; i++)
            {
                string binaryName = reader.String();
                int length = reader.UVarInt();
                binaries[i] = new InMemoryBinary(
                    binaryName,
                    Metadata.Read(reader, metadataKeys),
                    reader.BytesArray(length)
                );
            }

            // Recursive build recordings
            var childRecordings = new Recording[reader.UVarInt()];
            for (int i = 0; i < childRecordings.Length; i++)
            {
                childRecordings[i] = RecursiveBuildRecordingsV2(reader, metadataKeys, encoders, headers);
            }

            return new Recording(
                recordingID,
                recordingName,
                childRecordings,
                allStreams.ToArray(),
                recordingMetadata,
                binaries,
                binaryReferences
            );
        }
    }
}