﻿#if RAPV1
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System;
using UnityEngine;

namespace Recolude.Core.IO
{

    /// <summary>
    /// Responsible for converting V1 RAP files binary data to recordings for
    ///  playback.
    /// </summary>
    internal static class UnpackagerV1
    {

        
        internal static Recording UnpackageV1(Stream stream)
        {
            byte[] numberRecordingsByteData = new byte[4];
            stream.Read(numberRecordingsByteData, 0, 4);
            int numberRecordings = BitConverter.ToInt32(numberRecordingsByteData, 0);
            if (numberRecordings != 1)
            {
                throw new SystemException("Multiple recordings inside a single stream are no longer supported. Reach out to support to get an upgrade utility");
            }

            using (BinaryReader reader = new BinaryReader(stream))
            {
                Recolude.Core.IO.TransportV1.Recording recording = null;
                var transportDataCompressed = new MemoryStream(reader.ReadBytes((int)reader.ReadInt64()));
                using (DeflateStream dstream = new DeflateStream(transportDataCompressed, CompressionMode.Decompress))
                {
                    recording = Recolude.Core.IO.TransportV1.Recording.Parser.ParseFrom(dstream);
                }

                return FromTransport(recording);
            }
        }

        /// <summary>
        /// Converts a protobuf recording into a version that can be used for playback.
        /// </summary>
        /// <param name="transportRecording">Protobuf recording to be converted.</param>
        /// <returns>Recording that can be used for things like playback.</returns>
        /// <exception cref="System.ArgumentException">Thrown when the recordings presented is null.</exception>
        internal static Recording FromTransport(Recolude.Core.IO.TransportV1.Recording transportRecording)
        {
            if (transportRecording == null)
            {
                throw new ArgumentException("Nothing to convert (null recordings)");
            }

            Recording recording = new Recording(
                "",
                transportRecording.Name,
                FromTransport(transportRecording.Subjects),
                new ICaptureCollection<ICapture>[] {
                    FromTransport(transportRecording.CustomEvents),
                },
                new Metadata(new Dictionary<string, string>(transportRecording.Metadata))
            );

            return recording;
        }

        private static Recording[] FromTransport(Google.Protobuf.Collections.RepeatedField<Recolude.Core.IO.TransportV1.SubjectRecording> transportRecordings)
        {
            if (transportRecordings == null)
            {
                throw new Exception("Nothing to convert (null recordings)");
            }

            Recording[] recordings = new Recording[transportRecordings.Count];

            for (int recordIndex = 0; recordIndex < transportRecordings.Count; recordIndex++)
            {
                recordings[recordIndex] = new Recording(
                    transportRecordings[recordIndex].Id.ToString(),
                    transportRecordings[recordIndex].Name,
                    null,
                    new ICaptureCollection<ICapture>[] {
                        new PositionCollection(CollectionNames.Position, FromTransport(transportRecordings[recordIndex].CapturedPositions)),
                        new EulerRotationCollection(CollectionNames.Rotation, FromTransport(transportRecordings[recordIndex].CapturedRotations)),
                        new LifeCycleCollection(FromTransport(transportRecordings[recordIndex].LifecycleEvents)),
                        FromTransport(transportRecordings[recordIndex].CustomEvents)
                    },
                    new Metadata(new Dictionary<string, string>(transportRecordings[recordIndex].Metadata)),
                    null,
                    null
                );
            }

            return recordings;
        }

        private static VectorCapture[] FromTransport(Google.Protobuf.Collections.RepeatedField<Recolude.Core.IO.TransportV1.VectorCapture> transportCapture)
        {
            var vectorCapture = new VectorCapture[transportCapture.Count];
            for (int i = 0; i < transportCapture.Count; i++)
            {
                vectorCapture[i] = new VectorCapture(transportCapture[i].Time, new Vector3(transportCapture[i].X, transportCapture[i].Y, transportCapture[i].Z));
            }
            return vectorCapture;
        }

        internal static CustomEventCollection FromTransport(Google.Protobuf.Collections.RepeatedField<Recolude.Core.IO.TransportV1.CustomEventCapture> transportCapture)
        {
            var curstomCapture = new CustomEventCapture[transportCapture.Count];
            for (int i = 0; i < transportCapture.Count; i++)
            {
                if (transportCapture[i].Data != null && transportCapture[i].Data.Count > 0)
                {
                    curstomCapture[i] = new CustomEventCapture(transportCapture[i].Time, transportCapture[i].Name, new Dictionary<string, string>(transportCapture[i].Data));
                }
                else
                {
                    curstomCapture[i] = new CustomEventCapture(transportCapture[i].Time, transportCapture[i].Name, transportCapture[i].Contents);
                }
            }
            return new CustomEventCollection(curstomCapture);
        }

        private static UnityLifeCycleEventCapture[] FromTransport(Google.Protobuf.Collections.RepeatedField<Recolude.Core.IO.TransportV1.LifeCycleEventCapture> transportCapture)
        {
            var curstomCapture = new UnityLifeCycleEventCapture[transportCapture.Count];
            for (int i = 0; i < transportCapture.Count; i++)
            {
                curstomCapture[i] = new UnityLifeCycleEventCapture(transportCapture[i].Time, (UnityLifeCycleEvent)((int)transportCapture[i].Type));
            }
            return curstomCapture;
        }

    }
}
#endif