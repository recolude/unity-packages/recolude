namespace Recolude.Core
{
    public interface IProperty
    {
        // object Value { get; }
    }

    public interface IProperty<out T> : IProperty
    {
        T Value { get; }
    }
}