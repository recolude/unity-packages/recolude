namespace Recolude.Core
{
    /// <summary>
    /// Outline of what data get's serialized when put into RAP format. Each 
    /// recording can contain multiple sub-recordings, equating to a tree-like 
    /// structure.
    /// </summary>
    public interface IRecording
    {
        /// <summary>
        /// ID of the recording. Developers should strive to ensuring this 
        /// value remains unique and not-null
        /// </summary>
        /// <value>ID of the recording.</value>
        string ID { get; }

        /// <summary>
        /// Name of the recording. Developers shuold strive to ensure this value is never null;
        /// </summary>
        /// <value>Name of the recording.</value>
        string Name { get; }

        /// <summary>
        /// Helper method short hand for EndTime - StartTime; If there are no 
        /// captures within the recording or sub recordings, the duration is 0. 
        /// </summary>
        /// <value>Duration of the recording.</value>
        float Duration { get; }

        /// <summary>
        /// The time of the earliest capture found within all capture 
        /// collections within this recording and all sub recordings. If there 
        /// is no capture data within the recording and all sub recordings, the
        /// value should be NaN.
        /// </summary>
        /// <value>Start time of the recording.</value>
        float StartTime { get; }

        /// <summary>
        /// The time of the latest capture found within all capture 
        /// collections within this recording and all sub recordings. If there 
        /// is no capture data within the recording and all sub recordings, the
        /// value should be NaN.
        /// </summary>
        /// <value>End time of the recording.</value>
        float EndTime { get; }

        /// <summary>
        /// All capture data for this specific recording. It does not include 
        /// sub-recording capture data.
        /// </summary>
        /// <value>All capture data for this specific recording.</value>
        ICaptureCollection<ICapture>[] CaptureCollections { get; }

        /// <summary>
        /// All immediate child recordings.
        /// </summary>
        /// <value>All immediate child recordings.</value>
        IRecording[] Recordings { get; }

        /// <summary>
        /// Metadata associated with the recording.
        /// </summary>
        /// <value>Metadata associated with the recording.</value>
        Metadata Metadata { get; }

        /// <summary>
        /// Binaries associated with the recording.
        /// </summary>
        /// <value>Binaries associated with the recording.</value>
        IBinary[] Binaries { get; }

        /// <summary>
        /// References to different binaries associated with the recording.
        /// </summary>
        /// <value>References to different binaries associated with the recording.</value>
        BinaryReference[] BinaryReferences { get; }
    }
}