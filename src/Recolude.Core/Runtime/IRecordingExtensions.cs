using UnityEngine;

namespace Recolude.Core
{

    public static class IRecordingExtensions
    {

        public static ICaptureCollection<T> GetCollection<T>(this IRecording recording) where T : ICapture
        {
            if (recording.CaptureCollections == null)
            {
                return null;
            }

            foreach (var collection in recording.CaptureCollections)
            {
                if (collection == null)
                {
                    continue;
                }
                if (collection is ICaptureCollection<T>)
                {
                    return collection as ICaptureCollection<T>;
                }
            }

            return null;
        }

        public static ICaptureCollection<T> GetCollection<T>(this IRecording recording, string name) where T : ICapture
        {
            if (recording.CaptureCollections == null)
            {
                return null;
            }

            foreach (var collection in recording.CaptureCollections)
            {
                if (collection == null)
                {
                    continue;
                }
                var castedCollection = collection as ICaptureCollection<T>;
                if (castedCollection != null && collection.Name == name)
                {
                    return castedCollection;
                }
            }

            return null;
        }

        public static ICaptureCollection<Capture<Vector3>> PositionCollection(this IRecording recording)
        {
            return recording.GetCollection<Capture<Vector3>>(CollectionNames.Position);
        }

        public static ICaptureCollection<Capture<Vector3>> RotationCollection(this IRecording recording)
        {
            return recording.GetCollection<Capture<Vector3>>(CollectionNames.Rotation);
        }

        public static ICaptureCollection<Capture<CustomEvent>> CustomEventCollection(this IRecording recording)
        {
            return recording.GetCollection<Capture<CustomEvent>>(CollectionNames.CustomEvent);
        }

        public static ICaptureCollection<Capture<UnityLifeCycleEvent>> LifeCycleCollection(this IRecording recording)
        {
            return recording.GetCollection<Capture<UnityLifeCycleEvent>>(CollectionNames.LifeCycle);
        }

    }

}