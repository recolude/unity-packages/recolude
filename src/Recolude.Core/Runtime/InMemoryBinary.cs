using System.Collections.Generic;
using System.IO;

namespace Recolude.Core
{
    public class InMemoryBinary : IBinary
    {
        byte[] data;

        Metadata metadata;

        string name;

        public InMemoryBinary(string name, Metadata metadata, byte[] data)
        {
            this.data = data;
            this.metadata = metadata;
            this.name = name;
        }

        public Stream Data()
        {
            return new MemoryStream(data);
        }

        public override bool Equals(object obj)
        {
            return obj is InMemoryBinary binary &&
                   EqualityComparer<byte[]>.Default.Equals(data, binary.data) &&
                   EqualityComparer<Metadata>.Default.Equals(metadata, binary.metadata) &&
                   name == binary.name;
        }

        public override int GetHashCode()
        {
            int hashCode = -1428229477;
            hashCode = hashCode * -1521134295 + EqualityComparer<byte[]>.Default.GetHashCode(data);
            hashCode = hashCode * -1521134295 + EqualityComparer<Metadata>.Default.GetHashCode(metadata);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(name);
            return hashCode;
        }

        public Metadata Metadata()
        {
            return metadata;
        }

        public string Name()
        {
            return name;
        }

        public long Size()
        {
            return data.Length;
        }
    }
}