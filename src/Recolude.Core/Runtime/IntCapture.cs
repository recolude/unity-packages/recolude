using UnityEngine;

namespace Recolude.Core
{

    public class IntCapture : Capture<int>
    {

        public IntCapture(float time, int i) : base(time, i)
        {
        }

        public override ICapture SetTime(float newTime)
        {
            return new IntCapture(newTime, Value);
        }
        
        public override Capture<int> SetValue(int val)
        {
            return new IntCapture(Time, val);
        }

        public override string ToJSON()
        {
            return string.Format(
                "{{ \"Time\": {0}, \"Value\": {1} }}",
                Time,
                Value
            );
        }

        /// <summary>
        /// Builds a string that represents a single row in a csv file that contains this object's data.
        /// </summary>
        /// <returns>A row of csv data as a string.</returns>
        public override string ToCSV()
        {
            return string.Format(
                "{0}, {1}",
                Time,
                Value
            );
        }

        public override string ToString() {
            return $"{Time}: {Value})";
        }
    }

}