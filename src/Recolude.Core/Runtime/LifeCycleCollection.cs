
namespace Recolude.Core
{
    public class LifeCycleCollection : CaptureCollection<Capture<UnityLifeCycleEvent>>, IEnumCollection
    {
        static readonly string[] states = new string[] {
            "START", "ENABLE", "DISABLE", "DESTROY"
        };

        Capture<int>[] enumIndexCaptures;

        private Capture<int>[] ToEnumIndexes()
        {
            if (enumIndexCaptures == null)
            {
                enumIndexCaptures = new Capture<int>[captures.Length];
                for (int i = 0; i < captures.Length; i++)
                {
                    enumIndexCaptures[i] = new IntCapture(captures[i].Time, (int)captures[i].Value);
                }
            }

            return enumIndexCaptures;
        }

        Capture<UnityLifeCycleEvent>[] original;

        public LifeCycleCollection(string name, Capture<UnityLifeCycleEvent>[] captures) : base(name, captures)
        {
            this.original = captures;
        }

        public LifeCycleCollection(Capture<UnityLifeCycleEvent>[] captures) : base(CollectionNames.LifeCycle, captures)
        {
        }

        new public Capture<UnityLifeCycleEvent> this[int i]
        {
            get { return (Capture<UnityLifeCycleEvent>)original[i]; }
        }

        public override string Signature => "recolude.enum";

        Capture<int>[] ICaptureCollection<Capture<int>>.Captures => ToEnumIndexes();

        public string[] EnumMembers => states;
    }
}