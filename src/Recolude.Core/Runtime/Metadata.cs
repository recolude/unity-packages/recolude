using System;
using System.Collections.Generic;
using Recolude.Core.Properties;
using System.Collections;
using System.IO;
using System.Text;
using UnityEngine;
using Recolude.Core.IO;
using Recolude.Core.IO.PropertyWriters;

namespace Recolude.Core
{
    /// <summary>
    /// A helper class that's just a block of key value pairs that map string 
    /// keys to different types of data.
    /// </summary>
    public class Metadata : IEnumerable<KeyValuePair<string, IProperty>>
    {
        private readonly Dictionary<string, IProperty> data;

        public Metadata()
        {
            data = new Dictionary<string, IProperty>();
        }

        public Metadata(Dictionary<string, IProperty> data)
        {
            this.data = data ?? new Dictionary<string, IProperty>();
        }

        public Metadata(Dictionary<string, string> basicData)
        {
            data = new Dictionary<string, IProperty>();
            if (basicData == null)
            {
                return;
            }

            foreach (var keyVal in basicData)
            {
                data[keyVal.Key] = new Property<string>(keyVal.Value);
            }
        }

        public IProperty this[string key]
        {
            get => data[key];
            set => data[key] = value;
        }

        public static Metadata Read(BinaryUtilReader reader)
        {
            string[] keys = reader.StringArray();
            var propMapping = new Dictionary<string, IProperty>();
            foreach (var key in keys)
            {
                propMapping[key] = UnpackagerV2.ReadProperty(reader);
            }

            return new Metadata(propMapping);
        }

        public void Write(BinaryWriter writer)
        {
            RAPBinary.WriteUVarInt(writer, data.Count);

            string[] keys = new string[data.Count];
            int i = 0;
            foreach (var keyValue in data)
            {
                keys[i] = keyValue.Key;
                var keyBytes = Encoding.UTF8.GetBytes(keyValue.Key);
                RAPBinary.WriteUVarInt(writer, keyBytes.Length);
                writer.Write(keyBytes);
                i++;
            }

            foreach (var key in keys)
            {
                PropertyWriterCollection.Instance.Write(writer, data[key]);
            }
        }


        public static Metadata Read(BinaryUtilReader reader, string[] metadataKeys)
        {
            var propMapping = new Dictionary<string, IProperty>();
            var keyIndices = reader.UVarIntArray();
            for (var i = 0; i < keyIndices.Length; i++)
            {
                propMapping[metadataKeys[keyIndices[i]]] = UnpackagerV2.ReadProperty(reader);
            }

            return new Metadata(propMapping);
        }

        public bool IsString(string key)
        {
            if (!data.ContainsKey(key))
            {
                return false;
            }

            return data[key] is IProperty<string>;
        }

        public string AsString(string key)
        {
            return (data[key] as IProperty<string>).Value;
        }

        public bool IsInt(string key)
        {
            if (!data.ContainsKey(key))
            {
                return false;
            }

            return data[key] is IProperty<int>;
        }

        public int AsInt(string key)
        {
            return (data[key] as IProperty<int>).Value;
        }

        public bool AsBool(string key)
        {
            return (data[key] as IProperty<bool>).Value;
        }


        public bool IsFloat(string key)
        {
            if (!data.ContainsKey(key))
            {
                return false;
            }

            return data[key] is IProperty<float>;
        }

        public float AsFloat(string key)
        {
            return (data[key] as IProperty<float>).Value;
        }

        public byte AsByte(string key)
        {
            return (data[key] as IProperty<byte>).Value;
        }

        public bool IsType<T>(string key)
        {
            if (!data.ContainsKey(key))
            {
                return false;
            }

            return data[key] is IProperty<T>;
        }

        public T AsType<T>(string key)
        {
            return (data[key] as IProperty<T>).Value;
        }

        public Vector3 AsVector3(string key)
        {
            return (data[key] as IProperty<Vector3>).Value;
        }

        public Metadata AsMetadata(string key)
        {
            return (data[key] as IProperty<Metadata>).Value;
        }

        public DateTime AsTime(string key)
        {
            return (data[key] as IProperty<DateTime>).Value;
        }

        public Vector2 AsVector2(string key)
        {
            return (data[key] as IProperty<Vector2>).Value;
        }

        public string[] AsStringArray(string key)
        {
            return (data[key] as ArrayProperty<string>).Value;
        }

        public int[] AsIntArray(string key)
        {
            return (data[key] as ArrayProperty<int>).Value;
        }

        public float[] AsFloatArray(string key)
        {
            return (data[key] as ArrayProperty<float>).Value;
        }

        public bool[] AsBoolArray(string key)
        {
            return (data[key] as ArrayProperty<bool>).Value;
        }

        public byte[] AsByteArray(string key)
        {
            return (data[key] as ArrayProperty<byte>).Value;
        }

        public Vector2[] AsVector2Array(string key)
        {
            return (data[key] as ArrayProperty<Vector2>).Value;
        }

        public Vector3[] AsVector3Array(string key)
        {
            return (data[key] as ArrayProperty<Vector3>).Value;
        }

        public DateTime[] AsTimeArray(string key)
        {
            return (data[key] as ArrayProperty<DateTime>).Value;
        }

        public Metadata[] AsMetadataArray(string key)
        {
            return (data[key] as ArrayProperty<Metadata>).Value;
        }

        public bool IsVector3(string key)
        {
            return data[key] is IProperty<Vector3>;
        }

        public bool IsVector2(string key)
        {
            return data[key] is IProperty<Vector2>;
        }

        public bool ContainsKey(string key)
        {
            return data.ContainsKey(key);
        }

        public void Reset()
        {
            data.Clear();
        }

        public Dictionary<string, string> ToSimpleDictionary()
        {
            var results = new Dictionary<string, string>();
            foreach (var keyval in data)
            {
                results[keyval.Key] = keyval.Value.ToString();
            }

            return results;
        }

        public IEnumerator<KeyValuePair<string, IProperty>> GetEnumerator()
        {
            return data.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return data.GetEnumerator();
        }

        public override bool Equals(object obj)
        {
            var metadata = obj as Metadata;
            if (obj == null)
            {
                return false;
            }

            var dict1 = data;
            var dict2 = metadata.data;

            if (dict1 == dict2) return true;
            if ((dict1 == null) || (dict2 == null)) return false;
            if (dict1.Count != dict2.Count) return false;

            var valueComparer = EqualityComparer<IProperty>.Default;

            foreach (var kvp in dict1)
            {
                IProperty value2;
                if (!dict2.TryGetValue(kvp.Key, out value2)) return false;
                if (!valueComparer.Equals(kvp.Value, value2)) return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = 263693289;
            hashCode = hashCode * -1521134295 +
                       EqualityComparer<Dictionary<string, IProperty>>.Default.GetHashCode(data);
            hashCode = hashCode * -1521134295 + Count.GetHashCode();
            return hashCode;
        }

        public int Count
        {
            get => data.Count;
        }
    }
}