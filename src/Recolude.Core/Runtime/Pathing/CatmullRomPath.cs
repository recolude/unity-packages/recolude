﻿using UnityEngine;

namespace Recolude.Core.Pathing
{
    public class CatmullRomPath : Path
    {
        static Vector3 GetCatmullRomPosition(float t, float tension, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            Vector3 a = 2f * p1;
            Vector3 b = p2 - p0;
            Vector3 c = 2f * p0 - 5f * p1 + 4f * p2 - p3;
            Vector3 d = -p0 + 3f * p1 - 3f * p2 + p3;

            //The cubic polynomial: a + b * t + c * t^2 + d * t^3
            var tt = t * t;
            Vector3 pos = tension * (a + (b * t) + (c * tt) + (d * (tt * t)));

            return pos;
        }

        private readonly float recordingDuration;

        private readonly float tension;

        private int cursor;

        private Capture<Vector3> p0;

        private Capture<Vector3> p1;

        private Capture<Vector3> p2;

        private Capture<Vector3> p3;

        private float currentDuration;

        public CatmullRomPath(Capture<Vector3>[] points, float tension = 0.5f) : base(points)
        {
            recordingDuration = points[points.Length - 1].Time - points[0].Time;
            currentTime = points[0].Time;
            this.tension = tension;
            UpdateCursor(0);
        }

        private void UpdateCursor(int newCursor)
        {
            cursor = Mathf.Clamp(newCursor, 0, points.Length - 1);
            if (cursor > points.Length - 3)
            {
                // We're  in the horrible situation of needing to fake an extra
                // point to make the math work out 
                p0 = points[cursor - 1];
                p1 = points[cursor + 0];
                p2 = points[cursor + 1];

                var pos = (p1.Value - p0.Value) + p1.Value;
                var time = (p1.Time - p0.Time) + p1.Time;
                p3 = new VectorCapture(time, pos);
            }
            else if (cursor == 0)
            {
                // We're  in the horrible situation of needing to fake an extra
                // point to make the math work out 
                p1 = points[cursor + 0];
                p2 = points[cursor + 1];
                p3 = points[cursor + 2];

                var pos = (p1.Value - p2.Value) + p1.Value;
                var time = (p1.Time - p2.Time) + p1.Time;
                p0 = new VectorCapture(time, pos);
            }
            else
            {
                p0 = points[cursor - 1];
                p1 = points[cursor + 0];
                p2 = points[cursor + 1];
                p3 = points[cursor + 2];
            }

            currentDuration = p2.Time - p1.Time;
        }

        protected override void OnTimeChange()
        {
            if (currentTime > p2.Time)
            {
                UpdateCursor(cursor + 1);
            }
            else if (currentTime < p1.Time)
            {
                UpdateCursor(cursor - 1);
            }
        }

        public override Vector3 Position()
        {
            return GetCatmullRomPosition(
                (currentTime - p1.Time) / currentDuration,
                tension,
                p0.Value,
                p1.Value,
                p2.Value,
                p3.Value
            );
        }

        public override Vector3 AtTime(float time)
        {
            throw new System.NotImplementedException();
        }
    }
}