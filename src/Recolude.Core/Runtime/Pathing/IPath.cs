﻿using UnityEngine;

namespace Recolude.Core.Pathing
{
    public interface IPath
    {
        void Increment(float delta);

        Vector3 Position();

        Vector3 AtTime(float time);

        float Duration { get; }
    }
}