﻿using UnityEngine;

namespace Recolude.Core.Pathing
{
    public class LinearPath : Path
    {
        private int cursor;

        private Capture<Vector3> p0;

        private Capture<Vector3> p1;

        private float currentDuration;

        public LinearPath(Capture<Vector3>[] points) : base(points)
        {
            currentTime = points[0].Time;
            UpdateCursor(0);
        }

        private void UpdateCursor(int newCursor)
        {
            cursor = Mathf.Clamp(newCursor, 0, points.Length - 2);
            p0 = points[cursor + 0];
            p1 = points[cursor + 1];
            currentDuration = p1.Time - p0.Time;
        }

        protected override void OnTimeChange()
        {
            if (currentTime > p1.Time)
            {
                UpdateCursor(cursor + 1);
            }
            else if (currentTime < p0.Time)
            {
                UpdateCursor(cursor - 1);
            }
        }

        public override Vector3 Position()
        {
            var t = (currentTime - p0.Time) / currentDuration;
            return ((p1.Value - p0.Value) * t) + p0.Value;
        }

        public override Vector3 AtTime(float time)
        {
            var i = IndexBeforeTimestamp(time);

            if (i == points.Length-1)
            {
                return points[i].Value;
            }
            
            var left = points[i];
            var right = points[i+1];

            var p = (time - left.Time) / (right.Time - left.Time);
            return Vector3.Lerp(left.Value, right.Value, p);
        }
    }
}