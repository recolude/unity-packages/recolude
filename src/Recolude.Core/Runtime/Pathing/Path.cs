﻿using System;
using JetBrains.Annotations;
using UnityEngine;

namespace Recolude.Core.Pathing
{
    public abstract class Path : IPath
    {
        protected readonly Capture<Vector3>[] points;

        protected float currentTime;

        public float Duration { get; }

        protected Path([NotNull] Capture<Vector3>[] points)
        {
            if (points == null) throw new ArgumentNullException(nameof(points));
            if (points.Length == 0) throw new ArgumentException("Value cannot be an empty collection.", nameof(points));

            this.points = points;
            Duration = points[points.Length - 1].Time - points[0].Time;
        }

        public void Increment(float delta)
        {
            currentTime = Mathf.Clamp(
                currentTime + delta,
                points[0].Time,
                points[points.Length - 1].Time
            );
            OnTimeChange();
        }

        protected int IndexBeforeTimestamp(float time)
        {
            if (points.Length == 1)
            {
                return 0;
            }

            for (int i = 1; i < points.Length; i++)
            {
                if (points[i].Time < time) continue;
                return i - 1;
            }

            return points.Length - 1;
        }

        protected abstract void OnTimeChange();
        public abstract Vector3 Position();
        public abstract Vector3 AtTime(float time);
    }
}