﻿using UnityEngine;

namespace Recolude.Core.Playback
{
    /// <summary>
    /// Actor Builder Config serves as a base for defining how actors within a
    /// Recolude playback should be constructed across a project.
    /// </summary>
    public abstract class ActorBuilderConfig : ScriptableObject, IActorBuilder
    {
        protected const string MenuPath = "Recolude/Scene System/Playback/Actor Builders/";

        public abstract IActor[] Build(IRecording recording);
    }
}