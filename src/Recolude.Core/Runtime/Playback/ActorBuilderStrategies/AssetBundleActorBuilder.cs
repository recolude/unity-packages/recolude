﻿using System.Collections.Generic;
using UnityEngine;

namespace Recolude.Core.Playback.ActorBuilderStrategies
{

    /// <summary>
    /// This Actor Builder strategy attempts use the recording's name to look 
    /// up a game object inside the asset bundle provided and use that as the
    /// actor representation in playback.
    /// </summary>
    public class AssetBundleActorBuilder : IActorBuilder
    {
        private AssetBundle bundleToLoadFrom;

        private GameObject fallback;

        private IPlaybackCustomEventHandler customEventHandler;

        /// <summary>
        /// Builds a actor builder that loads actor representation from asset bundles. If the asset bundle fails to load the desired actor representation, then no actor will be used in playback. 
        /// </summary>
        /// <returns>A actor builder that loads actors from asset bundles.</returns>
        /// <param name="bundleToLoadFrom">The asset bundle to pull actors from</param>
        /// <exception cref="System.ArgumentException">Thrown when the bundle to load from is null.</exception>
        public AssetBundleActorBuilder(AssetBundle bundleToLoadFrom) : this(bundleToLoadFrom, null, null)
        {
        }

        /// <summary>
        /// Builds a actor builder that loads actor representation from asset bundles. If the asset bundle fails to load the desired actor representation, then the fallback object provided will be instantiated to be used. 
        /// </summary>
        /// <param name="bundleToLoadFrom">The asset bundle to pull actors from</param>
        /// <param name="fallback">Object to use whenever the asset bundles fails to give us anything</param>
        /// <param name="customEventHandler">The event handler the actors will use</param>
        /// <exception cref="System.ArgumentException">Thrown when the bundle to load from is null.</exception>
        public AssetBundleActorBuilder(AssetBundle bundleToLoadFrom, GameObject fallback, IPlaybackCustomEventHandler customEventHandler)
        {
            if (bundleToLoadFrom == null)
            {
                throw new System.ArgumentException("need a bundle to load actors from", "bundleToLoadFrom");
            }
            this.bundleToLoadFrom = bundleToLoadFrom;
            this.customEventHandler = customEventHandler;
            this.fallback = fallback;
        }

        public IActor[] Build(IRecording recording)
        {
            List<IActor> actors = new List<IActor>(recording.Recordings.Length);
            for (int i = 0; i < recording.Recordings.Length; i++)
            {
                var child = recording.Recordings[i];
                if (bundleToLoadFrom.Contains(child.Name))
                {
                    actors.Add(new GameObjectActor(child, GameObject.Instantiate(bundleToLoadFrom.LoadAsset<GameObject>(child.Name)), customEventHandler));
                }
                else if (fallback != null)
                {
                    actors.Add(new GameObjectActor(child, GameObject.Instantiate(fallback), customEventHandler));
                }
            }
            return actors.ToArray();
        }

    }

}