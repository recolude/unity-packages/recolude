using System.Collections.Generic;
using UnityEngine;

namespace Recolude.Core.Playback.ActorBuilderStrategies
{
    /// <summary>
    /// Actor builder strategy that tries to match the subject's name to a key in the dictionary for loading actors. If a key exists for an actor, but the value is null, an error is thrown.
    /// </summary>
    public class DictionaryActorBuilder : IActorBuilder
    {
        private readonly IDictionary<string, GameObject> mapping;

        private readonly GameObject fallback;

        private readonly IPlaybackCustomEventHandler customEventHandler;

        /// <summary>
        /// Creates a actor builer that trys to match the subject's name to a key in the dictionary for loading actors.
        /// </summary>
        /// <param name="mapping">The dictionary we look up actor representations from.</param>
        /// <param name="fallback">Object to use whenever the dictionary fails to give us anything.</param>
        /// <param name="customEventHandler">The event handler the actors will use</param>
        public DictionaryActorBuilder(IDictionary<string, GameObject> mapping, GameObject fallback,
            IPlaybackCustomEventHandler customEventHandler)
        {
            this.mapping = mapping;
            this.fallback = fallback;
            this.customEventHandler = customEventHandler;
        }

        /// <summary>
        /// Creates a actor builer that trys to match the subject's name to a key in the dictionary for loading actors.
        /// </summary>
        /// <param name="mapping">The dictionary we look up actor representations from.</param>
        public DictionaryActorBuilder(IDictionary<string, GameObject> mapping) : this(mapping, null, null)
        {
        }

        public IActor[] Build(IRecording recording)
        {
            List<IActor> actors = new List<IActor>(recording.Recordings.Length);

            for (int i = 0; i < recording.Recordings.Length; i++)
            {
                var child = recording.Recordings[i];
                if (mapping.ContainsKey(child.Name))
                {
                    if (mapping[child.Name] == null)
                    {
                        throw new System.InvalidOperationException(
                            "A mapping for the actor exists but the value is null.");
                    }

                    actors.Add(new GameObjectActor(child, GameObject.Instantiate(mapping[child.Name]),
                        customEventHandler));
                }
                else if (fallback != null)
                {
                    actors.Add(new GameObjectActor(child, GameObject.Instantiate(fallback), customEventHandler));
                }
            }

            return actors.ToArray();
        }
    }
}