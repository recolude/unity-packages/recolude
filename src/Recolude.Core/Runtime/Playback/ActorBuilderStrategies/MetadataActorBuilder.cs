using System.Collections.Generic;
using UnityEngine;

namespace Recolude.Core.Playback.ActorBuilderStrategies
{

    /// <summary>
    /// Mimics what [Recolude's webplayer](https://app.recolude.com/) does. 
    /// Read more about [Actor Builders](xref:Recolude.Core.Playback.IActorBuilder) 
    /// to learn how to build your own. 
    /// </summary>
    public class MetadataActorBuilder : IActorBuilder
    {

        private static readonly string RecoludeScale = "recolude-scale";

        private static readonly string RecoludeOffset = "recolude-offset";

        private static readonly string RecoludeColor = "recolude-color";

        private static readonly string RecoludeGeom = "recolude-geom";

        private static Vector3 ParseVectorString(string vec3string)
        {
            var components = vec3string.Split(',');
            if (components.Length != 3)
            {
                return new Vector3(1, 1, 1);
            }

            var x = float.Parse(components[0]);
            var y = float.Parse(components[1]);
            var z = float.Parse(components[2]);

            return new Vector3(x, y, z);
        }

        private GameObject ApplyScaling(Metadata metadata, GameObject gameObject)
        {
            if (metadata.ContainsKey(RecoludeScale))
            {
                if (metadata.IsString(RecoludeScale))
                {
                    gameObject.transform.localScale = ParseVectorString(metadata.AsString(RecoludeScale));
                }
                else if (metadata.IsVector3(RecoludeScale))
                {
                    gameObject.transform.localScale = metadata.AsVector3(RecoludeScale);
                }
                else
                {
                    throw new System.Exception($"{RecoludeScale} is present but is not a string or vector3");
                }
            }
            return gameObject;
        }

        private GameObject ApplyOffset(Metadata metadata, GameObject gameObject)
        {
            if (metadata.ContainsKey(RecoludeScale))
            {
                Vector3 offset;
                if (metadata.IsString(RecoludeOffset))
                {
                    offset = ParseVectorString(metadata.AsString(RecoludeOffset));
                }
                else if (metadata.IsVector3(RecoludeOffset))
                {
                    offset = metadata.AsVector3(RecoludeOffset);
                }
                else
                {
                    throw new System.Exception($"{RecoludeOffset} is present but is not a string or vector3");
                }
                gameObject.transform.position += offset;
                GameObject parent = new GameObject();
                gameObject.transform.SetParent(parent.transform);
                return parent;
            }
            return gameObject;
        }

        private GameObject ApplyColor(Metadata metadata, GameObject gameObject)
        {
            if (metadata.ContainsKey(RecoludeColor))
            {
                if (metadata.IsString(RecoludeColor))
                {
                    Color color = Color.white;
                    ColorUtility.TryParseHtmlString(metadata.AsString(RecoludeColor), out color);
                    gameObject.GetComponent<MeshRenderer>().material.color = color;
                }
                else
                {
                    throw new System.Exception($"{RecoludeColor} is present but is not a string");
                }
            }

            return gameObject;
        }

        private GameObject ApplyGeom(Metadata metadata)
        {
            if (metadata.ContainsKey(RecoludeGeom))
            {
                if (metadata.IsString(RecoludeGeom))
                {
                    switch (metadata.AsString(RecoludeGeom).ToLower())
                    {
                        case "cube":
                            return GameObject.CreatePrimitive(PrimitiveType.Cube);

                        case "sphere":
                            return GameObject.CreatePrimitive(PrimitiveType.Sphere);

                        case "plane":
                            return GameObject.CreatePrimitive(PrimitiveType.Plane);

                        case "capsule":
                            return GameObject.CreatePrimitive(PrimitiveType.Capsule);

                        default:
                            return GameObject.CreatePrimitive(PrimitiveType.Cube);
                    }
                }
                else
                {
                    throw new System.Exception($"{RecoludeGeom} is present but is not a string");
                }
            }

            return GameObject.CreatePrimitive(PrimitiveType.Cube);
        }

        private GameObject FromMetadata(Metadata metadata)
        {
            GameObject gameObject = ApplyGeom(metadata);
            gameObject = ApplyScaling(metadata, gameObject);
            gameObject = ApplyColor(metadata, gameObject);
            gameObject = ApplyOffset(metadata, gameObject);
            return gameObject;
        }

        public IActor[] Build(IRecording rootRecording)
        {
            IActor[] results = new IActor[rootRecording.Recordings.Length];
            for (int i = 0; i < rootRecording.Recordings.Length; i++)
            {
                var child = rootRecording.Recordings[i];
                results[i] = new GameObjectActor(child, FromMetadata(child.Metadata));
            }
            return results;
        }

    }

}