using System.Collections.Generic;
using UnityEngine;

namespace Recolude.Core.Playback.ActorBuilderStrategies
{

    /// <summary>
    /// Uses different [unity primitives](https://docs.unity3d.com/ScriptReference/PrimitiveType.html) to build actors.
    /// </summary>
    public class PrimitiveActorBuilder : IActorBuilder
    {

        private PrimitiveType primitiveType;

        private IPlaybackCustomEventHandler customEventHandler;

        private ActorCleanupMethod cleanupMethod;

        public PrimitiveActorBuilder(PrimitiveType primitiveType, ActorCleanupMethod cleanupMethod, IPlaybackCustomEventHandler customEventHandler)
        {
            this.primitiveType = primitiveType;
            this.cleanupMethod = cleanupMethod;
            this.customEventHandler = customEventHandler;
        }

        public PrimitiveActorBuilder(PrimitiveType primitiveType, ActorCleanupMethod cleanupMethod) : this(primitiveType, cleanupMethod, null)
        {
        }

        public IActor[] Build(IRecording rootRecording)
        {
            var parent = new GameObject(rootRecording.Name);
            List<IActor> actors = new List<IActor>();

            foreach (var child in rootRecording.Recordings)
            {
                var primitive = GameObject.CreatePrimitive(primitiveType);
                primitive.transform.SetParent(parent.transform);
                actors.Add(new GameObjectActor(child, primitive, customEventHandler, cleanupMethod));
            }

            actors.Add(new GameObjectActor(rootRecording, parent, cleanupMethod));

            return actors.ToArray();
        }

    }

}