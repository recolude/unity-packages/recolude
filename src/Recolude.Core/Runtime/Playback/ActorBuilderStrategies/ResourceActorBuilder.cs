using System.Collections.Generic;
using UnityEngine;

namespace Recolude.Core.Playback.ActorBuilderStrategies
{
    /// <summary>
    /// This Actor Builder strategy attempts to look up the recorded subject's name inside a resources folder and use
    /// that as the actor representation in playback. WARNING: Unity recommends avoiding the use of the resources
    /// folder. Consider using the AssetBundleActorBuilder.
    /// </summary>
    public class ResourceActorBuilder : IActorBuilder
    {
        private readonly string pathPrefix;

        private readonly GameObject fallback;

        private readonly IPlaybackCustomEventHandler customEventHandler;

        /// <summary>
        /// Creates a Actor Builder that loads actor representations from the resource's folder in the unity project.
        /// </summary>
        /// <param name="pathPrefix">String that get's prepended to the recorded subject's name when loading from the resources folder</param>
        /// <param name="fallback">Object to use whenever the resource folder fails to give us anything</param>
        /// <param name="customEventHandler">The event handler the actors will use</param>
        public ResourceActorBuilder(string pathPrefix, GameObject fallback,
            IPlaybackCustomEventHandler customEventHandler)
        {
            this.pathPrefix = pathPrefix;
            this.fallback = fallback;
            this.customEventHandler = customEventHandler;
        }

        /// <summary>
        /// Creates a Actor Builder that loads actor representations from the resource's folder in the unity project.
        /// </summary>
        /// <param name="pathPrefix">String that get's prepended to the recorded subject's name when loading from the resources folder</param>
        public ResourceActorBuilder(string pathPrefix) : this(pathPrefix, null, null)
        {
        }

        /// <summary>
        /// Creates a Actor Builder that loads actor representations from the resource's folder in the unity project.
        /// </summary>
        /// <param name="fallback">Object to use whenever the resource folder fails to give us anything</param>
        /// <returns></returns>
        public ResourceActorBuilder(GameObject fallback) : this("", fallback, null)
        {
        }

        /// <summary>
        /// Creates a Actor Builder that loads actor representations from the resource's folder in the unity project.
        /// </summary>
        public ResourceActorBuilder() : this("", null, null)
        {
        }

        public IActor[] Build(IRecording recording)
        {
            List<IActor> actors = new List<IActor>(recording.Recordings.Length);

            for (int i = 0; i < recording.Recordings.Length; i++)
            {
                var child = recording.Recordings[i];

                var path = child.Name;
                if (string.IsNullOrEmpty(pathPrefix) == false)
                {
                    path = string.Format("{0}/{1}", pathPrefix, child.Name);
                }

                GameObject reference = Resources.Load<GameObject>(path);

                if (reference != null)
                {
                    actors.Add(new GameObjectActor(child, GameObject.Instantiate(reference), customEventHandler));
                }
                else if (fallback != null)
                {
                    actors.Add(new GameObjectActor(child, GameObject.Instantiate(fallback), customEventHandler));
                }
            }

            return actors.ToArray();
        }
    }
}