namespace Recolude.Core.Playback
{

    /// <summary>
    /// Method in which actor representations get deleted from the scene their 
    /// in.
    /// </summary>
    public enum ActorCleanupMethod
    {
        NoCleanup,
        Destroy,
        DestroyImmediate
    }

}