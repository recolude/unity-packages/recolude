namespace Recolude.Core.Playback.CollectionControllers
{
    /// <summary>
    /// Plays back events that occur under continuous time conditions. 
    /// </summary>
    public class CustomEventPlayback : CollectionPlaybackController<Capture<CustomEvent>>
    {
        IPlaybackCustomEventHandler[] customEventHandlers;

        IRecording recording;

        public CustomEventPlayback(IRecording recording, IPlaybackCustomEventHandler[] customEventHandlers, Capture<CustomEvent>[] captures) : base(captures)
        {
            this.recording = recording;
            this.customEventHandlers = customEventHandlers;
        }

        public override void MoveTo(TimeMovementDetails movementDetails)
        {
            PlayEventsThatTranspired(movementDetails);
        }


        private void PlayEventsThatTranspired(TimeMovementDetails movementDetails)
        {
            if (customEventHandlers == null)
            {
                return;
            }

            if (movementDetails.DeltaTime == 0)
            {
                return;
            }

            if (movementDetails.MovementType != MovementType.Continuous)
            {
                return;
            }

            foreach (var customEvent in captures)
            {
                if (customEvent.FallsWithin(movementDetails.LastTime, movementDetails.CurrentTime))
                {
                    foreach (var handler in customEventHandlers)
                    {
                        handler.OnCustomEvent(recording, movementDetails, customEvent);
                    }
                }
            }
        }

        public override void OnPause() { }

        public override void OnPlay() { }

        public override void OnStop() { }
    }

}