using UnityEngine;

namespace Recolude.Core.Playback.CollectionControllers
{
    public abstract class InterpolatePlayback<T> : CollectionPlaybackController<T> where T : ICapture
    {
        private int lastCaptureIndex;

        public InterpolatePlayback(T[] captures) : base(captures)
        {
            lastCaptureIndex = 0;
        }

        public abstract void Interpolate(T start, T end, float progress);

        public override void MoveTo(TimeMovementDetails movementDetails)
        {
            if (captures == null || captures.Length == 0 || movementDetails.Direction == 0)
            {
                return;
            }

            var currentCaptureIndex = CaptureIndex(movementDetails.CurrentTime, movementDetails.Direction, lastCaptureIndex);
            var currentCapture = captures[currentCaptureIndex];
            var nextCapture = captures[Mathf.Clamp(currentCaptureIndex + movementDetails.Direction, 0, captures.Length - 1)];

            Interpolate(currentCapture, nextCapture, ProgressThroughOrientation(currentCaptureIndex, movementDetails.CurrentTime, movementDetails.Direction));

            lastCaptureIndex = currentCaptureIndex;
        }

        public override void OnPause()
        {
        }

        public override void OnPlay()
        {
        }

        public override void OnStop()
        {
        }

        private float ProgressThroughOrientation(int orientationIndex, float time, int dir)
        {
            int nextOrientation = Mathf.Clamp(orientationIndex + dir, 0, captures.Length - 1);
            if (nextOrientation == orientationIndex)
            {
                return 1f;
            }
            return (time - captures[orientationIndex].Time) / FrameDuration(orientationIndex, nextOrientation);
        }

        private float FrameDuration(int frameIndex, int nextFrameIndex)
        {
            return captures[nextFrameIndex].Time - captures[frameIndex].Time;
        }
    }
}