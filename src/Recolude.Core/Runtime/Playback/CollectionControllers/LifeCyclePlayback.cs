using UnityEngine;

namespace Recolude.Core.Playback.CollectionControllers
{
    public class LifeCyclePlayback : CollectionPlaybackController<Capture<UnityLifeCycleEvent>>
    {
        GameObject representation;

        private bool hasStartEvent;

        public LifeCyclePlayback(GameObject representation, Capture<UnityLifeCycleEvent>[] captures) : base(captures)
        {
            this.representation = representation;
            hasStartEvent = HasStartEvent();
        }

        public LifeCyclePlayback(Transform representation, Capture<UnityLifeCycleEvent>[] captures) : this(representation.gameObject, captures)
        {
        }

        public override void MoveTo(TimeMovementDetails movementDetails)
        {
            if (movementDetails.Direction == 0)
            {
                return;
            }
            var shouldDisable = DisableDueToLastLifeCycleEvent(LastLifeCycleEvent(movementDetails.CurrentTime, movementDetails.Direction));
            representation.SetActive(shouldDisable == false);
        }

        public override void OnPause() { }

        public override void OnPlay() { }

        public override void OnStop() { }


        private bool DisableDueToLastLifeCycleEvent(Capture<UnityLifeCycleEvent> e)
        {
            if (e == null && hasStartEvent)
            {
                return true;
            }
            return e != null && (e.Value == UnityLifeCycleEvent.Disable || e.Value == UnityLifeCycleEvent.Destroy);
        }

        private bool HasStartEvent()
        {
            for (int i = 0; i < captures.Length; i++)
            {
                if (captures[i].Value == UnityLifeCycleEvent.Start)
                {
                    return true;
                }
            }
            return false;
        }

        private Capture<UnityLifeCycleEvent> LastLifeCycleEvent(float newTime, int direction)
        {
            if (direction == 0)
            {
                throw new System.Exception("You can't adjust to lifecycle events when time isn't moving");
            }

            Capture<UnityLifeCycleEvent> lastEvent = null;
            for (int i = 0; i < captures.Length; i++)
            {
                if (newTime < captures[i].Time)
                {
                    return lastEvent;
                }
                lastEvent = captures[i];
            }

            return lastEvent;
        }
    }
}