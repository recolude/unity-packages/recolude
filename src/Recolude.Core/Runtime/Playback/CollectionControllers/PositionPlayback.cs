using UnityEngine;

namespace Recolude.Core.Playback.CollectionControllers
{
    public class PositionPlayback : InterpolatePlayback<Capture<Vector3>>
    {
        Transform representation;

        public PositionPlayback(Transform representation, Capture<Vector3>[] captures) : base(captures)
        {
            this.representation = representation;
        }

        public PositionPlayback(GameObject representation, Capture<Vector3>[] captures) : base(captures)
        {
            this.representation = representation.transform;
        }

        public override void Interpolate(Capture<Vector3> start, Capture<Vector3> end, float progress)
        {
            var newPos = Vector3.Lerp(start.Value, end.Value, progress);
            if (representation.parent != null)
            {
                newPos.Scale(representation.parent.localScale);
            }
            representation.localPosition = newPos;
        }
    }
}