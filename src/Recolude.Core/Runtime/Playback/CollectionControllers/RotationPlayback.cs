using UnityEngine;

namespace Recolude.Core.Playback.CollectionControllers
{
    public class RotationPlayback : InterpolatePlayback<Capture<Vector3>>
    {
        Transform representation;

        public RotationPlayback(Transform representation, Capture<Vector3>[] captures) : base(captures)
        {
            this.representation = representation;
        }

        public RotationPlayback(GameObject representation, Capture<Vector3>[] captures) : base(captures)
        {
            this.representation = representation.transform;
        }

        public override void Interpolate(Capture<Vector3> start, Capture<Vector3> end, float progress)
        {
            representation.localRotation = Quaternion.Slerp(
                Quaternion.Euler(start.Value),
                Quaternion.Euler(end.Value),
                progress
            );
        }
    }

}