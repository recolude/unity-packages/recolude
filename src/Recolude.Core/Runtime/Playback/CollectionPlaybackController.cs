using UnityEngine;

namespace Recolude.Core.Playback
{
    public abstract class CollectionPlaybackController<T> : IPlaybackController where T : ICapture
    {
        protected readonly T[] captures;

        private float lastTimeThroughPlayback;

        public CollectionPlaybackController(T[] captures)
        {
            this.lastTimeThroughPlayback = 0;
            this.captures = captures;
        }

        public abstract void MoveTo(TimeMovementDetails movementDetails);

        public void MoveTo(MovementType movementType, float newTime)
        {
            float deltaTime = newTime - lastTimeThroughPlayback;
            int direction = CalculateDirection(deltaTime);
            MoveTo(new TimeMovementDetails(movementType, direction, deltaTime, newTime));
            lastTimeThroughPlayback = newTime;
        }

        public abstract void OnPause();

        public abstract void OnPlay();

        public abstract void OnStop();

        protected static int CalculateDirection(float deltaTime)
        {
            if (deltaTime > 0)
            {
                return 1;
            }
            if (deltaTime < 0)
            {
                return -1;
            }
            return 0;
        }

        protected int CaptureIndex(float time, int direction, int lastCaptureIndex)
        {
            if (direction == 1)
            {
                int greaterThan = lastCaptureIndex;
                for (int i = lastCaptureIndex; i < captures.Length; i++)
                {
                    if (time < captures[i].Time)
                    {
                        return Mathf.Clamp(i - 1, 0, captures.Length - 1);
                    }
                    greaterThan = i;
                }
                return greaterThan;
            }
            else if (direction == -1)
            {
                int greaterThan = lastCaptureIndex;
                for (int i = lastCaptureIndex; i >= 0; i--)
                {
                    if (time > captures[i].Time)
                    {
                        return Mathf.Clamp(i + 1, 0, captures.Length - 1);
                    }
                    greaterThan = i;
                }
                return greaterThan;
            }
            throw new System.Exception("Direction is 0");
        }

    }

}