using UnityEngine;
using System.Collections.Generic;
using Recolude.Core.Playback.CollectionControllers;

namespace Recolude.Core.Playback
{

    public class GameObjectActor : IActor
    {
        GameObject representation;

        IPlaybackCustomEventHandler customEventHandler;

        ActorCleanupMethod cleanupMethod;

        IPlaybackController[] controllers;

        public GameObjectActor(IRecording recording, GameObject representation, IPlaybackCustomEventHandler customEventHandler, ActorCleanupMethod cleanupMethod)
        {
            this.representation = representation;
            this.customEventHandler = customEventHandler;
            this.cleanupMethod = cleanupMethod;

            // Initialize starting position and rotation...
            List<IPlaybackController> potentialControllers = new List<IPlaybackController>();

            if (representation != null)
            {
                var lifecycleCollection = recording.LifeCycleCollection();
                if (lifecycleCollection != null)
                {
                    potentialControllers.Add(new LifeCyclePlayback(representation, lifecycleCollection.Captures));
                }

                var rotationCollection = recording.RotationCollection();
                if (rotationCollection != null)
                {
                    potentialControllers.Add(new RotationPlayback(representation, rotationCollection.Captures));
                }

                var positionCollection = recording.PositionCollection();
                if (positionCollection != null)
                {
                    potentialControllers.Add(new PositionPlayback(representation, positionCollection.Captures));
                }
            }

            if (customEventHandler != null)
            {
                var eventCollection = recording.CustomEventCollection();
                if (eventCollection != null)
                {
                    potentialControllers.Add(new CustomEventPlayback(recording, new [] { customEventHandler }, eventCollection.Captures));
                }
            }

            controllers = potentialControllers.ToArray();
        }

        public GameObjectActor(IRecording recording, GameObject representation, ActorCleanupMethod cleanupMethod) : this(recording, representation, null, cleanupMethod)
        {
        }

        public GameObjectActor(IRecording recording, GameObject representation, IPlaybackCustomEventHandler customEventHandler) : this(recording, representation, customEventHandler, ActorCleanupMethod.Destroy)
        {
        }


        public GameObjectActor(IRecording recording, GameObject representation) : this(recording, representation, null, ActorCleanupMethod.Destroy)
        {
        }

        public GameObjectActor(IRecording recording, IPlaybackCustomEventHandler handler) : this(recording, null, handler, ActorCleanupMethod.Destroy)
        {
        }

        public GameObject Representation { get => representation; }

        public IPlaybackController[] PlaybackControllers => controllers;

        public void CleanupResources()
        {
            switch (cleanupMethod)
            {
                case ActorCleanupMethod.Destroy:
                    GameObject.Destroy(representation);
                    break;

                case ActorCleanupMethod.DestroyImmediate:
                    GameObject.DestroyImmediate(representation);
                    break;
            }
        }
    }
}