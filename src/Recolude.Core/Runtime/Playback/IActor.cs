namespace Recolude.Core.Playback
{
    /// <summary>
    /// A logical grouping of playback controllers which all share some 
    /// resource that not one individual controller owns, requiring them to be
    /// cleaned up together. Used by the playback system.
    /// </summary>
    public interface IActor
    {
        /// <summary>
        /// Generally corresponds to one playback controller per capture
        /// collection found within the original recording, but there's no 
        /// requirement.
        /// </summary>
        /// <value>All playback controllers associated with the actor.</value>
        IPlaybackController[] PlaybackControllers { get; }

        /// <summary>
        /// Called by the playback system when the entire playback is 
        /// stopped.
        /// </summary>
        void CleanupResources();
    }
}