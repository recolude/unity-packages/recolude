namespace Recolude.Core.Playback
{
    /// <summary>
    /// Implement this interface if you want your class to be used for setting
    /// up the actors in the scene for playback.
    /// </summary>
    public interface IActorBuilder
    {
        /// <summary>
        /// Build 0..n actors given the information presented.
        /// </summary>
        /// <param name="recording">The recording to build actors for</param>
        /// <returns>Actors to represent the original subject.</returns>
        IActor[] Build(IRecording recording);
    }
}