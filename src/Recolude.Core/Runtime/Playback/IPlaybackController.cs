namespace Recolude.Core.Playback
{
    /// <summary>
    /// The bulk of the playback system operates by informing controllers of 
    /// the current state of the playback.
    /// 
    /// When adding a new Capture Collection type into the Recolude system, you
    /// might want to start here for adding playback functionality.
    /// </summary>
    public interface IPlaybackController
    {
        /// <summary>
        /// Called by the playback system upon the next "tick" of playback.
        /// Used in situations where time is progressing in a continuous 
        /// fashion.
        /// </summary>
        /// <param name="movement">
        /// The type of movement time that took place within playback's system.
        /// </param>
        /// <param name="newTime">
        /// The current time through the playback after the latest tick.
        /// </param>
        void MoveTo(MovementType movement, float newTime);

        /// <summary>
        /// Called by the playback system when the playback is paused.
        /// </summary>
        void OnPause();

        /// <summary>
        /// Called by the playback system when the playback is started.
        /// </summary>
        void OnPlay();

        /// <summary>
        /// Called by the playback system when the playback is stopped.
        /// </summary>
        void OnStop();
    }
}