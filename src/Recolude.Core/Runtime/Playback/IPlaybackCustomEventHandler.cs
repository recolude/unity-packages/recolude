
namespace Recolude.Core.Playback
{

    /// <summary>
    /// Implement this interface if you want your class to be used for 
    /// handling custom events that are contained in the recording.
    /// </summary>
    public interface IPlaybackCustomEventHandler 
    {
        /// <summary>
        /// Called while the playback of a recording is occuring when a custom
        /// event occurs.
        /// </summary>
        /// <param name="recording">The recording the event is related too.</param>
        /// <param name="movementDetails">Details on the time transpired during
        ///  the frame this event fired.
        /// </param>
        /// <param name="customEvent">The event that occurred.</param>
        void OnCustomEvent(IRecording recording, TimeMovementDetails movementDetails, Capture<CustomEvent> customEvent);
    }
}