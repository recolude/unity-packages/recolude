namespace Recolude.Core.Playback
{

    /// <summary>
    /// Helper class to control multiple actors with one call.
    /// </summary>
    internal class MetaActor : IPlaybackController, IActor
    {
        IActor[] actors;

        public MetaActor(IActor[] actors)
        {
            this.actors = actors;
        }

        public IPlaybackController[] PlaybackControllers => throw new System.NotImplementedException();

        public IActor[] Actors { get; internal set; }

        public void CleanupResources()
        {
            foreach (var actor in actors)
            {
                actor.CleanupResources();
            }
        }

        public void MoveTo(MovementType movement, float newTime)
        {
            foreach (var actor in actors)
            {
                foreach (var controller in actor.PlaybackControllers)
                {
                    controller.MoveTo(movement, newTime);
                }
            }
        }

        public void OnPause()
        {
            foreach (var actor in actors)
            {
                foreach (var controller in actor.PlaybackControllers)
                {
                    controller.OnPause();
                }
            }
        }

        public void OnPlay()
        {
            foreach (var actor in actors)
            {
                foreach (var controller in actor.PlaybackControllers)
                {
                    controller.OnPlay();
                }
            }
        }

        public void OnStop()
        {
            foreach (var actor in actors)
            {
                foreach (var controller in actor.PlaybackControllers)
                {
                    controller.OnStop();
                }
            }
        }
    }
}