namespace Recolude.Core.Playback
{

    /// <summary>
    /// Describes the progression of a value within a function.
    /// </summary>
    public enum MovementType
    {
        /// <summary>
        /// There was no sudden breaks or jumps in the progression of time.
        /// </summary>
        Continuous,

        /// <summary>
        /// There was a sudden jump or break in the progression of time.
        /// 
        /// One example of this being called is when the user choses
        /// to skip 10 seconds ahead in playback. Also called when the playback
        /// automaitically loops going from the very end of the recording back
        /// /// to the very begining.
        /// </summary>
        Discrete
    }

}