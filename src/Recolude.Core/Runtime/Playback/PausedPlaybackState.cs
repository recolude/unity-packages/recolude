
namespace Recolude.Core.Playback
{

    internal class PausedPlaybackState : PlaybackState
    {
        public PausedPlaybackState(PlaybackStateInfo playbackStateInfo) : base(playbackStateInfo)
        {
        }

        public override PlaybackState Pause()
        {
            return this;
        }

        public override PlaybackState Play()
        {
            return new PlayingPlaybackState(playbackStateInfo, playbackStateInfo.ShiftedTimeThroughPlayback());
        }

        public override PlaybackState Stop()
        {
            playbackStateInfo.ClearActors();
            return new StoppedPlaybackState(playbackStateInfo);
        }

        public override PlaybackState Update()
        {
            return this;
        }

        public override void SetTimeThroughPlayback(float time)
        {
            playbackStateInfo.SetTime(time);
            playbackStateInfo.GetActors().MoveTo(MovementType.Discrete, playbackStateInfo.ShiftedTimeThroughPlayback());
        }
    }

}