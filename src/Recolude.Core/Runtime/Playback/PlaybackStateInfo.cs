using UnityEngine;
using Recolude.Core.Time;

namespace Recolude.Core.Playback
{
    internal class PlaybackStateInfo
    {
        /// <summary>
        /// How many seconds have passed in the playback recording
        /// </summary>
        private float timeThroughPlayback = 0;

        /// <summary>
        /// The current loaded recording that we may or may not be playing
        /// </summary>
        private readonly IRecording recording = null;

        /// <summary>
        /// Function for building the actors of the playback. 
        /// </summary>
        private readonly IActorBuilder actorBuilder;

        /// <summary>
        /// The actors currently being animated from the recording
        /// </summary>
        private MetaActor actors;

        /// <summary>
        /// How fast the recording is being played back. +1 means normal speed,
        /// -1 means normal speed but backwards. 0 is stopped.
        /// </summary>
        private float playbackSpeed;

        /// <summary>
        /// The parent all actors will be parented too
        /// </summary>
        private Transform actorsParent;

        /// <summary>
        /// Whether or not the recording should loop during playback.
        /// </summary>
        private readonly bool loop;

        private readonly ITimeProvider timeProvider;

        public PlaybackStateInfo(IRecording recording, IActorBuilder actorBuilder, Transform actorsParent, bool loop,
            ITimeProvider timeProvider)
        {
            this.recording = recording;
            this.actorBuilder = actorBuilder ?? throw new System.ArgumentNullException(nameof(actorBuilder));
            this.actorsParent = actorsParent;
            this.timeProvider = timeProvider;
            this.loop = loop;
            playbackSpeed = 1;
        }

        public bool ShouldLoop()
        {
            return loop;
        }

        public IRecording GetRecording()
        {
            return recording;
        }

        public ITimeProvider GetTimeProvider()
        {
            return timeProvider;
        }

        public float GetDuration()
        {
            return recording?.Duration ?? -1;
        }

        public float GetPlaybackSpeed()
        {
            return playbackSpeed;
        }

        public void SetPlaybackSpeed(float playbackSpeed)
        {
            this.playbackSpeed = playbackSpeed;
        }

        public void SetTime(float time)
        {
            timeThroughPlayback = time;
        }

        internal float ShiftedTimeThroughPlayback()
        {
            return GetTimeThroughPlayback() + recording.StartTime;
        }

        public float GetTimeThroughPlayback()
        {
            return timeThroughPlayback;
        }

        public float IncrementTimeThroughPlayback(float amount)
        {
            timeThroughPlayback += amount;
            return timeThroughPlayback;
        }

        /// <summary>
        /// Destroys all actors that exist in playback and overrides them
        /// </summary>
        /// <returns>The new actors created</returns>
        public void LoadActorsFromRecording()
        {
            ClearActors();
            actors = new MetaActor(actorBuilder.Build(recording));
        }

        /// <summary>
        /// Get the actors currently present in the scene
        /// </summary>
        /// <returns></returns>
        public MetaActor GetActors()
        {
            return actors;
        }

        /// <summary>
        /// Clears the current loaded recording of all actors in the scene
        /// </summary>
        public void ClearActors()
        {
            actors?.CleanupResources();
            actors = null;
        }
    }
}