using UnityEngine;

namespace Recolude.Core.Playback
{

    internal class PlayingPlaybackState : PlaybackState
    {
        /// <summary>
        /// The time of the last frame.
        /// </summary>
        private float lastRealTime = -1;

        public PlayingPlaybackState(PlaybackStateInfo playbackStateInfo, float startingTime) : base(playbackStateInfo)
        {
            if (playbackStateInfo.GetActors() == null)
            {
                playbackStateInfo.LoadActorsFromRecording();
            }

            playbackStateInfo.SetTime(startingTime);
            playbackStateInfo.GetActors().MoveTo(MovementType.Discrete, playbackStateInfo.ShiftedTimeThroughPlayback());
        }

        public PlayingPlaybackState(PlaybackStateInfo playbackStateInfo) : this(playbackStateInfo, 0) { }

        public override void SetTimeThroughPlayback(float time)
        {
            playbackStateInfo.SetTime(time);
            playbackStateInfo.GetActors().MoveTo(MovementType.Continuous, playbackStateInfo.ShiftedTimeThroughPlayback());
        }

        public override PlaybackState Pause()
        {
            return new PausedPlaybackState(playbackStateInfo);
        }

        public override PlaybackState Play()
        {
            return new PlayingPlaybackState(playbackStateInfo);
        }

        public override PlaybackState Stop()
        {
            playbackStateInfo.ClearActors();
            return new StoppedPlaybackState(playbackStateInfo);
        }

        public override PlaybackState Update()
        {
            float timeTranspired = TrueDeltaTime() * playbackStateInfo.GetPlaybackSpeed();

            playbackStateInfo.IncrementTimeThroughPlayback(timeTranspired);

            if (GetTimeThroughPlayback() > GetDuration())
            {
                if (playbackStateInfo.ShouldLoop() == false)
                {
                    return new PausedPlaybackState(playbackStateInfo);
                }
                playbackStateInfo.SetTime(0);
                playbackStateInfo.GetActors().MoveTo(MovementType.Discrete, playbackStateInfo.ShiftedTimeThroughPlayback());
            }
            else if (GetTimeThroughPlayback() < 0)
            {
                if (playbackStateInfo.ShouldLoop() == false)
                {
                    return new PausedPlaybackState(playbackStateInfo);
                }
                playbackStateInfo.SetTime(GetDuration());
                playbackStateInfo.GetActors().MoveTo(MovementType.Discrete, playbackStateInfo.ShiftedTimeThroughPlayback());
            }
            else
            {
                playbackStateInfo.GetActors().MoveTo(MovementType.Continuous, playbackStateInfo.ShiftedTimeThroughPlayback());
            }

            lastRealTime = playbackStateInfo.GetTimeProvider().CurrentTime();
            return this;
        }


        /// <summary>
        /// Returns the actual time since the start of Unity, instead of just the scene.
        /// Used so that playback can occur within the editor without having the need
        /// for the scene to be played.
        /// </summary>
        /// <returns>The delta time.</returns>
        private float TrueDeltaTime()
        {
            return lastRealTime == -1 ? 0 : playbackStateInfo.GetTimeProvider().CurrentTime() - lastRealTime;
        }

    }

}