
namespace Recolude.Core.Playback
{
    public struct TimeMovementDetails
    {
        MovementType movementType;

        private readonly int direction;

        private readonly float deltaTime;

        private readonly float currentTime;

        public TimeMovementDetails(MovementType movementType, int direction, float deltaTime, float currentTime)
        {
            this.movementType = movementType;
            this.direction = direction;
            this.deltaTime = deltaTime;
            this.currentTime = currentTime;
        }

        public MovementType MovementType { get => movementType; }

        public int Direction { get => direction; }

        public float DeltaTime { get => deltaTime; }

        public float CurrentTime { get => currentTime; }

        public float LastTime { get => CurrentTime - DeltaTime; }

    }

}