using UnityEngine;

namespace Recolude.Core
{
    public class PositionCollection : CaptureCollection<Capture<Vector3>>
    {
        public PositionCollection(Capture<Vector3>[] captures) : base(CollectionNames.Position, captures)
        {
        }

        public PositionCollection(string name, Capture<Vector3>[] captures) : base(name, captures)
        {
        }

        public override string Signature => "recolude.position";
    }
}