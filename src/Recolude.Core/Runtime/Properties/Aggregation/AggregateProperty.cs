﻿using System;
using System.Collections.Generic;

namespace Recolude.Core.Properties.Aggregation
{
    public abstract class AggregateProperty<T> : IProperty<T>
    {
        protected readonly List<Property<T>> aggregatedProperties;
        public abstract T Value { get; }

        protected AggregateProperty(IEnumerable<Property<T>> properties)
        {
            if (properties == null)
            {
                throw new ArgumentNullException(nameof(properties));
            }

            aggregatedProperties = new List<Property<T>>();
            aggregatedProperties.AddRange(properties);
        }

        protected AggregateProperty()
        {
            aggregatedProperties = new List<Property<T>>();
        }

        public void AddPropertyToAggregate(Property<T> property)
        {
            aggregatedProperties.Add(property);
        }
    }
}