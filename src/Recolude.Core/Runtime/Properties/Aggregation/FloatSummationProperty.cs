﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Recolude.Core.Properties.Aggregation
{
    public class FloatSummationProperty: AggregateProperty<float>
    {
        public FloatSummationProperty(IEnumerable<Property<float>> properties) : base(properties)
        {
        }
        
        public FloatSummationProperty() 
        {
        }

        public override float Value => aggregatedProperties.Sum(prop => prop.Value);

    }
}