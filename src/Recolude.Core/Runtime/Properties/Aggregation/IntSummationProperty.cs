﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Recolude.Core.Properties.Aggregation
{
    public class IntSummationProperty: AggregateProperty<int>
    {
        public IntSummationProperty(IEnumerable<Property<int>> properties) : base(properties)
        {
        }
        
        public IntSummationProperty() 
        {
        }

        public override int Value
        {
            get
            {
                return aggregatedProperties.Sum(prop => prop.Value);
            }
        }

    }
}