using System.Collections.Generic;

namespace Recolude.Core.Properties
{
    public class ArrayProperty<T> : AssignableProperty<T[]>
    {
        public ArrayProperty(params T[] value) : base(value)
        {
            if (value == null)
            {
                throw new System.ArgumentNullException(nameof(value));
            }
        }

        public int Length => value.Length;

        public T this[int index]
        {
            get => value[index];
            set => this.value[index] = value;
        }

        public override bool Equals(object obj)
        {
            if ((obj is ArrayProperty<T>) == false)
            {
                return false;
            }

            var prop = (ArrayProperty<T>)obj;

            if (Length != prop.Length)
            {
                return false;
            }

            for (int i = 0; i < value.Length; i++)
            {
                if (!EqualityComparer<T>.Default.Equals(value[i], prop.value[i]))
                {
                    return false;
                }
            }

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = -1800570481;
            hashCode = hashCode * -1521134295 + base.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<T[]>.Default.GetHashCode(value);
            hashCode = hashCode * -1521134295 + EqualityComparer<T[]>.Default.GetHashCode(Value);
            hashCode = hashCode * -1521134295 + Length.GetHashCode();
            return hashCode;
        }
    }
}