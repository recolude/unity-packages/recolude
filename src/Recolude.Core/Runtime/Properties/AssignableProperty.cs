﻿namespace Recolude.Core.Properties
{
    public class AssignableProperty<T> : Property<T>
    {
        public AssignableProperty(T value) : base(value)
        {
        }

        public void Set(T newValue)
        {
            value = newValue;
        }
    }
}