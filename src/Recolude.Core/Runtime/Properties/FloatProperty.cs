namespace Recolude.Core.Properties
{
    public class FloatProperty : AssignableProperty<float>
    {
        public FloatProperty(float value) : base(value)
        {
        }

        public void Add(float amountToAdd)
        {
            value += amountToAdd;
        }

        public void Subtract(float amountToSubtract)
        {
            value -= amountToSubtract;
        }
    }
}