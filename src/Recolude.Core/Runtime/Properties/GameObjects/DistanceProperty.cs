﻿using System;
using System.Globalization;
using System.IO;
using JetBrains.Annotations;
using UnityEngine;

namespace Recolude.Core.Properties.GameObjects
{
    /// <summary>
    /// A property whose value is dictated by the current distance between the
    /// two objects used to construct the property.
    /// </summary>
    public class DistanceProperty: IProperty<float>
    {
        private readonly Transform objectA;
 
        private readonly Transform objectB;

        public DistanceProperty(GameObject objectA, GameObject objectB)
        {
            if (objectA == null) throw new ArgumentNullException(nameof(objectA));
            if (objectB == null) throw new ArgumentNullException(nameof(objectB));
            this.objectA = objectA.transform;
            this.objectB = objectB.transform;
        }
        
        public DistanceProperty(Transform objectA, Transform objectB)
        {
            if (objectA == null) throw new ArgumentNullException(nameof(objectA));
            if (objectB == null) throw new ArgumentNullException(nameof(objectB));
            this.objectA = objectA;
            this.objectB = objectB;
        }
        
        public DistanceProperty(GameObject objectA, Transform objectB)
        {
            if (objectA == null) throw new ArgumentNullException(nameof(objectA));
            if (objectB == null) throw new ArgumentNullException(nameof(objectB));
            this.objectA = objectA.transform;
            this.objectB = objectB;
        }
        
        public DistanceProperty(Transform objectA, GameObject objectB)
        {
            if (objectA == null) throw new ArgumentNullException(nameof(objectA));
            if (objectB == null) throw new ArgumentNullException(nameof(objectB));
            this.objectA = objectA;
            this.objectB = objectB.transform;
        }

        public float Value => Vector3.Distance(objectA.position, objectB.position);

    }
}