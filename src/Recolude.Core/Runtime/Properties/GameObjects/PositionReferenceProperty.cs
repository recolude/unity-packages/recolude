﻿using System;
using System.IO;
using UnityEngine;

namespace Recolude.Core.Properties.GameObjects
{
    /// <summary>
    /// A property whose value is dictated by the current position of the
    /// game object used to construct the property.
    /// </summary>
    public class PositionReferenceProperty : IProperty<Vector3>
    {
        private readonly Transform objectToReference;

        public PositionReferenceProperty(GameObject objectToReference)
        {
            if (objectToReference == null) throw new ArgumentNullException(nameof(objectToReference));
            this.objectToReference = objectToReference.transform;
        }

        public PositionReferenceProperty(Transform objectToReference)
        {
            if (objectToReference == null) throw new ArgumentNullException(nameof(objectToReference));
            this.objectToReference = objectToReference;
        }

        public Vector3 Value => objectToReference.position;
    }
}