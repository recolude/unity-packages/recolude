namespace Recolude.Core.Properties
{
    public class IntProperty : AssignableProperty<int>
    {
        public IntProperty(int value) : base(value)
        {
        }

        public void Increment()
        {
            value += 1;
        }

        public void Decrement()
        {
            value -= 1;
        }

        public void Add(int amountToAdd)
        {
            value += amountToAdd;
        }

        public void Subtract(int amountToSubtract)
        {
            value -= amountToSubtract;
        }
    }
}