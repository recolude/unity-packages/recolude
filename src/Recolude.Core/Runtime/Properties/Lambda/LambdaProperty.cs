﻿using System;
using System.IO;

namespace Recolude.Core.Properties.Lambda
{
    public class LambdaProperty<T> : IProperty<T>
    {
        private readonly Func<T> expression;

        public LambdaProperty(Func<T> expression)
        {
            this.expression = expression;
        }

        public T Value => expression.Invoke();
    }
}