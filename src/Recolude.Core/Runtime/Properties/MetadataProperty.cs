using System.Collections.Generic;

namespace Recolude.Core.Properties
{
    public class MetadataProperty : AssignableProperty<Metadata>
    {
        public MetadataProperty(Metadata value) : base(value)
        {
            if (value == null)
            {
                throw new System.ArgumentNullException(nameof(value));
            }
        }

        public MetadataProperty(Dictionary<string, IProperty> value) : base(new Metadata(value))
        {
        }

        public MetadataProperty(Dictionary<string, string> value) : base(new Metadata(value))
        {
        }
    }
}