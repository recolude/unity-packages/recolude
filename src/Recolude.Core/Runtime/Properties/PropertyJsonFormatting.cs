﻿using Recolude.Core.Util;

namespace Recolude.Core.Properties
{
    public static class PropertyJsonFormatting
    {
        public static string String(string value)
        {
            return $"\"{FormattingUtil.JavaScriptStringEncode(value)}\"";
        }
    }
}