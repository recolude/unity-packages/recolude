﻿using System;
using System.Globalization;
using System.IO;
using Recolude.Core.Time;

namespace Recolude.Core.Properties.Time
{
    /// <summary>
    /// The value of this property corresponds to how much time has passed
    /// since it's instantiation, based upon the time provider provided.
    /// </summary>
    public class TimeElapsedProperty : IProperty<float>
    {
        private readonly ITimeProvider timeProvider;

        private readonly float startingTime;

        public TimeElapsedProperty(ITimeProvider timeProvider)
        {
            this.timeProvider = timeProvider ?? throw new ArgumentNullException(nameof(timeProvider));
            startingTime = timeProvider.CurrentTime();
        }

        public float Value => timeProvider.CurrentTime() - startingTime;
    }
}