using System;

namespace Recolude.Core.Properties
{
    public class TimeProperty : AssignableProperty<DateTime>
    {
        public TimeProperty(DateTime value) : base(value)
        {
        }
    }
}