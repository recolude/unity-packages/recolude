using UnityEngine;

namespace Recolude.Core.Properties
{
    public class Vector2Property : AssignableProperty<Vector2>
    {
        public Vector2Property(Vector2 value) : base(value)
        {
        }

        public Vector2Property(float x, float y) : base(new Vector2(x, y))
        {
        }
    }
}