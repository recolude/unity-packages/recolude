using UnityEngine;

namespace Recolude.Core.Properties
{
    public class Vector3Property : AssignableProperty<Vector3>
    {
        public Vector3Property(Vector3 value) : base(value)
        {
        }

        public Vector3Property(float x, float y, float z) : base(new Vector3(x, y, z))
        {
        }

        public override string ToString()
        {
            return $"{value.x}, {value.y}, {value.z}";
        }
    }
}