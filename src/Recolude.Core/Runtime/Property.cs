﻿using System;
using System.Collections.Generic;

namespace Recolude.Core
{
    public class Property<T> : IProperty<T>
    {
        protected T value;

        public T Value => value;

        public Property(T value)
        {
            this.value = value;
        }

        public override bool Equals(object obj)
        {
            return obj is Property<T> property &&
                   EqualityComparer<T>.Default.Equals(Value, property.Value);
        }

        public override int GetHashCode()
        {
            int hashCode = 1927018180;
            hashCode = hashCode * -1521134295 + EqualityComparer<T>.Default.GetHashCode(Value);
            return hashCode;
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}