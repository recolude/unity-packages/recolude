using System.Collections.Generic;
using UnityEngine;

namespace Recolude.Core.Record
{
    public abstract class CaptureRecorder<T> : ICaptureRecorder<T> where T : class, ICapture
    {
        protected readonly List<T> captures;

        readonly string name;

        protected CaptureRecorder(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new System.ArgumentNullException(nameof(name),"A capture recorder name can not be null");
            }
            this.name = name;
            captures = new List<T>();
        }

        public void Record(T capture)
        {
            captures.Add(capture);
        }

        public string Name => name;

        public void Clear()
        {
            captures.Clear();
        }

        public void Record(ICapture capture)
        {
            var correctCapture = capture as T;
            if (correctCapture == null)
            {
                throw new System.ArgumentException($"capture is not of type {typeof(T)}, is instead {capture.GetType()}");
            }
            captures.Add(correctCapture);
        }

        public abstract ICaptureCollection<T> ToCollection(float startTime, float endTime, IEnumerable<Vector2> pauseSlices);

    }
}