using UnityEngine;
using System.Collections.Generic;

namespace Recolude.Core.Record
{
    public abstract class CaptureTrajectoryRecorder<T> : ICaptureRecorder<Capture<T>>
    {
        private class InterpolateFilterAndShiftData
        {
            bool? lastCaptureWasInPause;

            Vector2 happenedInPauseRange;

            List<Capture<T>> newCaptures;

            Capture<T> previousCapture;

            float startTime;

            float endTime;

            IEnumerable<Vector2> pauseSlices;

            CaptureTrajectoryRecorder<T> recorder;

            public InterpolateFilterAndShiftData(CaptureTrajectoryRecorder<T> recorder, float startTime, float endTime, IEnumerable<Vector2> pauseSlices)
            {
                this.recorder = recorder;
                lastCaptureWasInPause = null;
                newCaptures = new List<Capture<T>>();
                happenedInPauseRange = Vector2.zero;
                this.startTime = startTime;
                this.endTime = endTime;
                this.pauseSlices = pauseSlices ?? new Vector2[0];
                previousCapture = null;
            }

            public void Interpret(Capture<T> capture)
            {
                if (capture.FallsWithin(startTime, endTime))
                {
                    bool happenedInPause = false;

                    float cumulativePauseTime = 0f;
                    foreach (var pause in pauseSlices)
                    {
                        if (capture.FallsWithin(pause.x, pause.y))
                        {
                            happenedInPauseRange = pause;
                            happenedInPause = true;
                        }
                        else if (pause.y > startTime && pause.y <= capture.Time)
                        {
                            cumulativePauseTime += (pause.y - pause.x);
                        }
                    }

                    if (happenedInPause)
                    {
                        if (lastCaptureWasInPause == false)
                        {
                            // We jumped from playing state to paused state, we must interpolate
                            var previous = previousCapture;
                            var progress = (happenedInPauseRange.x - previous.Time) / (capture.Time - previous.Time);
                            newCaptures.Add(this.recorder.NewCapture(happenedInPauseRange.x - cumulativePauseTime, this.recorder.Interpolate(previous.Value, capture.Value, progress)));
                        }
                    }
                    else
                    {
                        if (lastCaptureWasInPause == true)
                        {
                            // switched from being in pause to playing, must interpolate
                            var previous = previousCapture;
                            var progress = (happenedInPauseRange.y - previous.Time) / (capture.Time - previous.Time);
                            newCaptures.Add(this.recorder.NewCapture(happenedInPauseRange.y - cumulativePauseTime, this.recorder.Interpolate(previous.Value, capture.Value, progress)));
                        }
                        newCaptures.Add(capture.SetTime(capture.Time - cumulativePauseTime) as Capture<T>);
                    }
                    lastCaptureWasInPause = happenedInPause;
                }
                previousCapture = capture;
            }

            public Capture<T>[] Captures()
            {
                return newCaptures.ToArray();
            }
        }

        private float minimumDelta;

        private List<Capture<T>> captures;

        protected string name;

        public string Name { get => name; }

        public CaptureTrajectoryRecorder(string name, float minimumDelta)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new System.ArgumentNullException("A capture recorder name can not be null");
            }
            this.name = name;
            captures = new List<Capture<T>>();
            this.minimumDelta = minimumDelta;
        }

        protected abstract T Trajectory(Capture<T> a, Capture<T> b);

        protected abstract T Interpolate(T start, T end, float progress);

        protected abstract Capture<T> NewCapture(float time, T value);

        protected abstract bool Approximate(T a, T b, float minimumDelta);

        public abstract ICaptureCollection<Capture<T>> ToCollection(float startTime, float endTime, IEnumerable<Vector2> pauseSlices);

        private int GetStartIndex(
            List<Capture<T>> original,
            float startTime
        )
        {
            if (original == null)
            {
                return -1;
            }

            // Empty list!
            if (original.Count == 0)
            {
                return -1;
            }

            // List with only one element!
            if (original.Count == 1)
            {
                if (original[0].Time >= startTime)
                {
                    return 0;
                }
                else
                {
                    return -1;
                }
            }

            if (original[0].Time >= startTime)
            {
                return 0;
            }

            for (int i = 1; i < original.Count; i++)
            {
                if (original[i].Time == startTime)
                {
                    return i;
                }
                if (original[i].Time > startTime)
                {
                    return i - 1;
                }
            }
            return -1;
        }


        protected Capture<T>[] InterpolateFilterAndShift(
            float startTime,
            float endTime,
            IEnumerable<Vector2> pauseSlices
        )
        {
            var captureNodeIndex = GetStartIndex(captures, startTime);
            if (captureNodeIndex == -1)
            {
                return new Capture<T>[0];
            }

            var captureNode = captures[captureNodeIndex];

            var data = new InterpolateFilterAndShiftData(this, startTime, endTime, pauseSlices);
            if (captureNode.Time < startTime && captureNodeIndex < captures.Count - 1)
            {
                var progress = (startTime - captureNode.Time) / (captures[captureNodeIndex + 1].Time - captureNode.Time);
                data.Interpret(NewCapture(startTime, Interpolate(captureNode.Value, captures[captureNodeIndex + 1].Value, progress)));

                captureNodeIndex++;
                captureNode = captureNodeIndex < captures.Count ? captures[captureNodeIndex] : null;
            }

            while (captureNode != null && captureNode.Time <= endTime)
            {
                data.Interpret(captureNode);
                captureNodeIndex++;
                captureNode = captureNodeIndex < captures.Count ? captures[captureNodeIndex] : null;
            }

            if (captureNode != null)
            {
                var progress = (endTime - captures[captureNodeIndex - 1].Time) / (captureNode.Time - captures[captureNodeIndex - 1].Time);
                data.Interpret(NewCapture(endTime, Interpolate(captures[captureNodeIndex - 1].Value, captureNode.Value, progress)));
            }

            return data.Captures();
        }

        public void Record(ICapture capture)
        {
            var correctCapture = capture as Capture<T>;
            if (correctCapture != null)
            {
                Record(correctCapture);
            }
            else
            {
                throw new System.Exception($"Unrecognized capture type: {capture.GetType()}");
            }
        }

        public void Record(Capture<T> capture)
        {
            if (captures.Count > 1)
            {
                Capture<T> beforeLastPosition = captures[captures.Count - 2];
                Capture<T> lastPosition = captures[captures.Count - 1];

                if (capture.Time == lastPosition.Time && capture.Value.Equals(lastPosition.Value))
                {
                    return;
                }

                if (Approximate(Trajectory(beforeLastPosition, lastPosition), Trajectory(lastPosition, capture), minimumDelta))
                {
                    captures[captures.Count - 1] = capture;
                }
                else
                {
                    captures.Add(capture);
                }
            }
            else
            {
                captures.Add(capture);
            }
        }
    }
}