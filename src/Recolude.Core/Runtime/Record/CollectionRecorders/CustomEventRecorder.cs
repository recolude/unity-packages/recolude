using System.Collections.Generic;
using UnityEngine;
using Recolude.Core.Util;

namespace Recolude.Core.Record.CollectionRecorders
{
    public class CustomEventRecorder : CaptureRecorder<Capture<CustomEvent>>
    {
        public CustomEventRecorder(string name) : base(name)
        {
        }

        public CustomEventRecorder() : base(CollectionNames.CustomEvent)
        {
        }

        public override ICaptureCollection<Capture<CustomEvent>> ToCollection(float startTime, float endTime, IEnumerable<Vector2> pauseSlices)
        {
            return new CustomEventCollection(CollectionNames.CustomEvent, captures.FilterAndShift<Capture<CustomEvent>>(startTime, endTime, pauseSlices));
        }
    }
}