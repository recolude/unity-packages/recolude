using System.Collections.Generic;
using UnityEngine;
using Recolude.Core.Util;

namespace Recolude.Core.Record.CollectionRecorders
{
    public class LifeCycleRecorder : CaptureRecorder<Capture<UnityLifeCycleEvent>>
    {
        public LifeCycleRecorder(string name) : base(name)
        {
        }

        public LifeCycleRecorder() : base(CollectionNames.LifeCycle)
        {
        }

        public override ICaptureCollection<Capture<UnityLifeCycleEvent>> ToCollection(float startTime, float endTime, IEnumerable<Vector2> pauseSlices)
        {
            return new LifeCycleCollection(Name, captures.Squash<Capture<UnityLifeCycleEvent>>(startTime, endTime, pauseSlices));
        }
    }
}