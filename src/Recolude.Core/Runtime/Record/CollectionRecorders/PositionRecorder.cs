using UnityEngine;
using System.Collections.Generic;

namespace Recolude.Core.Record.CollectionRecorders
{
    public class PositionRecorder : CaptureTrajectoryRecorder<Vector3>
    {
        public PositionRecorder(string name, float minimumDelta) : base(name, minimumDelta) { }

        public PositionRecorder(float minimumDelta) : base(CollectionNames.Position, minimumDelta) { }

        protected override Vector3 Trajectory(Capture<Vector3> a, Capture<Vector3> b)
        {
            return (b.Value - a.Value) / (b.Time - a.Time);
        }

        protected override bool Approximate(Vector3 a, Vector3 b, float minimumDelta)
        {
            return Vector3.SqrMagnitude(a - b) <= minimumDelta;
        }

        public override ICaptureCollection<Capture<Vector3>> ToCollection(float startTime, float endTime, IEnumerable<Vector2> pauseSlices)
        {
            return new PositionCollection(name, InterpolateFilterAndShift(startTime, endTime, pauseSlices));
        }

        protected override Vector3 Interpolate(Vector3 start, Vector3 end, float progress)
        {
            var dist = end - start;
            return start + (dist * progress);
        }

        protected override Capture<Vector3> NewCapture(float time, Vector3 value)
        {
            return new VectorCapture(time, value);
        }
    }
}