﻿using System;
using Recolude.Core.Time;

namespace Recolude.Core.Record.CollectionRecorders
{
    public abstract class TimeAwareCaptureRecorder<T> : CaptureRecorder<Capture<T>>
    {
        private readonly ITimeProvider timeProvider;

        protected TimeAwareCaptureRecorder(string name, ITimeProvider timeProvider) : base(name)
        {
            this.timeProvider = timeProvider ?? throw new ArgumentNullException(nameof(timeProvider));
        }

        protected abstract Capture<T> NewCapture(float time, T val);

        public void Record(T val)
        {
            Record(NewCapture(timeProvider.CurrentTime(), val));
        }
    }
}