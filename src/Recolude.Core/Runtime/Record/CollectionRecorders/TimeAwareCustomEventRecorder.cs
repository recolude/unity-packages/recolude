﻿using System.Collections.Generic;
using Recolude.Core.Time;
using Recolude.Core.Util;
using UnityEngine;

namespace Recolude.Core.Record.CollectionRecorders
{
    public class TimeAwareCustomEventRecorder : TimeAwareCaptureRecorder<CustomEvent>
    {
        public TimeAwareCustomEventRecorder(string name, ITimeProvider timeProvider) : base(name, timeProvider)
        {
        }

        public TimeAwareCustomEventRecorder(ITimeProvider timeProvider) : base(CollectionNames.CustomEvent,
            timeProvider)
        {
        }

        public override ICaptureCollection<Capture<CustomEvent>> ToCollection(float startTime, float endTime,
            IEnumerable<Vector2> pauseSlices)
        {
            return new CustomEventCollection(CollectionNames.CustomEvent,
                captures.FilterAndShift<Capture<CustomEvent>>(startTime, endTime, pauseSlices));
        }

        protected override Capture<CustomEvent> NewCapture(float time, CustomEvent val)
        {
            return new CustomEventCapture(time, val);
        }
    }
}