﻿namespace Recolude.Core.Record.Emitters
{
    /// <summary>
    /// Provider of a value of a given type, and whether or not the value is
    /// to be considered value anymore given the current state of the source.
    /// </summary>
    /// <typeparam name="T">Type of data the source provides</typeparam>
    public interface ISource<out T>
    {
        /// <summary>
        /// Value of the source.
        /// </summary>
        /// <returns>
        /// Value of the source.
        /// </returns>
        T Value();

        /// <summary>
        /// Whether or not the value's coming from the source is to be trusted
        /// anymore. IE: Did the transform we where capturing the position from
        /// get destroyed?
        /// </summary>
        /// <returns>
        /// Whether or not the value's coming from the source is to be trusted
        /// anymore.
        /// </returns>
        bool Valid();
    }
}