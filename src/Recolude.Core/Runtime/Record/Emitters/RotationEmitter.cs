using UnityEngine;

namespace Recolude.Core.Record.Emitters
{
    public class RotationEmitter : IEmitter
    {
        Transform transform;

        CaptureTrajectoryRecorder<Vector3> recorder;

        public RotationEmitter(Transform transform, CaptureTrajectoryRecorder<Vector3> recorder)
        {
            this.transform = transform;
            this.recorder = recorder;
        }

        public RotationEmitter(GameObject gameObject, CaptureTrajectoryRecorder<Vector3> recorder) : this(gameObject.transform, recorder) { }

        public bool Emit(float time)
        {
            if (transform == null)
            {
                return false;
            }
            this.recorder.Record(new VectorCapture(time, transform.rotation.eulerAngles));
            return true;
        }
    }
}