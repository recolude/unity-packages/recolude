﻿using System;

namespace Recolude.Core.Record.Emitters
{
    public abstract class SourceEmitter<T> : IEmitter
    {
        private readonly ISource<T> source;

        private readonly ICaptureRecorder<Capture<T>> recorder;

        protected SourceEmitter(ISource<T> source, ICaptureRecorder<Capture<T>> recorder)
        {
            this.source = source ?? throw new ArgumentNullException(nameof(source));
            this.recorder = recorder ?? throw new ArgumentNullException(nameof(recorder));
        }

        public bool Emit(float time)
        {
            if (!source.Valid())
            {
                return false;
            }

            recorder.Record(Build(time, source.Value()));
            return true;
        }

        protected abstract Capture<T> Build(float time, T val);
    }
}