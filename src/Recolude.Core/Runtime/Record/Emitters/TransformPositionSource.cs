﻿using UnityEngine;

namespace Recolude.Core.Record.Emitters
{
    public class TransformPositionSource : TransformSource, ISource<Vector3>
    {

        public TransformPositionSource(Transform transform): base(transform)
        {
        }
        
        public TransformPositionSource(GameObject gameObject): this(gameObject.transform)
        {
        }

        public Vector3 Value()
        {
            return transform.position;
        }
      
    }
}