﻿using UnityEngine;

namespace Recolude.Core.Record.Emitters
{
    public class TransformRotationSource : TransformSource, ISource<Vector3>
    {
        public TransformRotationSource(Transform transform): base(transform)
        {
        }
        
        public TransformRotationSource(GameObject gameObject): this(gameObject.transform)
        {
        }

        public Vector3 Value()
        {
            return transform.eulerAngles;
        }
    }
}