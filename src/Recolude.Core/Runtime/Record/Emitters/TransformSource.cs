﻿using System;
using UnityEngine;

namespace Recolude.Core.Record.Emitters
{
    public abstract class TransformSource
    {
        protected readonly Transform transform;

        protected TransformSource(Transform transform)
        {
            this.transform = transform ? transform : throw new ArgumentNullException(nameof(transform));
        }
        
        public bool Valid()
        {
            return transform != null;
        }
    }
}