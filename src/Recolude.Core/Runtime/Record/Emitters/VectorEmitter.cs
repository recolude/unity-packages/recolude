using UnityEngine;

namespace Recolude.Core.Record.Emitters
{
    public class VectorEmitter : SourceEmitter<Vector3>
    {
        private readonly Vector3 offset;

        public VectorEmitter(ISource<Vector3> source, ICaptureRecorder<Capture<Vector3>> recorder, Vector3 offset): base(source, recorder)
        {
            this.offset = offset;
        }
        
        public VectorEmitter(ISource<Vector3> source, ICaptureRecorder<Capture<Vector3>> recorder): base(source, recorder)
        {
        }

        protected override Capture<Vector3> Build(float time, Vector3 val)
        {
            return new VectorCapture(time, val + offset);
        }
    }
}