using System.Collections.Generic;
using UnityEngine;

namespace Recolude.Core.Record
{
    public interface ICaptureRecorder<out T> where T : ICapture
    {
        string Name { get; }

        void Record(ICapture capture);

        ICaptureCollection<T> ToCollection(float startTime, float endTime, IEnumerable<Vector2> pauseSlices);
    }
}