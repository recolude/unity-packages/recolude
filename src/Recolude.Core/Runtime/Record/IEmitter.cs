
namespace Recolude.Core.Record
{
    public interface IEmitter
    {
        bool Emit(float time);
    }
}