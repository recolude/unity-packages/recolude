using Recolude.Core.Time;

namespace Recolude.Core.Record
{
    public interface ITimerManager
    {
        ITimeProvider TimeProvider { get; }
        
        Timer Build(float frameRate, params IEmitter[] emitters);
    }
}