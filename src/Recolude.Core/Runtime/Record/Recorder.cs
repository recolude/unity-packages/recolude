﻿using System.Collections.Generic;
using Recolude.Core.Properties;
using Recolude.Core.Record.CollectionRecorders;
using Recolude.Core.Time;
using UnityEngine;
using UnityEngine.Events;


namespace Recolude.Core.Record
{
    /// <summary>
    /// Used for building recordings and keeping up with all the child 
    /// recorders.
    /// </summary>
    /// <remarks>
    /// Metadata set will persist across multiple recordings unless cleared 
    /// with [ClearMetadata()](xref:Recolude.Core.Record.Recorder.ClearMetadata*).
    /// </remarks>
    [HelpURL("https://docs.recolude.com/tutorials/Recording%20Quick%20Start.html")]
    public class Recorder
    {
        /// <summary>
        /// The current state of the recording service, as to whether or not it
        /// is recording actors.
        /// </summary>
        private RecordingState currentState;

        /// <summary>
        /// Represents different periods in which the recording was paused.
        /// 
        /// (re)Initialized at the start of recording.
        /// </summary>
        private List<Vector2> pauseSlices;

        /// <summary>
        /// The time in seconds since the scene started when the pause command 
        /// was called while we where recording.
        /// </summary>
        private float timePaused;

        /// <summary>
        /// The time the recording started
        /// </summary>
        private float timeStarted;

        /// <summary>
        /// All subjects that are to be included in the recording
        /// </summary>
        private List<Recorder> subRecorders;

        private CustomEventRecorder customEventRecorder;

        private Metadata metadata;

        private ITimerManager timerManager;

        public ITimerManager TimerManager => timerManager;

        private RecordingEvent onRecordingComplete;

        private List<ICaptureRecorder<ICapture>> captureRecorders;

        private RecorderInfo recorderInfo;

        public Recorder(ITimerManager timerManager, RecorderInfo recorderInfo, List<Recorder> subRecorders,
            IEnumerable<ICaptureRecorder<ICapture>> captureRecorders)
        {
            this.timerManager = timerManager ?? throw new System.ArgumentNullException(nameof(timerManager));
            this.recorderInfo = recorderInfo ?? throw new System.ArgumentNullException(nameof(recorderInfo));

            this.subRecorders = subRecorders;
            if (subRecorders == null)
            {
                this.subRecorders = new List<Recorder>();
            }

            this.captureRecorders = captureRecorders == null
                ? new List<ICaptureRecorder<ICapture>>()
                : new List<ICaptureRecorder<ICapture>>(captureRecorders);

            foreach (var captureRecorder in this.captureRecorders)
            {
                var correctRecorder = captureRecorder as CustomEventRecorder;
                if (correctRecorder == null)
                {
                    continue;
                }

                if (correctRecorder.Name != CollectionNames.CustomEvent)
                {
                    continue;
                }

                this.customEventRecorder = correctRecorder;
                break;
            }

            if (customEventRecorder == null)
            {
                customEventRecorder = new CustomEventRecorder();
                this.captureRecorders.Add(customEventRecorder);
            }

            currentState = RecordingState.Stopped;
            metadata = recorderInfo.Metadata ?? new Metadata();
        }

        public Recorder() : this(new TimerManager(), new RecorderInfo(), null, null)
        {
        }

        public Recorder(string id, string name, Metadata metadata) : this(new TimerManager(),
            new RecorderInfo(id, name, metadata), null, null)
        {
        }

        public string ID => recorderInfo.Id;

        public string Name => recorderInfo.Name;

        /// <summary>
        /// Adds child recorders for this recorder to include when building recordings.
        /// </summary>
        /// <param name="childRecorders">The children recorders to keep up with.</param>
        public void Register(params Recorder[] childRecorders)
        {
            subRecorders.AddRange(childRecorders);
        }

        public Recorder Register(RecorderInfo info, IEnumerable<ICaptureRecorder<ICapture>> captureRecorders)
        {
            var recorder = new Recorder(
                timerManager,
                info,
                null,
                captureRecorders
            );
            subRecorders.Add(recorder);

            if (CurrentlyRecording())
            {
                recorder.Start();
            }
            else if (CurrentlyPaused())
            {
                recorder.Start();
                recorder.Pause();
            }

            return recorder;
        }

        public Recorder Register(string id, string name)
        {
            return Register(new RecorderInfo(id, name), null);
        }

        public Recorder Register(GameObject subject, string id, string name, Metadata metadata)
        {
            return Register(new RecorderInfo(id, name, metadata),
                RecorderStrategies.BasicSubjectCaptureRecorders(timerManager, subject));
        }

        public Recorder Register(GameObject subject, string name)
        {
            return Register(subject, subject.GetInstanceID().ToString(), name, new Metadata());
        }

        public Recorder Register(GameObject subject)
        {
            return Register(subject, subject.GetInstanceID().ToString(), subject.name, new Metadata());
        }

        public Recorder Register(GameObject subject, Metadata gameObjectMetadata)
        {
            return Register(subject, subject.GetInstanceID().ToString(), subject.name, gameObjectMetadata);
        }


        private RecordingEvent OnRecordingCompleteInstance()
        {
            if (onRecordingComplete == null)
            {
                onRecordingComplete = new RecordingEvent();
            }

            return onRecordingComplete;
        }

        /// <summary>
        /// Registers a callback to be invoked whenever the 
        /// [Finish()](xref:Recolude.Core.Record.Recorder.Finish) method is 
        /// called.
        /// </summary>
        /// <param name="callback">The callback to be executed</param>
        public void AddOnRecordingCompleteCallback(UnityAction<IRecording> callback)
        {
            OnRecordingCompleteInstance().AddListener(callback);
        }

        /// <summary>
        /// Removes a previously registered callback from being invoked when the 
        /// [Finish()](xref:Recolude.Core.Record.Recorder.Finish) method is 
        /// called.
        /// </summary>
        /// <param name="callback">The callback to be removed</param>
        public void RemoveOnRecordingCompleteCallback(UnityAction<IRecording> callback)
        {
            OnRecordingCompleteInstance().RemoveListener(callback);
        }

        /// <summary>
        /// Removes all callbacks from being called when 
        /// [Finish()](xref:Recolude.Core.Record.Recorder.Finish) is invoked
        /// </summary>
        public void ClearOnRecordingCompleteListeners()
        {
            OnRecordingCompleteInstance().RemoveAllListeners();
        }

        /// <summary>
        /// Set a key value pair for meta data. 
        /// </summary>
        /// <param name="key">Dictionary Key.</param>
        /// <param name="value">Value.</param>
        public void SetMetaData(string key, IProperty value)
        {
            metadata[key] = value;
        }

        public void SetMetaData(string key, string value)
        {
            SetMetaData(key, new Property<string>(value));
        }

        public void SetMetaData(string key, int value)
        {
            SetMetaData(key, new IntProperty(value));
        }

        public void SetMetaData<T>(string key, T value)
        {
            metadata[key] = new Property<T>(value);
        }

        public IProperty GetMetaData(string key)
        {
            return metadata[key];
        }

        public ITimeProvider TimeProvider => timerManager.TimeProvider;

        /// <summary>
        /// Whether or not we are accepting events occuring.
        /// </summary>
        /// <returns>True if we are accepting events.</returns>
        public bool CurrentlyRecording()
        {
            return currentState == RecordingState.Recording;
        }

        /// <summary>
        /// Whether or not the recorder is paused. When the recorder is paused, custom events logged will be ignored.
        /// </summary>
        /// <returns>True if the recorder is paused.</returns>
        public bool CurrentlyPaused()
        {
            return currentState == RecordingState.Paused;
        }

        /// <summary>
        /// Whether or not the recorder has begun recording.
        /// </summary>
        /// <returns>True if it has not started recording.</returns>
        public bool CurrentlyStopped()
        {
            return currentState == RecordingState.Stopped;
        }

        /// <summary>
        /// The current state the recorder is in.
        /// </summary>
        /// <returns>The current recorder state.</returns>
        public RecordingState CurrentState()
        {
            return currentState;
        }

        /// <summary>
        /// Stops the recorder and builds a recording for playback. Once a recorder is finished it is free to start making a whole new recording.
        /// </summary>
        /// <returns>A recording containing everything the recorder captured while not paused.</returns>
        /// <exception cref="System.InvalidOperationException">Thrown when the recorder is stopped.</exception>
        public Recording Finish()
        {
            if (CurrentlyStopped())
            {
                throw new System.InvalidOperationException("Not recording anything! Nothing to build!");
            }

            if (CurrentlyPaused())
            {
                Resume();
            }

            currentState = RecordingState.Stopped;

            float endTime = timerManager.TimeProvider.CurrentTime();
            if (timeStarted >= endTime)
            {
                Debug.LogWarningFormat(
                    "The starting time: {0}, is greater than or equal to the ending time: {1}. This means that Time.time is probably returning 0.0, which sometimes occurs when you save a recording in the OnDestroy() event when the application is closing. Setting the end time to Mathf.Infinity.",
                    timeStarted, endTime);
                endTime = Mathf.Infinity;
            }

            var recordings = new IRecording[subRecorders.Count];
            for (int i = 0; i < recordings.Length; i++)
            {
                recordings[i] = subRecorders[i].Finish();
            }

            var captureCollections = new ICaptureCollection<ICapture>[captureRecorders.Count];
            for (int i = 0; i < captureRecorders.Count; i++)
            {
                captureCollections[i] = captureRecorders[i].ToCollection(timeStarted, endTime, pauseSlices);
            }

            var rec = new Recording(
                recorderInfo.Id,
                recorderInfo.Name,
                recordings,
                captureCollections,
                metadata,
                recorderInfo.Binaries.ToArray(),
                recorderInfo.BinaryReferences.ToArray()
            );
            OnRecordingCompleteInstance().Invoke(rec);
            return rec;
        }

        /// <summary>
        /// Takes everything the recorder has seen so far and builds a recording from it, without stopping the recording process.
        /// </summary>
        /// <returns>A recording representing everything we've seen up until this point in time.</returns>
        /// <exception cref="System.InvalidOperationException">Thrown when the recorder is stopped.</exception>
        public IRecording CurrentRecording()
        {
            if (CurrentlyStopped())
            {
                throw new System.InvalidOperationException("Not recording anything! Nothing to build!");
            }

            if (CurrentlyPaused())
            {
                pauseSlices.Add(new Vector2(timePaused, timerManager.TimeProvider.CurrentTime()));
            }

            IRecording recording = Build(timeStarted, timerManager.TimeProvider.CurrentTime(), pauseSlices);

            if (CurrentlyPaused())
            {
                pauseSlices.RemoveAt(pauseSlices.Count - 1);
            }

            return recording;
        }

        public IRecording Build(float startTime, float endTime, IEnumerable<Vector2> pauseSlices)
        {
            if (CurrentlyStopped())
            {
                throw new System.InvalidOperationException("Not recording anything! Nothing to build!");
            }

            var recordings = new IRecording[subRecorders.Count];
            for (int i = 0; i < recordings.Length; i++)
            {
                recordings[i] = subRecorders[i].Build(startTime, endTime, pauseSlices);
            }

            var captureCollections = new ICaptureCollection<ICapture>[captureRecorders.Count];
            for (int i = 0; i < captureRecorders.Count; i++)
            {
                captureCollections[i] = captureRecorders[i].ToCollection(timeStarted, endTime, pauseSlices);
            }

            Recording recording = new Recording(
                recorderInfo.Id,
                recorderInfo.Name,
                recordings,
                captureCollections,
                metadata,
                null,
                null
            );

            return recording;
        }

        /// <summary>
        /// Returns a recorder to a recording state if it was paused.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Thrown when the recorder is not paused.</exception>
        public void Resume()
        {
            if (CurrentlyRecording())
            {
                throw new System.InvalidOperationException("Currentely recording! Nothing to resume!");
            }

            if (CurrentlyStopped())
            {
                throw new System.InvalidOperationException("Not recording anything! Nothing to resume!");
            }

            if (timerManager.TimeProvider.CurrentTime() - timePaused < 0)
            {
                throw new System.InvalidOperationException(
                    "You spent negative time paused... Ignoring Resume() and staying paused");
            }

            foreach (var subRecorder in subRecorders)
            {
                subRecorder.Resume();
            }

            pauseSlices.Add(new Vector2(timePaused, timerManager.TimeProvider.CurrentTime()));
            currentState = RecordingState.Recording;
        }

        /// <summary>
        /// Moves the recorder state to be paused. Custom events that occur during this state are ignored.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Thrown when the recorder is not recording.</exception>
        public void Pause()
        {
            if (CurrentlyStopped() || CurrentlyPaused())
            {
                throw new System.InvalidOperationException("Not recording anything! Nothing to pause!");
            }

            foreach (var subRecorder in subRecorders)
            {
                subRecorder.Pause();
            }

            timePaused = timerManager.TimeProvider.CurrentTime();
            currentState = RecordingState.Paused;
        }

        /// <summary>
        /// If you want to keep up with something special that occurred at a
        /// certain time in your recording, then you can call this function
        /// with the details of the special event.
        /// </summary>
        /// <param name="name">Name of the event.</param>
        /// <param name="contents">Details of the event.</param>
        /// <exception cref="System.InvalidOperationException">Thrown when the recorder is stopped.</exception>
        /// <remarks>Will be ignored if the recorder is currently paused.</remarks>
        public void CaptureCustomEvent(string name, string contents)
        {
            if (CurrentlyPaused())
            {
                return;
            }

            if (CurrentlyStopped())
            {
                throw new System.InvalidOperationException("Not recording anything! Can't capture event!");
            }

            if (customEventRecorder == null)
            {
                customEventRecorder = new CustomEventRecorder();
            }

            customEventRecorder.Record(new CustomEventCapture(timerManager.TimeProvider.CurrentTime(), name, contents));
        }

        /// <summary>
        /// If you want to keep up with something special that occurred at a
        /// certain time in your recording, then you can call this function
        /// with the details of the special event.
        /// </summary>
        /// <param name="name">Name of the event.</param>
        /// <exception cref="System.InvalidOperationException">Thrown when the recorder is stopped.</exception>
        /// <remarks>Will be ignored if the recorder is currently paused.</remarks>
        public void CaptureCustomEvent(string name)
        {
            if (CurrentlyPaused())
            {
                return;
            }

            if (CurrentlyStopped())
            {
                throw new System.InvalidOperationException("Not recording anything! Can't capture event!");
            }

            if (customEventRecorder == null)
            {
                customEventRecorder = new CustomEventRecorder();
            }

            customEventRecorder.Record(new CustomEventCapture(timerManager.TimeProvider.CurrentTime(), name));
        }

        /// <summary>
        /// If you want to keep up with something special that occurred at a certain time in your recording, then you can call this function with the details of the special event.
        /// </summary>
        /// <param name="name">Name of the event.</param>
        /// <param name="contents">Details of the event.</param>
        /// <exception cref="System.InvalidOperationException">Thrown when the recorder is stopped.</exception>
        /// <remarks>Will be ignored if the recorder is currently paused.</remarks>
        public void CaptureCustomEvent(string name, Metadata contents)
        {
            if (CurrentlyPaused())
            {
                return;
            }

            if (CurrentlyStopped())
            {
                throw new System.InvalidOperationException("Not recording anything! Can't capture event!");
            }

            if (customEventRecorder == null)
            {
                customEventRecorder = new CustomEventRecorder();
            }

            customEventRecorder.Record(new CustomEventCapture(timerManager.TimeProvider.CurrentTime(),
                new CustomEvent(name, contents)));
        }

        /// <summary>
        /// If you want to keep up with something special that occurred at a certain time in your recording, then you can call this function with the details of the special event.
        /// </summary>
        /// <param name="name">Name of the event.</param>
        /// <param name="contents">Details of the event.</param>
        /// <exception cref="System.InvalidOperationException">Thrown when the recorder is stopped.</exception>
        /// <remarks>Will be ignored if the recorder is currently paused.</remarks>
        public void CaptureCustomEvent(string name, Dictionary<string, IProperty> contents)
        {
            if (CurrentlyPaused())
            {
                return;
            }

            if (CurrentlyStopped())
            {
                throw new System.InvalidOperationException("Not recording anything! Can't capture event!");
            }

            if (customEventRecorder == null)
            {
                customEventRecorder = new CustomEventRecorder();
            }

            customEventRecorder.Record(new CustomEventCapture(timerManager.TimeProvider.CurrentTime(),
                new CustomEvent(name, new Metadata(contents))));
        }

        /// <summary>
        /// If you want to keep up with something special that occurred at a certain time in your recording, then you can call this function with the details of the special event.
        /// </summary>
        /// <param name="name">Name of the event.</param>
        /// <param name="contents">Details of the event.</param>
        /// <exception cref="System.InvalidOperationException">Thrown when the recorder is stopped.</exception>
        /// <remarks>Will be ignored if the recorder is currently paused.</remarks>
        public void CaptureCustomEvent(string name, Dictionary<string, string> contents)
        {
            if (CurrentlyPaused())
            {
                return;
            }

            if (CurrentlyStopped())
            {
                throw new System.InvalidOperationException("Not recording anything! Can't capture event!");
            }

            if (customEventRecorder == null)
            {
                customEventRecorder = new CollectionRecorders.CustomEventRecorder();
            }

            customEventRecorder.Record(new CustomEventCapture(timerManager.TimeProvider.CurrentTime(), name, contents));
        }

        /// <summary>
        /// Starts recording the subjects.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Thrown when a recording is already in progress.</exception>
        public void Start()
        {
            if (CurrentlyStopped() == false)
            {
                throw new System.InvalidOperationException(
                    "Can't start a new recording while one is in progress. Please finish the current recording before tryign to start a new one");
            }

            pauseSlices = new List<Vector2>();
            customEventRecorder.Clear();
            currentState = RecordingState.Recording;
            timeStarted = timerManager.TimeProvider.CurrentTime();

            foreach (var subRecorder in subRecorders)
            {
                subRecorder.Start();
            }
        }

        /// <summary>
        /// Removes all registered subjects that where going to be recorded, removing any chance that their data will make it to the final recording. 
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Thrown when a recording is already in progress.</exception>
        public void ClearSubjects()
        {
            if (CurrentlyStopped() == false)
            {
                throw new System.InvalidOperationException("Can't clear subjects while in the middle of a recording.");
            }

            subRecorders.Clear();
        }


        /// <summary>
        /// The different subjects to include in our recording.
        /// </summary>
        /// <returns>The different subjects to include in our recording.</returns>
        public List<Recorder> SubRecorders()
        {
            return subRecorders;
        }

        /// <summary>
        /// Clears all meta data that has been set up to this point.
        /// </summary>
        public void ClearMetadata()
        {
            metadata.Reset();
        }
    }
}