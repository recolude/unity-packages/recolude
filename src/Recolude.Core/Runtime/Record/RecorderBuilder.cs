using System.Collections.Generic;
using UnityEngine;
using Recolude.Core.Time;

namespace Recolude.Core.Record
{
    public class RecorderBuilder
    {
        RecorderInfo recorderInfo;

        List<RecorderBuilder> subRecorderBuilders;

        List<ICaptureRecorder<ICapture>> captureRecorders;

        ITimerManager timerManager;

        public RecorderBuilder(ITimerManager timerManager, string id, string recordingName)
        {
            this.timerManager = timerManager;
            recorderInfo = new RecorderInfo(id, recordingName);
            subRecorderBuilders = new List<RecorderBuilder>();
            captureRecorders = new List<ICaptureRecorder<ICapture>>();
        }

        public RecorderBuilder(ITimeProvider timeProvider) : this(new TimerManager(timeProvider), "", "Recording") { }

        public RecorderBuilder(string id, string recordingName) : this(new TimerManager(new UnityTimeProvider()), id, recordingName) { }

        public RecorderBuilder() : this(new TimerManager(new UnityTimeProvider()), "", "Recording") { }

        public void AddCaptureRecorder(ICaptureRecorder<ICapture> recorder)
        {
            captureRecorders.Add(recorder);
        }

        public void AddCaptureRecorders(IEnumerable<ICaptureRecorder<ICapture>> recorder)
        {
            captureRecorders.AddRange(recorder);
        }

        public RecorderBuilder AddSubject(GameObject gameObject)
        {
            var subjectBuilder = new RecorderBuilder(timerManager, gameObject.GetInstanceID().ToString(), gameObject.name);
            subjectBuilder.AddCaptureRecorders(RecorderStrategies.BasicSubjectCaptureRecorders(timerManager, gameObject));
            return subjectBuilder;
        }

        public Recorder Build()
        {
            var subRecorders = new List<Recorder>(subRecorderBuilders.Count);
            foreach (var builder in subRecorderBuilders)
            {
                subRecorders.Add(builder.Build());
            }
            return new Recorder(timerManager, recorderInfo, subRecorders, captureRecorders);
        }
    }
}