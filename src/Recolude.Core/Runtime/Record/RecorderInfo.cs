using System;
using System.Collections.Generic;
using UnityEngine;

namespace Recolude.Core.Record
{
    public class RecorderInfo
    {
        string id;

        string name;

        Metadata metadata;

        List<IBinary> binaries;

        List<BinaryReference> binaryReferences;

        public RecorderInfo(string id, string name, Metadata metadata)
        {
            this.id = id ?? throw new ArgumentNullException(nameof(id));
            this.name = name ?? throw new ArgumentNullException(nameof(name));
            this.metadata = metadata ?? new Metadata();

            binaries = new List<IBinary>();
            binaryReferences = new List<BinaryReference>();
        }

        public RecorderInfo(string id, string name) : this(id, name, new Metadata())
        {
        }
        
        public RecorderInfo(GameObject gameObject, Metadata metadata) : this(gameObject.GetInstanceID().ToString(), gameObject.name, metadata)
        {
        }

        public RecorderInfo() : this("", "Recording")
        {
        }

        public string Id { get => id; }
        public string Name { get => name; }
        public Metadata Metadata { get => metadata; }
        public List<IBinary> Binaries { get => binaries; }
        public List<BinaryReference> BinaryReferences { get => binaryReferences; }
    }
}