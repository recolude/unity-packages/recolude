using Recolude.Core.Record.CollectionRecorders;
using Recolude.Core.Record.Emitters;
using Recolude.Core.Record.Watchers;
using UnityEngine;

namespace Recolude.Core.Record
{

    public static class RecorderStrategies
    {
        public static ICaptureRecorder<ICapture>[] BasicSubjectCaptureRecorders(ITimerManager timerManager, GameObject gameObject)
        {
            return BasicSubjectCaptureRecorders(timerManager, gameObject, 10, 0.01f);
        }

        public static ICaptureRecorder<ICapture>[] BasicSubjectCaptureRecorders(ITimerManager timerManager, GameObject gameObject, int captureRate, float minimumDelta)
        {
            var captureRecorders = new ICaptureRecorder<ICapture>[3];

            // Life Cycle
            var lifeCycleRecorder = new LifeCycleRecorder();
            var lifeCycleWatcher = LifeCycleWatcher.Build(gameObject, timerManager.TimeProvider, lifeCycleRecorder);
            captureRecorders[0] = lifeCycleRecorder;

            // Position
            var positionRecorder = new PositionRecorder(minimumDelta);
            var positionSource = new TransformPositionSource(gameObject);
            var positionEmitter = new VectorEmitter(positionSource, positionRecorder);
            captureRecorders[1] = positionRecorder;
            timerManager.Build(captureRate, positionEmitter);

            // Rotation
            var rotationRecorder = new RotationRecorder(minimumDelta);
            var rotationEmitter = new RotationEmitter(gameObject, rotationRecorder);
            captureRecorders[2] = rotationRecorder;
            timerManager.Build(captureRate, rotationEmitter);

            return captureRecorders;
        }
    }

}