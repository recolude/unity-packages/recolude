using Recolude.Core.Time;
using System;
using System.Collections.Generic;
namespace Recolude.Core.Record
{
    public class TimerManager : ITimerManager
    {
        public ITimeProvider TimeProvider { get; }

        Dictionary<float, Timer> timerCache;

        public TimerManager(ITimeProvider timeProvider)
        {
            TimeProvider = timeProvider;
            timerCache = new Dictionary<float, Timer>();
            TimerManagerBehavior.Build(this);
        }

        /// <summary>
        /// Creates a new TimerManager that used the BasicTimeProvider as a time provider.
        /// </summary>
        /// <returns>TimerManager using BasicTimeProvider</returns>
        public TimerManager() : this(new UnityTimeProvider())
        {

        }

        public Timer Build(float frameRate, params IEmitter[] emitters)
        {
            if (timerCache.TryGetValue(frameRate, out var cachedTimer))
            {
                cachedTimer.AddEmitters(emitters);
                return cachedTimer;
            }

            var timer = new Timer(TimeProvider, frameRate, emitters);
            timerCache[frameRate] = timer;
            return timer;
        }

        internal void Tick()
        {
            foreach (var keyVal in timerCache)
            {
                keyVal.Value.Tick();
            }
        }
    }
}