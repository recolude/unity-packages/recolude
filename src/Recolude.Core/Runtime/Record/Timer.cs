using UnityEngine;
using Recolude.Core.Time;
using System.Collections.Generic;

namespace Recolude.Core.Record
{
    public class Timer
    {
        ITimeProvider timeProvider;

        List<IEmitter> emitters;

        float framerate;

        float frameDuration;

        float lastTime;

        float accumulatedTime;

        public Timer(ITimeProvider timeProvider, float frameRate, params IEmitter[] emitters)
        {
            this.timeProvider = timeProvider;
            this.framerate = frameRate;
            this.emitters = new List<IEmitter>(emitters);

            frameDuration = 1f / frameRate;
            accumulatedTime = 0;
            lastTime = timeProvider.CurrentTime();
        }

        public void AddEmitters(IEnumerable<IEmitter> emitters)
        {
            this.emitters.AddRange(emitters);
        }

        internal void Tick()
        {
            var newTime = timeProvider.CurrentTime();
            accumulatedTime += newTime - lastTime;
            if (accumulatedTime >= frameDuration)
            {
                accumulatedTime = 0f;
                for (int i = emitters.Count - 1; i >= 0; i--)
                {
                    if (emitters[i].Emit(newTime) == false)
                    {
                        emitters.RemoveAt(i);
                    }
                }
            }
            lastTime = newTime;
        }
    }
}