using Recolude.Core.Time;
using System.Collections.Generic;
using UnityEngine;

namespace Recolude.Core.Record
{
    public class TimerManagerBehavior : MonoBehaviour
    {
        TimerManager timerManager;

        public static TimerManagerBehavior Build(TimerManager timerManager)
        {
            var timeManagerGameObject = new GameObject("Timer Manager Instance");
            DontDestroyOnLoad(timeManagerGameObject);

            var managerBehavior = timeManagerGameObject.AddComponent<TimerManagerBehavior>();
            managerBehavior.timerManager = timerManager;

            return managerBehavior;
        }

        void LateUpdate()
        {
            timerManager.Tick();
        }

    }

}