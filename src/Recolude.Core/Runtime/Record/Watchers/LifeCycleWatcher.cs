using UnityEngine;
using Recolude.Core.Record.CollectionRecorders;
using Recolude.Core.Time;

namespace Recolude.Core.Record.Watchers
{
    public class LifeCycleWatcher : MonoBehaviour
    {
        ITimeProvider timeProvider;

        LifeCycleRecorder recorder;

        /// <summary>
        /// Builds a new watcher to watch the different unity life cycle events
        /// for the given game object. 
        /// </summary>
        /// <param name="gameObject">The game object to watch for new life cycle events.</param>
        /// <param name="timeProvider">What we use to time stamp our life cycle events.</param>
        /// <param name="recorder">The recorder to log our life cycle events to.</param>
        /// <returns>A LifeCycleWatcher attached to the specify gameObject.</returns>
        public static LifeCycleWatcher Build(GameObject gameObject, ITimeProvider timeProvider, LifeCycleRecorder recorder)
        {
            if (timeProvider == null)
            {
                throw new System.ArgumentNullException(nameof(timeProvider));
            }

            if (recorder == null)
            {
                throw new System.ArgumentNullException(nameof(recorder));
            }

            var observer = gameObject.AddComponent<LifeCycleWatcher>();
            observer.timeProvider = timeProvider;
            observer.Recorder = recorder;
            observer.Start();
            return observer;
        }

        public LifeCycleRecorder Recorder
        {
            get => recorder;
            private set
            {
                if (value == null)
                {
                    throw new System.ArgumentNullException(nameof(value), "Can not set null recorder");
                }
                recorder = value;

                if (startTimeEmittedWithoutRecorder)
                {
                    CaptureUnityEvent(UnityLifeCycleEvent.Start);
                }

                if (endableTimeEmittedWithoutRecorder)
                {
                    CaptureUnityEvent(UnityLifeCycleEvent.Enable);
                }

                if (disableTimeEmittedWithoutRecorder)
                {
                    CaptureUnityEvent(UnityLifeCycleEvent.Disable);
                }

                if (destroyTimeEmittedWithoutRecorder)
                {
                    CaptureUnityEvent(UnityLifeCycleEvent.Destroy);
                }
            }
        }

        bool startTimeEmittedWithoutRecorder;

        bool disableTimeEmittedWithoutRecorder;

        bool endableTimeEmittedWithoutRecorder;

        bool destroyTimeEmittedWithoutRecorder;

        void Start()
        {
            if (CaptureUnityEvent(UnityLifeCycleEvent.Start) == false)
            {
                startTimeEmittedWithoutRecorder = true;
            }
        }

        void OnDisable()
        {
            if (CaptureUnityEvent(UnityLifeCycleEvent.Disable) == false)
            {
                disableTimeEmittedWithoutRecorder = true;
            }
        }

        void OnDestroy()
        {
            if (CaptureUnityEvent(UnityLifeCycleEvent.Destroy) == false)
            {
                destroyTimeEmittedWithoutRecorder = true;
            }
        }

        void OnEnable()
        {
            if (CaptureUnityEvent(UnityLifeCycleEvent.Enable) == false)
            {
                endableTimeEmittedWithoutRecorder = true;
            }
        }

        private bool CaptureUnityEvent(UnityLifeCycleEvent e)
        {
            if (recorder == null)
            {
                return false;
            }
            var time = timeProvider.CurrentTime();
            recorder.Record(new UnityLifeCycleEventCapture(time, e));
            return true;
        }

    }

}