using UnityEngine;
namespace Recolude.Core
{

    public class Recording : IRecording
    {
        private string id;

        private string name;

        ICaptureCollection<ICapture>[] collections;

        IRecording[] recordings;

        Metadata metadata;

        IBinary[] binaries;

        BinaryReference[] binaryReferences;

        bool calculatedTimes;

        float calculatedDuration;

        float calculatedStart;

        float calculatedEnd;

        public Recording(string id, string name) : this(id, name, null, null, null, null, null)
        { }

        public Recording(params IRecording[] recordings) : this("", "", recordings, null, null, null, null)
        { }

        public Recording(string id, string name, IRecording[] recordings) : this(id, name, recordings, null, null, null, null)
        { }
        
        public Recording(string id, string name, Metadata metadata) : this(id, name, null, null, metadata, null, null)
        { }

        public Recording(string id, string name, IRecording[] recordings, ICaptureCollection<ICapture>[] collections, Metadata metadata) : this(id, name, recordings, collections, metadata, null, null)
        { }

        public Recording(string id, string name, IRecording[] recordings, ICaptureCollection<ICapture>[] collections, Metadata metadata, IBinary[] binaries, BinaryReference[] binaryReferences)
        {
            this.id = id;
            this.name = name;
            this.collections = collections ?? new ICaptureCollection<ICapture>[0];
            this.recordings = recordings ?? new IRecording[0];
            this.metadata = metadata ?? new Metadata();
            this.binaries = binaries ?? new IBinary[0];
            this.binaryReferences = binaryReferences ?? new BinaryReference[0];
        }

        private void CalculateAllTimes()
        {
            if ((recordings == null || recordings.Length == 0) && (collections == null || collections.Length == 0))
            {
                return;
            }

            calculatedStart = Mathf.Infinity;
            calculatedEnd = Mathf.NegativeInfinity;
            bool anyCalculationOccurred = false;

            if (recordings != null)
            {
                foreach (var subject in recordings)
                {
                    if (subject != null)
                    {
                        calculatedStart = Mathf.Min(calculatedStart, subject.StartTime);
                        calculatedEnd = Mathf.Max(calculatedEnd, subject.EndTime);
                        anyCalculationOccurred = true;
                    }
                }
            }

            foreach (var collection in collections)
            {
                foreach (var capture in collection.Captures)
                {
                    calculatedStart = Mathf.Min(calculatedStart, capture.Time);
                    calculatedEnd = Mathf.Max(calculatedEnd, capture.Time);
                    anyCalculationOccurred = true;
                }
            }

            if (anyCalculationOccurred == false)
            {
                this.calculatedDuration = 0;
                return;
            }

            this.calculatedDuration = calculatedEnd - calculatedStart;
            this.calculatedTimes = true;
        }

        public string ID => id;

        public string Name => name;

        public ICaptureCollection<ICapture>[] CaptureCollections => collections;

        public IRecording[] Recordings => recordings;

        public Metadata Metadata => metadata;

        public IBinary[] Binaries => binaries;

        public BinaryReference[] BinaryReferences => binaryReferences;

        public float Duration
        {
            get
            {
                if (!calculatedTimes)
                {
                    CalculateAllTimes();
                }
                return calculatedDuration;
            }
        }

        public float StartTime
        {
            get
            {
                if (!calculatedTimes)
                {
                    CalculateAllTimes();
                }
                return calculatedStart;
            }
        }

        public float EndTime
        {
            get
            {
                if (!calculatedTimes)
                {
                    CalculateAllTimes();
                }
                return calculatedEnd;
            }
        }
    }

}