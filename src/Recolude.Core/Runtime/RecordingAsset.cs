﻿using System;
using System.IO;
using UnityEngine;
using Recolude.Core.IO;

namespace Recolude.Core
{
    [Serializable]
    public class RecordingAsset : RecordingReference, ISerializationCallbackReceiver
    {
        [SerializeField] [HideInInspector] byte[] recordingData;

        public override bool Loaded => recordingData != null;

        public static RecordingAsset Build(IRecording recording)
        {
            var asset = ScriptableObject.CreateInstance<RecordingAsset>();
            asset.RecordingReferenced = recording;
            return asset;
        }

        public void OnAfterDeserialize()
        {
            using (var assetData = new MemoryStream(recordingData))
            using (var reader = new RAPReader(assetData))
            {
                this.RecordingReferenced = reader.Recording();
            }
        }

        public void OnBeforeSerialize()
        {
            using (var assetData = new MemoryStream())
            {
                using (var writer = new RAPWriter(assetData, true))
                {
                    writer.Write(this.RecordingReferenced);
                }

                this.recordingData = assetData.ToArray();
            }
        }
    }
}