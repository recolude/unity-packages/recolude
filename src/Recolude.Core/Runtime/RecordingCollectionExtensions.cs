using UnityEngine;

namespace Recolude.Core
{

    public static class RecordingCollectionExtensions
    {

        /// <summary>
        /// Creates a set of tables that represent the recordings in CSV form.
        /// </summary>
        /// <param name="recordings">The recordings to convert to CSV</param>
        /// <returns>CSV representation of the recordings</returns>
        public static IO.CSVPage[] ToCSV(this IRecording[] recordings)
        {
            var pages = new IO.CSVPage[6][];
            for (int p = 0; p < pages.Length; p++)
            {
                pages[p] = new IO.CSVPage[recordings.Length];
            }

            string[,] recordingsPage = new string[recordings.Length + 1, 2];
            recordingsPage[0, 0] = "ID";
            recordingsPage[0, 1] = "Name";

            for (int recIndex = 0; recIndex < recordings.Length; recIndex++)
            {
                recordingsPage[recIndex + 1, 0] = recIndex.ToString();
                recordingsPage[recIndex + 1, 1] = recordings[recIndex].Name;

                var recPages = recordings[recIndex].ToCSV();
                for (int pageIndex = 0; pageIndex < recPages.Length; pageIndex++)
                {
                    pages[pageIndex][recIndex] = recPages[pageIndex];
                }
            }

            return new IO.CSVPage[7]{
                new IO.CSVPage("Recordings", recordingsPage),
                IO.CSVPage.Combine("Subjects", "Recording", pages[0]),
                IO.CSVPage.Combine("MetaData", "Recording", pages[1]),
                IO.CSVPage.Combine("CustomEvents", "Recording", pages[2]),
                IO.CSVPage.Combine("PositionData", "Recording", pages[3]),
                IO.CSVPage.Combine("RotationData", "Recording", pages[4]),
                IO.CSVPage.Combine("LifeCycleEvents", "Recording", pages[5])
            };
        }

        private static string[,] LifeEventCSVEntries(IRecording recording)
        {
            int lifeEventEntries = 0;
            for (int i = 0; i < recording.Recordings.Length; i++)
            {
                lifeEventEntries += recording.Recordings[i].GetCollection<Capture<UnityLifeCycleEvent>>().Captures.Length;
            }
            var entries = new string[lifeEventEntries + 1, 3];

            entries[0, 0] = "SubjectID";
            entries[0, 1] = "Time";
            entries[0, 2] = "Event";

            int entryIndex = 1;
            foreach (var subj in recording.Recordings)
            {
                foreach (var data in subj.GetCollection<Capture<UnityLifeCycleEvent>>().Captures)
                {
                    entries[entryIndex, 0] = subj.ID.ToString();
                    entries[entryIndex, 1] = data.Time.ToString();
                    entries[entryIndex, 2] = ((UnityLifeCycleEvent)data.Value).ToString();
                    entryIndex++;
                }
            }

            return entries;
        }


        private static string[,] MetadataCSVEntries(IRecording recording)
        {
            int metadataEntries = recording.Metadata.Count;
            for (int i = 0; i < recording.Recordings.Length; i++)
            {
                metadataEntries += recording.Recordings[i].Metadata.Count;
            }
            var entries = new string[metadataEntries + 1, 3];

            entries[0, 0] = "SubjectID";
            entries[0, 1] = "Key";
            entries[0, 2] = "Value";

            int entryIndex = 1;
            foreach (var data in recording.Metadata)
            {
                entries[entryIndex, 0] = "";
                entries[entryIndex, 1] = data.Key;
                entries[entryIndex, 2] = data.Value.ToString();
                entryIndex++;
            }

            foreach (var subj in recording.Recordings)
            {
                foreach (var data in subj.Metadata)
                {
                    entries[entryIndex, 0] = subj.ID.ToString();
                    entries[entryIndex, 1] = data.Key;
                    entries[entryIndex, 2] = data.Value.ToString();
                    entryIndex++;
                }
            }

            return entries;
        }

        private static string[,] VectorEventsCSVEntries(bool pos, IRecording recording)
        {
            int entryCount = 0;
            for (int i = 0; i < recording.Recordings.Length; i++)
            {
                ICaptureCollection<Capture<Vector3>> captures;
                if (pos)
                {
                    captures = recording.Recordings[i].PositionCollection();
                }
                else
                {
                    captures = recording.Recordings[i].RotationCollection();
                }

                if (captures != null)
                {
                    entryCount += captures.Captures.Length;
                }
            }
            var entries = new string[entryCount + 1, 5];

            entries[0, 0] = "SubjectID";
            entries[0, 1] = "Time";
            entries[0, 2] = "X";
            entries[0, 3] = "Y";
            entries[0, 4] = "Z";

            int entryIndex = 1;
            foreach (var subj in recording.Recordings)
            {
                ICaptureCollection<Capture<Vector3>> captures;
                if (pos)
                {
                    captures = subj.PositionCollection();
                }
                else
                {
                    captures = subj.RotationCollection();
                }

                if (captures == null)
                {
                    continue;
                }

                foreach (var capture in captures.Captures)
                {
                    entries[entryIndex, 0] = subj.ID.ToString();
                    entries[entryIndex, 1] = capture.Time.ToString();
                    entries[entryIndex, 2] = capture.Value.x.ToString();
                    entries[entryIndex, 3] = capture.Value.y.ToString();
                    entries[entryIndex, 4] = capture.Value.z.ToString();
                    entryIndex++;
                }
            }

            return entries;
        }

        private static string[,] CustomEventsCSVEntries(IRecording recording)
        {
            int customEventEntries = 0;
            var parentEvents = recording.CustomEventCollection();
            foreach (var e in parentEvents.Captures)
            {
                customEventEntries += e.Value.Contents.Count;
            }

            for (int i = 0; i < recording.Recordings.Length; i++)
            {
                foreach (var e in recording.Recordings[i].GetCollection<Capture<CustomEvent>>(CollectionNames.CustomEvent).Captures)
                {
                    customEventEntries += e.Value.Contents.Count;
                }
            }
            var entries = new string[customEventEntries + 1, 6];

            entries[0, 0] = "SubjectID";
            entries[0, 1] = "Index";
            entries[0, 2] = "Time";
            entries[0, 3] = "Name";
            entries[0, 4] = "Key";
            entries[0, 5] = "Value";

            int entryIndex = 1;
            foreach (var data in parentEvents.Captures)
            {
                foreach (var keyVal in data.Value.Contents)
                {
                    entries[entryIndex, 0] = "";
                    entries[entryIndex, 1] = entryIndex.ToString();
                    entries[entryIndex, 2] = data.Time.ToString();
                    entries[entryIndex, 3] = data.Value.Name;
                    entries[entryIndex, 4] = keyVal.Key;
                    entries[entryIndex, 5] = keyVal.Value.ToString();
                }
                entryIndex++;
            }

            foreach (var subj in recording.Recordings)
            {
                foreach (var data in subj.GetCollection<Capture<CustomEvent>>(CollectionNames.CustomEvent).Captures)
                {
                    foreach (var keyVal in data.Value.Contents)
                    {
                        entries[entryIndex, 0] = subj.ID.ToString();
                        entries[entryIndex, 1] = entryIndex.ToString();
                        entries[entryIndex, 2] = data.Time.ToString();
                        entries[entryIndex, 3] = data.Value.Name;
                        entries[entryIndex, 4] = keyVal.Key;
                        entries[entryIndex, 5] = keyVal.Value.ToString();
                    }
                    entryIndex++;
                }
            }

            return entries;
        }



        /// <summary>
        /// Builds a CSV representation of the data contained within the recording.
        /// </summary>
        /// <returns>CSV representation of the data contained within the recording</returns>
        public static IO.CSVPage[] ToCSV(this IRecording recording)
        {
            string[,] subjects = new string[recording.Recordings.Length + 1, 2];
            subjects[0, 0] = "ID";
            subjects[0, 1] = "Name";
            for (int i = 0; i < recording.Recordings.Length; i++)
            {
                subjects[i + 1, 0] = recording.Recordings[i].ID;
                subjects[i + 1, 1] = recording.Recordings[i].Name;
            }

            return new IO.CSVPage[6]{
                new IO.CSVPage("Subjects", subjects),
                new IO.CSVPage("MetaData", MetadataCSVEntries(recording)),
                new IO.CSVPage("CustomEvents", CustomEventsCSVEntries(recording)),
                new IO.CSVPage("PositionData", VectorEventsCSVEntries(true, recording)),
                new IO.CSVPage("RotationData", VectorEventsCSVEntries(false, recording)),
                new IO.CSVPage("LifeCycleEvents", LifeEventCSVEntries(recording))
            };
        }


    }

}