using UnityEngine.Events;

namespace Recolude.Core
{

    [System.Serializable]
    public class RecordingEvent : UnityEvent<IRecording>
    {

    }

}