using UnityEngine;
using System;

namespace Recolude.Core
{
    /// <summary>
    /// An abstract scriptable object meant to easily allow for a reference to 
    /// a recording to be wired into different components in the inspector.
    /// </summary>
    public abstract class RecordingReference : ScriptableObject, IRecording
    {
        private static readonly InvalidOperationException unloadedException = new InvalidOperationException("The reference to the recording has not been loaded");

        public IRecording RecordingReferenced { get; protected set; }

        public abstract bool Loaded { get; }

        public string ID
        {
            get
            {
                if (!Loaded) { throw unloadedException; }
                return RecordingReferenced.ID;
            }
        }

        public string Name
        {
            get
            {
                if (!Loaded) { throw unloadedException; }
                return RecordingReferenced.Name;
            }
        }

        public float Duration
        {
            get
            {
                if (!Loaded) { throw unloadedException; }
                return RecordingReferenced.Duration;
            }
        }

        public float StartTime
        {
            get
            {
                if (!Loaded) { throw unloadedException; }
                return RecordingReferenced.StartTime;
            }
        }

        public float EndTime
        {
            get
            {
                if (!Loaded) { throw unloadedException; }
                return RecordingReferenced.EndTime;
            }
        }

        public ICaptureCollection<ICapture>[] CaptureCollections
        {
            get
            {
                if (!Loaded) { throw unloadedException; }
                return RecordingReferenced.CaptureCollections;
            }
        }

        public IRecording[] Recordings
        {
            get
            {
                if (!Loaded) { throw unloadedException; }
                return RecordingReferenced.Recordings;
            }
        }

        public Metadata Metadata
        {
            get
            {
                if (!Loaded) { throw unloadedException; }
                return RecordingReferenced.Metadata;
            }
        }

        public IBinary[] Binaries
        {
            get
            {
                if (!Loaded) { throw unloadedException; }
                return RecordingReferenced.Binaries;
            }
        }

        public BinaryReference[] BinaryReferences
        {
            get
            {
                if (!Loaded) { throw unloadedException; }
                return RecordingReferenced.BinaryReferences;
            }
        }
    }
}