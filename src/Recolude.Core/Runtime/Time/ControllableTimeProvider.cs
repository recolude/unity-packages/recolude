using System;

namespace Recolude.Core.Time
{
    /// <summary>
    /// A time provider that needs it's time explicitly set externally in order
    /// to change. 
    /// 
    /// Useful for testing!
    /// </summary>
    public class ControllableTimeProvider : ITimeProvider
    {
        float time;

        public ControllableTimeProvider(float initialTime)
        {
            if (float.IsNaN(initialTime))
            {
                throw new ArgumentException("Initial time can not be NaN", nameof(initialTime));
            }
            this.time = initialTime;
        }

        /// <summary>
        /// Creates a new provider with the initial set time of 0.
        /// </summary>
        public ControllableTimeProvider()
        {
            time = 0;
        }

        public void SetTime(float newTime)
        {
            if (float.IsNaN(newTime))
            {
                throw new ArgumentException("New time can not be NaN", nameof(newTime));
            }
            time = newTime;
        }

        /// <summary>
        /// The last time set.
        /// </summary>
        /// <returns>The last time set.</returns>
        public float CurrentTime()
        {
            return time;
        }

    }

}