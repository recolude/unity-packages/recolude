namespace Recolude.Core.Time
{
    /// <summary>
    /// The way Recolude.Core can retrieve the current time. Kind of 
    /// necessary with how many different ways you can query "time" inside of
    /// Unity.
    /// </summary>
    public interface ITimeProvider
    {
        /// <summary>
        /// The current game time.
        /// </summary>
        /// <returns>The current game time.</returns>
        float CurrentTime();
    }
}