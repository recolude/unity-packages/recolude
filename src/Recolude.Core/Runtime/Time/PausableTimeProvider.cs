﻿using System;

namespace Recolude.Core.Time
{
    /// <summary>
    /// Wraps a time provider to allow the developer to emulate pausing time.
    /// Accumulated time while paused will be used as an offset for providing 
    /// future time readings.
    /// </summary>
    public class PausableTimeProvider : ITimeProvider
    {
        ITimeProvider provider;

        float timeOffset;

        float timePaused;

        bool paused;

        /// <summary>
        /// Whether or not the current time provider is paused from advancing 
        /// time.
        /// </summary>
        /// <value>Whether or not the current time provider is paused.</value>
        public bool Paused { get => paused; }

        /// <summary>
        /// Create a new time provider that polls from the underlying 
        /// provider until paused.
        /// </summary>
        /// <param name="provider">The underlying provider to read from.</param>
        public PausableTimeProvider(ITimeProvider provider)
        {
            if (provider == null)
            {
                throw new ArgumentNullException(nameof(provider));
            }
            this.timeOffset = 0;
            this.provider = provider;
            this.paused = false;
            this.timePaused = 0;
        }

        /// <summary>
        /// Pauses the time provider from taking any more readings from the 
        /// underlying time provider.
        /// </summary>
        public void Pause()
        {
            if (paused)
            {
                throw new InvalidOperationException("Already paused");
            }
            paused = true;
            timePaused = provider.CurrentTime();
        }

        /// <summary>
        /// Allows this time provider to continue to take readings from the 
        /// underlying time provider.
        /// </summary>
        /// <returns>The time that transpired while paused.</returns>
        public float Unpause()
        {
            if (!paused)
            {
                throw new InvalidOperationException("Not currently paused");
            }
            paused = false;
            float timeTranspiredWhilePaused = provider.CurrentTime() - timePaused;
            timeOffset += timeTranspiredWhilePaused;
            return timeTranspiredWhilePaused;
        }

        /// <summary>
        /// The current time. 
        /// If paused, then it's the last read time from the underlying 
        /// provider.
        /// </summary>
        /// <returns>The current time.</returns>
        public float CurrentTime()
        {
            if (paused)
            {
                return timePaused;
            }
            return provider.CurrentTime() - timeOffset;
        }
    }
}