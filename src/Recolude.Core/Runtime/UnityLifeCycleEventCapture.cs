﻿using UnityEngine;

namespace Recolude.Core
{

    /// <summary>
    /// An event that represents an event that occurs to a monobehavior instance.
    /// </summary>
    [System.Serializable]
    public class UnityLifeCycleEventCapture : Capture<UnityLifeCycleEvent>
    {

        /// <summary>
        /// Create a new UnityLifeCycleEventCapture.
        /// </summary>
        /// <param name="time">The time the event occurred in the recording.</param>
        /// <param name="lifeCycleEvent">The lifecycle event that occurred</param>
        public UnityLifeCycleEventCapture(float time, UnityLifeCycleEvent lifeCycleEvent) : base(time, lifeCycleEvent)
        {
        }

        /// <summary>
        /// Creates a new UnityLifeCycleEventCapture with lifecycle event but with a modified time, leaving the original event unchanged.
        /// </summary>
        /// <param name="newTime">The new time the event occurred in the recording.</param>
        /// <returns>A entirely new capture that occurred with the time passed in.</returns>
        public override ICapture SetTime(float newTime)
        {
            return new UnityLifeCycleEventCapture(newTime, Value);
        }

        public override Capture<UnityLifeCycleEvent> SetValue(UnityLifeCycleEvent val)
        {
            return new UnityLifeCycleEventCapture(Time, val);
        }
        
        /// <summary>
        /// Builds a JSON string that represents the UnityLifeCycleEventCapture object.
        /// </summary>
        /// <returns>A JSON string.</returns>
        public override string ToJSON() { return string.Format("{{ \"Time\": {0}, \"Type\": \"{1}\" }}", Time, Value.ToString()); }

        /// <summary>
        /// Builds a string that represents a single row in a csv file that contains this object's data.
        /// </summary>
        /// <returns>A row of csv data as a string.</returns>
        public override string ToCSV() { return string.Format("{0}, {1}", Time, Value.ToString()); }

    }

}