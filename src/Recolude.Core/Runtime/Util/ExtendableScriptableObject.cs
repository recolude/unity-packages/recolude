﻿using System;
using UnityEngine;

namespace Recolude.Core.Util
{
    public class ExtendableScriptableObject : PropertyAttribute
    {
        public Type createType;

        public bool required;

        public ExtendableScriptableObject()
        {
            required = false;
        }

        public ExtendableScriptableObject(Type createType)
        {
            this.createType = createType;
            required = false;
        }
    }
}