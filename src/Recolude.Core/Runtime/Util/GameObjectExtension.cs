﻿using System;
using UnityEngine;

namespace Recolude.Core.Util
{
    public static class GameObjectExtension
    {
        public static T GetOrAddComponent<T>(this GameObject gameObject) where T : MonoBehaviour
        {
            if (gameObject.TryGetComponent<T>(out var component))
            {
                return component;
            }

            return gameObject.AddComponent<T>();
        }

        public static T RequireComponent<T>(this GameObject gameObject)
        {
            var component = gameObject.GetComponent<T>();
            if (component == null)
            {
                throw new SystemException(
                    $"Game Object '{gameObject.name}' did not have the required component '{nameof(T)}'");
            }

            return component;
        }

        public static T RequireComponentInChildren<T>(this GameObject gameObject)
        {
            var component = gameObject.GetComponentInChildren<T>();
            if (component == null)
            {
                throw new SystemException(
                    $"Game Object '{gameObject.name}' did not have the required component '{nameof(T)}' in any of it's children");
            }

            return component;
        }

        public static T RequireComponentInParent<T>(this GameObject gameObject)
        {
            var component = gameObject.GetComponentInParent<T>();
            if (component == null)
            {
                throw new SystemException(
                    $"Game Object '{gameObject.name}' did not have the required component '{nameof(T)}' in it's parent");
            }

            return component;
        }
    }
}