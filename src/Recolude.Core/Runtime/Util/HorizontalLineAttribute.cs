﻿using System;
using UnityEngine;

namespace Recolude.Core.Util
{
    [AttributeUsage(AttributeTargets.Field, Inherited = true)]
    public class HorizontalLineAttribute: PropertyAttribute
    {
        private const float DefaultHeight = 1f;

        public float Height { get; private set; }

        public Color Color { get; private set; }

        public HorizontalLineAttribute(float height = DefaultHeight)
        {
            Height = height;
            Color = new Color(0, 0, 0);
        }
    }
}