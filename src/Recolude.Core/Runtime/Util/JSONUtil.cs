using System.Runtime.CompilerServices;
using System.Text;
using Recolude.Core.IO.PropertyWriters;
using UnityEngine;

namespace Recolude.Core.Util
{
    public static class JSON
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static byte ToByte(float f)
        {
            return (byte)(Mathf.Clamp01(f) * 255);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static string ColorToHex(Color color)
        {
            return $"#{ToByte(color.r):X2}{ToByte(color.g):X2}{ToByte(color.b):X2}";
        }


        /// <summary>
        /// Converts the Recording to a json formatted string.
        /// </summary>
        /// <returns>Json formatted String.</returns>
        public static string ToJSON(IRecording recording)
        {
            StringBuilder sb = new StringBuilder("{");
            sb.AppendFormat("\"Name\": \"{0}\", ", FormattingUtil.JavaScriptStringEncode(recording.Name));
            sb.AppendFormat("\"Duration\": {0}, ", recording.Duration);
            sb.AppendFormat("\"Metadata\": {0}, ", PropertyWriterCollection.Instance.JsonValue(recording.Metadata));

            sb.Append("\"CustomEvents\": [");
            var capturedCustomEvents =
                recording.GetCollection<Capture<CustomEvent>>(CollectionNames.CustomEvent).Captures;
            for (int i = 0; i < capturedCustomEvents.Length; i++)
            {
                sb.Append(capturedCustomEvents[i].ToJSON());
                if (i < capturedCustomEvents.Length - 1)
                {
                    sb.Append(",");
                }
            }

            sb.Append("], \"SubRecordings\": [");
            for (int i = 0; i < recording.Recordings.Length; i++)
            {
                sb.Append(ToJSON(recording.Recordings[i]));
                if (i < recording.Recordings.Length - 1)
                {
                    sb.Append(",");
                }
            }

            sb.Append("] }");

            return sb.ToString();
        }
    }
}