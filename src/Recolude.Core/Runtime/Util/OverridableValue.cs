﻿using System;
using UnityEngine;

namespace Recolude.Core.Util
{
    [Serializable]
    public class OverridableValue<T>
    {
        [SerializeField] private bool overrideValue;

        [SerializeField] private T value;

        public OverridableValue(T initialValue)
        {
            value = initialValue;
        }

        public bool OverrideValue => overrideValue;

        public T Value => value;

        public T GetValue(T defaultVal)
        {
            return OverrideValue ? Value : defaultVal;
        }
    }

    public class OverrideName : PropertyAttribute
    {
        private const string DefaultOverrideKeyword = "Override";

        public readonly string label;

        public readonly string overrideKeyword;

        public OverrideName(string label, string overrideKeyword = DefaultOverrideKeyword)
        {
            this.label = label;
            this.overrideKeyword = overrideKeyword;
            if (string.IsNullOrWhiteSpace(this.overrideKeyword))
            {
                this.overrideKeyword = DefaultOverrideKeyword;
            }
        }
    }
}