﻿using System;
using UnityEngine;
using UnityEngine.Rendering;
using Random = UnityEngine.Random;

namespace Recolude.Core.Util
{
    public class TerrainObjectInstancing : MonoBehaviour
    {
        [Serializable]
        public class InstancableObject
        {
            [SerializeField] private Mesh mesh;
            [SerializeField] private Material material;

            public Mesh Mesh => mesh;

            public Material Material => material;
        }

        [SerializeField] private Terrain terrain;

        [SerializeField] private InstancableObject[] objectsToInstance;

        [SerializeField] private Bounds[] areasToAvoid;

        [Header("Instancing Configuration")] [SerializeField]
        private LayerMask layer;

        [SerializeField] private int population;
        [SerializeField] private bool receiveShadows;
        [SerializeField] private ShadowCastingMode shadowCasting;

        [Range(-1, 1)] [SerializeField] private float minimumCullingDelta;
        [SerializeField] private float distanceToCull;

        [Header("Styling + Variation")]
        // [SerializeField]
        // private Color color1;
        //
        // [SerializeField] private Color color2;
        [SerializeField]
        private Vector2 range;

        [SerializeField] private Vector3 sizeMin;
        [SerializeField] private Vector3 sizeMax;

        private Matrix4x4[] matrices;

        // private MaterialPropertyBlock block;
        private Vector3[] positions;
        private Camera mainCam;
        private Matrix4x4[] notCulledMatrices;


        private Vector3 RandomPosition()
        {
            Vector3 position = new Vector3(Random.Range(0, range.x), 0, Random.Range(0, range.y));

            foreach (var area in areasToAvoid)
            {
                if (area.Contains(position))
                {
                    return RandomPosition();
                }
            }

            return position;
        }

        private void Setup()
        {
            
            for (int i = 0; i < areasToAvoid.Length; i++)
            {
                var center = areasToAvoid[i].center;
                center.y = 0;
                areasToAvoid[i].center = center;
            }
            
            matrices = new Matrix4x4[population];
            positions = new Vector3[population];
            Vector4[] colors = new Vector4[population];
            // block = new MaterialPropertyBlock();
            for (int i = 0; i < population; i++)
            {
                // Build matrix.
                var position = RandomPosition();
                position.y = terrain.SampleHeight(position) - 1;
                Quaternion rotation =
                    Quaternion.Euler(Random.Range(-4, 4), Random.Range(-180, 180), Random.Range(-4, 4));
                var sizeRange = sizeMax - sizeMin;
                Vector3 scale = sizeMin + new Vector3(Random.Range(0, sizeRange.x), Random.Range(0, sizeRange.y),
                    Random.Range(0, sizeRange.z));

                var mat = Matrix4x4.TRS(position, rotation, scale);
                matrices[i] = mat;
                positions[i] = position;
                // colors[i] = Color.Lerp(color1, color2, Random.value);
            }

            // Custom shader needed to read these!!
            // block.SetVectorArray("_BaseColors", colors);
        }


        private void Start()
        {
            mainCam = Camera.main;
            notCulledMatrices = new Matrix4x4[population];
            Setup();
        }

        private void Update()
        {
            var added = 0;

            var distSqr = Mathf.Pow(distanceToCull, 2);
            var transform = mainCam.transform;

            Vector3 flattenedMainCamPosition = transform.position;
            Vector3 flattenedMainCamForward = transform.forward;
            flattenedMainCamPosition.y = 0f;
            flattenedMainCamForward.y = 0f;

            for (var i = 0; i < population; i++)
            {
                Vector3 flattenedPosition = positions[i];

                flattenedPosition.y = 0f;

                var dir = flattenedPosition - flattenedMainCamPosition;
                if (dir.sqrMagnitude < distSqr &&
                    Vector3.Dot(dir.normalized, flattenedMainCamForward) > minimumCullingDelta)
                {
                    notCulledMatrices[added] = matrices[i];
                    added++;
                }
            }

            foreach (var io in objectsToInstance)
            {
                Graphics.DrawMeshInstanced(
                    io.Mesh,
                    0,
                    io.Material,
                    notCulledMatrices,
                    added,
                    null, // block,
                    shadowCasting,
                    receiveShadows,
                    layer.value,
                    mainCam
                );
            }
        }

        private void OnDrawGizmosSelected()
        {
            if (terrain == null)
            {
                return;
            }

            Gizmos.color = Color.red;
            foreach (var area in areasToAvoid)
            {
                var size = area.size / 2;
                var center = area.center;

                var bottomLeft = terrain.SampleHeight(center + new Vector3(-size.x, 0, -size.z));
                var bottomRight = terrain.SampleHeight(center + new Vector3(size.x, 0, -size.z));
                var topLeft = terrain.SampleHeight(center + new Vector3(-size.x, 0, size.z));
                var topRight = terrain.SampleHeight(center + new Vector3(size.x, 0, size.z));
                var top = Mathf.Max(bottomLeft + bottomRight, topLeft, topRight);
                var bottom = Mathf.Min(bottomLeft + bottomRight, topLeft, topRight);

                center.y = (top + bottom) / 2f;
                Gizmos.DrawCube(center, area.size);
            }
        }
    }
}