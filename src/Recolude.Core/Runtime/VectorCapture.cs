﻿using UnityEngine;

namespace Recolude.Core
{

    /// <summary>
    /// An event that is meant to store a vector value that was present at some
    /// point in time. Used to represent things such as position and rotation.
    /// </summary>
    [System.Serializable]
    public class VectorCapture : Capture<Vector3>
    {
        /// <summary>
        /// Creates a new capture of a vector value in a point in time.
        /// </summary>
        /// <param name="time">A point in time the vector was observed.</param>
        /// <param name="vector">The value of the vector observed.</param>
        public VectorCapture(float time, Vector3 vector) : base(time, vector)
        {
        }

        /// <summary>
        /// Creates a new VectorCapture with the vector value but with a 
        /// modified time, leaving the original event unchanged.
        /// </summary>
        /// <param name="newTime">The new time the event occurred in the recording.</param>
        /// <returns>A entirely new capture that occurred with the time passed in.</returns>
        public override ICapture SetTime(float newTime)
        {
            return new VectorCapture(newTime, Value);
        }

        public override Capture<Vector3> SetValue(Vector3 val)
        {
            return new VectorCapture(Time, val);
        }

        /// <summary>
        /// Builds a JSON string that represents the VectorCapture object.
        /// </summary>
        /// <returns>A JSON string.</returns>
        public override string ToJSON()
        {
            return $"{{ \"Time\": {Time}, \"X\": {Value.x}, \"Y\": {Value.y}, \"Z\": {Value.z} }}";
        }

        /// <summary>
        /// Builds a string that represents a single row in a csv file that
        /// contains this object's data.
        /// </summary>
        /// <returns>A row of csv data as a string.</returns>
        public override string ToCSV()
        {
            return $"{Time}, {Value.x}, {Value.y}, {Value.z}";
        }

        public override string ToString()
        {
            return $"{Time}-({Value.x}, {Value.y}, {Value.z})";
        }
    }

}