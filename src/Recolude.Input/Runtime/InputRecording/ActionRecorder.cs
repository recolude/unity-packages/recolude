using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.InputSystem;

using Recolude.Core;
using Recolude.Core.Record;
using Recolude.Core.Properties;

public class ActionRecorder : MonoBehaviour
{

    [SerializeField]
    private List<InputAction> actionsToRecord;

    private List<Recorder> actionRecorders;

    // Start is called before the first frame update
    void Start()
    {
        actionRecorders = new List<Recorder>(actionsToRecord.Count);
        foreach (var action in actionsToRecord)
        {
            actionRecorders.Add(new Recorder(
                actionRecorders.Count.ToString(),
                action.name,
                new Metadata(new Dictionary<string, IProperty>(){
                    {"type", new StringProperty(action.type.ToString())}
                })
            ));

            // You'd listen to these, but here we care about playback...
            // and we can't just
            // action.started
            // action.performed
            // action.canceled
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
