using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Layouts;

using Recolude.Core;
using Recolude.Core.Record;
using Recolude.Core.Properties;

namespace Recolude.Input.InputRecording
{

    public class DeviceRecorder
    {
        private InputDevice deviceToRecord;

        private Recorder deviceRecorder;

        private static Metadata DeviceDescriptionToMetadata(InputDeviceDescription description)
        {
            var md = new Metadata();

            if (!string.IsNullOrWhiteSpace(description.capabilities))
            {
                md["capabilities"] = new StringProperty(description.capabilities);
            }

            if (!string.IsNullOrWhiteSpace(description.deviceClass))
            {
                md["deviceClass"] = new StringProperty(description.deviceClass);
            }

            if (!string.IsNullOrWhiteSpace(description.interfaceName))
            {
                md["interfaceName"] = new StringProperty(description.interfaceName);
            }

            if (!string.IsNullOrWhiteSpace(description.manufacturer))
            {
                md["manufacturer"] = new StringProperty(description.manufacturer);
            }

            if (!string.IsNullOrWhiteSpace(description.product))
            {
                md["product"] = new StringProperty(description.product);
            }

            if (!string.IsNullOrWhiteSpace(description.serial))
            {
                md["serial"] = new StringProperty(description.serial);
            }

            if (!string.IsNullOrWhiteSpace(description.version))
            {
                md["version"] = new StringProperty(description.version);
            }

            return md;
        }

        public DeviceRecorder(InputDevice deviceToRecord)
        {
            this.deviceToRecord = deviceToRecord;

            this.deviceRecorder = new Recorder(
                deviceToRecord.deviceId.ToString(),
                deviceToRecord.shortDisplayName,
                new Metadata(new Dictionary<string, IProperty>() {
                    {"display-name", new StringProperty("deviceToRecord.displayName")},
                    {"name",  new StringProperty(deviceToRecord.name)},
                    {"can-run-in-background", new BoolProperty(deviceToRecord.canRunInBackground)},
                    {"remote", new BoolProperty(deviceToRecord.remote)},
                    {"description", new MetadataProperty(DeviceDescriptionToMetadata(deviceToRecord.description))},
                    {"name", new MetadataProperty(DeviceDescriptionToMetadata(deviceToRecord.description))},
                })
            );

            foreach (var input in this.deviceToRecord.allControls)
            {
                // input.
            }
        }



    }

}
