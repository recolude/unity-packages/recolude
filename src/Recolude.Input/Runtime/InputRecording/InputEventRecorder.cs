using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.LowLevel;

using Recolude.Core;
using Recolude.Core.Record;
using Recolude.Core.Properties;

namespace Recolude.Input.InputRecording
{
    public unsafe class InputEventRecorder
    {

        Recorder recorder;

        public InputEventRecorder()
        {
            recorder = new Recorder();
            InputSystem.onEvent += OnEvent;
        }

        void OnEvent(InputEventPtr inputEvent, InputDevice device) {
            Debug.Log(*inputEvent.data);
        }
    }

}