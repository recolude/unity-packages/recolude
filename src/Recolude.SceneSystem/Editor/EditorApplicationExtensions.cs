﻿using Recolude.SceneSystem.ScenePlayback;
using Recolude.SceneSystem.SceneRecording;
using UnityEditor;
using UnityEngine;

namespace Recolude.SceneSystemEditor
{
    [InitializeOnLoad]
    public static class EditorApplicationExtensions
    {
        private const string iconsFolder = "Packages/com.recolude.core/Editor/";

        private const string recoludeIcon = iconsFolder + "RecoludeUnityScriptIcon.png";

        private const string recordingIcon = iconsFolder + "RecoludeRecordingIcon.png";

        private const float IconSize = 14;

        static EditorApplicationExtensions()
        {
            EditorApplication.hierarchyWindowItemOnGUI += HierarchyWindowItemOnGUI;
        }


        private static Texture2D recoludeIconInstance;

        private static Texture2D RecoludeIcon
        {
            get
            {
                if (recoludeIconInstance == null)
                {
                    recoludeIconInstance = EditorGUIUtility.FindTexture(recoludeIcon);
                }

                return recoludeIconInstance;
            }
        }

        private static Texture2D recordingIconInstance;

        private static Texture2D RecordingIcon
        {
            get
            {
                if (recordingIconInstance == null)
                {
                    recordingIconInstance = EditorGUIUtility.FindTexture(recordingIcon);
                }

                return recordingIconInstance;
            }
        }

        static void RenderIcon(Rect selectionRect)
        {
            if (RecoludeIcon == null) return;

            var size = new Rect(
                selectionRect.xMax - IconSize + 1,
                selectionRect.yMin + 1,
                IconSize,
                IconSize
            );
            GUI.DrawTexture(size, RecoludeIcon);
        }

        static void RenderRecordingIcon(Rect selectionRect)
        {
            if (RecordingIcon == null) return;

            var size = new Rect(
                selectionRect.xMax - (IconSize * 2.2f) + 2,
                selectionRect.yMin + 2,
                IconSize - 4,
                IconSize - 4
            );
            GUI.DrawTexture(size, RecordingIcon);
        }

        static void HierarchyWindowItemOnGUI(int instanceID, Rect selectionRect)
        {
            var obj = EditorUtility.InstanceIDToObject(instanceID);

            var go = obj as GameObject;
            if (go == null)
            {
                return;
            }

            if (go.TryGetComponent<RecordingSystem>(out var rs))
            {
                RenderIcon(selectionRect);
                if (rs.Recording)
                {
                    RenderRecordingIcon(selectionRect);
                }
            }

            if (go.TryGetComponent<PlaybackSystem>(out var ps))
            {
                RenderIcon(selectionRect);
            }

            if (go.TryGetComponent<RecordingConfig>(out var rc))
            {
                RenderIcon(selectionRect);
                if (rc.Recording)
                {
                    RenderRecordingIcon(selectionRect);
                }
            }
        }
    }
}