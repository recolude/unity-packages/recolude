﻿using Recolude.SceneSystem.Events;
using UnityEditor;
using UnityEngine;

namespace Recolude.SceneSystemEditor.Events
{
    [CustomEditor(typeof(GameEventBase), true)]
    public sealed class GameEventEditor : BaseGameEventEditor
    {
        private GameEvent Target => (GameEvent)target;

        protected override void DrawRaiseButton()
        {
            if (GUILayout.Button("Raise"))
            {
                Target.Raise();
            }
        }
    }
}