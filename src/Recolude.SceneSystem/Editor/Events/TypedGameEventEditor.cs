﻿using System;
using System.Reflection;
using Recolude.SceneSystem.Events;
using UnityEditor;
using UnityEngine;

namespace Recolude.SceneSystemEditor.Events
{
    [CustomEditor(typeof(GameEvent<>), true)]
    public class TypedGameEventEditor : BaseGameEventEditor
    {
        private MethodInfo _raiseMethod;

        protected override void OnEnable()
        {
            base.OnEnable();

            _raiseMethod = target.GetType().BaseType.GetMethod("Raise",
                BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public);
        }

        protected override void DrawRaiseButton()
        {
            SerializedProperty property = serializedObject.FindProperty("debugValue");

            // Sometimes it takes a second for unity to initialize this value for whatever reason
            if (property == null)
            {
                return;
            }
            
            using (var scope = new EditorGUI.ChangeCheckScope())
            {
                Type debugValueType = GetDebugValueType(property);
                // GenericPropertyDrawer.DrawPropertyDrawerLayout(property, debugValueType);
                EditorGUILayout.PropertyField(property);

                if (scope.changed)
                {
                    serializedObject.ApplyModifiedProperties();
                }
            }

            if (GUILayout.Button("Raise"))
            {
                CallMethod(GetDebugValue(property));
            }
        }

        private object GetDebugValue(SerializedProperty property)
        {
            Type targetType = property.serializedObject.targetObject.GetType();
            FieldInfo targetField = targetType.GetField("debugValue", BindingFlags.Instance | BindingFlags.NonPublic);

            return targetField.GetValue(property.serializedObject.targetObject);
        }

        private Type GetDebugValueType(SerializedProperty property)
        {
            Type targetType = property.serializedObject.targetObject.GetType();
            FieldInfo targetField = targetType.GetField("debugValue", BindingFlags.Instance | BindingFlags.NonPublic);

            return targetField.FieldType;
        }

        private void CallMethod(object value)
        {
            _raiseMethod.Invoke(target, new object[1] { value });
        }
    }
}