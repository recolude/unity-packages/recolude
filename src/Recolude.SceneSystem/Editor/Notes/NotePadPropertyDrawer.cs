﻿using Recolude.SceneSystem.Notes;
using UnityEditor;
using UnityEngine;

namespace Recolude.SceneSystemEditor.Notes
{
    [CustomPropertyDrawer(typeof(NotePad))]
    public class NotePadPropertyDrawer : PropertyDrawer
    {
        private const float noteHeightModifer = 1.5f;

        private const float noteSpacing = 5;

        private const float editMessgaeButtonWidth = 50;

        private const float noteTypeWdith = 150;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // base.OnGUI(position, property, label);
            var propertyAttribute = property.FindPropertyRelative("notes");

            for (int i = 0; i < propertyAttribute.arraySize; i++)
            {
                var noteProperty = propertyAttribute.GetArrayElementAtIndex(i);
                var messageProperty = noteProperty.FindPropertyRelative("message");
                var editing = noteProperty.FindPropertyRelative("editing");
                var noteType = noteProperty.FindPropertyRelative("noteType");

                var propPos = position;
                propPos.height = EditorGUIUtility.singleLineHeight * noteHeightModifer;
                propPos.y = position.y + (propPos.height * i) + (noteSpacing * i);
                propPos.width = position.width - editMessgaeButtonWidth;

                var editButtonPos = propPos;
                editButtonPos.width = editMessgaeButtonWidth;
                editButtonPos.x = propPos.x + propPos.width;
                if (GUI.Button(editButtonPos, editing.boolValue ? "Finish" : "Edit"))
                {
                    editing.boolValue = !editing.boolValue;
                }

                var currentNoteType = (NoteType)noteType.enumValueIndex;
                

                if (editing.boolValue)
                {
                    var noteTypePos = propPos;
                    noteTypePos.width = noteTypeWdith;
                    // EditorGUI.PropertyField(noteTypePos, noteType);
                    noteType.enumValueIndex = (int)(NoteType)EditorGUI.EnumPopup(noteTypePos, currentNoteType);

                    propPos.width -= editMessgaeButtonWidth + noteTypeWdith;
                    propPos.x += noteTypeWdith;
                    // EditorGUI.PropertyField(propPos, messageProperty);
                    messageProperty.stringValue = EditorGUI.TextField(propPos, messageProperty.stringValue);


                    var deleteButton = editButtonPos;
                    deleteButton.x -= editMessgaeButtonWidth;
                    if (GUI.Button(deleteButton, "Delete"))
                    {
                        propertyAttribute.DeleteArrayElementAtIndex(i);
                    }
                }
                else
                {
                    var messageType = MessageType.Info;
                    switch (currentNoteType)
                    {
                        case NoteType.Info:
                            messageType = MessageType.Info;
                            break;
                            
                        case NoteType.Warning:
                            messageType = MessageType.Warning;
                            break;
                            
                        case NoteType.Error:
                            messageType = MessageType.Error;
                            break;
                    }
                    EditorGUI.HelpBox(propPos, messageProperty.stringValue, messageType);
                }
            }

            var buttonPos = position;
            buttonPos.height = EditorGUIUtility.singleLineHeight;
            buttonPos.y = position.y + position.height - EditorGUIUtility.singleLineHeight;
            if (GUI.Button(buttonPos, "Add Note"))
            {
                propertyAttribute.InsertArrayElementAtIndex(0);
                // propertyAttribute.set
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            // return base.GetPropertyHeight(property, label);
            // var notePad = property.objectReferenceValue as NotePad;
            var propertyAttribute = property.FindPropertyRelative("notes");
            return EditorGUIUtility.singleLineHeight +
                   (propertyAttribute.arraySize * EditorGUIUtility.singleLineHeight * noteHeightModifer) +
                   (propertyAttribute.arraySize * noteSpacing);
        }
    }
}