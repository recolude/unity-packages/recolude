﻿using System.IO;
using Recolude.Core.IO.Encoders.Position;
using Recolude.SceneSystem.EncoderConfigs;
using Recolude.SceneSystem.Properties.Runtime.DataStores;
using Recolude.SceneSystem.RecordingSerialization;
using Recolude.SceneSystem.SceneRecording;
using Recolude.SceneSystem.SceneRecording.CaptureRecorders;
using UnityEditor;
using UnityEngine;

namespace Recolude.SceneSystemEditor.RecordingSerialization
{
    public class SerializationConfigEditor
    {
        private const string recoludeFolder = "Assets/Recolude/";
        private const string encodersFolder = recoludeFolder + "Encoders/";
        private const string recordingFolder = recoludeFolder + "Recording/";
        private const string runtimeStoresFolder = recoludeFolder + "RuntimeStores/";

        private static SerializationConfig SetupSerialization()
        {
            SerializationConfig serializationConfig = ScriptableObject.CreateInstance<SerializationConfig>();

            var positionEncoder = PositionEncoderConfig.Build(StorageTechnique.Oct48);
            var rotationEncoder = RotationEncoderConfig.Build(Core.IO.Encoders.Euler.StorageTechnique.Raw16);
            var customEventEncoder = CustomEventRecorderConfig.Build();
            var enumEncoder = EnumEncoderConfig.Build();

            Directory.CreateDirectory(encodersFolder);

            AssetDatabase.CreateAsset(positionEncoder,
                AssetDatabase.GenerateUniqueAssetPath(encodersFolder + "Position Encoder.asset"));
            AssetDatabase.CreateAsset(rotationEncoder,
                AssetDatabase.GenerateUniqueAssetPath(encodersFolder + "Rotation Encoder.asset"));
            AssetDatabase.CreateAsset(customEventEncoder,
                AssetDatabase.GenerateUniqueAssetPath(encodersFolder + "Event Encoder.asset"));
            AssetDatabase.CreateAsset(enumEncoder,
                AssetDatabase.GenerateUniqueAssetPath(encodersFolder + "Enum Encoder.asset"));

            serializationConfig.AddEncoders(positionEncoder, rotationEncoder, customEventEncoder, enumEncoder);

            string name = AssetDatabase.GenerateUniqueAssetPath("Assets/Recolude/Serialization Config.asset");
            AssetDatabase.CreateAsset(serializationConfig, name);
            return serializationConfig;
        }

        private static void SetupRecording()
        {
            Directory.CreateDirectory(recordingFolder);

            var positionRecorder = PositionRecorderConfig.Build(10, 0.01f);
            AssetDatabase.CreateAsset(positionRecorder,
                AssetDatabase.GenerateUniqueAssetPath(recordingFolder + "Position Recorder.asset"));


            var rotationRecorder = RotationRecorderConfig.Build(10, 0.01f);
            AssetDatabase.CreateAsset(rotationRecorder,
                AssetDatabase.GenerateUniqueAssetPath(recordingFolder + "Rotation Recorder.asset"));

            var recordingSytemRuntimeSet = RecordingSystemRuntimeSet.Build();
            AssetDatabase.CreateAsset(recordingSytemRuntimeSet,
                AssetDatabase.GenerateUniqueAssetPath(recordingFolder + "Global Recording System Runtime Set.asset"));
        }

        private static void SetupRuntimeStores()
        {
            Directory.CreateDirectory(runtimeStoresFolder);

            var applicationLifetimeRuntimeStore = ApplicationLifetimeRuntimeStore.Build();
            AssetDatabase.CreateAsset(applicationLifetimeRuntimeStore,
                AssetDatabase.GenerateUniqueAssetPath(runtimeStoresFolder +
                                                      "Application Lifetime Runtime Store.asset"));


            var sceneLifetimeRuntimeStore = SceneLifetimeRuntimeStore.Build();
            AssetDatabase.CreateAsset(sceneLifetimeRuntimeStore,
                AssetDatabase.GenerateUniqueAssetPath(runtimeStoresFolder + "Scene Lifetime Runtime Store.asset"));
        }

        [MenuItem("Assets/Recolude/Setup Project With Default Settings", priority = 30)]
        private static void BuildProjectDefaultSerialization()
        {
            SetupRecording();
            SetupRuntimeStores();
            SerializationConfig serializationConfig = SetupSerialization();

            AssetDatabase.SaveAssets();

            EditorUtility.FocusProjectWindow();

            Selection.activeObject = serializationConfig;
        }
    }
}