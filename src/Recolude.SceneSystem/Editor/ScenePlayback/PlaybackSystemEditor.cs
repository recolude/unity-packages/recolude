﻿using System;
using Recolude.SceneSystem.Clock;
using Recolude.SceneSystem.ScenePlayback;
using Recolude.SceneSystem.ScenePlayback.ActorBuilders;
using Recolude.SceneSystem.ScenePlayback.RecordingResolvers;
using UnityEditor;
using UnityEngine;

namespace Recolude.SceneSystemEditor.ScenePlayback
{
    [CustomEditor(typeof(PlaybackSystem))]
    public class PlaybackSystemEditor : Editor
    {
        private void UnloadedState(PlaybackSystem system)
        {
            if (GUILayout.Button("Begin Playback"))
            {
                system.BeginPlayback();
            }
        }

        private void LoadedState(PlaybackSystem system)
        {
            var playback = system.PlaybackBehavior;
            if (playback == null)
            {
                return;
            }

            if (playback.CurrentlyPaused())
            {
                if (GUILayout.Button("Resume Playback"))
                {
                    if (Math.Abs(playback.GetTimeThroughPlayback() - playback.RecordingDuration()) < 0.001)
                    {
                        playback.Play(0);
                    }
                    else
                    {
                        playback.Play();
                    }
                }

            }

            if (playback.CurrentlyPlaying() && GUILayout.Button("Pause Playback"))
            {
                playback.Pause();
            }

            var currentSpeed = playback.GetPlaybackSpeed();
            var newSpeed = EditorGUILayout.FloatField("Playback Speed", currentSpeed);
            if (currentSpeed != newSpeed)
            {
                playback.SetPlaybackSpeed(newSpeed);
            }

            var curTime = playback.GetTimeThroughPlayback();
            var newTime = EditorGUILayout.Slider("Progress", curTime, 0, playback.RecordingDuration());
            if (curTime != newTime)
            {
                playback.SetTimeThroughPlayback(newTime);
            }
            
            Repaint();
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (!Application.isPlaying)
            {
                return;
            }

            EditorGUILayout.Space(20);

            var playbackSystem = (PlaybackSystem)target;

            switch (playbackSystem.State)
            {
                case PlaybackSystem.SystemState.Unloaded:
                    UnloadedState(playbackSystem);
                    break;

                case PlaybackSystem.SystemState.Loading:
                    EditorGUILayout.LabelField("Loading Recording...");
                    break;

                case PlaybackSystem.SystemState.Loaded:
                    LoadedState(playbackSystem);
                    break;
            }
        }

        private const string NewPlaybackSystemMenu = "GameObject/Recolude/Playback/New Playback System";

        [MenuItem(NewPlaybackSystemMenu, priority = 30)]
        private static void NewPlaybackSystem()
        {
            var playbackSystemObj = new GameObject("Playback System");
            var clock = playbackSystemObj.AddComponent<UnityEngineClock>();
            var actorBuilder = playbackSystemObj.AddComponent<ActorBuilderConfigSlot>();
            var recorderResolver = playbackSystemObj.AddComponent<RecordingResolverConfigSlot>();

            PlaybackSystem.Build(
                playbackSystemObj,
                clock,
                recorderResolver,
                actorBuilder,
                false
            );

            Undo.RegisterCreatedObjectUndo(playbackSystemObj, "Create Playback System");
            Selection.activeObject = playbackSystemObj;
        }

        [MenuItem(NewPlaybackSystemMenu, true)]
        private static bool NewPlaybackSystemValidation()
        {
            return Selection.activeObject == null;
        }
    }
}