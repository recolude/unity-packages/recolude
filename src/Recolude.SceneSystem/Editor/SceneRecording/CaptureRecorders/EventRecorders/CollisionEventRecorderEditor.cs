﻿using Recolude.Core.Util;
using Recolude.SceneSystem.SceneRecording.CaptureRecorders.EventRecorders;
using UnityEditor;
using UnityEngine;

namespace Recolude.SceneSystemEditor.SceneRecording.CaptureRecorders.EventRecorders
{
    [CustomEditor(typeof(CollisionEventRecorder))]
    public class CollisionEventRecorderEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            EventRecorderEditor.InspectorGUI(this, target);

            var captureEnter = serializedObject.FindProperty("captureOnCollisionEnter");
            EditorGUILayout.PropertyField(captureEnter);

            if (captureEnter.boolValue)
            {
                EditorGUI.indentLevel++;
                var onCollisionEnterEventName = serializedObject.FindProperty("onCollisionEnterEventName");
                EditorGUILayout.PropertyField(onCollisionEnterEventName);

                var onCollisionEnter = serializedObject.FindProperty("onCollisionEnter");
                EditorGUILayout.PropertyField(onCollisionEnter);
                EditorGUI.indentLevel--;
            }

            var captureExit = serializedObject.FindProperty("captureOnCollisionExit");
            EditorGUILayout.PropertyField(captureExit);

            if (captureExit.boolValue)
            {
                EditorGUI.indentLevel++;
                var onCollisionEnterEventName = serializedObject.FindProperty("onCollisionExitEventName");
                EditorGUILayout.PropertyField(onCollisionEnterEventName);

                var onCollisionEnter = serializedObject.FindProperty("onCollisionExit");
                EditorGUILayout.PropertyField(onCollisionEnter);
                EditorGUI.indentLevel--;
            }

            serializedObject.ApplyModifiedProperties();
            // base.OnInspectorGUI();
        }
    }
}