﻿using Recolude.SceneSystem.SceneRecording.CaptureRecorders.EventRecorders;
using UnityEditor;

namespace Recolude.SceneSystemEditor.SceneRecording.CaptureRecorders.EventRecorders
{
    public static class EventRecorderEditor
    {
        public static void InspectorGUI(UnityEditor.Editor editor, UnityEngine.Object target)
        {
            if (target == null)
            {
                return;
            }

            var eventRecorder = (EventRecorder)target;
            if (eventRecorder == null)
            {
                return;
            }

            if (!eventRecorder.HasCustomEventRecorderConfig)
            {
                EditorGUILayout.HelpBox("A Custom Event Recorder is Required", MessageType.Error);
            }
        }
    }
}