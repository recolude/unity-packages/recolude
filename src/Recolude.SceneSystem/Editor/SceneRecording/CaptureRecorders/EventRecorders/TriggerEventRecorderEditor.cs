﻿using Recolude.SceneSystem.SceneRecording.CaptureRecorders.EventRecorders;
using UnityEditor;

namespace Recolude.SceneSystemEditor.SceneRecording.CaptureRecorders.EventRecorders
{
    [CustomEditor(typeof(TriggerEventRecorder))]
    public class TriggerEventRecorderEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            EventRecorderEditor.InspectorGUI(this, target);
            base.OnInspectorGUI();
        }
    }
}