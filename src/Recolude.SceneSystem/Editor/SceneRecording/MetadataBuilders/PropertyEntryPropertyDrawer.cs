﻿using Recolude.SceneSystem.SceneRecording.MetadataBuilders;
using UnityEditor;
using UnityEngine;

namespace Recolude.SceneSystemEditor.SceneRecording.MetadataBuilders
{
    [CustomPropertyDrawer(typeof(PropertyEntry))]
    public class PropertyEntryPropertyDrawer : PropertyDrawer
    {
        private const string propertyNameSerializedName = "propertyName";
        private const string includeSerializedName = "include";

        private const float expandedSpacing = 5f;
        
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty include = property.FindPropertyRelative(includeSerializedName);

            var boolPos = position;
            boolPos.height = EditorGUIUtility.singleLineHeight;
            EditorGUI.PropertyField(boolPos, include, label);

            if (include.boolValue)
            {
                EditorGUI.indentLevel++;

                SerializedProperty propertyName = property.FindPropertyRelative(propertyNameSerializedName);
                
                var fieldPos = position;
                fieldPos.height = EditorGUI.GetPropertyHeight(propertyName);;
                fieldPos.y += EditorGUIUtility.singleLineHeight;
                
                EditorGUI.PropertyField(fieldPos, propertyName, new GUIContent("Metadata Property Key"));
                
                EditorGUI.indentLevel--;
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            SerializedProperty include = property.FindPropertyRelative(includeSerializedName);
            var height = EditorGUIUtility.singleLineHeight;
            if (!include.boolValue) return height;
            
            EditorGUI.indentLevel++;
            height += EditorGUI.GetPropertyHeight(property.FindPropertyRelative(propertyNameSerializedName));
            EditorGUI.indentLevel--;

            return height;
        }
    }
}