﻿using Recolude.SceneSystem.SceneRecording;
using Recolude.SceneSystem.SceneRecording.CaptureRecorders;
using UnityEditor;
using UnityEngine;

namespace Recolude.SceneSystemEditor.SceneRecording
{
    public class RecordingConfigEditor
    {
        [MenuItem("GameObject/Recolude/Recording/Setup Default Recording Config", priority = 30)]
        private static void NewRecordingConfig()
        {
            if (Selection.gameObjects.Length == 0)
            {
                return;
            }

            foreach (var go in Selection.gameObjects)
            {
                if (go == null)
                {
                    continue;
                }

                RecordingConfig.Build(
                    go,
                    null,
                    new CaptureRecorderConfigSlot[]
                    {
                        go.AddComponent<PositionRecorderConfigSlot>(),
                        go.AddComponent<RotationRecorderConfigSlot>(),
                        go.AddComponent<LifecycleRecorderConfigSlot>(),
                        go.AddComponent<CustomEventRecorderConfigSlot>(),
                    }
                );
            }

            // The reason we have to clear immediately is because without this, 
            // this specific function Will get called once per selected object.
            // So if we have 30 selected objects, we will end up calling this 
            // function 30 times, adding a total of 1200 objects.
            Selection.objects = null;
        }

        [MenuItem("GameObject/Recolude/Recording/Setup Default Recording Config", true)]
        private static bool NewRecordingConfigValidation()
        {
            if (Selection.activeObject == null)
            {
                return false;
            }
            return Selection.activeObject is GameObject;
        }
    }
}