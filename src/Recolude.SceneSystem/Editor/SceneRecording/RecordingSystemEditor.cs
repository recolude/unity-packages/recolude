﻿using Recolude.SceneSystem.Clock;
using Recolude.SceneSystem.SceneRecording;
using Recolude.SceneSystem.SceneRecording.CaptureRecorders;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Recolude.SceneSystemEditor.SceneRecording
{
    [CustomEditor(typeof(RecordingSystem))]
    public class RecordingSystemEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (!Application.isPlaying)
            {
                return;
            }

            EditorGUILayout.Space(20);

            var recordingSystem = (RecordingSystem)target;

            if (recordingSystem.Recording)
            {
                if (GUILayout.Button("Stop Recording"))
                {
                    recordingSystem.StopRecording();
                }
            }
            else
            {
                if (GUILayout.Button("Begin Recording"))
                {
                    recordingSystem.StartRecording();
                }
            }
        }

        [MenuItem("GameObject/Recolude/Recording/New Recording System", priority = 30)]
        private static void NewRecordingSystem()
        {
            var recordingSystemObj = new GameObject("Recording System");
            var recordingSystem = recordingSystemObj.AddComponent<RecordingSystem>();

            var clock = recordingSystemObj.AddComponent<UnityEngineClock>();
            recordingSystem.SetClock(clock);

            var eventRecorder = recordingSystemObj.AddComponent<CustomEventRecorderConfigSlot>();
            recordingSystem.AddCaptureRecorder(eventRecorder);

            Scene scene = SceneManager.GetActiveScene();
            var recordingName = scene.name;
            if (string.IsNullOrWhiteSpace(recordingName))
            {
                recordingName = "Recording";
            }
            recordingSystem.SetRecordingName(recordingName);

            Undo.RegisterCreatedObjectUndo(recordingSystemObj, "Create Recording System");
            Selection.activeObject = recordingSystemObj;
        }

        [MenuItem("GameObject/Recolude/Recording/New Recording System", true)]
        private static bool NewRecordingSystemValidation()
        {
            return Selection.activeObject == null;
        }
    }
}