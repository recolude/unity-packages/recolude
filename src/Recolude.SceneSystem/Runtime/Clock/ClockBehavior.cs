﻿using Recolude.Core.Time;
using UnityEngine;

namespace Recolude.SceneSystem.Clock
{
    public abstract class ClockBehavior : MonoBehaviour, ITimeProvider
    {
        public abstract float CurrentTime();
    }
}