﻿using System;
using UnityEngine;

namespace Recolude.SceneSystem.Clock
{
    [AddComponentMenu("Recolude/Scene Recording/Unity Clock")]
    public class UnityEngineClock : ClockBehavior
    {
        public enum TimeSources
        {
            Time,
            FixedTime,
            UnscaledTime,
            FixedUnscaledTime,
            RealtimeSinceStartup,
            TimeSinceLevelLoad
        }

        [SerializeField] private TimeSources source;

        public override float CurrentTime()
        {
            switch (source)
            {
                case TimeSources.Time:
                    return UnityEngine.Time.time;

                case TimeSources.FixedTime:
                    return UnityEngine.Time.fixedTime;

                case TimeSources.UnscaledTime:
                    return UnityEngine.Time.unscaledTime;

                case TimeSources.FixedUnscaledTime:
                    return UnityEngine.Time.fixedUnscaledTime;

                case TimeSources.RealtimeSinceStartup:
                    return UnityEngine.Time.realtimeSinceStartup;

                case TimeSources.TimeSinceLevelLoad:
                    return UnityEngine.Time.timeSinceLevelLoad;

                default:
                    throw new SystemException($"unimplemented unity engine time: {source}");
            }
        }
    }
}