﻿using Recolude.Core;
using Recolude.Core.IO;
using Recolude.Core.IO.Encoders.Event;
using UnityEngine;

namespace Recolude.SceneSystem.EncoderConfigs
{
    [CreateAssetMenu(fileName = "Custom Event Encoder Config",
        menuName = "Recolude/Scene System/Encoders/Custom Event", order = 1)]
    public class CustomEventRecorderConfig : EncoderConfig
    {
        public static CustomEventRecorderConfig Build()
        {
            CustomEventRecorderConfig asset = CreateInstance<CustomEventRecorderConfig>();
            return asset;
        }
        
        public override IEncoder<ICaptureCollection<ICapture>> Encoder => new EventEncoder();
    }
}