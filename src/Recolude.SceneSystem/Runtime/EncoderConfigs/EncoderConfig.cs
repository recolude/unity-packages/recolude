﻿using Recolude.Core;
using Recolude.Core.IO;
using Recolude.Core.IO.Encoders.Enum;
using UnityEngine;

namespace Recolude.SceneSystem.EncoderConfigs
{
    public abstract class EncoderConfig : ScriptableObject
    {
        public abstract IEncoder<ICaptureCollection<ICapture>> Encoder { get; }
    }
}