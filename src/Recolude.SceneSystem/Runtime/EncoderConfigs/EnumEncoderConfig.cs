﻿using Recolude.Core;
using Recolude.Core.IO;
using Recolude.Core.IO.Encoders.Enum;
using UnityEngine;

namespace Recolude.SceneSystem.EncoderConfigs
{
    [CreateAssetMenu(fileName = "Enum Encoder Config",
        menuName = "Recolude/Scene System/Encoders/Enum", order = 1)]
    public class EnumEncoderConfig : EncoderConfig
    {
        public static EnumEncoderConfig Build()
        {
            EnumEncoderConfig asset = CreateInstance<EnumEncoderConfig>();
            return asset;
        }
        
        public override IEncoder<ICaptureCollection<ICapture>> Encoder => new EnumEncoder();
    }
}