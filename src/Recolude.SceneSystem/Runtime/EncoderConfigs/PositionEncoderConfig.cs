﻿using Recolude.Core;
using Recolude.Core.IO;
using Recolude.Core.IO.Encoders.Position;
using UnityEngine;

namespace Recolude.SceneSystem.EncoderConfigs
{
    [CreateAssetMenu(fileName = "Position Encoder Config",
        menuName = "Recolude/Scene System/Encoders/Position", order = 1)]
    public class PositionEncoderConfig : EncoderConfig
    {
        [SerializeField] private StorageTechnique storageTechnique;

        public static PositionEncoderConfig Build(StorageTechnique technique)
        {
            PositionEncoderConfig asset = CreateInstance<PositionEncoderConfig>();
            asset.storageTechnique = technique;
            return asset;
        }

        public override IEncoder<ICaptureCollection<ICapture>> Encoder =>
            new Vector3PositionEncoder(storageTechnique);
    }
}