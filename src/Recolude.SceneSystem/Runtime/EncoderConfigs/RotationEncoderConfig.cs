﻿using Recolude.Core;
using Recolude.Core.IO;
using Recolude.Core.IO.Encoders.Euler;
using UnityEngine;

namespace Recolude.SceneSystem.EncoderConfigs
{
    [CreateAssetMenu(fileName = "Rotation Encoder Config",
        menuName = "Recolude/Scene System/Encoders/Rotation", order = 1)]
    public class RotationEncoderConfig : EncoderConfig
    {
        [SerializeField] private StorageTechnique storageTechnique;

        public static RotationEncoderConfig Build(StorageTechnique technique)
        {
            RotationEncoderConfig asset = CreateInstance<RotationEncoderConfig>();
            asset.storageTechnique = technique;
            return asset;
        }
        
        public override IEncoder<ICaptureCollection<ICapture>> Encoder => new EulerEncoder(storageTechnique);
    }
}