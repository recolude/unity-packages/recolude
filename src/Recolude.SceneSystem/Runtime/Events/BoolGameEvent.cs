﻿using UnityEngine;

namespace Recolude.SceneSystem.Events
{
    [CreateAssetMenu(fileName = "Bool Game Event", menuName = Menu + "Bool Event", order = 1)]
    public class BoolGameEvent : GameEvent<bool>
    {
    }
}