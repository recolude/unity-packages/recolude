﻿using UnityEngine;

namespace Recolude.SceneSystem.Events
{
    [CreateAssetMenu(fileName = "Byte Game Event", menuName = Menu + "Byte Event", order = 1)]
    public class ByteGameEvent: GameEvent<byte>
    {
        
    }
}