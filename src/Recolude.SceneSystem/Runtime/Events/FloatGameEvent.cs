﻿using UnityEngine;

namespace Recolude.SceneSystem.Events
{
    [CreateAssetMenu(fileName = "Float Game Event", menuName = Menu + "Float Event", order = 1)]
    public class FloatGameEvent: GameEvent<float>
    {
    }
}