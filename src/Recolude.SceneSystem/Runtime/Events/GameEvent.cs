﻿using UnityEngine;

namespace Recolude.SceneSystem.Events
{
    [CreateAssetMenu(fileName = "Game Event", menuName = Menu + "Game Event", order = 1)]
    public class GameEvent : GameEventBase
    {
    }

    // public class GameEvent<T> : GameEventBase<T>
    // {
    // }
}