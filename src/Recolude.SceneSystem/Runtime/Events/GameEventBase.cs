﻿using System.Collections.Generic;
using Recolude.SceneSystem.Listeners;
using UnityEngine;

namespace Recolude.SceneSystem.Events
{
    public abstract class GameEvent<T> : GameEventBase, IGameEvent<T> //, IStackTraceObject
    {
        private List<IGameEventListener<T>> typedListeners = new List<IGameEventListener<T>>();
        private List<System.Action<T>> typedActions = new List<System.Action<T>>();

        [SerializeField] protected T debugValue = default(T);

        public void Raise(T value)
        {
            // AddStackTrace(value);

            for (int i = typedListeners.Count - 1; i >= 0; i--)
                typedListeners[i].OnEventRaised(value);

            for (int i = listeners.Count - 1; i >= 0; i--)
                listeners[i].OnEventRaised();

            for (int i = typedActions.Count - 1; i >= 0; i--)
                typedActions[i](value);

            for (int i = actions.Count - 1; i >= 0; i--)
                actions[i]();
        }

        protected List<IGameEventListener<T>> TypedListeners
        {
            get
            {
                if (typedListeners == null)
                {
                    typedListeners = new List<IGameEventListener<T>>();
                }

                return typedListeners;
            }
        }

        protected List<System.Action<T>> TypedActions
        {
            get
            {
                if (typedActions == null)
                {
                    typedActions = new List<System.Action<T>>();
                }

                return typedActions;
            }
        }

        public void AddListener(IGameEventListener<T> listener)
        {
            if (!TypedListeners.Contains(listener))
                typedListeners.Add(listener);
        }

        public void RemoveListener(IGameEventListener<T> listener)
        {
            if (TypedListeners.Contains(listener))
                typedListeners.Remove(listener);
        }

        public void AddListener(System.Action<T> action)
        {
            if (!TypedActions.Contains(action))
                typedActions.Add(action);
        }

        public void RemoveListener(System.Action<T> action)
        {
            if (TypedActions.Contains(action))
                typedActions.Remove(action);
        }

        public override string ToString()
        {
            return "GameEventBase<" + typeof(T) + ">";
        }
    }

    public abstract class GameEventBase : ScriptableObject, IGameEvent //, IStackTraceObject
    {
        protected const string Menu = "Recolude/Scene System/Events/";

        protected List<IGameEventListener> listeners = new List<IGameEventListener>();
        protected List<System.Action> actions = new List<System.Action>();

        protected List<IGameEventListener> Listeners
        {
            get
            {
                if (listeners == null)
                {
                    listeners = new List<IGameEventListener>();
                }

                return listeners;
            }
        }

        protected List<System.Action> Actions
        {
            get
            {
                if (actions == null)
                {
                    actions = new List<System.Action>();
                }

                return actions;
            }
        }

//         public List<StackTraceEntry> StackTraces
//         {
//             get { return _stackTraces; }
//         }
//
//         private List<StackTraceEntry> _stackTraces = new List<StackTraceEntry>();
//
//         public void AddStackTrace()
//         {
// #if UNITY_EDITOR
//             if (SOArchitecturePreferences.IsDebugEnabled)
//                 _stackTraces.Insert(0, StackTraceEntry.Create());
// #endif
//         }
//
//         public void AddStackTrace(object value)
//         {
// #if UNITY_EDITOR
//             if (SOArchitecturePreferences.IsDebugEnabled)
//                 _stackTraces.Insert(0, StackTraceEntry.Create(value));
// #endif
//         }

        public virtual void Raise()
        {
            // AddStackTrace();

            for (int i = Listeners.Count - 1; i >= 0; i--)
                listeners[i].OnEventRaised();

            for (int i = Actions.Count - 1; i >= 0; i--)
                actions[i]();
        }

        public void AddListener(IGameEventListener listener)
        {
            if (!Listeners.Contains(listener))
                listeners.Add(listener);
        }

        public void RemoveListener(IGameEventListener listener)
        {
            if (Listeners.Contains(listener))
                listeners.Remove(listener);
        }

        public void AddListener(System.Action action)
        {
            if (!Actions.Contains(action))
                actions.Add(action);
        }

        public void RemoveListener(System.Action action)
        {
            if (Actions.Contains(action))
                actions.Remove(action);
        }

        public virtual void RemoveAll()
        {
            Listeners.RemoveRange(0, listeners.Count);
            Actions.RemoveRange(0, actions.Count);
        }
    }
}