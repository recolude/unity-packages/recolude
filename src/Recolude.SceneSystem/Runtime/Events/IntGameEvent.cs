﻿using UnityEngine;

namespace Recolude.SceneSystem.Events
{
    [CreateAssetMenu(fileName = "Int Game Event", menuName = Menu + "Int Event", order = 1)]
    public class IntGameEvent : GameEvent<int>
    {
    }
}