﻿using UnityEngine;

namespace Recolude.SceneSystem.Events
{
    [CreateAssetMenu(fileName = "String Game Event", menuName = Menu + "String Event", order = 1)]
    public class StringGameEvent : GameEvent<string>
    {
    }
}