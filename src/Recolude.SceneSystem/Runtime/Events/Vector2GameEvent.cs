﻿using UnityEngine;

namespace Recolude.SceneSystem.Events
{
    [CreateAssetMenu(fileName = "Vector2 Game Event", menuName = Menu + "Vector2 Event", order = 1)]
    public class Vector2GameEvent: GameEvent<Vector2>
    {
        
    }
}