﻿using UnityEngine;

namespace Recolude.SceneSystem.Events
{
    [CreateAssetMenu(fileName = "Vector3 Game Event", menuName = Menu + "Vector3 Event", order = 1)]
    public class Vector3GameEvent : GameEvent<Vector3>
    {
    }
}