﻿using Recolude.SceneSystem.Events;
using UnityEngine;
using UnityEngine.Events;

namespace Recolude.SceneSystem.Listeners
{
    
    [AddComponentMenu(Menu + "Bool Game Event Listener")]
    public class BoolGameEventListener : BaseGameEventListener<bool, BoolGameEvent, UnityEvent<bool>>
    {
    }
}