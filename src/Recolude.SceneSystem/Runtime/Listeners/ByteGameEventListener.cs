﻿using Recolude.SceneSystem.Events;
using UnityEngine;
using UnityEngine.Events;

namespace Recolude.SceneSystem.Listeners
{
    [AddComponentMenu(Menu + "Float Game Event Listener")]
    public class ByteGameEventListener : BaseGameEventListener<byte, ByteGameEvent, UnityEvent<byte>>
    {
    }
}