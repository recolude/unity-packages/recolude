﻿using Recolude.SceneSystem.Events;
using UnityEngine;
using UnityEngine.Events;

namespace Recolude.SceneSystem.Listeners
{
    [AddComponentMenu(Menu + "Float Game Event Listener")]
    public class FloatGameEventListener : BaseGameEventListener<float, FloatGameEvent, UnityEvent<float>>
    {
    }
}