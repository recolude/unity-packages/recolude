﻿using Recolude.SceneSystem.Events;
using UnityEngine;
using UnityEngine.Events;

namespace Recolude.SceneSystem.Listeners
{
    [AddComponentMenu(Menu + "Game Event Listener")]
    [ExecuteInEditMode]
    public class GameEventListener : BaseGameEventListener<GameEventBase, UnityEvent>
    {
    }
}