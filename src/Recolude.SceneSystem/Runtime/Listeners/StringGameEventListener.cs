﻿using Recolude.SceneSystem.Events;
using UnityEngine;
using UnityEngine.Events;

namespace Recolude.SceneSystem.Listeners
{
    [AddComponentMenu(Menu + "String Game Event Listener")]
    public class StringGameEventListener: BaseGameEventListener<string, StringGameEvent, UnityEvent<string>>
    {
        
    }
}