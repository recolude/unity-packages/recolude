﻿using Recolude.SceneSystem.Events;
using UnityEngine;
using UnityEngine.Events;

namespace Recolude.SceneSystem.Listeners
{
    [AddComponentMenu(Menu + "Vector2 Game Event Listener")]
    public class Vector2GameEventListener : BaseGameEventListener<Vector2, Vector2GameEvent, UnityEvent<Vector2>>
    {
    }
}