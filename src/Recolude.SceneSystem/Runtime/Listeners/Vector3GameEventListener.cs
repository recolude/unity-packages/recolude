﻿using Recolude.SceneSystem.Events;
using UnityEngine;
using UnityEngine.Events;

namespace Recolude.SceneSystem.Listeners
{
    [AddComponentMenu(Menu + "Vector3 Game Event Listener")]
    public class Vector3GameEventListener : BaseGameEventListener<Vector3, Vector3GameEvent, UnityEvent<Vector3>>
    {
    }
}