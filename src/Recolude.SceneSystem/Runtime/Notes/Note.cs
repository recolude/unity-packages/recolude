﻿using System;
using UnityEngine;

namespace Recolude.SceneSystem.Notes
{
    [Serializable]
    public class Note
    {
        [SerializeField] private NoteType noteType;

        [SerializeField] private bool editing;
        
        [SerializeField] private string message;
    }
}