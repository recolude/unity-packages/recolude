﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Recolude.SceneSystem.Notes
{
    [Serializable]
    public class NotePad
    {
#if UNITY_EDITOR
        [SerializeField] private List<Note> notes;
#endif
    }
}