﻿namespace Recolude.SceneSystem.Notes
{
    public enum NoteType
    {
        Info,
        Warning,
        Error
    }
}