﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties
{
    public abstract class EnumStringProperty<T> : ScriptableProperty<string> where T : System.Enum
    {
        [SerializeField] private T value;

        protected abstract string ValueToString(T val);

        public override string Value => ValueToString(value);
    }
}