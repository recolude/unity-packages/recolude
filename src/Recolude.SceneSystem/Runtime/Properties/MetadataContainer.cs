﻿using System;
using Recolude.Core;
using Recolude.Core.Util;
using Recolude.SceneSystem.Properties.Reference;
using UnityEngine;

namespace Recolude.SceneSystem.Properties
{
    [CreateAssetMenu(fileName = "Metadata Block", menuName = "Recolude/Scene System/Metadata Block", order = 1)]
    public class MetadataContainer : ScriptableObject
    {
        [Serializable]
        public class StringPropertyDict : SerializableDictionary<PropertyReference<string>, ScriptablePropertyBase>
        {
        }

        [SerializeField] private StringPropertyDict metadata;

        public Metadata Metadata
        {
            get
            {
                var metadataBlock = new Metadata();
                foreach (var entry in metadata)
                {
                    metadataBlock[entry.Key.Value] = entry.Value;
                }

                return metadataBlock;
            }
        }
    }
}