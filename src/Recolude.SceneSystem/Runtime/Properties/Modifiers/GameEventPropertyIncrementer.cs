﻿using Recolude.Core.Util;
using Recolude.SceneSystem.Events;
using Recolude.SceneSystem.Properties.Runtime;
using UnityEngine;

namespace Recolude.SceneSystem.Properties.Modifiers
{
    [AddComponentMenu("Recolude/Scene Recording/Properties/Modifiers/Game Event Property Incrementer")]
    public class GameEventPropertyIncrementer : MonoBehaviour
    {
        [Tooltip("Event that when raised, increments the property")]
        [SerializeField] [RequiredValue] GameEventBase gameEvent;

        [Tooltip("The property to increment when the event is raised")]
        [SerializeField] [RequiredValue] private RuntimeProperty<int> propertyToIncrement;

        [Tooltip("How much to increment the properties value by when the event is raised")] [SerializeField]
        private int amountToIncrement = 1;

        private void Start()
        {
            gameEvent.AddListener(OnEventRaised);
        }

        private void OnEventRaised()
        {
            propertyToIncrement.Set(propertyToIncrement.Value + amountToIncrement);
        }
    }
}