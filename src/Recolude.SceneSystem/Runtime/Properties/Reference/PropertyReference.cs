﻿using System;
using Recolude.Core;
using UnityEngine;

namespace Recolude.SceneSystem.Properties.Reference
{
    [Serializable]
    public class PropertyReference<T> : IProperty<T>
    {
        [SerializeField] private bool useConstant;

        [SerializeField] public T constantValue;

        [SerializeField] public ScriptableProperty<T> variable;

        public PropertyReference(T val)
        {
            constantValue = val;
            useConstant = true;
        }

        public T Value => useConstant ? constantValue : variable.Value;
    }
}