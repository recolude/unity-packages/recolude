﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Runtime
{
    [CreateAssetMenu(fileName = "Runtime Bool Property", menuName = MenuPath + "Bool", order = 1)]
    public class BoolRuntimeProperty: RuntimeProperty<bool>
    {
    }
}