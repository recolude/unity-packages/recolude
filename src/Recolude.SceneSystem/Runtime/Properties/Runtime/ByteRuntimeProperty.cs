﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Runtime
{
    [CreateAssetMenu(fileName = "Runtime Byte Property", menuName = MenuPath + "Byte", order = 1)]
    public class ByteRuntimeProperty: RuntimeProperty<byte>
    {
        
    }
}