﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Runtime.DataStores
{
    [CreateAssetMenu(fileName = "Application Lifetime Runtime Store", menuName = MenuPath + "Application Lifetime",
        order = 1)]
    public class ApplicationLifetimeRuntimeStore : RuntimeStore
    {
        private const string gameObjectName = "Application Lifetime Runtime Store";

        private SceneLifetimeRuntimeStoreBehavior behavior;

        public SceneLifetimeRuntimeStoreBehavior Behavior
        {
            get
            {
                if (behavior == null)
                {
                    var go = new GameObject(gameObjectName);
                    DontDestroyOnLoad(go);
                    behavior = go.AddComponent<SceneLifetimeRuntimeStoreBehavior>();
                }

                return behavior;
            }
        }
        
        public static ApplicationLifetimeRuntimeStore Build()
        {
            var asset = CreateInstance<ApplicationLifetimeRuntimeStore>();
            return asset;
        }

        public override T Get<T>(ScriptableObject key, T fallback)
        {
            return Behavior.Get(key, fallback);
        }

        public override void Set<T>(ScriptableObject key, T fallback)
        {
            Behavior.Set(key, fallback);
        }
    }
}