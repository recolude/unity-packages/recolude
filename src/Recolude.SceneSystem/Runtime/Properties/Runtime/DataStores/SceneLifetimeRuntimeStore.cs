﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Runtime.DataStores
{
    [CreateAssetMenu(fileName = "Scene Lifetime Runtime Store", menuName = MenuPath + "Scene Lifetime", order = 1)]
    public class SceneLifetimeRuntimeStore : RuntimeStore
    {
        private const string gameObjectName = "Scene Lifetime Runtime Store";

        private SceneLifetimeRuntimeStoreBehavior behavior;

        public SceneLifetimeRuntimeStoreBehavior Behavior
        {
            get
            {
                if (behavior == null)
                {
                    behavior = new GameObject(gameObjectName).AddComponent<SceneLifetimeRuntimeStoreBehavior>();
                }

                return behavior;
            }
        }
        
        public static SceneLifetimeRuntimeStore Build()
        {
            var asset = CreateInstance<SceneLifetimeRuntimeStore>();
            return asset;
        }

        public override T Get<T>(ScriptableObject key, T fallback)
        {
            return Behavior.Get(key, fallback);
        }

        public override void Set<T>(ScriptableObject key, T fallback)
        {
            Behavior.Set(key, fallback);
        }
    }
}