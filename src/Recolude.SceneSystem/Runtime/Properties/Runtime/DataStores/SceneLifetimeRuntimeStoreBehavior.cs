﻿using System.Collections.Generic;
using UnityEngine;

namespace Recolude.SceneSystem.Properties.Runtime.DataStores
{
    public class SceneLifetimeRuntimeStoreBehavior : MonoBehaviour
    {
        private Dictionary<ScriptableObject, object> data;

        private void Awake()
        {
            data = new Dictionary<ScriptableObject, object>();
        }

        public T Get<T>(ScriptableObject key, T fallback)
        {
            if (data.TryGetValue(key, out var val))
            {
                return (T)val;
            }

            data[key] = fallback;
            return fallback;
        }

        public void Set<T>(ScriptableObject key, T val)
        {
            data[key] = val;
        }
    }
}