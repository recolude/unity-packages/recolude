﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Runtime
{
    [CreateAssetMenu(fileName = "Runtime Float Property", menuName = MenuPath + "Float", order = 1)]
    public class FloatRuntimeProperty: RuntimeProperty<float>
    {
        
    }
}