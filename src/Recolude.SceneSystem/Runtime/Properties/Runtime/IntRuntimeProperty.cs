﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Runtime
{
    [CreateAssetMenu(fileName = "Runtime Int Property", menuName = MenuPath + "Int", order = 1)]
    public class IntRuntimeProperty : RuntimeProperty<int>
    {
        public void Increment()
        {
            Set(Value + 1);
        }
        
        public void Decrement()
        {
            Set(Value - 1);
        }
    }
}