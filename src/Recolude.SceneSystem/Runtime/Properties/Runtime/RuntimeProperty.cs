﻿using Recolude.Core.Util;
using Recolude.SceneSystem.Properties.Reference;
using UnityEngine;

namespace Recolude.SceneSystem.Properties.Runtime
{
    public abstract class RuntimeProperty<T> : ScriptableProperty<T>
    {
        protected const string MenuPath = ScriptablePropertyMenu + "Runtime/";

        [Tooltip("Value to use when one isn't found within the runtime store")] [SerializeField]
        private PropertyReference<T> fallbackValue;

        [Tooltip("What to store and retrieve the variable from")] [SerializeField] [RequiredValue]
        private RuntimeStore store;

        public override T Value => store.Get(this, fallbackValue.Value);

        public void Set(T value)
        {
            store.Set(this, value);
        }
    }
}