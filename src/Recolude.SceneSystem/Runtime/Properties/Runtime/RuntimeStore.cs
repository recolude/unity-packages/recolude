﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Runtime
{
    public abstract class RuntimeStore : ScriptableObject
    {
        protected const string MenuPath = "Recolude/Scene System/Runtime Stores/";
        public abstract T Get<T>(ScriptableObject key, T fallback);

        public abstract void Set<T>(ScriptableObject key, T value);
    }
}