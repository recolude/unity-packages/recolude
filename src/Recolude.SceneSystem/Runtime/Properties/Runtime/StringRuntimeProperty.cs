﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Runtime
{
    [CreateAssetMenu(fileName = "Runtime String Property", menuName = MenuPath + "String", order = 1)]
    public class StringRuntimeProperty: RuntimeProperty<string>
    {
        
    }
}