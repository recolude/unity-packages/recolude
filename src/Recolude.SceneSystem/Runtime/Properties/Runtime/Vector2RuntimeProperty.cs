﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Runtime
{
    [CreateAssetMenu(fileName = "Runtime Vector2 Property", menuName = MenuPath + "Vector2", order = 1)]
    public class Vector2RuntimeProperty : RuntimeProperty<Vector2>
    {
    }
}