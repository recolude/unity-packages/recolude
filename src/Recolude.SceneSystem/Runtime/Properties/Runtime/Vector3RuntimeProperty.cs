﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Runtime
{
    [CreateAssetMenu(fileName = "Runtime Vector3 Property", menuName = MenuPath + "Vector 3", order = 1)]
    public class Vector3RuntimeProperty : RuntimeProperty<Vector3>
    {
    }
}