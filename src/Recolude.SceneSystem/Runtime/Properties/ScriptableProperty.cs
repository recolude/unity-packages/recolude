﻿using Recolude.Core;
using Recolude.SceneSystem.Notes;
using UnityEngine;

namespace Recolude.SceneSystem.Properties
{
    public abstract class ScriptablePropertyBase : ScriptableObject, IProperty
    {
        protected const string ScriptablePropertyMenu = "Recolude/Scene System/Properties/";

        [SerializeField] private NotePad notePad;
    }

    public abstract class ScriptableProperty<T> : ScriptablePropertyBase, IProperty<T>
    {
        public abstract T Value { get; }
    }

    public abstract class ArrayScriptableProperty<T> : ScriptableProperty<T[]>, IProperty<T[]>
    {
    }
}