﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Static
{
    [CreateAssetMenu(fileName = "Static Bool Array Property", menuName = MenuPath + "Bool Array", order = 1)]
    public class BoolArrayStaticProperty : ArrayStaticProperty<bool>
    {
    }
}