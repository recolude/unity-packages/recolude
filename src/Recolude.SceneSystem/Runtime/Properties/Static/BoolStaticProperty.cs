﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Static
{
    [CreateAssetMenu(fileName = "Static Bool Property", menuName = MenuPath + "Bool", order = 1)]
    public class BoolStaticProperty : StaticProperty<bool>
    {
    }
}