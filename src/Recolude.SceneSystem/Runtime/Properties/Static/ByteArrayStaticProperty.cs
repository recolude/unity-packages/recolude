﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Static
{
    [CreateAssetMenu(fileName = "Static Byte Array Property", menuName = MenuPath + "Byte Array", order = 1)]
    public class ByteArrayStaticProperty : ArrayStaticProperty<byte>
    {
    }
}