﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Static
{
    [CreateAssetMenu(fileName = "Static Byte Property", menuName = MenuPath + "Byte", order = 1)]
    public class ByteStaticProperty : StaticProperty<byte>
    {
    }
}