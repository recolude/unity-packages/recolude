﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Static
{
    [CreateAssetMenu(fileName = "Static Float Array Property", menuName = MenuPath + "Float Array", order = 1)]
    public class FloatArrayStaticProperty : ArrayStaticProperty<float>
    {
    }
}