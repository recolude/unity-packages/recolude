﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Static
{
    [CreateAssetMenu(fileName = "Static Float Property", menuName = MenuPath + "Float", order = 1)]
    public class FloatStaticProperty : StaticProperty<float>
    {
    }
}