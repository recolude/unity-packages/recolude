﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Static
{
    [CreateAssetMenu(fileName = "Static Int Array Property", menuName = MenuPath + "Int Array", order = 1)]
    public class IntArrayStaticProperty: ArrayStaticProperty<int>
    {
        
    }
}