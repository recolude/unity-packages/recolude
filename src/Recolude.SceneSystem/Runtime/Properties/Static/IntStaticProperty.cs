﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Static
{
    [CreateAssetMenu(fileName = "Static Int Property", menuName = MenuPath + "Int", order = 1)]
    public class IntStaticProperty : StaticProperty<int>
    {
    }
}