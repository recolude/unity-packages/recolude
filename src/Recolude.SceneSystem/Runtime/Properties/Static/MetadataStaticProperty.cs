﻿using Recolude.Core;
using Recolude.Core.Util;
using UnityEngine;

namespace Recolude.SceneSystem.Properties.Static
{
    [CreateAssetMenu(fileName = "Static Metadata Property", menuName = ScriptablePropertyMenu + "Static/Metadata",
        order = 1)]
    public class MetadataStaticProperty : ScriptableProperty<Metadata>
    {
        [ExtendableScriptableObject] [SerializeField]
        private MetadataContainer metadata;

        public override Metadata Value => metadata.Metadata;
    }
}