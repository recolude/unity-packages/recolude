﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Static
{
    public abstract class StaticProperty<T> : ScriptableProperty<T>
    {
        protected const string MenuPath = ScriptablePropertyMenu + "Static/";

        [SerializeField] protected T value;

        public override T Value => value;
    }

    public abstract class ArrayStaticProperty<T> : ArrayScriptableProperty<T>
    {
        protected const string MenuPath = ScriptablePropertyMenu + "Static/";

        [SerializeField] protected T[] value;

        public override T[] Value => value;
    }
}