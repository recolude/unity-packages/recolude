﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Static
{
    [CreateAssetMenu(fileName = "Static String Array Property", menuName = MenuPath + "String Array", order = 1)]
    public class StringArrayStaticProperty: ArrayStaticProperty<string>
         {
    }
}