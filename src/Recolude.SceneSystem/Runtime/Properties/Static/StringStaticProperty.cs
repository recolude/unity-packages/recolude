﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Static
{
    [CreateAssetMenu(fileName = "Static String Property", menuName = MenuPath + "String", order = 1)]
    public class StringStaticProperty : StaticProperty<string>
    {
    }
}