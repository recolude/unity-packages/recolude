﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Static
{
    [CreateAssetMenu(fileName = "Static Vector2 Array Property", menuName = MenuPath + "Vector2 Array", order = 1)]
    public class Vector2ArrayStaticProperty: ArrayStaticProperty<Vector2>
    {
        
    }
}