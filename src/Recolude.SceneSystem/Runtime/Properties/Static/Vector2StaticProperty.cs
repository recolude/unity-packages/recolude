﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Static
{
    [CreateAssetMenu(fileName = "Static Vector2 Property", menuName = MenuPath + "Vector2", order = 1)]
    public class Vector2StaticProperty : StaticProperty<Vector2>
    {
        
    }
}