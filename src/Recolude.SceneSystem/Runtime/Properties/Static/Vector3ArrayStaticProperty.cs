﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Static
{
    [CreateAssetMenu(fileName = "Static Vector3 Array Property", menuName = MenuPath + "Vector3 Array", order = 1)]
    public class Vector3ArrayStaticProperty : ArrayStaticProperty<Vector3>
    {
    }
}