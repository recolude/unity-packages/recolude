﻿using UnityEngine;

namespace Recolude.SceneSystem.Properties.Static
{
    [CreateAssetMenu(fileName = "Static Vector3 Property", menuName = MenuPath + "Vector3", order = 1)]
    public class Vector3StaticProperty: StaticProperty<Vector3>
    {
        
    }
}