﻿using System.Collections.Generic;
using Recolude.Core;
using Recolude.Core.Util;
using UnityEngine;

namespace Recolude.SceneSystem.Properties.Webplayer
{
    [CreateAssetMenu(fileName = "Web Player Ambient Light Property",
        menuName = "Recolude/Scene System/Properties/Web Player/Ambient Light", order = 1)]
    public class AmbientLightOptionProperty : ScriptableProperty<Metadata>
    {
        [SerializeField] private Color lightColor = Color.white;

        [SerializeField] private float strength = 0.2f;

        public override Metadata Value => new Metadata(new Dictionary<string, IProperty>()
        {
            { "color", new Property<string>(JSON.ColorToHex(lightColor)) },
            { "strength", new Property<float>(strength) },
        });
    }
}