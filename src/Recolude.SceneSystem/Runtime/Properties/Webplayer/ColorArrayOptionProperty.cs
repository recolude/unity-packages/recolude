﻿using Recolude.Core.Util;
using UnityEngine;

namespace Recolude.SceneSystem.Properties.Webplayer
{
    [CreateAssetMenu(fileName = "Web Player Color Array Property",
        menuName = "Recolude/Scene System/Properties/Web Player/Color Array", order = 1)]
    public class ColorArrayOptionProperty : ArrayScriptableProperty<string>
    {
        [SerializeField] private Color[] colors;

        public Color[] Colors => colors;

        public override string[] Value
        {
            get
            {
                string[] colorHexes = new string[colors.Length];

                for (var i = 0; i < colors.Length; i++)
                {
                    colorHexes[i] = JSON.ColorToHex(colors[i]);
                }

                return colorHexes;
            }
        }
    }
}