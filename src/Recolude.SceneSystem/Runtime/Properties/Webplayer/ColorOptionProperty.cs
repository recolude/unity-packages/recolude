﻿using Recolude.Core.Util;
using UnityEngine;

namespace Recolude.SceneSystem.Properties.Webplayer
{
    [CreateAssetMenu(fileName = "Web Player Color Property",
        menuName = "Recolude/Scene System/Properties/Web Player/Color", order = 1)]
    public class ColorOptionProperty : ScriptableProperty<string>
    {
        [SerializeField] private Color color;

        public Color Color => color;

        public override string Value => JSON.ColorToHex(color);
    }
}