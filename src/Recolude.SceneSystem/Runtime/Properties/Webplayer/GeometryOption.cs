﻿namespace Recolude.SceneSystem.Properties.Webplayer
{
    public enum GeometryOption
    {
        Cube,
        Sphere,
        Plane,
        Capsule,
        VrHead,
        None,
    }
}