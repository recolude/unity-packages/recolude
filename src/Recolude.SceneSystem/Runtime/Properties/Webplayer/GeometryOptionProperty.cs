﻿using System;
using UnityEngine;

namespace Recolude.SceneSystem.Properties.Webplayer
{
    [CreateAssetMenu(fileName = "Web Player Subject Geometry Property",
        menuName = "Recolude/Scene System/Properties/Web Player/Subject Geometry", order = 1)]
    public class GeometryOptionProperty : EnumStringProperty<GeometryOption>
    {
        protected override string ValueToString(GeometryOption val)
        {
            return val switch
            {
                GeometryOption.Cube => "cube",
                GeometryOption.Sphere => "sphere",
                GeometryOption.Plane => "plane",
                GeometryOption.None => "none",
                GeometryOption.Capsule => "capsule",
                GeometryOption.VrHead => "vr-head",
                _ => throw new NotImplementedException($"Unimplemented geometry option: {val}")
            };
        }
    }
}