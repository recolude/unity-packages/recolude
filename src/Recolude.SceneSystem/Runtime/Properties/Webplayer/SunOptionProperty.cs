﻿using System.Collections.Generic;
using Recolude.Core;
using Recolude.Core.Util;
using UnityEngine;

namespace Recolude.SceneSystem.Properties.Webplayer
{
    [CreateAssetMenu(fileName = "Web Player Sun Property",
        menuName = "Recolude/Scene System/Properties/Web Player/Sun", order = 1)]
    public class SunOptionProperty : ScriptableProperty<Metadata>
    {
        [SerializeField] private Color lightColor = new Color(255 / 255f, 242 / 255f, 236 / 255f);

        [SerializeField] private Vector3 position = new Vector3(-50, 200, -100);

        [SerializeField] private float strength = 1;

        public override Metadata Value => new Metadata(new Dictionary<string, IProperty>()
        {
            { "color", new Property<string>(JSON.ColorToHex(lightColor)) },
            { "position", new Property<Vector3>(position) },
            { "strength", new Property<float>(strength) },
        });
    }
}