﻿using Recolude.Core;

namespace Recolude.SceneSystem.Properties.Webplayer
{
    public class WebplayerConfig: ScriptableProperty<Metadata>
    {
        public override Metadata Value { get; }
    }
}