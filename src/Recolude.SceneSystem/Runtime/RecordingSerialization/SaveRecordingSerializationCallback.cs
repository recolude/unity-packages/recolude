﻿using System;
using System.IO;
using Recolude.Core.IO;
using Recolude.Core.Util;
using UnityEngine;

namespace Recolude.SceneSystem.RecordingSerialization
{
    [CreateAssetMenu(fileName = "Save Recording Config", menuName = SubMenu + "Save Recording", order = 1)]
    public class SaveRecordingSerializationCallback : SerializationCallbackConfig
    {
        enum BasePath
        {
            PersistentDataPath, // Application.persistentDataPath
            TemporaryCachePath, // Application.temporaryCachePath
            StreamingAssetsPath, // Application.streamingAssetsPath
            DataPath, // Application.dataPath
        }

        enum NamePostfix
        {
            RecordingCount,
            TimeStamp
        }

        private static string ResolveBasePath(BasePath basePath)
        {
            return basePath switch
            {
                BasePath.PersistentDataPath => Application.persistentDataPath,
                BasePath.TemporaryCachePath => Application.temporaryCachePath,
                BasePath.StreamingAssetsPath => Application.streamingAssetsPath,
                BasePath.DataPath => Application.dataPath,
                _ => throw new NotImplementedException($"Unimplemented resolver for {basePath}")
            };
        }

        [SerializeField] private BasePath basePath;

        [OverrideName("Additional Subpath", overrideKeyword: "Add")] [SerializeField]
        private OverridableValue<string> additionalSubpath;

        [SerializeField] private string recordingName = "Recording";

        [SerializeField] private NamePostfix namePostfix;

        [Tooltip("Log the full path the recording is saved to")] [SerializeField]
        private bool logRecordingPath;

        protected override void Serialize(SerializationCommandBuilder command)
        {
            var finalDirectory = ResolveBasePath(basePath);
            if (additionalSubpath.OverrideValue)
            {
                finalDirectory = Path.Combine(finalDirectory, additionalSubpath.Value);
            }

            Directory.CreateDirectory(finalDirectory);

            var finalRecordingName = recordingName;
            switch (namePostfix)
            {
                case NamePostfix.TimeStamp:
                    finalRecordingName = $"{finalRecordingName} {DateTime.Now.ToString("yyyyMMddHHmmss")}.rap";
                    break;

                case NamePostfix.RecordingCount:
                    var files = Directory.GetFiles(finalDirectory, $"{finalRecordingName}*");
                    finalRecordingName = $"{finalRecordingName} {files.Length}.rap";
                    break;
            }

            var finalPath = Path.Combine(finalDirectory, finalRecordingName);
            if (logRecordingPath)
            {
                Debug.Log(finalPath);
            }

            using var f = File.Create(finalPath);
            command.WithStream(f).Run();
        }
    }
}