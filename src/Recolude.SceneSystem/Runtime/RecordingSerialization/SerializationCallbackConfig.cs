﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Recolude.Core.IO;
using UnityEngine;

namespace Recolude.SceneSystem.RecordingSerialization
{
    public abstract class SerializationCallbackConfig : ScriptableObject
    {
        protected const string SubMenu = "Recolude/Scene System/Serialization Callbacks/";

        [System.Flags]
        public enum SerializationEnvironment
        {
            Everything = 0,

            // AllEditors = 1,
            OSXEditor = 1 << 1, //	In the Unity editor on macOS.
            OSXPlayer = 1 << 2, //	In the player on macOS.
            WindowsPlayer = 1 << 3, //	In the player on Windows.
            WindowsEditor = 1 << 4, //	In the Unity editor on Windows.
            IPhonePlayer = 1 << 5, //	In the player on the iPhone.
            Android = 1 << 6, //	In the player on Android devices.
            LinuxPlayer = 1 << 7, //	In the player on Linux.
            LinuxEditor = 1 << 8, //	In the Unity editor on Linux.
            WebGLPlayer = 1 << 9, //	In the player on WebGL
            WSAPlayerX86 = 1 << 10, //	In the player on Windows Store Apps when CPU architecture is X86.
            WSAPlayerX64 = 1 << 11, //	In the player on Windows Store Apps when CPU architecture is X64.
            WSAPlayerARM = 1 << 12, //	In the player on Windows Store Apps when CPU architecture is ARM.
            PS4 = 1 << 13, //	In the player on the Playstation 4.
            XboxOne = 1 << 14, //	In the player on Xbox One.
            tvOS = 1 << 15, //	In the player on the Apple's tvOS.
            Switch = 1 << 16, //	In the player on Nintendo Switch.
            Stadia = 1 << 17, //	In the player on Stadia.
            PS5 = 1 << 18, //	In the player on the Playstation 5.
            // LinuxServer = 1 << 19, //	In the server on Linux.
            // WindowsServer = 1 << 20, //	In the server on Windows.
            // OSXServer = 1 << 21, //	In the server on macOS.
        }

        private static Dictionary<RuntimePlatform, SerializationEnvironment> platformToEnv =
            new Dictionary<RuntimePlatform, SerializationEnvironment>()
            {
                { RuntimePlatform.OSXEditor, SerializationEnvironment.OSXEditor },
                { RuntimePlatform.OSXPlayer, SerializationEnvironment.OSXPlayer },
                { RuntimePlatform.WindowsPlayer, SerializationEnvironment.WindowsPlayer },
                { RuntimePlatform.WindowsEditor, SerializationEnvironment.WindowsEditor },
                { RuntimePlatform.IPhonePlayer, SerializationEnvironment.IPhonePlayer },
                { RuntimePlatform.Android, SerializationEnvironment.Android },
                { RuntimePlatform.LinuxPlayer, SerializationEnvironment.LinuxPlayer },
                { RuntimePlatform.LinuxEditor, SerializationEnvironment.LinuxEditor },
                { RuntimePlatform.WebGLPlayer, SerializationEnvironment.WebGLPlayer },
                { RuntimePlatform.WSAPlayerX86, SerializationEnvironment.WSAPlayerX86 },
                { RuntimePlatform.WSAPlayerX64, SerializationEnvironment.WSAPlayerX64 },
                { RuntimePlatform.WSAPlayerARM, SerializationEnvironment.WSAPlayerARM },
                { RuntimePlatform.PS4, SerializationEnvironment.PS4 },
                { RuntimePlatform.XboxOne, SerializationEnvironment.XboxOne },
                { RuntimePlatform.tvOS, SerializationEnvironment.tvOS },
                { RuntimePlatform.Switch, SerializationEnvironment.Switch },
                { RuntimePlatform.Stadia, SerializationEnvironment.Stadia },
                { RuntimePlatform.PS5, SerializationEnvironment.PS5 },
                // { SerializationEnvironment.LinuxServer, RuntimePlatform.LinuxServer },
                // { SerializationEnvironment.WindowsServer, RuntimePlatform.WindowsServer },
                // { SerializationEnvironment.OSXServer, RuntimePlatform.OSXServer },
            };

        [Tooltip("The different platforms this serialization callback should execute in")] [SerializeField]
        private SerializationEnvironment serializationEnvironment;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool IsFlagSet(SerializationEnvironment val, SerializationEnvironment flag)
        {
            return (val & flag) == flag;
        }

        private bool ShouldSerialize()
        {
            if (serializationEnvironment == SerializationEnvironment.Everything)
            {
                return true;
            }

            return IsFlagSet(serializationEnvironment, platformToEnv[Application.platform]);
        }

        public void SerializeRecording(SerializationCommandBuilder command)
        {
            if (ShouldSerialize())
            {
                Serialize(command);
            }
        }

        protected abstract void Serialize(SerializationCommandBuilder command);
    }
}