﻿using System;
using System.Collections.Generic;
using Recolude.Core;
using Recolude.Core.IO;
using Recolude.Core.Util;
using Recolude.SceneSystem.EncoderConfigs;
using UnityEngine;
using CompressionLevel = System.IO.Compression.CompressionLevel;

namespace Recolude.SceneSystem.RecordingSerialization
{
    [CreateAssetMenu(fileName = "Serialization Config",
        menuName = "Recolude/Scene System/Serialization Config", order = 1)]
    public class SerializationConfig : ScriptableObject
    {
        public TimeStorageTechnique TimeStorageTechnique => timeStorageTechnique;

        public CompressionLevel CompressionLevel => compressionLevel;

        [SerializeField] private TimeStorageTechnique timeStorageTechnique = TimeStorageTechnique.BST16;

        [SerializeField] private CompressionLevel compressionLevel;

        [ExtendableScriptableObject] [SerializeField]
        private List<EncoderConfig> encoders;

        public void AddEncoders(params EncoderConfig[] encodersToAdd)
        {
            if (encoders == null)
            {
                encoders = new List<EncoderConfig>();
            }
            encoders.AddRange(encodersToAdd);
        }

        public IEncoder<ICaptureCollection<ICapture>>[] Encoders()
        {
            var allEncoders = new List<IEncoder<ICaptureCollection<ICapture>>>(encoders.Count);

            foreach (var encoderConfig in encoders)
            {
                if (encoderConfig == null)
                {
                    continue;
                }

                var encoder = encoderConfig.Encoder;
                if (encoder == null)
                {
                    throw new SystemException($"Encoder config constructed null encoder");
                }

                allEncoders.Add(encoder);
            }

            return allEncoders.ToArray();
        }
    }
}