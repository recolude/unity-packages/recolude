﻿using Recolude.Core;
using Recolude.Core.IO;
using Recolude.Core.Util;
using UnityEngine;

namespace Recolude.SceneSystem.RecordingSerialization
{
    [AddComponentMenu("Recolude/Serialization/Serialization System")]
    public class SerializationSystem : MonoBehaviour
    {
        [ExtendableScriptableObject] [SerializeField]
        private SerializationConfig serializationConfig;

        [ExtendableScriptableObject(typeof(UploadRecordingSerializationCallback))] [SerializeField]
        private SerializationCallbackConfig[] serializationCallbacks;

        public static SerializationSystem Build(
            GameObject gameObject,
            SerializationConfig serializationConfig,
            SerializationCallbackConfig[] serializationCallbacks
        )
        {
            var system = gameObject.AddComponent<SerializationSystem>();

            system.serializationConfig = serializationConfig;
            system.serializationCallbacks = serializationCallbacks;

            return system;
        }

        public void SerializeRecording(IRecording recording)
        {
            var config = new SerializationCommandBuilder(recording)
                .WithEncoders(serializationConfig.Encoders())
                .WithCompressionLevel(serializationConfig.CompressionLevel)
                .WithTimeStorageTechnique(serializationConfig.TimeStorageTechnique);

            foreach (var callback in serializationCallbacks)
            {
                callback.SerializeRecording(config);
            }
        }
    }
}