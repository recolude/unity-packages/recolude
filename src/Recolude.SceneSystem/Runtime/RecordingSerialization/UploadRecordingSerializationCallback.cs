﻿using System.Collections;
using Recolude.API;
using Recolude.API.RecordingStorage;
using Recolude.Core.IO;
using Recolude.Core.Util;
using UnityEngine;
using UnityEngine.Events;

namespace Recolude.SceneSystem.RecordingSerialization
{
    [CreateAssetMenu(fileName = "Upload Recording Config", menuName = SubMenu + "Upload Recording", order = 1)]
    public class UploadRecordingSerializationCallback : SerializationCallbackConfig
    {
        [SerializeField] [ExtendableScriptableObject]
        private RecoludeConfig apiConfig;

        [SerializeField] [OverrideName("Recording Visibility")]
        private OverridableValue<RecordingVisibility> recordingVisibility;

        [Tooltip("Called when a recording is successfully uploaded with the ID of the recording in Recolude")]
        [SerializeField]
        private UnityEvent<string> onUploadSuccess;

        [Tooltip("Called when a recording fails to upload, passing the error message")] [SerializeField]
        private UnityEvent<string> onUploadError;


        [Tooltip("Whether or not to print to console when we fail to upload")] [SerializeField]
        private bool logFailure;

        [Tooltip("Whether or not to print to console when we successfully upload")] [SerializeField]
        private bool logRecordingIDOnSuccess;

        protected override void Serialize(SerializationCommandBuilder command)
        {
            var visibility = recordingVisibility.GetValue(apiConfig.DefaultUploadVisibility());
            RecordingUploader.GetInstance()
                .StartCoroutine(UploadRecording(apiConfig.SaveRecording(command, visibility)));
        }

        private IEnumerator UploadRecording(ISaveRecordingOperation op)
        {
            yield return op.Run();

            var err = op.Error();
            if (!string.IsNullOrEmpty(err))
            {
                if (logFailure)
                {
                    Debug.LogErrorFormat("Unable to upload recording: {0}", err);
                }

                onUploadError?.Invoke(err);
            }
            else
            {
                if (logRecordingIDOnSuccess)
                {
                    Debug.LogFormat("Uploaded recording with ID {0}", op.RecordingID());
                }

                onUploadSuccess?.Invoke(op.RecordingID());
            }
        }
    }
}