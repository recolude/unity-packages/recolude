﻿using Recolude.Core;
using Recolude.Core.Playback;
using UnityEngine;

namespace Recolude.SceneSystem.ScenePlayback.ActorBuilders
{
    public abstract class ActorBuilder : MonoBehaviour, IActorBuilder
    {
        protected abstract IActorBuilder Builder { get; }

        public IActor[] Build(IRecording recording)
        {
            return Builder.Build(recording);
        }
    }
}