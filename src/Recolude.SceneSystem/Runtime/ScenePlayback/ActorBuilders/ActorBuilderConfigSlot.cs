﻿using Recolude.Core.Playback;
using Recolude.Core.Util;
using UnityEngine;

namespace Recolude.SceneSystem.ScenePlayback.ActorBuilders
{
    /// <summary>
    /// A type of actor builder that only exists to serve as a way to specify
    /// some scriptable object ot be used as an actor builder
    /// </summary>
    [AddComponentMenu("Recolude/Scene Playback/Actor Builders/Actor Builder Config")]
    public class ActorBuilderConfigSlot : ActorBuilder
    {
        [ExtendableScriptableObject(createType = typeof(RegexActorBuilderConfig))] [SerializeField]
        private ActorBuilderConfig actorBuilder;

        protected override IActorBuilder Builder => actorBuilder;
    }
}