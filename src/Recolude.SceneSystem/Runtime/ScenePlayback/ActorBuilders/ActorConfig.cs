﻿using Recolude.Core;
using Recolude.Core.Playback;
using Recolude.Core.Util;
using Recolude.SceneSystem.ScenePlayback.ActorEventEmitters;
using UnityEngine;

namespace Recolude.SceneSystem.ScenePlayback.ActorBuilders
{
    [CreateAssetMenu(fileName = "Basic Actor Config", menuName = Menu + "Basic", order = 1)]
    public class ActorConfig : BaseActorConfig
    {
        [SerializeField] private BaseActorConfig[] childConfigs;

        [SerializeField] private GameObject representation;

        [SerializeField] private ActorCleanupMethod cleanupMethod = ActorCleanupMethod.Destroy;

        [ExtendableScriptableObject] [SerializeField]
        private ActorEventEmitterCollection emitters;

        public override IActor[] Build(IRecording recording)
        {
            return new IActor[]
                { new GameObjectActor(recording, Instantiate(representation), emitters, cleanupMethod) };
        }
    }
}