﻿using Recolude.Core;
using Recolude.Core.Playback;
using UnityEngine;

namespace Recolude.SceneSystem.ScenePlayback.ActorBuilders
{
    public abstract class BaseActorConfig : ScriptableObject, IActorBuilder
    {
        protected const string Menu = "Recolude/Scene System/Playback/Actor Configs/";

        public abstract IActor[] Build(IRecording recording);
    }
}