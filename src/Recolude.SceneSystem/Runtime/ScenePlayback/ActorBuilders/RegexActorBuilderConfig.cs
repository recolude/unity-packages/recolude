﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Recolude.Core;
using Recolude.Core.Playback;
using Recolude.Core.Util;
using Recolude.SceneSystem.ScenePlayback.ActorEventEmitters;
using UnityEngine;

namespace Recolude.SceneSystem.ScenePlayback.ActorBuilders
{
    [CreateAssetMenu(fileName = "Regex Actor Builder", menuName = MenuPath + "Regex", order = 1)]
    public class RegexActorBuilderConfig : ActorBuilderConfig
    {
        [Serializable]
        public class RegexEntry : IActorBuilder
        {
            [RequiredValue] [SerializeField] private string recordingNameRegex;

            [ExtendableScriptableObject(typeof(ActorConfig))] [SerializeField]
            private BaseActorConfig config;

            public bool Matches(IRecording recording)
            {
                return Regex.IsMatch(recording.Name, recordingNameRegex);
            }

            public IActor[] Build(IRecording recording)
            {
                return config.Build(recording);
            }
        }

        [OverrideName("Fallback")]
        [SerializeField]
        [Tooltip("Fallback to use when none of the current actors you set fit")]
        private OverridableValue<BaseActorConfig> fallback;

        [ExtendableScriptableObject] [SerializeField]
        private ActorEventEmitterCollection globalEmitters;

        [SerializeField] private RegexEntry[] actors;

        private RegexEntry MatchingEntry(IRecording recording)
        {
            foreach (var entry in actors)
            {
                if (entry.Matches(recording))
                {
                    return entry;
                }
            }

            return null;
        }

        public override IActor[] Build(IRecording recording)
        {
            List<IActor> builtActors = new List<IActor>(recording.Recordings.Length);

            foreach (var child in recording.Recordings)
            {
                var entry = MatchingEntry(child);
                if (entry != null)
                {
                    builtActors.AddRange(entry.Build(child));
                }
                else if (fallback != null)
                {
                    builtActors.AddRange(fallback.Value.Build(child));
                }
            }

            if (globalEmitters != null)
            {
                builtActors.Add(new GameObjectActor(recording, null, globalEmitters));
            }

            return builtActors.ToArray();
        }
    }
}