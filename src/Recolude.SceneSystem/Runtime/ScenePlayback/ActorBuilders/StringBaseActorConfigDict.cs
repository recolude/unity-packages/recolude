﻿using System;
using Recolude.Core.Util;

namespace Recolude.SceneSystem.ScenePlayback.ActorBuilders
{
    [Serializable]
    public class StringBaseActorConfigDict : SerializableDictionary<string, BaseActorConfig>
    {
    }
}