﻿using Recolude.Core;
using UnityEngine;

namespace Recolude.SceneSystem.ScenePlayback.ActorEventEmitters
{
    public abstract class ActorEventEmitter : ScriptableObject, IActorEventEmitter
    {
        public abstract bool Matches(CustomEvent customEvent);
        public abstract void Act(CustomEvent customEvent);
    }
}