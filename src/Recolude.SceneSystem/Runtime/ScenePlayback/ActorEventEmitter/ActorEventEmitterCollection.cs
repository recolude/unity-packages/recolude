﻿using Recolude.Core;
using Recolude.Core.AssetManagement;
using Recolude.Core.Playback;

namespace Recolude.SceneSystem.ScenePlayback.ActorEventEmitters
{
    public class ActorEventEmitterCollection : AssetCollection<ActorEventEmitter>, IPlaybackCustomEventHandler
    {
        public void OnCustomEvent(
            IRecording recording,
            TimeMovementDetails movementDetails,
            Capture<CustomEvent> customEvent
        )
        {
            if (assets == null)
            {
                return;
            }

            foreach (var emitter in assets)
            {
                if (emitter == null)
                {
                    continue;
                }

                if (emitter.Matches(customEvent.Value))
                {
                    emitter.Act(customEvent.Value);
                }
            }
        }
    }
}