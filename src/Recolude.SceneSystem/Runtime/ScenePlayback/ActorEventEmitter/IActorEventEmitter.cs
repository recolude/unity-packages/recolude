﻿using Recolude.Core;

namespace Recolude.SceneSystem.ScenePlayback.ActorEventEmitters
{
    public interface IActorEventEmitter
    {
        bool Matches(CustomEvent customEvent);

        void Act(CustomEvent customEvent);
    }
}