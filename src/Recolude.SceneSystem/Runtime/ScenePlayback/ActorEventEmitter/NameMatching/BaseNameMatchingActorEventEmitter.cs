﻿using Recolude.Core;
using Recolude.SceneSystem.Properties.Reference;
using UnityEngine;

namespace Recolude.SceneSystem.ScenePlayback.ActorEventEmitters.NameMatching
{
    public abstract class BaseNameMatchingActorEventEmitter : ActorEventEmitter
    {
        protected const string Menu = "Recolude/Scene System/Playback/Event Emitters/Name Matching/";

        [SerializeField] private PropertyReference<string> eventName;

        protected abstract void RaiseEvent(CustomEvent customEvent);

        public override void Act(CustomEvent customEvent)
        {
            RaiseEvent(customEvent);
        }

        public override bool Matches(CustomEvent customEvent)
        {
            return customEvent.Name == eventName.Value;
        }
    }
}