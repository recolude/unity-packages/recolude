﻿using UnityEngine;

namespace Recolude.SceneSystem.ScenePlayback.ActorEventEmitters.NameMatching
{
    [CreateAssetMenu(fileName = "Name Matching Bool Emitter", menuName = Menu + "Bool Emitter", order = 1)]
    public class BoolNameMatchingActorEventEmitter: TypedNameMatchingActorEventEmitter<bool>
    {
        
    }
}