﻿using UnityEngine;

namespace Recolude.SceneSystem.ScenePlayback.ActorEventEmitters.NameMatching
{
    [CreateAssetMenu(fileName = "Name Matching Byte Emitter", menuName = Menu + "Byte Emitter", order = 1)]
    public class ByteNameMatchingActorEventEmitter: TypedNameMatchingActorEventEmitter<byte>
    {
        
    }
}