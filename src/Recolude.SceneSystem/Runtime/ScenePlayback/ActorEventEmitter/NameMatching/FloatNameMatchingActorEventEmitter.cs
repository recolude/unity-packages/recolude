﻿using UnityEngine;

namespace Recolude.SceneSystem.ScenePlayback.ActorEventEmitters.NameMatching
{
    [CreateAssetMenu(fileName = "Name Matching Float Emitter", menuName = Menu + "Float Emitter", order = 1)]
    public class FloatNameMatchingActorEventEmitter: TypedNameMatchingActorEventEmitter<float>
    {
        
    }
}