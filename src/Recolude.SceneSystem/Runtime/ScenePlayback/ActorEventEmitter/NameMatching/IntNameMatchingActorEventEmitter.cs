﻿using UnityEngine;

namespace Recolude.SceneSystem.ScenePlayback.ActorEventEmitters.NameMatching
{
    [CreateAssetMenu(fileName = "Name Matching Int Emitter", menuName = Menu + "Int Emitter", order = 1)]
    public class IntNameMatchingActorEventEmitter : TypedNameMatchingActorEventEmitter<int>
    {
    }
}