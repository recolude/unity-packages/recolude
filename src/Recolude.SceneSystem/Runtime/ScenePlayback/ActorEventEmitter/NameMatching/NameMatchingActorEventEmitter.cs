﻿using Recolude.Core;
using Recolude.Core.Util;
using Recolude.SceneSystem.Events;
using UnityEngine;

namespace Recolude.SceneSystem.ScenePlayback.ActorEventEmitters.NameMatching
{
    [CreateAssetMenu(fileName = "Name Matching Emitter", menuName = Menu + "Emitter", order = 1)]
    public class NameMatchingActorEventEmitter : BaseNameMatchingActorEventEmitter
    {
        [RequiredValue] [SerializeField] private GameEvent gameEvent;

        protected override void RaiseEvent(CustomEvent customEvent)
        {
            gameEvent.Raise();
        }
    }
}