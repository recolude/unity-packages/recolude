﻿using UnityEngine;

namespace Recolude.SceneSystem.ScenePlayback.ActorEventEmitters.NameMatching
{
    [CreateAssetMenu(fileName = "Name Matching String Emitter", menuName = Menu + "String Emitter",
        order = 1)]
    public class StringNameMatchingActorEventEmitter : TypedNameMatchingActorEventEmitter<string>
    {
    }
}