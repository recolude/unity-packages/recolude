﻿using Recolude.Core;
using Recolude.Core.Util;
using Recolude.SceneSystem.Events;
using Recolude.SceneSystem.Properties.Reference;
using UnityEngine;

namespace Recolude.SceneSystem.ScenePlayback.ActorEventEmitters.NameMatching
{
    public abstract class TypedNameMatchingActorEventEmitter<T> : BaseNameMatchingActorEventEmitter
    {
        [SerializeField] private PropertyReference<string> propertyName;

        [RequiredValue] [SerializeField] private GameEvent<T> gameEvent;

        protected override void RaiseEvent(CustomEvent customEvent)
        {
            gameEvent.Raise(customEvent.Contents.AsType<T>(propertyName.Value));
        }
    }
}