﻿using UnityEngine;

namespace Recolude.SceneSystem.ScenePlayback.ActorEventEmitters.NameMatching
{
    [CreateAssetMenu(fileName = "Name Matching Vector2 Emitter", menuName = Menu + "Vector2 Emitter", order = 1)]
    public class Vector2NameMatchingActorEventEmitter : TypedNameMatchingActorEventEmitter<Vector2>
    {
    }
}