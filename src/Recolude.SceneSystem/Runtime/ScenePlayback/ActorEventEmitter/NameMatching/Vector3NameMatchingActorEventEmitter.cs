﻿using UnityEngine;

namespace Recolude.SceneSystem.ScenePlayback.ActorEventEmitters.NameMatching
{
    [CreateAssetMenu(fileName = "Name Matching Vector3 Emitter", menuName = Menu + "Vector3 Emitter",
        order = 1)]
    public class Vector3NameMatchingActorEventEmitter : TypedNameMatchingActorEventEmitter<Vector3>
    {
    }
}