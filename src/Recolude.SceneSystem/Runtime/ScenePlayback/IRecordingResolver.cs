﻿namespace Recolude.SceneSystem.ScenePlayback
{
    public interface IRecordingResolver
    {
        IRetrieveRecordingRequest RetrieveRecording();
    }
}