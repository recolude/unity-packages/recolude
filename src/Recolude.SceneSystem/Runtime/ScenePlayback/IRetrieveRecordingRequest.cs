﻿using System.Collections;
using Recolude.Core;

namespace Recolude.SceneSystem.ScenePlayback
{
    public interface IRetrieveRecordingRequest
    {
        IEnumerator Run();

        IRecording Recording { get; }

        string Error { get; }
    }
}