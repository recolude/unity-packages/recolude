﻿using System;
using System.Collections;
using Recolude.Core;
using Recolude.Core.Playback;
using Recolude.Core.Util;
using Recolude.SceneSystem.Clock;
using Recolude.SceneSystem.ScenePlayback.ActorBuilders;
using Recolude.SceneSystem.ScenePlayback.RecordingResolvers;
using UnityEngine;

namespace Recolude.SceneSystem.ScenePlayback
{
    [AddComponentMenu("Recolude/Scene Playback/Playback System")]
    public class PlaybackSystem : MonoBehaviour
    {
        public enum SystemState
        {
            Unloaded,
            Loading,
            Loaded
        }

        #region Configuration Variables

        [Tooltip("Required if you want to build a playback system without having a specific recorder")] [SerializeField]
        private RecordingResolver recordingResolver;

        [RequiredValue] [SerializeField] private ActorBuilder actorBuilder;

        [RequiredValue] [SerializeField] private ClockBehavior clock;

        [SerializeField] private bool loop;

        #endregion

        private SystemState currentState;

        private PlaybackBehavior playbackBehavior;

        public PlaybackBehavior PlaybackBehavior => playbackBehavior;

        public SystemState State => currentState;

        private void Awake()
        {
            currentState = SystemState.Unloaded;
            playbackBehavior = null;
        }

        public void BeginPlayback()
        {
            BeginPlayback(recordingResolver);
        }

        public void BeginPlayback(IRecordingResolver resolver)
        {
            if (resolver == null)
                throw new ArgumentNullException(nameof(resolver), "A recording resolver is required to begin playback");
            currentState = SystemState.Loading;
            StartCoroutine(RetrieveRecording(resolver));
        }

        public void BeginPlayback(IRecording recording)
        {
            Setup(recording);
        }

        public PlaybackBehavior BuildPlayback(IRecording recording)
        {
            return PlaybackBehavior.Build(recording, actorBuilder, loop, clock);
        }

        public static PlaybackSystem Build(
            GameObject gameObject,
            ClockBehavior clock,
            RecordingResolver recordingResolver,
            ActorBuilder actorBuilder,
            bool loop
        )
        {
            var system = gameObject.AddComponent<PlaybackSystem>();

            system.clock = clock;
            system.recordingResolver = recordingResolver;
            system.actorBuilder = actorBuilder;
            system.loop = loop;

            return system;
        }


        private IEnumerator RetrieveRecording(IRecordingResolver resolver)
        {
            var request = resolver.RetrieveRecording();
            yield return request.Run();

            var err = request.Error;
            if (!string.IsNullOrEmpty(err))
            {
                Debug.LogError($"Playback System unable To retrieve recording: {err}");
                yield break;
            }

            Setup(request.Recording);
        }

        private void Setup(IRecording recording)
        {
            playbackBehavior = BuildPlayback(recording);
            currentState = SystemState.Loaded;
            playbackBehavior.Play();
        }
    }
}