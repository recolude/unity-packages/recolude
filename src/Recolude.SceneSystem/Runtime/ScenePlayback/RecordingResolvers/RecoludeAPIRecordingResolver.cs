﻿using System;
using System.Collections.Generic;
using Recolude.API;
using Recolude.Core;
using Recolude.Core.IO;
using Recolude.Core.Util;
using Recolude.SceneSystem.EncoderConfigs;
using UnityEngine;

namespace Recolude.SceneSystem.ScenePlayback.RecordingResolvers
{
    [CreateAssetMenu(fileName = "API Recording Resolver",
        menuName = "Recolude/Scene System/Playback/Recording Resolvers/API", order = 1)]
    public class RecoludeAPIRecordingResolver : RecordingResolverConfig
    {
        [RequiredValue] [SerializeField] private string recordingID;

        [ExtendableScriptableObject(required = true)] [SerializeField]
        private RecoludeConfig api;

        [ExtendableScriptableObject] [SerializeField]
        private EncoderConfig[] encoders;

        public override IRetrieveRecordingRequest RetrieveRecording()
        {
            return new WebRetrieveRecordingRequest(recordingID, api, Encoders());
        }

        private IEncoder<ICaptureCollection<ICapture>>[] Encoders()
        {
            var allEncoders = new List<IEncoder<ICaptureCollection<ICapture>>>(encoders.Length);

            foreach (var encoderConfig in encoders)
            {
                if (encoderConfig == null)
                {
                    continue;
                }

                var encoder = encoderConfig.Encoder;
                if (encoder == null)
                {
                    throw new SystemException($"Encoder config constructed null encoder");
                }

                allEncoders.Add(encoder);
            }

            return allEncoders.ToArray();
        }
    }
}