﻿using UnityEngine;

namespace Recolude.SceneSystem.ScenePlayback.RecordingResolvers
{
    public abstract class RecordingResolver: MonoBehaviour, IRecordingResolver
    {
        public abstract IRetrieveRecordingRequest RetrieveRecording();

    }
}