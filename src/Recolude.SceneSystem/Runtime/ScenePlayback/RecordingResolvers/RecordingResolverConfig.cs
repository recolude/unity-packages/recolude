﻿using UnityEngine;

namespace Recolude.SceneSystem.ScenePlayback.RecordingResolvers
{
    public abstract class RecordingResolverConfig : ScriptableObject, IRecordingResolver
    {
        public abstract IRetrieveRecordingRequest RetrieveRecording();
    }
}