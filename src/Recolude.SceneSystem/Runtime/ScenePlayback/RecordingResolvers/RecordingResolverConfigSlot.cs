﻿using Recolude.Core.Util;
using UnityEngine;

namespace Recolude.SceneSystem.ScenePlayback.RecordingResolvers
{
    [AddComponentMenu("Recolude/Scene Playback/Recording Resolver/Recording Resolver Config")]
    public class RecordingResolverConfigSlot : RecordingResolver
    {
        [ExtendableScriptableObject(typeof(RecoludeAPIRecordingResolver))] [SerializeField]
        private RecordingResolverConfig resolverConfig;

        public override IRetrieveRecordingRequest RetrieveRecording()
        {
            return resolverConfig.RetrieveRecording();
        }
    }
}