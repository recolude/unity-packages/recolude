﻿using System.Collections;
using Recolude.API;
using Recolude.API.RecordingStorage;
using Recolude.Core;
using Recolude.Core.IO;

namespace Recolude.SceneSystem.ScenePlayback.RecordingResolvers
{
    public class WebRetrieveRecordingRequest : IRetrieveRecordingRequest
    {
        private readonly ILoadRecordingOperation loadRecordingOperation;

        public WebRetrieveRecordingRequest(
            string id, 
            RecoludeConfig api,
            IEncoder<ICaptureCollection<ICapture>>[] encoders
        )
        {
            loadRecordingOperation = api.LoadRecording(id, encoders);
        }

        public IEnumerator Run()
        {
            yield return loadRecordingOperation.Run();
        }

        public IRecording Recording => loadRecordingOperation.Recording();

        public string Error => loadRecordingOperation.Error();
    }
}