﻿using Recolude.Core;
using Recolude.Core.Record;
using Recolude.Core.Util;
using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording
{
    public abstract class CaptureRecorderConfigSlot : MonoBehaviour
    {
        [OverrideName("Collection Name")] [SerializeField]
        private OverridableValue<string> overrideCollectionName;

        protected abstract string DefaultCollectionName { get; }

        protected string CollectionName => overrideCollectionName.GetValue(DefaultCollectionName);

        public abstract ICaptureRecorder<ICapture> BuildRecorder(ITimerManager timerManager);
    }
}