﻿using System;
using Recolude.Core;
using Recolude.Core.Record;
using Recolude.Core.Record.CollectionRecorders;
using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording.CaptureRecorders
{
    public class OnBuildRecorderEventArgs : EventArgs
    {
        public TimeAwareCustomEventRecorder Recorder { get; }

        public OnBuildRecorderEventArgs(TimeAwareCustomEventRecorder recorder)
        {
            Recorder = recorder;
        }
    }
    
    [AddComponentMenu("Recolude/Scene Recording/Custom Event Recorder")]
    public class CustomEventRecorderConfigSlot : CaptureRecorderConfigSlot
    {
        protected override string DefaultCollectionName => CollectionNames.CustomEvent;

        public override ICaptureRecorder<ICapture> BuildRecorder(ITimerManager timerManager)
        {
            var recorder = new TimeAwareCustomEventRecorder(CollectionName, timerManager.TimeProvider);
            OnBuildRecorder?.Invoke(this, new OnBuildRecorderEventArgs(recorder));
            return recorder;
        }

        public event EventHandler<OnBuildRecorderEventArgs> OnBuildRecorder;
    }
}