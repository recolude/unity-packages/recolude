﻿using Recolude.Core;
using Recolude.Core.Util;
using Recolude.SceneSystem.Properties.Reference;
using Recolude.SceneSystem.SceneRecording.MetadataBuilders;
using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording.CaptureRecorders.EventRecorders
{
    [AddComponentMenu("Recolude/Scene Recording/Events/Collision Event Recorder")]
    public class CollisionEventRecorder : EventRecorder
    {
        private const string onCollisionEnterDefaultEventName = "On Collision Enter";

        private const string onCollisionExitDefaultEventName = "On Collision Exit";

        #region Configuration Variables

        [SerializeField] private bool captureOnCollisionEnter;

        [OverrideName("On Collision Enter Event Name")] [SerializeField]
        private OverridableValue<PropertyReference<string>> onCollisionEnterEventName =
            new OverridableValue<PropertyReference<string>>(
                new PropertyReference<string>(onCollisionEnterDefaultEventName));

        [SerializeField] private CollisionMetadataBuilder onCollisionEnter;

        [SerializeField] private bool captureOnCollisionExit;

        [OverrideName("On Collision Exit Event Name")] [SerializeField]
        private OverridableValue<PropertyReference<string>> onCollisionExitEventName =
            new OverridableValue<PropertyReference<string>>(
                new PropertyReference<string>(onCollisionEnterDefaultEventName));

        [SerializeField] private CollisionMetadataBuilder onCollisionExit;

        #endregion

        public bool CaptureOnCollisionEnter => captureOnCollisionEnter;

        public CollisionMetadataBuilder OnCollisionEnterBuilder => onCollisionEnter;

        public bool CaptureOnCollisionExit => captureOnCollisionExit;

        public CollisionMetadataBuilder OnCollisionExitBuilder => onCollisionExit;

        void OnCollisionEnter(Collision collision)
        {
            if (!HasRecorders || !CaptureOnCollisionEnter) return;

            var eventName = onCollisionEnterDefaultEventName;
            if (onCollisionEnterEventName.OverrideValue)
            {
                eventName = onCollisionEnterEventName.Value.Value;
            }

            var metadata = onCollisionEnter.Build(collision);
            var collisionEvent = new CustomEvent(eventName, metadata);
            foreach (var recorder in recorders)
            {
                recorder.Record(collisionEvent);
            }
        }

        void OnCollisionExit(Collision collision)
        {
            if (!HasRecorders || !captureOnCollisionExit) return;

            var eventName = onCollisionExitDefaultEventName;
            if (onCollisionExitEventName.OverrideValue)
            {
                eventName = onCollisionExitEventName.Value.Value;
            }


            var metadata = onCollisionExit.Build(collision);
            var collisionEvent = new CustomEvent(eventName, metadata);
            foreach (var recorder in recorders)
            {
                recorder.Record(collisionEvent);
            }
        }
    }
}