﻿using System.Collections.Generic;
using Recolude.Core.Record.CollectionRecorders;
using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording.CaptureRecorders.EventRecorders
{
    public abstract class EventRecorder : MonoBehaviour
    {
        [SerializeField] protected CustomEventRecorderConfigSlot[] customEventRecorderConfigSlots;

        protected List<TimeAwareCustomEventRecorder> recorders;

        private void Awake()
        {
            recorders = new List<TimeAwareCustomEventRecorder>();
            if (customEventRecorderConfigSlots == null)
            {
                return;
            }

            foreach (var slot in customEventRecorderConfigSlots)
            {
                if (slot == null)
                {
                    return;
                }

                slot.OnBuildRecorder += CustomEventRecorderConfigSlotOnOnBuildRecorder;
            }
        }

        private void CustomEventRecorderConfigSlotOnOnBuildRecorder(object sender, OnBuildRecorderEventArgs e)
        {
            recorders.Add(e.Recorder);
        }

        protected bool HasRecorders => recorders.Count > 0;

        public bool HasCustomEventRecorderConfig
        {
            get
            {
                if (customEventRecorderConfigSlots == null)
                {
                    return false;
                }

                foreach (var slot in customEventRecorderConfigSlots)
                {
                    if (slot != null)
                    {
                        return true;
                    }
                }

                return false;
            }
        }
    }
}