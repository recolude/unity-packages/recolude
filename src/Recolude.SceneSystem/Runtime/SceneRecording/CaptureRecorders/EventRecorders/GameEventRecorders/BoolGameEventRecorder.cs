﻿using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording.CaptureRecorders.EventRecorders.GameEventRecorders
{
    [AddComponentMenu(SubMenu + "Bool Game Event Recorder")]
    public class BoolGameEventRecorder : TypedGameEventRecorder<bool>
    {
    }
}