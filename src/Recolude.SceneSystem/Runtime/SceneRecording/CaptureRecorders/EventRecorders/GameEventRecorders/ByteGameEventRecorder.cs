﻿using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording.CaptureRecorders.EventRecorders.GameEventRecorders
{
    [AddComponentMenu(SubMenu + "Byte Game Event Recorder")]
    public class ByteGameEventRecorder : TypedGameEventRecorder<byte>
    {
    }
}