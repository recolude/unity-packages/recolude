﻿using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording.CaptureRecorders.EventRecorders.GameEventRecorders
{
    [AddComponentMenu(SubMenu + "Float Game Event Recorder")]
    public class FloatGameEventRecorder : TypedGameEventRecorder<float>
    {
    }
}