﻿using Recolude.Core;
using Recolude.Core.Util;
using Recolude.SceneSystem.Events;
using Recolude.SceneSystem.Properties.Reference;
using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording.CaptureRecorders.EventRecorders.GameEventRecorders
{
    [AddComponentMenu("Recolude/Scene Recording/Events/Game Event Recorder")]
    public class GameEventRecorder : EventRecorder
    {
        [RequiredValue] [SerializeField] private GameEvent gameEvent;

        [SerializeField] private PropertyReference<string> eventNameToEmit;

        private void OnEnable()
        {
            gameEvent.AddListener(OnEventRaise);
        }

        private void OnDisable()
        {
            gameEvent.AddListener(OnEventRaise);
        }

        private void OnEventRaise()
        {
            if (!HasRecorders) return;

            var customEvent = new CustomEvent(eventNameToEmit.Value, new Metadata());
            foreach (var recorder in recorders)
            {
                recorder.Record(customEvent);
            }
        }
    }
}