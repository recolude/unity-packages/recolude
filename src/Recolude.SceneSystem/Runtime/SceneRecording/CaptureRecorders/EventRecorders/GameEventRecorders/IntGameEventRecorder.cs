﻿using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording.CaptureRecorders.EventRecorders.GameEventRecorders
{
    [AddComponentMenu(SubMenu + "Int Game Event Recorder")]
    public class IntGameEventRecorder: TypedGameEventRecorder<int>
    {
        
    }
}