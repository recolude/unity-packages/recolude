﻿using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording.CaptureRecorders.EventRecorders.GameEventRecorders
{
    [AddComponentMenu(SubMenu + "String Game Event Recorder")]
    public class StringGameEventRecorder : TypedGameEventRecorder<string>
    {
    }
}