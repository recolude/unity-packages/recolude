﻿using Recolude.Core;
using Recolude.Core.Util;
using Recolude.SceneSystem.Events;
using Recolude.SceneSystem.Properties.Reference;
using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording.CaptureRecorders.EventRecorders.GameEventRecorders
{
    public abstract class TypedGameEventRecorder<T> : EventRecorder
    {
        protected const string SubMenu = "Recolude/Scene Recording/Events/";

        [RequiredValue] [SerializeField] private GameEvent<T> gameEvent;

        [SerializeField] private PropertyReference<string> eventNameToEmit;

        [SerializeField] private PropertyReference<string> propertyName;

        private void OnEnable()
        {
            gameEvent.AddListener(OnEventRaise);
        }

        private void OnDisable()
        {
            gameEvent.AddListener(OnEventRaise);
        }

        private void OnEventRaise(T val)
        {
            if (!HasRecorders) return;

            var metadata = new Metadata
            {
                [propertyName.Value] = new Property<T>(val)
            };

            var customEvent = new CustomEvent(eventNameToEmit.Value, metadata);
            foreach (var recorder in recorders)
            {
                recorder.Record(customEvent);
            }
        }
    }
}