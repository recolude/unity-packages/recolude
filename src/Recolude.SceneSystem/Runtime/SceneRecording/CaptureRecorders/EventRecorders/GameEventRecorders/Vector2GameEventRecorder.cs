﻿using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording.CaptureRecorders.EventRecorders.GameEventRecorders
{
    [AddComponentMenu(SubMenu + "Vector2 Game Event Recorder")]
    public class Vector2GameEventRecorder: TypedGameEventRecorder<Vector2>
    {
        
    }
}