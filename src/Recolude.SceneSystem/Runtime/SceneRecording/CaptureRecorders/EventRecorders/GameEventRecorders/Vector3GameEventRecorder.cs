﻿using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording.CaptureRecorders.EventRecorders.GameEventRecorders
{
    [AddComponentMenu(SubMenu + "Vector3 Game Event Recorder")]
    public class Vector3GameEventRecorder : TypedGameEventRecorder<Vector3>
    {
    }
}