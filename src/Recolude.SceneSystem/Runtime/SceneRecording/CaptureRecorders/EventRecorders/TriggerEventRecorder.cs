﻿using Recolude.Core;
using Recolude.SceneSystem.SceneRecording.MetadataBuilders;
using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording.CaptureRecorders.EventRecorders
{
    [AddComponentMenu("Recolude/Scene Recording/Events/Trigger Event Recorder")]
    public class TriggerEventRecorder : EventRecorder
    {
        #region Configuration Variables

        [SerializeField] private bool captureOnTriggerEnter;

        [SerializeField] private TriggerMetadataBuilder onTriggerEnter;

        [SerializeField] private bool captureOnTriggerExit;

        [SerializeField] private TriggerMetadataBuilder onTriggerExit;

        #endregion

        private void OnTriggerEnter(Collider col)
        {
            if (!HasRecorders || !captureOnTriggerEnter) return;

            var metadata = onTriggerEnter.Build(col);
            var collisionEvent = new CustomEvent("On Trigger Enter", metadata);
            foreach (var recorder in recorders)
            {
                recorder.Record(collisionEvent);
            }
        }

        void OnTriggerExit(Collider col)
        {
            if (!HasRecorders || !captureOnTriggerExit) return;

            var metadata = onTriggerExit.Build(col);
            var collisionEvent = new CustomEvent("On Trigger Exit", metadata);
            foreach (var recorder in recorders)
            {
                recorder.Record(collisionEvent);
            }
        }
    }
}