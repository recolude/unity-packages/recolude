﻿using Recolude.Core;
using Recolude.Core.Record;
using Recolude.Core.Record.CollectionRecorders;
using Recolude.Core.Record.Watchers;
using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording.CaptureRecorders
{
    [AddComponentMenu("Recolude/Scene Recording/Lifecycle Recorder")]
    public class LifecycleRecorderConfigSlot : CaptureRecorderConfigSlot
    {
        protected override string DefaultCollectionName => CollectionNames.LifeCycle;


        public override ICaptureRecorder<ICapture> BuildRecorder(ITimerManager timerManager)
        {
            var lifeCycleRecorder = new LifeCycleRecorder(CollectionName);
            var lifeCycleWatcher = LifeCycleWatcher.Build(gameObject, timerManager.TimeProvider, lifeCycleRecorder);
            return lifeCycleRecorder;
        }
    }
}