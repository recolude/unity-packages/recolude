﻿using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording.CaptureRecorders
{
    [CreateAssetMenu(fileName = "Position Recorder Config",
        menuName = "Recolude/Scene System/Recording/Capture Config/Position", order = 1)]
    public class PositionRecorderConfig : CaptureRecorderConfig
    {
        [SerializeField] [Tooltip("The amount of distance that must be traveled to consider the object to have moved")]
        private float minimumDelta = 0.01f;


        [SerializeField] [Tooltip("How many times to observe position a second")]
        private int captureRate = 10;

        public static PositionRecorderConfig Build(int captureRate, float minimumDelta)
        {
            var asset = CreateInstance<PositionRecorderConfig>();
            asset.captureRate = captureRate;
            asset.minimumDelta = minimumDelta;
            return asset;
        }

        /// <summary>
        /// How many times to observe position a second.
        /// </summary>
        public int CaptureRate => captureRate;

        /// <summary>
        /// The amount of distance that must be traveled to consider the object to have moved
        /// </summary>
        public float MinimumDelta => minimumDelta;
    }
}