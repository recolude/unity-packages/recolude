﻿using Recolude.Core;
using Recolude.Core.Record;
using Recolude.Core.Record.CollectionRecorders;
using Recolude.Core.Record.Emitters;
using Recolude.Core.Util;
using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording.CaptureRecorders
{
    [AddComponentMenu("Recolude/Scene Recording/Position Recorder")]
    public class PositionRecorderConfigSlot : CaptureRecorderConfigSlot
    {
        [ExtendableScriptableObject] [SerializeField] [RequiredValue]
        private PositionRecorderConfig config;

        protected override string DefaultCollectionName => CollectionNames.Position;

        public override ICaptureRecorder<ICapture> BuildRecorder(ITimerManager timerManager)
        {
            var positionRecorder = new PositionRecorder(CollectionName, config.MinimumDelta);
            var source = new TransformPositionSource(gameObject);
            var positionEmitter = new VectorEmitter(source, positionRecorder);
            timerManager.Build(config.CaptureRate, positionEmitter);
            return positionRecorder;
        }
    }
}