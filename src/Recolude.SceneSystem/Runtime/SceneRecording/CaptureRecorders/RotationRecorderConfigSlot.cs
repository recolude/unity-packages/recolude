﻿using Recolude.Core;
using Recolude.Core.Record;
using Recolude.Core.Record.CollectionRecorders;
using Recolude.Core.Record.Emitters;
using Recolude.Core.Util;
using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording.CaptureRecorders
{
    [AddComponentMenu("Recolude/Scene Recording/Rotation Recorder")]
    public class RotationRecorderConfigSlot : CaptureRecorderConfigSlot
    {
        [ExtendableScriptableObject] [SerializeField] [RequiredValue]
        private RotationRecorderConfig config;

        protected override string DefaultCollectionName => CollectionNames.Rotation;

        public override ICaptureRecorder<ICapture> BuildRecorder(ITimerManager timerManager)
        {
            var rotationRecorder = new RotationRecorder(CollectionName, config.MinimumDelta);
            var source = new TransformRotationSource(gameObject);
            var rotationEmitter = new VectorEmitter(source, rotationRecorder);
            timerManager.Build(config.CaptureRate, rotationEmitter);
            return rotationRecorder;
        }
    }
}