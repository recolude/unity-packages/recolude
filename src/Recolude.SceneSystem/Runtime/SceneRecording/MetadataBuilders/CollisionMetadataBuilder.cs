﻿using System;
using Recolude.Core;
using Recolude.Core.Properties;
using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording.MetadataBuilders
{
    [Serializable]
    public class CollisionMetadataBuilder : MetadataBuilder<Collision>
    {
        [Tooltip("Record the number of contacts for this collision")] [SerializeField]
        private PropertyEntry contactPointCount = new PropertyEntry(false, "Contact Point Count");


        [Tooltip("Record the position of the contact point")] [SerializeField]
        private PropertyEntry contactPoint = new PropertyEntry(false, "Contact Point");


        [Tooltip("Record the total impulse applied to the contact pair to resolve the collision")] [SerializeField]
        private PropertyEntry impulse = new PropertyEntry(false, "Impulse");


        [Tooltip("Record the relative linear velocity of the two colliding objects")] [SerializeField]
        private PropertyEntry relativeVelocity = new PropertyEntry(false, "Relative Velocity");


        [Tooltip("Record the name of what we're colliding with")] [SerializeField]
        private PropertyEntry collidersName = new PropertyEntry(false, "Name");


        [Tooltip("Record the normal of the first contact point of the collision")] [SerializeField]
        private PropertyEntry normal = new PropertyEntry(false, "Normal");

        
        public override void Apply(Metadata metadata, Collision collision)
        {
            if (metadata == null) throw new ArgumentNullException(nameof(metadata));
            if (collision == null) throw new ArgumentNullException(nameof(collision));

            if (contactPointCount.Include)
            {
                metadata[contactPointCount.PropertyName] = new IntProperty(collision.contactCount);
            }

            if (contactPoint.Include)
            {
                metadata[contactPoint.PropertyName] = new Vector3Property(collision.contacts[0].point);
            }

            if (impulse.Include)
            {
                metadata[impulse.PropertyName] = new Vector3Property(collision.impulse);
            }

            if (relativeVelocity.Include)
            {
                metadata[relativeVelocity.PropertyName] = new Vector3Property(collision.relativeVelocity);
            }

            if (collidersName.Include)
            {
                metadata[collidersName.PropertyName] = new Property<string>(collision.gameObject.name);
            }

            if (normal.Include)
            {
                metadata[normal.PropertyName] = new Property<Vector3>(collision.contacts[0].normal);
            }
        }
    }
}