﻿using System;
using Recolude.Core;

namespace Recolude.SceneSystem.SceneRecording.MetadataBuilders
{
    [Serializable]
    public abstract class MetadataBuilder<T>
    {
        public Metadata Build(T val)
        {
            var metadata = new Metadata();
            Apply(metadata, val);
            return metadata;
        }

        public abstract void Apply(Metadata metadata, T val);
    }
}