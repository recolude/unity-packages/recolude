﻿using System;
using Recolude.SceneSystem.Properties.Reference;
using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording.MetadataBuilders
{
    [Serializable]
    public class PropertyEntry
    {
        [SerializeField] private PropertyReference<string> propertyName;

        [SerializeField] private bool include;

        public PropertyEntry(bool defaultInclude, string defaultPropertyName)
        {
            propertyName = new PropertyReference<string>(defaultPropertyName);
            include = defaultInclude;
        }

        public bool Include => include;

        public string PropertyName => propertyName.Value;
    }
}