﻿using System;
using Recolude.Core;
using Recolude.Core.Properties;
using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording.MetadataBuilders
{
    [Serializable]
    public class TriggerMetadataBuilder : MetadataBuilder<Collider>
    {
        [Tooltip("Record the name of what we're colliding with")] [SerializeField]
        private bool collidersName;

        public override void Apply(Metadata metadata, Collider collider)
        {
            if (metadata == null) throw new ArgumentNullException(nameof(metadata));
            if (collider == null) throw new ArgumentNullException(nameof(collider));

            if (collidersName)
            {
                metadata["Name"] = new Property<string>(collider.gameObject.name);
            }
        }
    }
}