﻿using System;
using System.Collections.Generic;
using Recolude.Core;
using Recolude.Core.Record;
using Recolude.SceneSystem.SceneRecording.CaptureRecorders;
using Recolude.Core.Util;
using Recolude.SceneSystem.Properties;
using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording
{
    [AddComponentMenu("Recolude/Scene Recording/Recording Config")]
    public class RecordingConfig : MonoBehaviour
    {
        #region Configuration Variables

        [OverrideName("Recording Name")] [SerializeField]
        private OverridableValue<string> overrideRecordingName;

        [ExtendableScriptableObject] [SerializeField]
        private MetadataContainer metadata;

        [SerializeField] private List<RecordingConfig> childRecorders;

        [SerializeField] private List<CaptureRecorderConfigSlot> captureRecorders;

        [SerializeField] private RecordingSystemRuntimeSet[] recordingSystemRuntimeSets;

        #endregion

        #region Runtime Variables

        private Recorder latestBuiltRecorder;

        #endregion


        public static RecordingConfig Build(
            GameObject go,
            IEnumerable<RecordingConfig> childRecorders,
            IEnumerable<CaptureRecorderConfigSlot> captureRecorders
        )
        {
            var config = go.AddComponent<RecordingConfig>();
            config.childRecorders = new List<RecordingConfig>(childRecorders);
            config.captureRecorders = new List<CaptureRecorderConfigSlot>(captureRecorders);
            return config;
        }

        private void Start()
        {
            if (recordingSystemRuntimeSets == null)
            {
                return;
            }

            foreach (var set in recordingSystemRuntimeSets)
            {
                if (set == null)
                {
                    continue;
                }

                foreach (var system in set)
                {
                    system.AddRecorder(this);
                }
            }
        }


        private void OnEnable()
        {
            if (recordingSystemRuntimeSets == null)
            {
                return;
            }

            foreach (var set in recordingSystemRuntimeSets)
            {
                if (set == null)
                {
                    continue;
                }

                foreach (var system in set)
                {
                    system.AddRecorder(this);
                }

                set.ItemAdded += SetOnItemAdded;
            }
        }

        private void OnDisable()
        {
            if (recordingSystemRuntimeSets == null)
            {
                return;
            }

            foreach (var set in recordingSystemRuntimeSets)
            {
                if (set == null)
                {
                    continue;
                }

                set.ItemAdded -= SetOnItemAdded;
            }
        }

        public bool Recording => latestBuiltRecorder != null && latestBuiltRecorder.CurrentlyRecording();


        private void SetOnItemAdded(object sender, RecordingSystem system)
        {
            system.AddRecorder(this);
        }

        public void AddChildRecorder(RecordingConfig recorderConfig)
        {
            if (recorderConfig == null) throw new ArgumentNullException(nameof(recorderConfig));
            childRecorders.Add(recorderConfig);
            
            if (latestBuiltRecorder != null)
            {
                var childRecorder = recorderConfig.BuildRecorder(latestBuiltRecorder.TimerManager);
                latestBuiltRecorder.Register(childRecorder);
                
                if (latestBuiltRecorder.CurrentlyRecording())
                {
                    childRecorder.Start();
                }
                else if (latestBuiltRecorder.CurrentlyPaused())
                {
                    childRecorder.Start();
                    childRecorder.Pause();
                }
            }
        }

        /// <summary>
        /// Builds a recorder that tracks the passed in game object instead of
        /// the game object this Recording Config is attached too.
        /// </summary>
        /// <param name="go">Game Object to record.</param>
        /// <param name="timerManager">Time manager to use for building captures.</param>
        /// <returns>Recorder configured to record the passed in game object.</returns>
        public Recorder BuildRecorder(GameObject go, ITimerManager timerManager)
        {
            var childRecorderInstances = new List<Recorder>(childRecorders.Count);
            foreach (var rec in childRecorders)
            {
                if (rec == null)
                {
                    continue;
                }

                childRecorderInstances.Add(rec.BuildRecorder(timerManager));
            }

            var captureRecorderInstances = new List<ICaptureRecorder<ICapture>>(captureRecorders.Count);
            foreach (var rec in captureRecorders)
            {
                if (rec == null)
                {
                    continue;
                }

                captureRecorderInstances.Add(rec.BuildRecorder(timerManager));
            }

            Metadata metadataInstance = null;
            if (metadata != null)
            {
                metadataInstance = metadata.Metadata;
            }

            var info = new RecorderInfo(
                go.GetInstanceID().ToString(),
                overrideRecordingName.GetValue(go.name),
                metadataInstance
            );
            latestBuiltRecorder = new Recorder(timerManager, info, childRecorderInstances, captureRecorderInstances);
            return latestBuiltRecorder;
        }

        public Recorder BuildRecorder(ITimerManager timerManager)
        {
            return BuildRecorder(gameObject, timerManager);
        }

        private void AddCaptureRecorder<T>() where T : CaptureRecorderConfigSlot
        {
            if (!gameObject.TryGetComponent<T>(out var component))
            {
                component = gameObject.AddComponent<T>();
            }

            if (!captureRecorders.Contains(component))
            {
                captureRecorders.Add(component);
            }
        }

        [ContextMenu("Add Default Capture Recorders")]
        private void AddDefaultCaptureRecorders()
        {
            AddCaptureRecorder<PositionRecorderConfigSlot>();
            AddCaptureRecorder<RotationRecorderConfigSlot>();
            AddCaptureRecorder<LifecycleRecorderConfigSlot>();
            AddCaptureRecorder<CustomEventRecorderConfigSlot>();
        }
    }
}