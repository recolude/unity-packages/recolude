﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Recolude.Core;
using Recolude.Core.Record;
using Recolude.Core.Time;
using Recolude.Core.Util;
using Recolude.SceneSystem.Clock;
using Recolude.SceneSystem.Properties;
using UnityEngine;
using UnityEngine.Events;
using Recording = Recolude.Core.Recording;

namespace Recolude.SceneSystem.SceneRecording
{
    [AddComponentMenu("Recolude/Scene Recording/Recording System")]
    public class RecordingSystem : MonoBehaviour
    {
        #region Configuration Variables

        [RequiredValue] [SerializeField] private string recordingName;

        [ExtendableScriptableObject] [SerializeField]
        private MetadataContainer metadata;

        [RequiredValue] [SerializeField] private ClockBehavior clock;

        [SerializeField] private List<RecordingConfig> childRecorders;

        [SerializeField] [ItemCanBeNull] private List<CaptureRecorderConfigSlot> captureRecorders;

        [SerializeField] private UnityEvent<Recording> onRecordingComplete;

        [Tooltip("Runtime set to register this system to")] [SerializeField]
        private RecordingSystemRuntimeSet runtimeSet;

        #endregion

        #region Runtime Variables

        private Recorder recorder;

        #endregion

        public Recorder StartRecording()
        {
            recorder = BuildRecorder();
            recorder.Start();
            return recorder;
        }

        public Recording StopRecording()
        {
            var recording = recorder.Finish();
            onRecordingComplete?.Invoke(recording);
            return recording;
        }

        public bool Recording => recorder != null && recorder.CurrentlyRecording();

        private ITimeProvider TimeProvider()
        {
            if (clock == null)
            {
                return new UnityTimeProvider();
            }

            return clock;
        }

        private void OnEnable()
        {
            if (runtimeSet != null)
            {
                runtimeSet.Add(this);
            }
        }

        private void OnDisable()
        {
            if (runtimeSet != null)
            {
                runtimeSet.Remove(this);
            }
        }

        private TimerManager TimerManager()
        {
            return new TimerManager(TimeProvider());
        }

        public void SetClock(ClockBehavior clock)
        {
            this.clock = clock;
        }

        public void SetRecordingName(string name)
        {
            recordingName = name;
        }

        public void RegisterOnRecodingCompleteCallback(UnityAction<Recording> action)
        {
            if (onRecordingComplete == null)
            {
                onRecordingComplete = new UnityEvent<Recording>();
            }

            onRecordingComplete.AddListener(action);
        }

        public void AddRecorder(RecordingConfig recorderConfig)
        {
            if (recorderConfig == null) throw new ArgumentNullException(nameof(recorderConfig));
            childRecorders.Add(recorderConfig);

            if (recorder != null)
            {
                var childRecorder = recorderConfig.BuildRecorder(TimerManager());
                recorder.Register(childRecorder);

                if (recorder.CurrentlyRecording())
                {
                    childRecorder.Start();
                }
                else if (recorder.CurrentlyPaused())
                {
                    childRecorder.Start();
                    childRecorder.Pause();
                }
            }
        }

        public void AddCaptureRecorder(CaptureRecorderConfigSlot captureRecorder)
        {
            if (captureRecorders == null)
            {
                captureRecorders = new List<CaptureRecorderConfigSlot>();
            }

            captureRecorders.Add(captureRecorder);
        }

        public Recorder BuildRecorder()
        {
            var timeManager = TimerManager();
            var childRecorderInstances = new List<Recorder>(childRecorders.Count);
            for (int i = 0; i < childRecorders.Count; i++)
            {
                if (childRecorders[i] == null)
                {
                    continue;
                }

                childRecorderInstances.Add(childRecorders[i].BuildRecorder(timeManager));
            }

            var captureRecorderInstances = new ICaptureRecorder<ICapture>[captureRecorders.Count];
            for (int i = 0; i < captureRecorders.Count; i++)
            {
                if (captureRecorders[i] == null)
                {
                    continue;
                }

                captureRecorderInstances[i] = captureRecorders[i].BuildRecorder(timeManager);
            }

            Metadata metadataInstance = null;
            if (metadata != null)
            {
                metadataInstance = metadata.Metadata;
            }

            var info = new RecorderInfo(gameObject.GetInstanceID().ToString(), recordingName, metadataInstance);
            var rec = new Recorder(timeManager, info, childRecorderInstances, captureRecorderInstances);
            return rec;
        }
    }
}