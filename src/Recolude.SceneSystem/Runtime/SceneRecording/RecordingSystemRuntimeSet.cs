﻿using Recolude.SceneSystem.Sets;
using UnityEngine;

namespace Recolude.SceneSystem.SceneRecording
{
    [CreateAssetMenu(
        fileName = "Recording System Runtime Set",
        menuName = "Recolude/Scene System/Recording/Recording System Runtime Set",
        order = 1
    )]
    public class RecordingSystemRuntimeSet : RuntimeSet<RecordingSystem>
    {
        public static RecordingSystemRuntimeSet Build()
        {
            var asset = CreateInstance<RecordingSystemRuntimeSet>();
            return asset;
        }
        
    }
}