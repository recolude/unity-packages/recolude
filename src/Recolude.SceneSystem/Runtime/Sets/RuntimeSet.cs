﻿using System;
using System.Collections;
using System.Collections.Generic;
using Recolude.SceneSystem.Notes;
using UnityEngine;

namespace Recolude.SceneSystem.Sets
{
    public abstract class RuntimeSet<T> : ScriptableObject, IEnumerable<T>
    {
        [SerializeField] private NotePad notePad;

        [SerializeField] private List<T> items;

        protected List<T> Items => items ??= new List<T>();
        
        public event EventHandler<T> ItemAdded;

        public event EventHandler<T> ItemRemoved;
        
        public void Add(T thing)
        {
            if (!Items.Contains(thing))
            {
                items.Add(thing);
                ItemAdded?.Invoke(this, thing);
            }
        }

        public void Remove(T thing)
        {
            if (Items.Contains(thing))
            {
                items.Remove(thing);
                ItemRemoved?.Invoke(this, thing);
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        
    }
}