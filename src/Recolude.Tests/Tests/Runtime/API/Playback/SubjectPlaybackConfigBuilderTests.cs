using NUnit.Framework;
using UnityEngine;
using Recolude.API.Playback;
using Recolude.Core;
using Recolude.Core.Playback;
using Recolude.Core.Record;

namespace Tests.Playback
{
    public class SubjectPlaybackConfigBuilderTests
    {

        [Test]
        public void ToMetadata()
        {
            var metadata = new SubjectPlaybackConfigBuilder()
                .Color(0, 1, 0)
                .Geometry(SubjectRepresentation.Head)
                .Scale(Vector3.one * 0.1f)
                .ToMetadata();

            Assert.AreEqual(3, metadata.Count);
            Assert.AreEqual("vr-head", metadata["recolude-geom"].ToString());
            Assert.AreEqual("0.1, 0.1, 0.1", metadata["recolude-scale"].ToString());
            Assert.AreEqual("#00FF00", metadata["recolude-color"].ToString());
        }


        [Test]
        public void ApplyToSubjectRecorder()
        {
            var myRecorder = new Recorder(new TimerManager(), new RecorderInfo(), null, null);
            var subject = myRecorder.Register(new GameObject());
            new SubjectPlaybackConfigBuilder()
                .Color(0, 1, 0)
                .Geometry(SubjectRepresentation.Head)
                .Scale(Vector3.one * 0.1f)
                .Apply(subject);

            Assert.AreEqual("vr-head", subject.GetMetaData("recolude-geom").ToString());
            Assert.AreEqual("0.1, 0.1, 0.1", subject.GetMetaData("recolude-scale").ToString());
            Assert.AreEqual("#00FF00", subject.GetMetaData("recolude-color").ToString());
        }
    }
}