using System.Collections;
using System.Text;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System.IO;
using NSubstitute;

using Recolude.Core;
using Recolude.Core.IO;
using Recolude.API.RecordingStorage;
using Recolude.API.RecordingStorage.FailSafe;

namespace Tests.RecordingStorage
{
    public class RecordingStorageFailSafeTests
    {
        public string ReadAll(Stream sourceStream)
        {
            using (var memoryStream = new MemoryStream())
            {
                sourceStream.CopyTo(memoryStream);
                return Encoding.UTF8.GetString(memoryStream.ToArray());
            }
        }

        [UnityTest]
        public IEnumerator FailedDeletionLogged()
        {
            // *************************** ARRANGE ****************************
            var binaryStorage = new InMemoryBinaryStorage();
            var planAStorage = Substitute.For<IRecordingStorage>();

            var deleteReq = Substitute.For<IDeleteRecordingOperation>();
            deleteReq.Error().Returns("something bad");
            planAStorage.DeleteRecording("example").Returns(deleteReq);

            var recordingStorageFailSafe = new RecordingStorageFailSafe(planAStorage, binaryStorage);

            // ***************************** ACT ******************************
            var deleteReqFailsafe = recordingStorageFailSafe.DeleteRecording("example");
            yield return deleteReqFailsafe.Run();

            // **************************** ASSERT ****************************
            Assert.IsTrue(binaryStorage.Exists("failsafe.json"));
            Assert.AreEqual("{\"version\":1,\"failedDeletions\":[{\"recordingID\":\"example\",\"failedAttempts\":1}],\"failedSaves\":[]}", ReadAll(binaryStorage.Read("failsafe.json")));
        }

        [UnityTest]
        public IEnumerator FailedDeletionLogged_IncrementsFailedAttemptCount()
        {
            // *************************** ARRANGE ****************************
            var binaryStorage = new InMemoryBinaryStorage();
            binaryStorage.Write(
                new MemoryStream(
                    Encoding.UTF8.GetBytes("{\"version\":1,\"failedDeletions\":[{\"recordingID\":\"example\",\"failedAttempts\":1}],\"failedSaves\":[]}")
                ),
                "failsafe.json"
            );
            var planAStorage = Substitute.For<IRecordingStorage>();

            var deleteReq = Substitute.For<IDeleteRecordingOperation>();
            deleteReq.Error().Returns("something bad");
            planAStorage.DeleteRecording("example").Returns(deleteReq);

            var recordingStorageFailSafe = new RecordingStorageFailSafe(planAStorage, binaryStorage);

            // ***************************** ACT ******************************
            var deleteReqFailsafe = recordingStorageFailSafe.DeleteRecording("example");
            yield return deleteReqFailsafe.Run();

            // **************************** ASSERT ****************************
            Assert.IsTrue(binaryStorage.Exists("failsafe.json"));
            Assert.AreEqual("{\"version\":1,\"failedDeletions\":[{\"recordingID\":\"example\",\"failedAttempts\":2}],\"failedSaves\":[]}", ReadAll(binaryStorage.Read("failsafe.json")));
        }

        [UnityTest]
        public IEnumerator FailedDeletionLogged_AppendFailedAttempts()
        {
            // *************************** ARRANGE ****************************
            var binaryStorage = new InMemoryBinaryStorage();
            binaryStorage.Write(
                new MemoryStream(
                    Encoding.UTF8.GetBytes("{\"version\":1,\"failedDeletions\":[{\"recordingID\":\"example\",\"failedAttempts\":1}],\"failedSaves\":[]}")
                ),
                "failsafe.json"
            );
            var planAStorage = Substitute.For<IRecordingStorage>();

            var deleteReq = Substitute.For<IDeleteRecordingOperation>();
            deleteReq.Error().Returns("something bad");
            planAStorage.DeleteRecording("another example").Returns(deleteReq);

            var recordingStorageFailSafe = new RecordingStorageFailSafe(planAStorage, binaryStorage);

            // ***************************** ACT ******************************
            var deleteReqFailsafe = recordingStorageFailSafe.DeleteRecording("another example");
            yield return deleteReqFailsafe.Run();

            // **************************** ASSERT ****************************
            Assert.IsTrue(binaryStorage.Exists("failsafe.json"));
            Assert.AreEqual(
                "{\"version\":1,\"failedDeletions\":[{\"recordingID\":\"example\",\"failedAttempts\":1},{\"recordingID\":\"another example\",\"failedAttempts\":1}],\"failedSaves\":[]}",
                ReadAll(binaryStorage.Read("failsafe.json"))
            );
        }

        [UnityTest]
        public IEnumerator FailedSaveLogged()
        {
            // *************************** ARRANGE ****************************
            var recording = new Recording("", "uhuhuh", null, null, new Metadata());
            var planAStorage = Substitute.For<IRecordingStorage>();
            var saveReq = Substitute.For<ISaveRecordingOperation>();
            saveReq.Error().Returns("something bad");
            planAStorage.SaveRecording(recording).Returns(saveReq);

            var binaryStorage = new InMemoryBinaryStorage();
            var recordingStorageFailSafe = new RecordingStorageFailSafe(planAStorage, binaryStorage);

            // ***************************** ACT ******************************
            var saveReqFailsafe = recordingStorageFailSafe.SaveRecording(recording);
            yield return saveReqFailsafe.Run();

            // **************************** ASSERT ****************************
            Assert.IsTrue(binaryStorage.Exists("failsafe.json"));
            Assert.IsTrue(binaryStorage.Exists("1"));
            Assert.AreEqual("{\"version\":1,\"failedDeletions\":[],\"failedSaves\":[{\"failedAttempts\":1,\"recEntry\":1}]}", ReadAll(binaryStorage.Read("failsafe.json")));

            var recBuff = binaryStorage.Read("1");
            Assert.NotNull(recBuff);
            Assert.IsTrue(recBuff.Length > 0);
            using (var rapReader = new RAPReader(recBuff))
            {
                var recsReadBack = rapReader.Recording();
                Assert.NotNull(recsReadBack);
                Assert.AreEqual("uhuhuh", recsReadBack.Name);
            }
        }

        [UnityTest]
        public IEnumerator FailedSaveLogged_AppendsToFailedSaves()
        {
            // *************************** ARRANGE ****************************
            var recording = new Recording("", "uhuhuh", null, null, new Metadata());
            var planAStorage = Substitute.For<IRecordingStorage>();
            var saveReq = Substitute.For<ISaveRecordingOperation>();
            saveReq.Error().Returns("something bad");
            planAStorage.SaveRecording(recording).Returns(saveReq);

            var binaryStorage = new InMemoryBinaryStorage();
            binaryStorage.Write(
               new MemoryStream(
                   Encoding.UTF8.GetBytes("{\"version\":1,\"failedDeletions\":[],\"failedSaves\":[{\"failedAttempts\":1,\"recEntry\":1}]}")
               ),
               "failsafe.json"
           );
            var recordingStorageFailSafe = new RecordingStorageFailSafe(planAStorage, binaryStorage);

            // ***************************** ACT ******************************
            var deleteReqFailsafe = recordingStorageFailSafe.SaveRecording(recording);
            yield return deleteReqFailsafe.Run();

            // **************************** ASSERT ****************************
            Assert.IsTrue(binaryStorage.Exists("failsafe.json"));
            Assert.AreEqual("{\"version\":1,\"failedDeletions\":[],\"failedSaves\":[{\"failedAttempts\":1,\"recEntry\":1},{\"failedAttempts\":1,\"recEntry\":2}]}", ReadAll(binaryStorage.Read("failsafe.json")));

            Assert.IsTrue(binaryStorage.Exists("2"));
            var recBuff = binaryStorage.Read("2");
            Assert.NotNull(recBuff);
            Assert.IsTrue(recBuff.Length > 0);
            using (var rapReader = new RAPReader(recBuff))
            {
                var recsReadBack = rapReader.Recording();
                Assert.NotNull(recsReadBack);
                Assert.AreEqual("uhuhuh", recsReadBack.Name);
            }
        }

        [UnityTest]
        public IEnumerator RetryFailedSaves_RetriesAndIncrementsFailureOnFailure()
        {
            // *************************** ARRANGE ****************************
            var recording = new Recording("", "uhuhuh", null, null, new Metadata());
            var planAStorage = Substitute.For<IRecordingStorage>();
            var saveReq = Substitute.For<ISaveRecordingOperation>();
            saveReq.Error().Returns("something bad");
            planAStorage.SaveRecording(default).ReturnsForAnyArgs(saveReq);

            var binaryStorage = new InMemoryBinaryStorage();
            using (var mem = new MemoryStream())
            {
                using (var rapWriter = new RAPWriter(mem, true))
                {
                    rapWriter.Write(recording);
                }
                mem.Seek(0, SeekOrigin.Begin);
                binaryStorage.Write(mem, "1");
            }
            binaryStorage.Write(
                new MemoryStream(
                    Encoding.UTF8.GetBytes("{\"version\":1,\"failedDeletions\":[],\"failedSaves\":[{\"failedAttempts\":1,\"recEntry\":1}]}")
                ),
                "failsafe.json"
            );
            var recordingStorageFailSafe = new RecordingStorageFailSafe(planAStorage, binaryStorage);

            // ***************************** ACT ******************************
            yield return recordingStorageFailSafe.RetryFailedSaves();

            // **************************** ASSERT ****************************
            Assert.IsTrue(binaryStorage.Exists("failsafe.json"));
            Assert.AreEqual("{\"version\":1,\"failedDeletions\":[],\"failedSaves\":[{\"failedAttempts\":2,\"recEntry\":1}]}", ReadAll(binaryStorage.Read("failsafe.json")));
            Assert.IsTrue(binaryStorage.Exists("1"));
        }

        [UnityTest]
        public IEnumerator RetryFailedSaves_RemovesCachedRecordingOnSuccess()
        {
            // *************************** ARRANGE ****************************
            var recording = new Recording("", "uhuhuh", null, null, new Metadata());
            var planAStorage = Substitute.For<IRecordingStorage>();
            var saveReq = Substitute.For<ISaveRecordingOperation>();
            saveReq.Error().Returns(x => { return null; });
            planAStorage.SaveRecording(recording).Returns(saveReq);

            var binaryStorage = new InMemoryBinaryStorage();
            using (var mem = new MemoryStream())
            {
                using (var rapWriter = new RAPWriter(mem, true))
                {
                    rapWriter.Write(recording);
                }
                mem.Seek(0, SeekOrigin.Begin);
                binaryStorage.Write(mem, "1");
            }
            binaryStorage.Write(
                new MemoryStream(
                    Encoding.UTF8.GetBytes("{\"version\":1,\"failedDeletions\":[],\"failedSaves\":[{\"failedAttempts\":1,\"recEntry\":1}]}")
                ),
                "failsafe.json"
            );
            var recordingStorageFailSafe = new RecordingStorageFailSafe(planAStorage, binaryStorage);

            // ***************************** ACT ******************************
            yield return recordingStorageFailSafe.RetryFailedSaves();

            // **************************** ASSERT ****************************
            Assert.IsFalse(binaryStorage.Exists("failsafe.json"));
            Assert.IsFalse(binaryStorage.Exists("1"));
        }

        [UnityTest]
        public IEnumerator RetryFailedSaves_KeepsFailsafeFileIfFailedDeletionsExist()
        {
            // *************************** ARRANGE ****************************
            var recording = new Recording("", "uhuhuh", null, null, new Metadata());
            var planAStorage = Substitute.For<IRecordingStorage>();
            var saveReq = Substitute.For<ISaveRecordingOperation>();
            saveReq.Error().Returns(x => { return null; });
            planAStorage.SaveRecording(recording).Returns(saveReq);

            var binaryStorage = new InMemoryBinaryStorage();
            using (var mem = new MemoryStream())
            {
                using (var rapWriter = new RAPWriter(mem, true))
                {
                    rapWriter.Write(recording);
                }
                mem.Seek(0, SeekOrigin.Begin);
                binaryStorage.Write(mem, "1");
            }
            binaryStorage.Write(
                new MemoryStream(
                    Encoding.UTF8.GetBytes("{\"version\":1,\"failedDeletions\":[{\"recordingID\":\"example\",\"failedAttempts\":1}],\"failedSaves\":[{\"failedAttempts\":1,\"recEntry\":1}]}")
                ),
                "failsafe.json"
            );
            var recordingStorageFailSafe = new RecordingStorageFailSafe(planAStorage, binaryStorage);

            // ***************************** ACT ******************************
            yield return recordingStorageFailSafe.RetryFailedSaves();

            // **************************** ASSERT ****************************
            Assert.IsTrue(binaryStorage.Exists("failsafe.json"));
            Assert.IsFalse(binaryStorage.Exists("1"));
            Assert.AreEqual("{\"version\":1,\"failedDeletions\":[{\"recordingID\":\"example\",\"failedAttempts\":1}],\"failedSaves\":[]}", ReadAll(binaryStorage.Read("failsafe.json")));
        }

        [Test]
        public void Reset_ClearsEverything()
        {
            // *************************** ARRANGE ****************************
            var recording = new Recording("", "uhuhuh", null, null, new Metadata());

            var binaryStorage = new InMemoryBinaryStorage();
            using (var mem = new MemoryStream())
            {
                using (var rapWriter = new RAPWriter(mem, true))
                {
                    rapWriter.Write(recording);
                }
                mem.Seek(0, SeekOrigin.Begin);
                binaryStorage.Write(mem, "1");
            }
            binaryStorage.Write(
                new MemoryStream(
                    Encoding.UTF8.GetBytes("{\"version\":1,\"failedDeletions\":[],\"failedSaves\":[{\"failedAttempts\":1,\"recEntry\":1}]}")
                ),
                "failsafe.json"
            );
            var recordingStorageFailSafe = new RecordingStorageFailSafe(null, binaryStorage);

            // ***************************** ACT ******************************
            recordingStorageFailSafe.Reset();

            // **************************** ASSERT ****************************
            Assert.IsFalse(binaryStorage.Exists("failsafe.json"));
            Assert.IsFalse(binaryStorage.Exists("1"));
        }

        [UnityTest]
        public IEnumerator RetryFailedDeletions_RetriesAndIncrementsFailureOnFailure()
        {
            // *************************** ARRANGE ****************************
            var planAStorage = Substitute.For<IRecordingStorage>();
            var delReq = Substitute.For<IDeleteRecordingOperation>();
            delReq.Error().Returns("something bad");
            planAStorage.DeleteRecording("example").Returns(delReq);

            var binaryStorage = new InMemoryBinaryStorage();
            binaryStorage.Write(
                new MemoryStream(
                    Encoding.UTF8.GetBytes("{\"version\":1,\"failedDeletions\":[{\"recordingID\":\"example\",\"failedAttempts\":1}],\"failedSaves\":[]}")
                ),
                "failsafe.json"
            );
            var recordingStorageFailSafe = new RecordingStorageFailSafe(planAStorage, binaryStorage);

            // ***************************** ACT ******************************
            yield return recordingStorageFailSafe.RetryFailedDeletions();

            // **************************** ASSERT ****************************
            Assert.IsTrue(binaryStorage.Exists("failsafe.json"));
            Assert.AreEqual("{\"version\":1,\"failedDeletions\":[{\"recordingID\":\"example\",\"failedAttempts\":2}],\"failedSaves\":[]}", ReadAll(binaryStorage.Read("failsafe.json")));
        }

        [UnityTest]
        public IEnumerator RetryFailedDeletions_ClearsOnSuccess()
        {
            // *************************** ARRANGE ****************************
            var planAStorage = Substitute.For<IRecordingStorage>();
            var delReq = Substitute.For<IDeleteRecordingOperation>();
            delReq.Error().Returns(x => { return null; });
            planAStorage.DeleteRecording("example").Returns(delReq);

            var binaryStorage = new InMemoryBinaryStorage();
            binaryStorage.Write(
                new MemoryStream(
                    Encoding.UTF8.GetBytes("{\"version\":1,\"failedDeletions\":[{\"recordingID\":\"example\",\"failedAttempts\":1}],\"failedSaves\":[]}")
                ),
                "failsafe.json"
            );
            var recordingStorageFailSafe = new RecordingStorageFailSafe(planAStorage, binaryStorage);

            // ***************************** ACT ******************************
            yield return recordingStorageFailSafe.RetryFailedDeletions();

            // **************************** ASSERT ****************************
            Assert.IsTrue(binaryStorage.Exists("failsafe.json"));
            Assert.AreEqual("{\"version\":1,\"failedDeletions\":[],\"failedSaves\":[]}", ReadAll(binaryStorage.Read("failsafe.json")));
        }

    }

}