
using NUnit.Framework;
using UnityEngine;


namespace Tests
{
    public class AssertUtils
    {

        public static void InDelta(float a, float b, float delta)
        {
            var dif = Mathf.Abs(a - b);
            Assert.True(dif <= delta, $"{a} and {b} have a difference of {dif}, which is greater than {delta}");
        }

    }

}