using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System.IO;

using Recolude.Core;

namespace Tests
{
    public class CustomEventCaptureTest
    {

        [Test]
        public void CustomDictionaryEventCapture_SingleValue()
        {
            // *************************** ARRANGE ***************************
            var e = new CustomEventCapture(69, "some name", "some value");

            // ***************************** ACT *****************************
            var contents = e.Contents;

            // **************************** ASSERT ****************************
            Assert.NotNull(contents);
            Assert.AreEqual(1, contents.Count);
            Assert.True(contents.ContainsKey("value"));
            Assert.AreEqual("some value", contents.AsString("value"));
        }

        [Test]
        public void CustomDictionaryEventCapture_MultipleValues()
        {
            // *************************** ARRANGE ***************************
            var e = new CustomEventCapture(69, "some name", new Dictionary<string, string>() {
                {"a", "b"},
                {"c", "d"},
            });

            // ***************************** ACT *****************************
            var contents = e.Contents;

            // **************************** ASSERT ****************************
            Assert.NotNull(contents);
            Assert.AreEqual(2, contents.Count);
            Assert.True(contents.ContainsKey("a"));
            Assert.True(contents.ContainsKey("c"));
            Assert.AreEqual("b", contents.AsString("a"));
            Assert.AreEqual("d", contents.AsString("c"));
        }

        [Test]
        public void CustomDictionaryEventCapture_SimpleSurvivesTimeChange()
        {
            // *************************** ARRANGE ***************************
            var e = new CustomEventCapture(69, "x", "y");

            // ***************************** ACT *****************************
            var result = e.SetTime(70) as CustomEventCapture;

            // **************************** ASSERT ****************************
            Assert.NotNull(result);
            Assert.AreEqual("x", result.Name);
            Assert.AreEqual(1, result.Contents.Count);
            Assert.True(result.Contents.ContainsKey("value"));
            Assert.AreEqual("y", result.Contents.AsString("value"));
        }

        [Test]
        public void CustomDictionaryEventCapture_SurvivesTimeChange()
        {
            // *************************** ARRANGE ***************************
            var e = new CustomEventCapture(69, "some name", new Dictionary<string, string>() {
                {"a", "b"},
                {"c", "d"},
            });

            // ***************************** ACT *****************************
            var result = e.SetTime(70) as CustomEventCapture;

            // **************************** ASSERT ****************************
            Assert.NotNull(result);
            Assert.AreEqual("some name", result.Name);
            Assert.AreEqual(2, result.Contents.Count);
            Assert.True(result.Contents.ContainsKey("a"));
            Assert.True(result.Contents.ContainsKey("c"));
            Assert.AreEqual("b", result.Contents.AsString("a"));
            Assert.AreEqual("d", result.Contents.AsString("c"));
        }


        [Test]
        public void CustomDictionaryEventCapture_JSON()
        {
            // *************************** ARRANGE ***************************
            var dict = new Dictionary<string, string>(){
                { "a",  "B" },
                { "c",  "d" }
            };
            var answer = @"[{""Key"": ""a"", ""Value"": ""B""}, {""Key"": ""c"", ""Value"": ""d""}]";

            // ***************************** ACT *****************************
            var jsonStr = new CustomEventCapture(69, "some name", dict).ToJSON();

            // **************************** ASSERT ****************************
            Assert.AreEqual(@"{ ""Time"": 69, ""Name"": ""some name"", ""Contents"": " + answer + "}", jsonStr);
        }

        [Test]
        public void CustomDictionaryEventCapture_JSONSinglyEntry()
        {
            // *************************** ARRANGE ***************************
            var dict = new Dictionary<string, string>(){
                { "abc",  "xyz" },
            };
            var answer = @"[{""Key"": ""abc"", ""Value"": ""xyz""}]";

            // ***************************** ACT *****************************
            var jsonStr = new CustomEventCapture(75, "single entry test", dict).ToJSON();

            // **************************** ASSERT ****************************
            Assert.AreEqual(@"{ ""Time"": 75, ""Name"": ""single entry test"", ""Contents"": " + answer + "}", jsonStr);
        }


        [Test]
        public void CustomDictionaryEventCapture_CSVSingleEntry()
        {
            // *************************** ARRANGE ***************************
            var dict = new Dictionary<string, string>(){
                { "abc",  "xyz" },
            };

            // ***************************** ACT *****************************
            var csvStr = new CustomEventCapture(75, "csv single entry", dict).ToCSV();

            // **************************** ASSERT ****************************
            Assert.AreEqual("75, abc, xyz\n", csvStr);
        }

        [Test]
        public void CustomDictionaryEventCapture_CSV()
        {
            // *************************** ARRANGE ***************************
            var dict = new Dictionary<string, string>(){
                { "a",  "B" },
                { "c",  "d" }
            };

            // ***************************** ACT *****************************
            var csvStr = new CustomEventCapture(75, "csv multi entry", dict).ToCSV();

            // **************************** ASSERT ****************************
            Assert.AreEqual("75, a, B\n75, c, d\n", csvStr);
        }

    }

}