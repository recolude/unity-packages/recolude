using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System.IO;

using Recolude.Core.IO;

namespace Tests.IO
{
    public class CSVPageTests
    {

        [Test]
        public void GetName()
        {
            // *************************** ARRANGE ***************************
            var page = new CSVPage("GetName", new string[,]{
                { "x", "y" },
                { "a", "b" },
            });

            // ***************************** ACT *****************************
            var name = page.GetName();

            // **************************** ASSERT ****************************
            Assert.AreEqual("GetName", name);
        }

        [Test]
        public void ToString_Basic()
        {
            // *************************** ARRANGE ***************************
            var page = new CSVPage("ToString_Basic", new string[,]{
                { "x", "y" },
                { "a", "b" },
            });

            // ***************************** ACT *****************************
            var results = page.ToString();

            // **************************** ASSERT ****************************
            Assert.AreEqual("\"x\",\"y\"\n\"a\",\"b\"", results);
        }

        [Test]
        public void ToString_SingleEntry()
        {
            // *************************** ARRANGE ***************************
            var page = new CSVPage("test", new string[,]{
                { "x", "y" },
            });

            // ***************************** ACT *****************************
            var results = page.ToString();

            // **************************** ASSERT ****************************
            Assert.AreEqual("\"x\",\"y\"", results);
        }

        [Test]
        public void ToString_EscapesCommas()
        {
            // *************************** ARRANGE ***************************
            var page = new CSVPage("test", new string[,]{
                { "x,", "y" },
                { "a", ",b," },
            });

            // ***************************** ACT *****************************
            var results = page.ToString();

            // **************************** ASSERT ****************************
            Assert.AreEqual("\"x,\",\"y\"\n\"a\",\",b,\"", results);
        }

        [Test]
        public void ToString_EscapesQuotes()
        {
            // *************************** ARRANGE ***************************
            var page = new CSVPage("test", new string[,]{
                { "x", "\"y\"" },
                { "a\"\"", "b," },
            });

            // ***************************** ACT *****************************
            var results = page.ToString();

            // **************************** ASSERT ****************************
            Assert.AreEqual("\"x\",\"\"\"y\"\"\"\n\"a\"\"\"\"\",\"b,\"", results);
        }

        [Test]
        public void ToString_EscapesMixed()
        {
            // *************************** ARRANGE ***************************
            var page = new CSVPage("test", new string[,]{
                { ",,,x,", "\"y, z\"" },
                { "a", "\",\": 6" },
            });

            // ***************************** ACT *****************************
            var results = page.ToString();

            // **************************** ASSERT ****************************
            Assert.AreEqual("\",,,x,\",\"\"\"y, z\"\"\"\n\"a\",\"\"\",\"\": 6\"", results);
        }

        [Test]
        public void ToString_ReturnsEmptyEntries()
        {
            // *************************** ARRANGE ***************************
            var page = new CSVPage("test", null);

            // ***************************** ACT *****************************
            var results = page.ToString();

            // **************************** ASSERT ****************************
            Assert.AreEqual("", results);
        }

        [Test]
        public void ToString_HandlesSpecificNullEntries()
        {
            // *************************** ARRANGE ***************************
            var page = new CSVPage("test", new string[,]{
                { "a", null },
            });

            // ***************************** ACT *****************************
            var results = page.ToString();

            // **************************** ASSERT ****************************
            Assert.AreEqual("\"a\",\"\"", results);
        }


        [Test]
        public void Combine_Basic()
        {
            // *************************** ARRANGE ***************************
            var page = new CSVPage("test 1", new string[,]{
                { "a", "b" },
                { "c", "d" },
                { "e", "f" },
            });

            var page2 = new CSVPage("test 2", new string[,]{
                { "a", "b" },
                { "1", "2" },
                { "3", "4" },
            });

            // ***************************** ACT *****************************
            var result = CSVPage.Combine("new name", "order", page, page2);

            // **************************** ASSERT ****************************
            Assert.NotNull(result);
            Assert.AreEqual("new name", result.GetName());            
            Assert.AreEqual(
                string.Join(
                    "\n",
                    "\"order\",\"a\",\"b\"",
                    "\"0\",\"c\",\"d\"",
                    "\"0\",\"e\",\"f\"",
                    "\"1\",\"1\",\"2\"",
                    "\"1\",\"3\",\"4\""
                ), 
                result.ToString()
            );

        }

    }

}