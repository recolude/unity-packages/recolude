using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;

using Recolude.Core.IO.Encoders;
using Recolude.Core;


namespace Tests.IO.Encoders
{
    public static class EncoderTestUtils
    {
        public static float[] Times(ICaptureCollection<ICapture> expected)
        {
            float[] times = new float[expected.Captures.Length];
            for (int i = 0; i < expected.Captures.Length; i++)
            {
                times[i] = expected.Captures[i].Time;
            }
            return times;
        }

        public static Capture<Vector3>[] RandomPositionCaptures(int numCaptures)
        {
            var captures = new Capture<Vector3>[numCaptures];
            var curTime = -100f;
            var curPosition = new Vector3(Random.value, Random.value, Random.value) * 10f;
            for (int i = 0; i < captures.Length; i++)
            {
                curTime += Random.value;
                curPosition += (new Vector3(Random.value, Random.value, Random.value) * 10f) - new Vector3(5, 5, 5);
                captures[i] = new VectorCapture(curTime, curPosition);
            }
            return captures;
        }

        public static Capture<Vector3>[] RandomRotationCaptures(int numCaptures)
        {
            var captures = new Capture<Vector3>[numCaptures];
            var curTime = -100f;
            for (int i = 0; i < captures.Length; i++)
            {
                curTime += Random.value;
                captures[i] = new VectorCapture(curTime, new Vector3(Random.value, Random.value, Random.value) * 259);
            }
            return captures;
        }

        public static void AssertPositionCollectionsEqual(
            ICaptureCollection<Capture<Vector3>> expected,
            ICaptureCollection<Capture<Vector3>> actual,
            float maxComponentDelta,
            float timeDelta
        )
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);

            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Signature, actual.Signature);
            Assert.AreEqual(expected.Captures.Length, actual.Captures.Length);

            for (int i = 0; i < expected.Captures.Length; i++)
            {
                AssertUtils.InDelta(expected.Captures[i].Time, actual.Captures[i].Time, timeDelta);
                AssertUtils.InDelta(expected.Captures[i].Value.x, actual.Captures[i].Value.x, maxComponentDelta);
                AssertUtils.InDelta(expected.Captures[i].Value.y, actual.Captures[i].Value.y, maxComponentDelta);
                AssertUtils.InDelta(expected.Captures[i].Value.z, actual.Captures[i].Value.z, maxComponentDelta);
            }
        }

        public static void AssertRotationCollectionsEqual(
            ICaptureCollection<Capture<Vector3>> expected,
            ICaptureCollection<Capture<Vector3>> actual,
            float maxComponentDelta,
            float timeDelta
        )
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);

            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Signature, actual.Signature);
            Assert.AreEqual(expected.Captures.Length, actual.Captures.Length);

            for (int i = 0; i < expected.Captures.Length; i++)
            {
                AssertUtils.InDelta(expected.Captures[i].Time, actual.Captures[i].Time, timeDelta);
                AssertUtils.InDelta(expected.Captures[i].Value.x, actual.Captures[i].Value.x, maxComponentDelta);
                AssertUtils.InDelta(expected.Captures[i].Value.y, actual.Captures[i].Value.y, maxComponentDelta);
                AssertUtils.InDelta(expected.Captures[i].Value.z, actual.Captures[i].Value.z, maxComponentDelta);
            }
        }
    }

}