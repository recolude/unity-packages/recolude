using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;

using Recolude.Core.IO.Encoders.Enum;
using Recolude.Core;


namespace Tests.IO.Encoders
{
    public class EnumEncoderTests
    {

        void AssertCollectionsEqual(
            IEnumCollection expected,
            IEnumCollection actual
        )
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);

            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Signature, actual.Signature);
            Assert.AreEqual(expected.EnumMembers.Length, actual.EnumMembers.Length);
            Assert.AreEqual(expected.Captures.Length, actual.Captures.Length);

            for (int i = 0; i < expected.Captures.Length; i++)
            {
                Assert.AreEqual(expected.Captures[i].Time, actual.Captures[i].Time, "time mismatch? wtf");
                Assert.AreEqual((int)expected.Captures[i].Value, actual.Captures[i].Value);
            }

            for (int i = 0; i < expected.EnumMembers.Length; i++)
            {
                Assert.AreEqual(expected.EnumMembers[i], actual.EnumMembers[i]);
            }
        }

        [Test]
        public void AcceptsEnumCollection()
        {
            var encoder = new EnumEncoder();
            Assert.True(encoder.Accepts(new LifeCycleCollection(new Capture<UnityLifeCycleEvent>[] { })));
        }

        [Test]
        public void RejectsNonEnumCollection()
        {
            var encoder = new EnumEncoder();
            Assert.False(encoder.Accepts(new PositionCollection(new Capture<Vector3>[] { })));
        }

        [Test]
        public void Signature()
        {
            Assert.AreEqual("recolude.enum", new LifeCycleCollection(new Capture<UnityLifeCycleEvent>[] { }).Signature);
        }

        [Test]
        public void SingleEmptyCollection()
        {
            // *************************** ARRANGE ***************************
            var originalCollection = new LifeCycleCollection(new Capture<UnityLifeCycleEvent>[] { });
            var encoder = new EnumEncoder();

            // ***************************** ACT *****************************
            var encodedData = encoder.Encode(new ICaptureCollection<ICapture>[] { originalCollection });
            var decodedCollection = encoder.Decode(
                originalCollection.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[0],
                new float[0]
            );

            // **************************** ASSERT ****************************
            AssertCollectionsEqual(originalCollection, decodedCollection);
        }

        [Test]
        public void SingleCaptureSingleCollection()
        {
            // *************************** ARRANGE ***************************
            var originalCollection = new LifeCycleCollection(new Capture<UnityLifeCycleEvent>[] {
                new UnityLifeCycleEventCapture(0, UnityLifeCycleEvent.Disable),
            });
            var encoder = new EnumEncoder();

            // ***************************** ACT *****************************
            var encodedData = encoder.Encode(new ICaptureCollection<ICapture>[] { originalCollection });
            var decodedCollection = encoder.Decode(
                originalCollection.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[0],
                EncoderTestUtils.Times(originalCollection)
            );

            // **************************** ASSERT ****************************
            AssertCollectionsEqual(originalCollection, decodedCollection);
        }

        [Test]
        public void MultipleCaptureSingleCollection()
        {
            // *************************** ARRANGE ***************************
            var originalCollection = new LifeCycleCollection(new Capture<UnityLifeCycleEvent>[] {
                new UnityLifeCycleEventCapture(0, UnityLifeCycleEvent.Disable),
                new UnityLifeCycleEventCapture(1, UnityLifeCycleEvent.Enable),
                new UnityLifeCycleEventCapture(2, UnityLifeCycleEvent.Destroy),
                new UnityLifeCycleEventCapture(3, UnityLifeCycleEvent.Disable),
                new UnityLifeCycleEventCapture(4, UnityLifeCycleEvent.Enable),
                new UnityLifeCycleEventCapture(7, UnityLifeCycleEvent.Start),
                new UnityLifeCycleEventCapture(12, UnityLifeCycleEvent.Enable),
            });
            var encoder = new EnumEncoder();

            // ***************************** ACT *****************************
            var encodedData = encoder.Encode(new ICaptureCollection<ICapture>[] { originalCollection });
            var decodedCollection = encoder.Decode(
                originalCollection.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[0],
                EncoderTestUtils.Times(originalCollection)
            );

            // **************************** ASSERT ****************************
            AssertCollectionsEqual(originalCollection, decodedCollection);
        }

        [Test]
        public void MultipleCaptureMultipeCollectionSameEnumType()
        {
            // *************************** ARRANGE ***************************
            var originalCollection1 = new LifeCycleCollection(new Capture<UnityLifeCycleEvent>[] {
                new UnityLifeCycleEventCapture(1, UnityLifeCycleEvent.Disable),
                new UnityLifeCycleEventCapture(5, UnityLifeCycleEvent.Disable),
                new UnityLifeCycleEventCapture(5, UnityLifeCycleEvent.Enable),
                new UnityLifeCycleEventCapture(9, UnityLifeCycleEvent.Start),
                new UnityLifeCycleEventCapture(12, UnityLifeCycleEvent.Enable),
            });
            var originalCollection2 = new LifeCycleCollection(new Capture<UnityLifeCycleEvent>[] {
            });
            var originalCollection3 = new LifeCycleCollection(new Capture<UnityLifeCycleEvent>[] {
                new UnityLifeCycleEventCapture(9, UnityLifeCycleEvent.Disable),
                new UnityLifeCycleEventCapture(10, UnityLifeCycleEvent.Destroy),
                new UnityLifeCycleEventCapture(11, UnityLifeCycleEvent.Disable),
            });
            var originalCollection4 = new LifeCycleCollection(new Capture<UnityLifeCycleEvent>[] {
                new UnityLifeCycleEventCapture(0, UnityLifeCycleEvent.Disable),
                new UnityLifeCycleEventCapture(1, UnityLifeCycleEvent.Enable),
                new UnityLifeCycleEventCapture(2, UnityLifeCycleEvent.Destroy),
                new UnityLifeCycleEventCapture(3, UnityLifeCycleEvent.Disable),
                new UnityLifeCycleEventCapture(4, UnityLifeCycleEvent.Enable),
                new UnityLifeCycleEventCapture(7, UnityLifeCycleEvent.Start),
                new UnityLifeCycleEventCapture(12, UnityLifeCycleEvent.Enable),
            });
            var encoder = new EnumEncoder();

            // ***************************** ACT *****************************
            var encodedData = encoder.Encode(new ICaptureCollection<ICapture>[] {
                originalCollection1,
                originalCollection2,
                originalCollection3,
                originalCollection4,
            });
            var decodedCollection1 = encoder.Decode(
                originalCollection1.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[0],
                EncoderTestUtils.Times(originalCollection1)
            );
            var decodedCollection2 = encoder.Decode(
                originalCollection2.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[1],
                EncoderTestUtils.Times(originalCollection2)
            );
            var decodedCollection3 = encoder.Decode(
                originalCollection3.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[2],
                EncoderTestUtils.Times(originalCollection3)
            );
            var decodedCollection4 = encoder.Decode(
                originalCollection4.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[3],
                EncoderTestUtils.Times(originalCollection4)
            );

            // **************************** ASSERT ****************************
            AssertCollectionsEqual(originalCollection1, decodedCollection1);
            AssertCollectionsEqual(originalCollection2, decodedCollection2);
            AssertCollectionsEqual(originalCollection3, decodedCollection3);
            AssertCollectionsEqual(originalCollection4, decodedCollection4);
        }

        [Test]
        public void MultipleCaptureMultipeCollectionDifferentEnumTypes()
        {
            // *************************** ARRANGE ***************************
            var originalCollection1 = new LifeCycleCollection(new Capture<UnityLifeCycleEvent>[] {
                new UnityLifeCycleEventCapture(1, UnityLifeCycleEvent.Disable),
                new UnityLifeCycleEventCapture(5, UnityLifeCycleEvent.Disable),
                new UnityLifeCycleEventCapture(5, UnityLifeCycleEvent.Enable),
                new UnityLifeCycleEventCapture(9, UnityLifeCycleEvent.Start),
                new UnityLifeCycleEventCapture(12, UnityLifeCycleEvent.Enable),
            });
            var originalCollection2 = new LifeCycleCollection(new Capture<UnityLifeCycleEvent>[] {
            });
            var originalCollection3 = new EnumCollection("Testy", new string[] { "a", "b" }, new Capture<int>[]{
                new IntCapture(0, 1),
                new IntCapture(0, 0),
                new IntCapture(0, 1),
                new IntCapture(0, 1),
                new IntCapture(0, 0),
                new IntCapture(0, 0),
                new IntCapture(0, 1),
            });
            var originalCollection4 = new LifeCycleCollection(new Capture<UnityLifeCycleEvent>[] {
                new UnityLifeCycleEventCapture(0, UnityLifeCycleEvent.Disable),
                new UnityLifeCycleEventCapture(1, UnityLifeCycleEvent.Enable),
                new UnityLifeCycleEventCapture(2, UnityLifeCycleEvent.Destroy),
                new UnityLifeCycleEventCapture(3, UnityLifeCycleEvent.Disable),
                new UnityLifeCycleEventCapture(4, UnityLifeCycleEvent.Enable),
                new UnityLifeCycleEventCapture(7, UnityLifeCycleEvent.Start),
                new UnityLifeCycleEventCapture(12, UnityLifeCycleEvent.Enable),
            });
            var originalCollection5 = new EnumCollection("Barf", new string[] { "a", "b", "c" }, new Capture<int>[]{
                new IntCapture(0, 1),
                new IntCapture(1, 1),
                new IntCapture(3, 2),
                new IntCapture(7, 2),
                new IntCapture(12, 0),
                new IntCapture(20, 1),
            });
            var originalCollection6 = new EnumCollection("another", new string[] { "6", "7" }, new Capture<int>[]{
                new IntCapture(0, 0),
                new IntCapture(0, 0),
                new IntCapture(0, 1),
                new IntCapture(0, 1),
                new IntCapture(0, 1),
                new IntCapture(0, 0),
                new IntCapture(0, 1),
            });
            var encoder = new EnumEncoder();

            // ***************************** ACT *****************************
            var encodedData = encoder.Encode(new ICaptureCollection<ICapture>[] {
                originalCollection1,
                originalCollection2,
                originalCollection3,
                originalCollection4,
                originalCollection5,
                originalCollection6,
            });
            var decodedCollection1 = encoder.Decode(
                originalCollection1.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[0],
                EncoderTestUtils.Times(originalCollection1)
            );
            var decodedCollection2 = encoder.Decode(
                originalCollection2.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[1],
                EncoderTestUtils.Times(originalCollection2)
            );
            var decodedCollection3 = encoder.Decode(
                originalCollection3.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[2],
                EncoderTestUtils.Times(originalCollection3)
            );
            var decodedCollection4 = encoder.Decode(
                originalCollection4.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[3],
                EncoderTestUtils.Times(originalCollection4)
            );
            var decodedCollection5 = encoder.Decode(
                originalCollection5.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[4],
                EncoderTestUtils.Times(originalCollection5)
            );
            var decodedCollection6 = encoder.Decode(
                originalCollection6.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[5],
                EncoderTestUtils.Times(originalCollection6)
            );

            // **************************** ASSERT ****************************
            AssertCollectionsEqual(originalCollection1, decodedCollection1);
            AssertCollectionsEqual(originalCollection2, decodedCollection2);
            AssertCollectionsEqual(originalCollection3, decodedCollection3);
            AssertCollectionsEqual(originalCollection4, decodedCollection4);
            AssertCollectionsEqual(originalCollection5, decodedCollection5);
            AssertCollectionsEqual(originalCollection6, decodedCollection6);
        }

    }
}