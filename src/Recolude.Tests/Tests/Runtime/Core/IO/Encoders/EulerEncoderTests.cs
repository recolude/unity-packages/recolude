using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;

using Recolude.Core.IO.Encoders.Euler;
using Recolude.Core;


namespace Tests.IO.Encoders
{
    public class EulerEncoderTests
    {
        

        

        [Test]
        public void Signature()
        {
            Assert.AreEqual("recolude.euler", new EulerRotationCollection(new Capture<Vector3>[] { }).Signature);
        }

        [Test]
        public void AcceptsRotationCollection()
        {
            var encoder = new EulerEncoder();
            Assert.True(encoder.Accepts(new EulerRotationCollection(new Capture<Vector3>[] { })));
        }

        [Test]
        public void RejectsNonRotationCollection()
        {
            var encoder = new EulerEncoder();
            Assert.False(encoder.Accepts(new CustomEventCollection(new Capture<CustomEvent>[] { })));
        }

        [Test]
        [TestCase(StorageTechnique.Raw16)]
        [TestCase(StorageTechnique.Raw32)]
        [TestCase(StorageTechnique.Raw64)]
        public void SingleEmptyCollection(StorageTechnique technique)
        {
            // *************************** ARRANGE ***************************
            var originalCollection = new EulerRotationCollection(new Capture<Vector3>[] { });
            var encoder = new EulerEncoder(technique);

            // ***************************** ACT *****************************
            var encodedData = encoder.Encode(new ICaptureCollection<ICapture>[] { originalCollection });
            var decodedCollection = encoder.Decode(
                originalCollection.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[0],
                new float[0]
            );

            // **************************** ASSERT ****************************
            EncoderTestUtils.AssertRotationCollectionsEqual(originalCollection, decodedCollection, 0, 0);
        }

        [Test]
        [TestCase(StorageTechnique.Raw16)]
        [TestCase(StorageTechnique.Raw32)]
        [TestCase(StorageTechnique.Raw64)]
        public void SingleCaptureSingleCollection(StorageTechnique technique)
        {
            // *************************** ARRANGE ***************************
            var originalCollection = new EulerRotationCollection(new Capture<Vector3>[] {
                new VectorCapture(0, new Vector3(1, 2, 3))
            });
            var encoder = new EulerEncoder(technique);

            // ***************************** ACT *****************************
            var encodedData = encoder.Encode(new ICaptureCollection<ICapture>[] { originalCollection });
            var decodedCollection = encoder.Decode(
                originalCollection.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[0],
                EncoderTestUtils.Times(originalCollection)
            );

            // **************************** ASSERT ****************************
            EncoderTestUtils.AssertRotationCollectionsEqual(originalCollection, decodedCollection, 0, 0);
        }

        [Test]
        [TestCase(StorageTechnique.Raw16, 0.006f)]
        [TestCase(StorageTechnique.Raw32, 0)]
        [TestCase(StorageTechnique.Raw64, 0)]
        public void MultipleCaptures_SingleCollection(StorageTechnique technique, float delta)
        {
            // *************************** ARRANGE ***************************
            var originalCollection = new EulerRotationCollection(new Capture<Vector3>[] {
                new VectorCapture(-40, new Vector3(359, 0, 0)),
                new VectorCapture(0, new Vector3(1, 44, 8)),
                new VectorCapture(1, new Vector3(222, 55, 123))
            });
            var encoder = new EulerEncoder(technique);

            // ***************************** ACT *****************************
            var encodedData = encoder.Encode(new ICaptureCollection<ICapture>[] { originalCollection });
            var decodedCollection = encoder.Decode(
                originalCollection.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[0],
                EncoderTestUtils.Times(originalCollection)
            );

            // **************************** ASSERT ****************************
            EncoderTestUtils.AssertRotationCollectionsEqual(originalCollection, decodedCollection, delta, 0);
        }

        [Test]
        [TestCase(StorageTechnique.Raw16, 0.006f)]
        [TestCase(StorageTechnique.Raw32, 0)]
        [TestCase(StorageTechnique.Raw64, 0)]
        public void ThousandCapturesThreeCollections(StorageTechnique technique, float delta)
        {
            // *************************** ARRANGE ***************************
            Random.InitState(0);
            var originalCollection1 = new EulerRotationCollection("Col 1", EncoderTestUtils.RandomRotationCaptures(1000));
            var originalCollection2 = new EulerRotationCollection("Col 2", EncoderTestUtils.RandomRotationCaptures(1000));
            var originalCollection3 = new EulerRotationCollection("Col 3", EncoderTestUtils.RandomRotationCaptures(1000));
            var encoder = new EulerEncoder(technique);

            // ***************************** ACT *****************************
            var encodedData = encoder.Encode(new ICaptureCollection<ICapture>[] {
                originalCollection1,
                originalCollection2,
                originalCollection3,
            });

            var decodedCollection1 = encoder.Decode(
                originalCollection1.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[0],
                EncoderTestUtils.Times(originalCollection1)
            );

            var decodedCollection2 = encoder.Decode(
                originalCollection2.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[1],
                EncoderTestUtils.Times(originalCollection2)
            );

            var decodedCollection3 = encoder.Decode(
                originalCollection3.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[2],
                EncoderTestUtils.Times(originalCollection3)
            );

            // **************************** ASSERT ****************************
            EncoderTestUtils.AssertRotationCollectionsEqual(originalCollection1, decodedCollection1, delta, 0);
            EncoderTestUtils.AssertRotationCollectionsEqual(originalCollection2, decodedCollection2, delta, 0);
            EncoderTestUtils.AssertRotationCollectionsEqual(originalCollection3, decodedCollection3, delta, 0);
        }

    }

}