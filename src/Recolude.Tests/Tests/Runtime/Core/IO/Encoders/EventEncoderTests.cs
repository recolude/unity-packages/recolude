using System;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;

using Recolude.Core.IO.Encoders.Event;
using Recolude.Core;
using Recolude.Core.Properties;

namespace Tests.IO.Encoders
{
    public class EventEncoderTests
    {

        void AssertMetadataEqual(Metadata expected, Metadata metadata)
        {
            Assert.AreEqual(expected.Count, metadata.Count);
            foreach (var item in expected)
            {
                Assert.True(metadata.ContainsKey(item.Key));
                Assert.AreEqual(item.Value, metadata[item.Key]);
            }
        }
        
        void AssertCollectionsEqual(
            ICaptureCollection<Capture<CustomEvent>> expected,
            ICaptureCollection<Capture<CustomEvent>> actual
        )
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);

            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Signature, actual.Signature);
            Assert.AreEqual(expected.Captures.Length, actual.Captures.Length);

            for (int i = 0; i < expected.Captures.Length; i++)
            {
                var expectedCapture = expected.Captures[i];
                var actualCapture = actual.Captures[i];
                Assert.AreEqual(expectedCapture.Time, actualCapture.Time, "time mismatch? wtf");
                Assert.AreEqual(expectedCapture.Value.Name, actualCapture.Value.Name, "Event Names do not match");

                AssertMetadataEqual(expectedCapture.Value.Contents, actualCapture.Value.Contents);
                // Assert.AreEqual(expected.Captures[i].Value, actualCapture.Value);
            }

        }

        [Test]
        public void Signature()
        {
            Assert.AreEqual("recolude.event", new CustomEventCollection(new Capture<CustomEvent>[] { }).Signature);
        }

        [Test]
        public void AcceptsEventCollection()
        {
            var encoder = new EventEncoder();
            Assert.True(encoder.Accepts(new CustomEventCollection(new Capture<CustomEvent>[] { })));
        }

        [Test]
        public void RejectsNonEventCollection()
        {
            var encoder = new EventEncoder();
            Assert.False(encoder.Accepts(new PositionCollection(new Capture<Vector3>[] { })));
        }

        [Test]
        public void SingleEmptyCollection()
        {
            // *************************** ARRANGE ***************************
            var originalCollection = new CustomEventCollection(new Capture<CustomEvent>[] { });
            var encoder = new EventEncoder();

            // ***************************** ACT *****************************
            var encodedData = encoder.Encode(new ICaptureCollection<ICapture>[] { originalCollection });
            var decodedCollection = encoder.Decode(
                originalCollection.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[0],
                new float[0]
            );

            // **************************** ASSERT ****************************
            AssertCollectionsEqual(originalCollection, decodedCollection);
        }

        [Test]
        public void SingleCapture_NoData_SingleCollection()
        {
            // *************************** ARRANGE ***************************
            var originalCollection = new CustomEventCollection(new Capture<CustomEvent>[] {
                new CustomEventCapture(6, new CustomEvent("aahhh", new Metadata()))
            });
            var encoder = new EventEncoder();

            // ***************************** ACT *****************************
            var encodedData = encoder.Encode(new ICaptureCollection<ICapture>[] { originalCollection });
            var decodedCollection = encoder.Decode(
                originalCollection.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[0],
                EncoderTestUtils.Times(originalCollection)
            );

            // **************************** ASSERT ****************************
            AssertCollectionsEqual(originalCollection, decodedCollection);
        }

        [Test]
        public void SingleCapture_SingleKey_SingleCollection()
        {
            // *************************** ARRANGE ***************************
            var originalCollection = new CustomEventCollection(new Capture<CustomEvent>[] {
                new CustomEventCapture(6, new CustomEvent("SINGLE KEY", new Metadata(new Dictionary<string, IProperty>(){
                    {"my-key", new Property<string>("My property")}
                })))
            });
            var encoder = new EventEncoder();

            // ***************************** ACT *****************************
            var encodedData = encoder.Encode(new ICaptureCollection<ICapture>[] { originalCollection });
            var decodedCollection = encoder.Decode(
                originalCollection.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[0],
                EncoderTestUtils.Times(originalCollection)
            );

            // **************************** ASSERT ****************************
            AssertCollectionsEqual(originalCollection, decodedCollection);
        }

        [Test]
        public void SingleCapture_MultipleKeys_SingleCollection()
        {
            // *************************** ARRANGE ***************************
            var originalCollection = new CustomEventCollection(new Capture<CustomEvent>[] {
                new CustomEventCapture(6, new CustomEvent("SINGLE KEY", new Metadata(new Dictionary<string, IProperty>(){
                    {"my-key", new Property<string>("My property")},
                    {"int-key", new IntProperty(420)},
                    {"float-key", new FloatProperty(123.45f)}
                })))
            });
            var encoder = new EventEncoder();

            // ***************************** ACT *****************************
            var encodedData = encoder.Encode(new ICaptureCollection<ICapture>[] { originalCollection });
            var decodedCollection = encoder.Decode(
                originalCollection.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[0],
                EncoderTestUtils.Times(originalCollection)
            );

            // **************************** ASSERT ****************************
            AssertCollectionsEqual(originalCollection, decodedCollection);
        }

        [Test]
        public void MultipleCapture_MultipleKeys_SingleCollection()
        {
            // *************************** ARRANGE ***************************
            var originalCollection = new CustomEventCollection("MultipleCapture_MultipleKeys_SingleCollection", new Capture<CustomEvent>[] {
                new CustomEventCapture(6, new CustomEvent("Furst", new Metadata(new Dictionary<string, IProperty>(){
                    {"my-key", new Property<string>("My property")},
                    {"int-key", new IntProperty(420)},
                    {"float-key", new FloatProperty(123.45f)},
                    {"should b tru", new Property<bool>(true)}
                }))),

                new CustomEventCapture(10, new CustomEvent("Not First", new Metadata(new Dictionary<string, IProperty>(){
                }))),

                new CustomEventCapture(13, new CustomEvent("Not First", new Metadata(new Dictionary<string, IProperty>(){
                    {"my-key", new Property<string>("My property")},
                    {"int-key", new IntProperty(12)},
                    {"vector3", new Vector3Property(100, -9999, 110024)},
                    {"should b false", new Property<bool>(false)}
                }))),

                new CustomEventCapture(20, new CustomEvent("Last", new Metadata(new Dictionary<string, IProperty>(){
                    {"my-key", new Property<string>("Something Different")},
                    {"an arry", new ArrayProperty<byte>(new byte[]{1, 2, 6, 1} )},
                    {"vector2", new Vector2Property(69, 2000)},
                })))
            });
            var encoder = new EventEncoder();

            // ***************************** ACT *****************************
            var encodedData = encoder.Encode(new ICaptureCollection<ICapture>[] { originalCollection });
            var decodedCollection = encoder.Decode(
                originalCollection.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[0],
                EncoderTestUtils.Times(originalCollection)
            );

            // **************************** ASSERT ****************************
            AssertCollectionsEqual(originalCollection, decodedCollection);
        }

        [Test]
        public void MultipleCapture_MultipleKeys_MultipleCollections()
        {
            // *************************** ARRANGE ***************************
            var originalCollection1 = new CustomEventCollection("MultipleCapture_MultipleKeys_MultipleCollections", new Capture<CustomEvent>[] {
                new CustomEventCapture(6, new CustomEvent("Furst", new Metadata(new Dictionary<string, IProperty>(){
                    {"my-key", new Property<string>("My property")},
                    {"int-key", new IntProperty(420)},
                    {"float-key", new FloatProperty(123.45f)},
                    {"should b tru", new Property<bool>(true)}
                }))),

                new CustomEventCapture(10, new CustomEvent("Not First", new Metadata(new Dictionary<string, IProperty>(){
                }))),
            });

            var originalCollection2 = new CustomEventCollection("MultipleCapture_MultipleKeys_MultipleCollections", new Capture<CustomEvent>[] {
                new CustomEventCapture(13, new CustomEvent("Not First", new Metadata(new Dictionary<string, IProperty>(){
                    {"my-key", new Property<string>("My property")},
                    {"int-key", new IntProperty(12)},
                    {"vector3", new Vector3Property(100, -9999, 110024)},
                    {"should b false", new Property<bool>(false)}
                }))),

                new CustomEventCapture(20, new CustomEvent("Last", new Metadata(new Dictionary<string, IProperty>(){
                    {"my-key", new Property<string>("Something Different")},
                    {"an arry", new ArrayProperty<byte>(new byte[]{1, 2, 6, 1} )},
                    {"vector2", new Vector2Property(69, 2000)},
                })))
            });

            var originalCollection3 = new CustomEventCollection("All the god damn rest of the properties", new Capture<CustomEvent>[] {
                new CustomEventCapture(55, new CustomEvent("Basic Types", new Metadata(new Dictionary<string, IProperty>(){
                    {"my-key", new Property<string>("My property")},
                    {"int-key", new IntProperty(12)},
                    {"float-prop", new FloatProperty(3.13131313f)},
                    {"true-prop", new Property<bool>(true)},
                    {"false-prop", new Property<bool>(false)},
                    {"byte-prop", new Property<byte>(222)},
                }))),

                new CustomEventCapture(99, new CustomEvent("More Complex", new Metadata(new Dictionary<string, IProperty>(){
                    {"vector2", new Vector2Property(69, 2000)},
                    {"vector3", new Vector3Property(100, -9999, 110024)},
                    {"metadata", new MetadataProperty(new Dictionary<string, IProperty>(){
                        {"something-different", new IntProperty(999)},
                        {"something-else", new FloatProperty(70002.212f)},
                        {"something-crazy", new Property<byte>(255)},
                    })},
                    {"time", new TimeProperty(new System.DateTime(1995, 11, 28, 4, 20, 35, 666))}
                }))),

                new CustomEventCapture(1000, new CustomEvent("More Complex", new Metadata(new Dictionary<string, IProperty>(){
                    {"string arry", new ArrayProperty<string>("a", "b", "c")},
                    {"int arry", new ArrayProperty<int>(9001, -121, 33000)},
                    {"float arry", new ArrayProperty<float>(-2323f, 0.0003111f, 92.242f)},
                    {"bool array", new ArrayProperty<bool>(false, false, false, false, false,  true, false, true)},
                    {"byte arry", new ArrayProperty<byte>(new byte[]{1, 2, 6, 1} )},
                    {"vec2 ary", new ArrayProperty<Vector2>(new Vector2(1, 2), new Vector2(3, -11))},
                    {"vec3 ary", new ArrayProperty<Vector3>(new Vector3(1, 2), new Vector3(3, -11))},
                    {"datetime ary", new ArrayProperty<DateTime>(new System.DateTime(1,2,3), new System.DateTime(4,5,6))},
                    {"metadata", new ArrayProperty<Metadata>(
                        new Metadata(new Dictionary<string, IProperty>(){
                            {"something-different", new IntProperty(999)},
                            {"something-else", new FloatProperty(70002.212f)},
                            {"something-crazy", new Property<byte>(255)},
                        }),
                        new Metadata(new Dictionary<string, IProperty>(){
                            {"something-different", new IntProperty(999)},
                            {"something-else", new FloatProperty(70002.212f)},
                            {"something-crazy", new Property<byte>(255)},
                        }),
                        new Metadata(new Dictionary<string, IProperty>(){
                            {"something-different", new IntProperty(999)},
                            {"something-else", new FloatProperty(70002.212f)},
                            {"something-crazy", new Property<byte>(255)},
                        })
                    )}
                })))
            });
            var encoder = new EventEncoder();

            // ***************************** ACT *****************************
            var encodedData = encoder.Encode(new ICaptureCollection<ICapture>[] { originalCollection1, originalCollection2, originalCollection3 });
            var decodedCollection1 = encoder.Decode(
                originalCollection1.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[0],
                EncoderTestUtils.Times(originalCollection1)
            );
            var decodedCollection2 = encoder.Decode(
                originalCollection2.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[1],
                EncoderTestUtils.Times(originalCollection2)
            );
            var decodedCollection3 = encoder.Decode(
                originalCollection3.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[2],
                EncoderTestUtils.Times(originalCollection3)
            );

            // **************************** ASSERT ****************************
            AssertCollectionsEqual(originalCollection1, decodedCollection1);
            AssertCollectionsEqual(originalCollection2, decodedCollection2);
            AssertCollectionsEqual(originalCollection3, decodedCollection3);
        }

    }

}