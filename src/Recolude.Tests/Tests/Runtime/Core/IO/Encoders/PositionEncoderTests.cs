using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using Recolude.Core.IO.Encoders.Position;
using Recolude.Core;


namespace Tests.IO.Encoders
{
    public class PositionEncoderTests
    {
        [Test]
        public void Signature()
        {
            Assert.AreEqual("recolude.position", new PositionCollection(new Capture<Vector3>[] { }).Signature);
        }

        [Test]
        public void AcceptsPositionCollection()
        {
            var encoder = new Vector3PositionEncoder();
            Assert.True(encoder.Accepts(new PositionCollection(new Capture<Vector3>[] { })));
        }

        [Test]
        public void RejectsNonPositionCollection()
        {
            var encoder = new Vector3PositionEncoder();
            Assert.False(encoder.Accepts(new CustomEventCollection(new Capture<CustomEvent>[] { })));
        }

        [Test]
        [TestCase(StorageTechnique.Oct24)]
        [TestCase(StorageTechnique.Oct48)]
        [TestCase(StorageTechnique.Raw32)]
        [TestCase(StorageTechnique.Raw64)]
        public void SingleEmptyCollection(StorageTechnique technique)
        {
            // *************************** ARRANGE ***************************
            var originalCollection = new PositionCollection(new Capture<Vector3>[] { });
            var encoder = new Vector3PositionEncoder(technique);

            // ***************************** ACT *****************************
            var encodedData = encoder.Encode(new ICaptureCollection<ICapture>[] { originalCollection });
            var decodedCollection = encoder.Decode(
                originalCollection.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[0],
                new float[0]
            );

            // **************************** ASSERT ****************************
            EncoderTestUtils.AssertPositionCollectionsEqual(originalCollection, decodedCollection, 0, 0);
        }

        [Test]
        [TestCase(StorageTechnique.Oct24)]
        [TestCase(StorageTechnique.Oct48)]
        [TestCase(StorageTechnique.Raw32)]
        [TestCase(StorageTechnique.Raw64)]
        public void SingleCaptureSingleCollection(StorageTechnique technique)
        {
            // *************************** ARRANGE ***************************
            var originalCollection = new PositionCollection(new Capture<Vector3>[]
            {
                new VectorCapture(0, new Vector3(1, 2, 3))
            });
            var encoder = new Vector3PositionEncoder(technique);

            // ***************************** ACT *****************************
            var encodedData = encoder.Encode(new ICaptureCollection<ICapture>[] { originalCollection });
            var decodedCollection = encoder.Decode(
                originalCollection.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[0],
                EncoderTestUtils.Times(originalCollection)
            );

            // **************************** ASSERT ****************************
            EncoderTestUtils.AssertPositionCollectionsEqual(originalCollection, decodedCollection, 0, 0);
        }

        [Test]
        [TestCase(StorageTechnique.Oct24)]
        [TestCase(StorageTechnique.Oct48)]
        [TestCase(StorageTechnique.Raw32)]
        [TestCase(StorageTechnique.Raw64)]
        public void TwoCapturesSingleCollection(StorageTechnique technique)
        {
            // *************************** ARRANGE ***************************
            var originalCollection = new PositionCollection(new Capture<Vector3>[]
            {
                new VectorCapture(0, new Vector3(1, 2, 3)),
                new VectorCapture(1, new Vector3(3, 4, 5))
            });
            var encoder = new Vector3PositionEncoder(technique);

            // ***************************** ACT *****************************
            var encodedData = encoder.Encode(new ICaptureCollection<ICapture>[] { originalCollection });
            var decodedCollection = encoder.Decode(
                originalCollection.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[0],
                EncoderTestUtils.Times(originalCollection)
            );

            // **************************** ASSERT ****************************
            EncoderTestUtils.AssertPositionCollectionsEqual(originalCollection, decodedCollection, 0, 0);
        }

        [Test]
        [TestCase(StorageTechnique.Oct24, 0.1f)]
        [TestCase(StorageTechnique.Oct48, 0.001f)]
        [TestCase(StorageTechnique.Raw32, 0)]
        [TestCase(StorageTechnique.Raw64, 0)]
        public void FourCapturesSingleCollection(StorageTechnique technique, float delta)
        {
            // *************************** ARRANGE ***************************
            var originalCollection = new PositionCollection(new Capture<Vector3>[]
            {
                new VectorCapture(0, new Vector3(0, 1, 2)),
                new VectorCapture(1, new Vector3(4, 3, 2)),
                new VectorCapture(2, new Vector3(6, 7, 8)),
                new VectorCapture(3, new Vector3(1, 1, 1))
            });
            var encoder = new Vector3PositionEncoder(technique);

            // ***************************** ACT *****************************
            var encodedData = encoder.Encode(new ICaptureCollection<ICapture>[] { originalCollection });
            var decodedCollection = encoder.Decode(
                originalCollection.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[0],
                EncoderTestUtils.Times(originalCollection)
            );

            // **************************** ASSERT ****************************
            EncoderTestUtils.AssertPositionCollectionsEqual(originalCollection, decodedCollection, delta, 0);
        }

        [Test]
        [TestCase(StorageTechnique.Oct24, 0.1f)]
        [TestCase(StorageTechnique.Oct48, 0.001f)]
        [TestCase(StorageTechnique.Raw32, 0)]
        [TestCase(StorageTechnique.Raw64, 0)]
        public void HundredThousandCapturesSingleCollection(StorageTechnique technique,
            float delta)
        {
            // *************************** ARRANGE ***************************
            Random.InitState(0);
            var originalCollection = new PositionCollection(EncoderTestUtils.RandomPositionCaptures(100000));
            var encoder = new Vector3PositionEncoder(technique);

            // ***************************** ACT *****************************
            var encodedData = encoder.Encode(new ICaptureCollection<ICapture>[] { originalCollection });
            var decodedCollection = encoder.Decode(
                originalCollection.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[0],
                EncoderTestUtils.Times(originalCollection)
            );

            // **************************** ASSERT ****************************
            EncoderTestUtils.AssertPositionCollectionsEqual(originalCollection, decodedCollection, delta, 0);
        }

        [Test]
        [TestCase(StorageTechnique.Oct24, 0.1f)]
        [TestCase(StorageTechnique.Oct48, 0.001f)]
        [TestCase(StorageTechnique.Raw32, 0)]
        [TestCase(StorageTechnique.Raw64, 0)]
        public void ThousandCapturesThreeCollections(StorageTechnique technique, float delta)
        {
            // *************************** ARRANGE ***************************
            Random.InitState(0);
            var originalCollection1 = new PositionCollection("Col 1", EncoderTestUtils.RandomPositionCaptures(1000));
            var originalCollection2 = new PositionCollection("Col 2", EncoderTestUtils.RandomPositionCaptures(1000));
            var originalCollection3 = new PositionCollection("Col 3", EncoderTestUtils.RandomPositionCaptures(1000));
            var encoder = new Vector3PositionEncoder(technique);

            // ***************************** ACT *****************************
            var encodedData = encoder.Encode(new ICaptureCollection<ICapture>[]
            {
                originalCollection1,
                originalCollection2,
                originalCollection3,
            });

            var decodedCollection1 = encoder.Decode(
                originalCollection1.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[0],
                EncoderTestUtils.Times(originalCollection1)
            );

            var decodedCollection2 = encoder.Decode(
                originalCollection2.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[1],
                EncoderTestUtils.Times(originalCollection2)
            );

            var decodedCollection3 = encoder.Decode(
                originalCollection3.Name,
                encodedData.Header,
                encodedData.EncodedCollectionsData[2],
                EncoderTestUtils.Times(originalCollection3)
            );

            // **************************** ASSERT ****************************
            EncoderTestUtils.AssertPositionCollectionsEqual(originalCollection1, decodedCollection1, delta, 0);
            EncoderTestUtils.AssertPositionCollectionsEqual(originalCollection2, decodedCollection2, delta, 0);
            EncoderTestUtils.AssertPositionCollectionsEqual(originalCollection3, decodedCollection3, delta, 0);
        }
    }
}