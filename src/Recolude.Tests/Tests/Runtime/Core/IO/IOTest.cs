﻿using System;
using System.IO.Compression;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Recolude.Core.IO;
using Recolude.Core.Properties;
using Recolude.Core;
using Recolude.Core.IO.PropertyWriters;
using CompressionLevel = System.IO.Compression.CompressionLevel;

namespace Tests.IO
{
    public class IOTest
    {
        private IRecording SerializeAndDeserialize(IRecording rec,
            CompressionLevel compressionLevel, TimeStorageTechnique timeStorageTechnique)
        {
            using (MemoryStream inStream = new MemoryStream())
            {
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();

                using (RAPWriter rapWriter = new RAPWriter(inStream, true))
                {
                    rapWriter.Write(rec, compressionLevel, timeStorageTechnique);
                }

                stopWatch.Stop();
                // Get the elapsed time as a TimeSpan value.
                TimeSpan ts = stopWatch.Elapsed;

                // Format and display the TimeSpan value.
                string elapsedTime = String.Format("{0:00}h {1:00}m {2:00}s {3:00}ms",
                    ts.Hours, ts.Minutes, ts.Seconds,
                    ts.Milliseconds);
                UnityEngine.Debug.Log("Write RunTime " + elapsedTime);

                UnityEngine.Debug.Log(inStream.Length);
                inStream.Seek(0, SeekOrigin.Begin);

                stopWatch = new Stopwatch();
                stopWatch.Start();

                // read back from scene
                IRecording decoded = null;
                using (var rapReader = new RAPReader(inStream))
                {
                    decoded = rapReader.Recording();
                }

                stopWatch.Stop();
                // Get the elapsed time as a TimeSpan value.
                ts = stopWatch.Elapsed;

                // Format and display the TimeSpan value.
                elapsedTime = String.Format("{0:00}h {1:00}m {2:00}s {3:00}ms",
                    ts.Hours, ts.Minutes, ts.Seconds,
                    ts.Milliseconds);
                UnityEngine.Debug.Log("Read RunTime " + elapsedTime);
                return decoded;
            }
        }

        private void AssertMetadataEqual(Metadata expected, Metadata actual)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);
            Assert.AreEqual(expected.Count, actual.Count);

            byte[] expectedData;
            using (var expectedStream = new MemoryStream())
            {
                using (var writer = new BinaryWriter(expectedStream))
                {
                    PropertyWriterCollection.Instance.WriteValue(writer, expected);
                }

                expectedData = expectedStream.ToArray();
            }

            byte[] actualData;
            using (var actualStream = new MemoryStream())
            {
                using (var writer = new BinaryWriter(actualStream))
                {
                    PropertyWriterCollection.Instance.WriteValue(writer, actual);
                }

                actualData = actualStream.ToArray();
            }

            Assert.AreEqual(expectedData.Length, actualData.Length);
            for (int i = 0; i < expectedData.Length; i++)
            {
                Assert.AreEqual(expectedData[i], actualData[i]);
            }
        }


        private void AssertRecordingsEqual(IRecording expected, IRecording actual, float timeDelta)
        {
            Assert.NotNull(expected);
            Assert.NotNull(actual);

            Assert.AreEqual(expected.ID, actual.ID);
            Assert.AreEqual(expected.Name, actual.Name);

            Assert.AreEqual(expected.Recordings.Length, actual.Recordings.Length);
            Assert.AreEqual(expected.Binaries.Length, actual.Binaries.Length);
            Assert.AreEqual(expected.BinaryReferences.Length, actual.BinaryReferences.Length);
            Assert.AreEqual(expected.CaptureCollections.Length, actual.CaptureCollections.Length);

            AssertMetadataEqual(expected.Metadata, actual.Metadata);

            // Ensure Children Recording Are Equal
            for (int i = 0; i < expected.Recordings.Length; i++)
            {
                AssertRecordingsEqual(expected.Recordings[i], actual.Recordings[i], timeDelta);
            }

            for (int i = 0; i < expected.CaptureCollections.Length; i++)
            {
                Assert.AreEqual(expected.CaptureCollections[i].Name, actual.CaptureCollections[i].Name);
                Assert.AreEqual(expected.CaptureCollections[i].Signature, actual.CaptureCollections[i].Signature);
                Assert.AreEqual(expected.CaptureCollections[i].Captures.Length,
                    actual.CaptureCollections[i].Captures.Length);
                for (int captureIndex = 0;
                     captureIndex < expected.CaptureCollections[i].Captures.Length;
                     captureIndex++)
                {
                    AssertUtils.InDelta(
                        expected.CaptureCollections[i].Captures[captureIndex].Time,
                        actual.CaptureCollections[i].Captures[captureIndex].Time,
                        timeDelta
                    );
                }
            }
        }


        [Test]
        [TestCase(CompressionLevel.NoCompression)]
        [TestCase(CompressionLevel.Optimal)]
        [TestCase(CompressionLevel.Fastest)]
        public void WriteRead_EmptySingleRecording(CompressionLevel level)
        {
            // *************************** ARRANGE ***************************
            var rec = new Recording("some id", "my cool name");

            // ***************************** ACT *****************************
            IRecording recDecoded = SerializeAndDeserialize(rec, level, TimeStorageTechnique.BST16);

            // **************************** ASSERT ****************************
            Assert.AreEqual("some id", recDecoded.ID);
            Assert.AreEqual("my cool name", recDecoded.Name);
            Assert.AreEqual(0, recDecoded.Binaries.Length);
            Assert.AreEqual(0, recDecoded.CaptureCollections.Length);
            Assert.AreEqual(0, recDecoded.BinaryReferences.Length);
            Assert.AreEqual(0, recDecoded.Metadata.Count);
            Assert.AreEqual(0, recDecoded.Recordings.Length);
        }

        [Test]
        public void Write_NullRecordingThrows()
        {
            using (MemoryStream inStream = new MemoryStream())
            {
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();

                using (RAPWriter rapWriter = new RAPWriter(inStream, true))
                {
                    Assert.Throws<ArgumentNullException>(() => rapWriter.Write(null, CompressionLevel.Fastest));
                }
            }
        }

        [Test]
        public void Write_NullMetadataPropertyThrows()
        {
            using (MemoryStream inStream = new MemoryStream())
            {
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();

                using (RAPWriter rapWriter = new RAPWriter(inStream, true))
                {
                    var rec = new Recording("", "Name", new Metadata(new Dictionary<string, IProperty>()
                    {
                        { "something", null }
                    }));
                    Assert.Throws<ArgumentException>(() => rapWriter.Write(rec, CompressionLevel.Fastest),
                        "Can not serialize null metadata property value: 'something' on recording: 'Name'");
                }
            }
        }

        [Test]
        [TestCase(CompressionLevel.NoCompression)]
        [TestCase(CompressionLevel.Optimal)]
        [TestCase(CompressionLevel.Fastest)]
        public void WriteRead_EmptySingleRecording_SingleMetadataEntry(CompressionLevel level)
        {
            // *************************** ARRANGE ***************************
            var md = new Metadata();
            md["Test-Str"] = new Property<string>("test val");
            var rec = new Recording(
                "single-metadata",
                "WriteRead_EmptySingleRecording_SingleMetadataEntry",
                new IRecording[0],
                new ICaptureCollection<ICapture>[0],
                md,
                new IBinary[0],
                new BinaryReference[0]
            );

            // ***************************** ACT *****************************
            IRecording recDecoded = SerializeAndDeserialize(
                rec,
                level,
                TimeStorageTechnique.BST16
            );

            // **************************** ASSERT ****************************
            Assert.AreEqual("single-metadata", recDecoded.ID);
            Assert.AreEqual("WriteRead_EmptySingleRecording_SingleMetadataEntry", recDecoded.Name);
            Assert.AreEqual(0, recDecoded.Binaries.Length);
            Assert.AreEqual(0, recDecoded.CaptureCollections.Length);
            Assert.AreEqual(0, recDecoded.BinaryReferences.Length);
            Assert.AreEqual(0, recDecoded.Recordings.Length);
            Assert.AreEqual(1, recDecoded.Metadata.Count);

            Assert.True(recDecoded.Metadata.ContainsKey("Test-Str"));
            Assert.AreEqual("test val", recDecoded.Metadata.AsString("Test-Str"));
        }

        [Test]
        [TestCase(CompressionLevel.NoCompression)]
        [TestCase(CompressionLevel.Optimal)]
        [TestCase(CompressionLevel.Fastest)]
        public void WriteRead_EmptySingleRecording_TwoMetadataEntry(CompressionLevel level)
        {
            // *************************** ARRANGE ***************************
            var md = new Metadata();
            md["Test-Str"] = new Property<string>("test val");
            md["Test-Int"] = new IntProperty(420);
            var rec = new Recording(
                "single-metadata",
                "WriteRead_EmptySingleRecording_SingleMetadataEntry",
                new IRecording[0],
                new ICaptureCollection<ICapture>[0],
                md,
                new IBinary[0],
                new BinaryReference[0]
            );

            // ***************************** ACT *****************************
            IRecording recDecoded = SerializeAndDeserialize(
                rec,
                level, TimeStorageTechnique.BST16
            );

            // **************************** ASSERT ****************************
            Assert.AreEqual("single-metadata", recDecoded.ID);
            Assert.AreEqual("WriteRead_EmptySingleRecording_SingleMetadataEntry", recDecoded.Name);
            Assert.AreEqual(0, recDecoded.Binaries.Length);
            Assert.AreEqual(0, recDecoded.CaptureCollections.Length);
            Assert.AreEqual(0, recDecoded.BinaryReferences.Length);
            Assert.AreEqual(2, recDecoded.Metadata.Count);
            Assert.AreEqual(0, recDecoded.Recordings.Length);

            Assert.True(recDecoded.Metadata.ContainsKey("Test-Str"));
            Assert.AreEqual("test val", recDecoded.Metadata.AsString("Test-Str"));

            Assert.True(recDecoded.Metadata.ContainsKey("Test-Int"));
            Assert.AreEqual(420, recDecoded.Metadata.AsInt("Test-Int"));
        }

        [Test]
        [TestCase(CompressionLevel.NoCompression)]
        [TestCase(CompressionLevel.Optimal)]
        [TestCase(CompressionLevel.Fastest)]
        public void WriteRead_EmptySingleRecording_ManyMetadataEntry(CompressionLevel level)
        {
            // *************************** ARRANGE ***************************
            var md = new Metadata();
            md["Test-Str"] = new Property<string>("test val");
            md["Test-Int"] = new IntProperty(420);
            md["Test-Bool-True"] = new Property<bool>(true);
            md["Test-Bool-False"] = new Property<bool>(false);
            md["Test-Float"] = new FloatProperty(1.2345f);
            md["Test-Byte-Array"] = new ArrayProperty<byte>(new byte[] { 1, 2, 3, 4, 5 });

            var rec = new Recording(
                "multiple-metadata",
                "WriteRead_EmptySingleRecording_ManyMetadataEntry",
                new IRecording[0],
                new ICaptureCollection<ICapture>[0],
                md,
                new IBinary[0],
                new BinaryReference[0]
            );

            // ***************************** ACT *****************************
            IRecording recDecoded = SerializeAndDeserialize(
                rec,
                level, TimeStorageTechnique.BST16
            );

            // **************************** ASSERT ****************************
            Assert.AreEqual("multiple-metadata", recDecoded.ID);
            Assert.AreEqual("WriteRead_EmptySingleRecording_ManyMetadataEntry", recDecoded.Name);
            Assert.AreEqual(0, recDecoded.Binaries.Length);
            Assert.AreEqual(0, recDecoded.CaptureCollections.Length);
            Assert.AreEqual(0, recDecoded.BinaryReferences.Length);
            Assert.AreEqual(6, recDecoded.Metadata.Count);
            Assert.AreEqual(0, recDecoded.Recordings.Length);

            Assert.True(recDecoded.Metadata.ContainsKey("Test-Str"));
            Assert.AreEqual("test val", recDecoded.Metadata.AsString("Test-Str"));

            Assert.True(recDecoded.Metadata.ContainsKey("Test-Int"));
            Assert.AreEqual(420, recDecoded.Metadata.AsInt("Test-Int"));

            Assert.True(recDecoded.Metadata.ContainsKey("Test-Bool-True"));
            Assert.AreEqual(true, recDecoded.Metadata.AsBool("Test-Bool-True"));

            Assert.True(recDecoded.Metadata.ContainsKey("Test-Bool-False"));
            Assert.AreEqual(false, recDecoded.Metadata.AsBool("Test-Bool-False"));

            Assert.True(recDecoded.Metadata.ContainsKey("Test-Float"));
            Assert.AreEqual(1.2345f, recDecoded.Metadata.AsFloat("Test-Float"));

            Assert.True(recDecoded.Metadata.ContainsKey("Test-Byte-Array"));
            Assert.AreEqual(5, recDecoded.Metadata.AsByteArray("Test-Byte-Array").Length);
            Assert.AreEqual(1, recDecoded.Metadata.AsByteArray("Test-Byte-Array")[0]);
            Assert.AreEqual(2, recDecoded.Metadata.AsByteArray("Test-Byte-Array")[1]);
            Assert.AreEqual(3, recDecoded.Metadata.AsByteArray("Test-Byte-Array")[2]);
            Assert.AreEqual(4, recDecoded.Metadata.AsByteArray("Test-Byte-Array")[3]);
            Assert.AreEqual(5, recDecoded.Metadata.AsByteArray("Test-Byte-Array")[4]);
        }

        [Test]
        [TestCase(CompressionLevel.NoCompression)]
        [TestCase(CompressionLevel.Optimal)]
        [TestCase(CompressionLevel.Fastest)]
        public void WriteRead_EmptyRecordingWithChild(CompressionLevel level)
        {
            // *************************** ARRANGE ***************************
            var rec = new Recording(
                "single_child",
                "WriteRead_EmptyRecordingWithChild",
                new IRecording[]
                {
                    new Recording(
                        "child",
                        "Only Child",
                        new IRecording[] { },
                        new ICaptureCollection<ICapture>[0],
                        new Metadata(),
                        new IBinary[0],
                        new BinaryReference[0]
                    ),
                },
                new ICaptureCollection<ICapture>[0],
                new Metadata(),
                new IBinary[0],
                new BinaryReference[0]
            );

            // ***************************** ACT *****************************
            IRecording recDecoded = SerializeAndDeserialize(rec, level, TimeStorageTechnique.BST16);

            // **************************** ASSERT ****************************
            AssertRecordingsEqual(rec, recDecoded, 0);
        }

        [Test]
        [TestCase(CompressionLevel.NoCompression)]
        [TestCase(CompressionLevel.Optimal)]
        [TestCase(CompressionLevel.Fastest)]
        public void WriteRead_EmptyRecordingWithMultipleChildren(CompressionLevel level)
        {
            // *************************** ARRANGE ***************************
            var rec = new Recording(
                "multiple_children",
                "WriteRead_EmptyRecordingWithMultipleChildren",
                new IRecording[]
                {
                    new Recording(
                        "1-child",
                        "The 1st Kid",
                        new IRecording[] { },
                        new ICaptureCollection<ICapture>[0],
                        new Metadata(),
                        new IBinary[0],
                        new BinaryReference[0]
                    ),
                    new Recording(
                        "2-child",
                        "The 2nd Kid",
                        new IRecording[] { },
                        new ICaptureCollection<ICapture>[0],
                        new Metadata(),
                        new IBinary[0],
                        new BinaryReference[0]
                    ),
                    new Recording(
                        "3-child",
                        "The 3rd Kid",
                        new IRecording[] { },
                        new ICaptureCollection<ICapture>[0],
                        new Metadata(),
                        new IBinary[0],
                        new BinaryReference[0]
                    ),
                    new Recording(
                        "4-child",
                        "The 4th Kid",
                        new IRecording[] { },
                        new ICaptureCollection<ICapture>[0],
                        new Metadata(),
                        new IBinary[0],
                        new BinaryReference[0]
                    ),
                },
                new ICaptureCollection<ICapture>[0],
                new Metadata(),
                new IBinary[0],
                new BinaryReference[0]
            );

            // ***************************** ACT *****************************
            IRecording recDecoded = SerializeAndDeserialize(rec, level, TimeStorageTechnique.BST16);

            // **************************** ASSERT ****************************
            AssertRecordingsEqual(rec, recDecoded, 0);
        }

        [Test]
        [TestCase(CompressionLevel.NoCompression)]
        [TestCase(CompressionLevel.Optimal)]
        [TestCase(CompressionLevel.Fastest)]
        public void WriteRead_EmptyRecordingWithMultipleChildrenAndMetadata(
            CompressionLevel level)
        {
            // *************************** ARRANGE ***************************
            var rec = new Recording(
                "multiple_children",
                "WriteRead_EmptyRecordingWithMultipleChildren",
                new IRecording[]
                {
                    new Recording(
                        "1-child",
                        "The 1st Kid",
                        new IRecording[] { },
                        new ICaptureCollection<ICapture>[0],
                        new Metadata(new Dictionary<string, IProperty>()
                        {
                            { "repeating-id", new Property<string>("repeating ID value 1") },
                            { "some int", new IntProperty(69) }
                        }),
                        new IBinary[0],
                        new BinaryReference[0]
                    ),
                    new Recording(
                        "2-child",
                        "The 2nd Kid",
                        new IRecording[] { },
                        new ICaptureCollection<ICapture>[0],
                        new Metadata(new Dictionary<string, IProperty>()
                        {
                            { "repeating-id", new Property<string>("repeating ID value 2") },
                            { "some float", new FloatProperty(1.2345f) }
                        }),
                        new IBinary[0],
                        new BinaryReference[0]
                    ),
                    new Recording(
                        "3-child",
                        "The 3rd Kid",
                        new IRecording[] { },
                        new ICaptureCollection<ICapture>[0],
                        new Metadata(new Dictionary<string, IProperty>()
                        {
                            { "repeating-id", new Property<string>("repeating ID value 3") },
                        }),
                        new IBinary[0],
                        new BinaryReference[0]
                    ),
                    new Recording(
                        "4-child",
                        "The 4th Kid",
                        new IRecording[] { },
                        new ICaptureCollection<ICapture>[0],
                        new Metadata(new Dictionary<string, IProperty>()
                        {
                            { "repeating-id", new Property<string>("repeating ID value 4") },
                        }),
                        new IBinary[0],
                        new BinaryReference[0]
                    ),
                },
                new ICaptureCollection<ICapture>[0],
                new Metadata(new Dictionary<string, IProperty>()
                {
                    { "repeating-id", new Property<string>("repeating ID value 0") },
                }),
                new IBinary[0],
                new BinaryReference[0]
            );

            // ***************************** ACT *****************************
            IRecording recDecoded = SerializeAndDeserialize(rec, level, TimeStorageTechnique.BST16);

            // **************************** ASSERT ****************************
            AssertRecordingsEqual(rec, recDecoded, 0);
        }

        [Test]
        [TestCase(CompressionLevel.NoCompression)]
        [TestCase(CompressionLevel.Optimal)]
        [TestCase(CompressionLevel.Fastest)]
        public void WriteRead_SingleRecording_EmptyPositionCaptureCollection(
            CompressionLevel level)
        {
            // *************************** ARRANGE ****************************
            var rec = new Recording(
                "single collection",
                "WriteRead_SingleRecording_EmptyPositionCaptureCollection",
                new IRecording[] { },
                new ICaptureCollection<ICapture>[]
                {
                    new PositionCollection("Example Position Collection",
                        new Capture<Vector3>[]
                        {
                        })
                },
                new Metadata(),
                new IBinary[0],
                new BinaryReference[0]
            );

            // ***************************** ACT ******************************
            IRecording recDecoded = SerializeAndDeserialize(rec, level, TimeStorageTechnique.BST16);

            // **************************** ASSERT ****************************
            AssertRecordingsEqual(rec, recDecoded, 0);

            var posCollection = recDecoded.GetCollection<Capture<Vector3>>();
            Assert.NotNull(posCollection);
            Assert.AreEqual(0, posCollection.Captures.Length);
            Assert.AreEqual("Example Position Collection", posCollection.Name);
            Assert.AreEqual("recolude.position", posCollection.Signature);
        }

        [Test]
        [TestCase(CompressionLevel.NoCompression)]
        [TestCase(CompressionLevel.Optimal)]
        [TestCase(CompressionLevel.Fastest)]
        public void WriteRead_SingleRecording_SinglePositionCaptureCollection(
            CompressionLevel level)
        {
            // *************************** ARRANGE ****************************
            var rec = new Recording(
                "single collection",
                "WriteRead_SingleRecording_SinglePositionCaptureCollection",
                new IRecording[] { },
                new ICaptureCollection<ICapture>[]
                {
                    new PositionCollection("Example Position Collection",
                        new Capture<Vector3>[]
                        {
                            new VectorCapture(0, new Vector3(0, 1, 2))
                        })
                },
                new Metadata(),
                new IBinary[0],
                new BinaryReference[0]
            );

            // ***************************** ACT ******************************
            IRecording recDecoded = SerializeAndDeserialize(rec, level, TimeStorageTechnique.BST16);

            // **************************** ASSERT ****************************
            AssertRecordingsEqual(rec, recDecoded, 0);

            var posCollection = recDecoded.GetCollection<Capture<Vector3>>();
            Assert.NotNull(posCollection);
            Assert.AreEqual(1, posCollection.Captures.Length);
            Assert.AreEqual("Example Position Collection", posCollection.Name);
            Assert.AreEqual("recolude.position", posCollection.Signature);
            Assert.AreEqual(0, posCollection.Captures[0].Time);
            Assert.AreEqual(0, posCollection.Captures[0].Value.x);
            Assert.AreEqual(1, posCollection.Captures[0].Value.y);
            Assert.AreEqual(2, posCollection.Captures[0].Value.z);
        }

        [Test]
        [TestCase(CompressionLevel.NoCompression)]
        [TestCase(CompressionLevel.Optimal)]
        [TestCase(CompressionLevel.Fastest)]
        public void WriteRead_MultipleRecording_SinglePositionCaptureCollection(
            CompressionLevel level)
        {
            // *************************** ARRANGE ****************************
            var rec = new Recording(
                "parent single collection",
                "WriteRead_MultipleRecording_SinglePositionCaptureCollection",
                new IRecording[]
                {
                    new Recording(
                        "child1",
                        "Child 1",
                        new IRecording[] { },
                        new ICaptureCollection<ICapture>[]
                        {
                            new PositionCollection(new Capture<Vector3>[]
                            {
                                new VectorCapture(0, new Vector3(0, 1, 2))
                            })
                        },
                        new Metadata()
                    ),
                    new Recording(
                        "child2",
                        "Child 2",
                        new IRecording[] { },
                        new ICaptureCollection<ICapture>[]
                        {
                        },
                        new Metadata()
                    ),
                    new Recording(
                        "child3",
                        "Child 3",
                        new IRecording[] { },
                        new ICaptureCollection<ICapture>[]
                        {
                            new PositionCollection("Example Position Collection",
                                new Capture<Vector3>[]
                                {
                                    new VectorCapture(0, new Vector3(0, 1, 2)),
                                    new VectorCapture(1, new Vector3(4, 3, 2)),
                                    new VectorCapture(2, new Vector3(6, 7, 8)),
                                    new VectorCapture(3, new Vector3(1, 1, 1))
                                })
                        },
                        new Metadata()
                    )
                },
                new ICaptureCollection<ICapture>[]
                {
                    new PositionCollection("Example Position Collection",
                        new Capture<Vector3>[]
                        {
                            new VectorCapture(0, new Vector3(0, 1, 2)),
                            new VectorCapture(2, new Vector3(3, 4, 5))
                        })
                },
                new Metadata()
            );

            // ***************************** ACT ******************************
            IRecording recDecoded = SerializeAndDeserialize(rec, level, TimeStorageTechnique.BST16);

            // **************************** ASSERT ****************************
            AssertRecordingsEqual(rec, recDecoded, 0.0001f);

            var parentPosCollection = recDecoded.GetCollection<Capture<Vector3>>();
            Assert.NotNull(parentPosCollection);
            Assert.AreEqual(2, parentPosCollection.Captures.Length);
            Assert.AreEqual(0, parentPosCollection.Captures[0].Value.x);
            Assert.AreEqual(1, parentPosCollection.Captures[0].Value.y);
            Assert.AreEqual(2, parentPosCollection.Captures[0].Value.z);
            Assert.AreEqual(3, parentPosCollection.Captures[1].Value.x);
            Assert.AreEqual(4, parentPosCollection.Captures[1].Value.y);
            Assert.AreEqual(5, parentPosCollection.Captures[1].Value.z);

            var child1PosCol = recDecoded.Recordings[0].GetCollection<Capture<Vector3>>();
            Assert.NotNull(child1PosCol);
            Assert.AreEqual(1, child1PosCol.Captures.Length);
            Assert.AreEqual(0, child1PosCol.Captures[0].Value.x);
            Assert.AreEqual(1, child1PosCol.Captures[0].Value.y);
            Assert.AreEqual(2, child1PosCol.Captures[0].Value.z);

            var child2PosCol = recDecoded.Recordings[1].GetCollection<Capture<Vector3>>();
            Assert.Null(child2PosCol);

            var child3PosCol = recDecoded.Recordings[2].GetCollection<Capture<Vector3>>();
            Assert.NotNull(child3PosCol);
            Assert.AreEqual(4, child3PosCol.Captures.Length);
            AssertUtils.InDelta(0, child3PosCol.Captures[0].Value.x, 0.1f);
            AssertUtils.InDelta(1, child3PosCol.Captures[0].Value.y, 0.1f);
            AssertUtils.InDelta(2, child3PosCol.Captures[0].Value.z, 0.1f);

            AssertUtils.InDelta(4, child3PosCol.Captures[1].Value.x, 0.1f);
            AssertUtils.InDelta(3, child3PosCol.Captures[1].Value.y, 0.1f);
            AssertUtils.InDelta(2, child3PosCol.Captures[1].Value.z, 0.1f);

            AssertUtils.InDelta(6, child3PosCol.Captures[2].Value.x, 0.1f);
            AssertUtils.InDelta(7, child3PosCol.Captures[2].Value.y, 0.1f);
            AssertUtils.InDelta(8, child3PosCol.Captures[2].Value.z, 0.1f);

            AssertUtils.InDelta(1, child3PosCol.Captures[3].Value.x, 0.1f);
            AssertUtils.InDelta(1, child3PosCol.Captures[3].Value.y, 0.1f);
            AssertUtils.InDelta(1, child3PosCol.Captures[3].Value.z, 0.1f);
        }

        [Test]
        [TestCase(CompressionLevel.NoCompression)]
        [TestCase(CompressionLevel.Optimal)]
        [TestCase(CompressionLevel.Fastest)]
        public void WriteRead_MultipleRecording_MultipleCaptureCollection_MetadataAndStuff(
            CompressionLevel level)
        {
            // *************************** ARRANGE ****************************
            var rec = new Recording(
                "parent single collection",
                "WriteRead_MultipleRecording_MultipleCaptureCollection",
                new IRecording[]
                {
                    new Recording(
                        "child1",
                        "Child 1",
                        new IRecording[] { },
                        new ICaptureCollection<ICapture>[]
                        {
                            new PositionCollection(new Capture<Vector3>[]
                            {
                                new VectorCapture(0, new Vector3(0, 1, 2))
                            }),
                            new EulerRotationCollection(new Capture<Vector3>[]
                            {
                                new VectorCapture(0, new Vector3(0, 1, 2))
                            })
                        },
                        new Metadata(new Dictionary<string, IProperty>()
                        {
                            { "childType", new Property<string>("child 1") },
                            { "numStreams", new IntProperty(0) }
                        })
                    ),
                    new Recording(
                        "child2",
                        "Child 2",
                        new IRecording[] { },
                        new ICaptureCollection<ICapture>[]
                        {
                        },
                        new Metadata(new Dictionary<string, IProperty>()
                        {
                            { "childType", new Property<string>("empty child") }
                        })
                    ),
                    new Recording(
                        "child3",
                        "Child 3",
                        new IRecording[] { },
                        new ICaptureCollection<ICapture>[]
                        {
                            new PositionCollection("Example Position Collection",
                                new Capture<Vector3>[]
                                {
                                    new VectorCapture(0, new Vector3(0, 1, 2)),
                                    new VectorCapture(1, new Vector3(4, 3, 2)),
                                    new VectorCapture(2, new Vector3(6, 7, 8)),
                                    new VectorCapture(3, new Vector3(1, 1, 1))
                                }
                            ),
                            new CustomEventCollection(new Capture<CustomEvent>[]
                            {
                                new CustomEventCapture(
                                    -89,
                                    new CustomEvent("event 1", new Metadata(
                                        new Dictionary<string, IProperty>()
                                        {
                                            { "dude its still going", new FloatProperty(69) }
                                        }
                                    ))
                                )
                            })
                        },
                        new Metadata(new Dictionary<string, IProperty>()
                        {
                            { "childType", new Property<string>("more stuff") }
                        })
                    )
                },
                new ICaptureCollection<ICapture>[]
                {
                    new PositionCollection(
                        "Example Position Collection",
                        new Capture<Vector3>[]
                        {
                            new VectorCapture(0, new Vector3(0, 1, 2)),
                            new VectorCapture(2, new Vector3(3, 4, 5))
                        }
                    ),
                    new LifeCycleCollection(new Capture<UnityLifeCycleEvent>[]
                    {
                        new UnityLifeCycleEventCapture(12, UnityLifeCycleEvent.Start),
                        new UnityLifeCycleEventCapture(13, UnityLifeCycleEvent.Destroy),
                    })
                },
                new Metadata(new Dictionary<string, IProperty>()
                {
                    { "childType", new Property<string>("not a child") }
                })
            );

            // ***************************** ACT ******************************
            IRecording recDecoded = SerializeAndDeserialize(rec, level, TimeStorageTechnique.BST16);

            // **************************** ASSERT ****************************
            AssertRecordingsEqual(rec, recDecoded, 0.0001f);

            // Parent =========================================================
            Assert.AreEqual(2, recDecoded.CaptureCollections.Length);
            Assert.AreEqual(1, recDecoded.Metadata.Count);
            Assert.AreEqual("not a child", recDecoded.Metadata.AsString("childType"));

            var parentPosCollection = recDecoded.GetCollection<Capture<Vector3>>();
            Assert.NotNull(parentPosCollection);
            Assert.AreEqual(2, parentPosCollection.Captures.Length);
            Assert.AreEqual(0, parentPosCollection.Captures[0].Value.x);
            Assert.AreEqual(1, parentPosCollection.Captures[0].Value.y);
            Assert.AreEqual(2, parentPosCollection.Captures[0].Value.z);
            Assert.AreEqual(3, parentPosCollection.Captures[1].Value.x);
            Assert.AreEqual(4, parentPosCollection.Captures[1].Value.y);
            Assert.AreEqual(5, parentPosCollection.Captures[1].Value.z);

            var parentLifeCycleCollection = recDecoded.LifeCycleCollection();
            Assert.NotNull(parentLifeCycleCollection);
            Assert.AreEqual(UnityLifeCycleEvent.Start, parentLifeCycleCollection.Captures[0].Value);
            Assert.AreEqual(UnityLifeCycleEvent.Destroy, parentLifeCycleCollection.Captures[1].Value);

            // Child 1 ========================================================
            Assert.AreEqual(2, recDecoded.Recordings[0].CaptureCollections.Length);
            Assert.AreEqual(2, recDecoded.Recordings[0].Metadata.Count);
            Assert.AreEqual("child 1", recDecoded.Recordings[0].Metadata.AsString("childType"));
            Assert.AreEqual(0, recDecoded.Recordings[0].Metadata.AsInt("numStreams"));

            var child1PosCol = recDecoded.Recordings[0].GetCollection<Capture<Vector3>>();
            Assert.NotNull(child1PosCol);
            Assert.AreEqual(1, child1PosCol.Captures.Length);
            Assert.AreEqual(0, child1PosCol.Captures[0].Value.x);
            Assert.AreEqual(1, child1PosCol.Captures[0].Value.y);
            Assert.AreEqual(2, child1PosCol.Captures[0].Value.z);

            var child1RotCol = recDecoded.Recordings[0].RotationCollection();
            Assert.NotNull(child1RotCol);
            Assert.AreEqual(1, child1RotCol.Captures.Length);
            Assert.AreEqual(0, child1RotCol.Captures[0].Value.x);
            Assert.AreEqual(1, child1RotCol.Captures[0].Value.y);
            Assert.AreEqual(2, child1RotCol.Captures[0].Value.z);

            // Child 2 ========================================================
            Assert.AreEqual(0, recDecoded.Recordings[1].CaptureCollections.Length);
            Assert.AreEqual(1, recDecoded.Recordings[1].Metadata.Count);
            Assert.AreEqual("empty child", recDecoded.Recordings[1].Metadata.AsString("childType"));
            var child2PosCol = recDecoded.Recordings[1].GetCollection<Capture<Vector3>>();
            Assert.Null(child2PosCol);

            // Child 3 ========================================================
            Assert.AreEqual(2, recDecoded.Recordings[2].CaptureCollections.Length);
            Assert.AreEqual(1, recDecoded.Recordings[2].Metadata.Count);
            Assert.AreEqual("more stuff", recDecoded.Recordings[2].Metadata.AsString("childType"));

            var child3PosCol = recDecoded.Recordings[2].GetCollection<Capture<Vector3>>();
            Assert.NotNull(child3PosCol);
            Assert.AreEqual(4, child3PosCol.Captures.Length);
            AssertUtils.InDelta(0, child3PosCol.Captures[0].Value.x, 0.1f);
            AssertUtils.InDelta(1, child3PosCol.Captures[0].Value.y, 0.1f);
            AssertUtils.InDelta(2, child3PosCol.Captures[0].Value.z, 0.1f);

            AssertUtils.InDelta(4, child3PosCol.Captures[1].Value.x, 0.1f);
            AssertUtils.InDelta(3, child3PosCol.Captures[1].Value.y, 0.1f);
            AssertUtils.InDelta(2, child3PosCol.Captures[1].Value.z, 0.1f);

            AssertUtils.InDelta(6, child3PosCol.Captures[2].Value.x, 0.1f);
            AssertUtils.InDelta(7, child3PosCol.Captures[2].Value.y, 0.1f);
            AssertUtils.InDelta(8, child3PosCol.Captures[2].Value.z, 0.1f);

            AssertUtils.InDelta(1, child3PosCol.Captures[3].Value.x, 0.1f);
            AssertUtils.InDelta(1, child3PosCol.Captures[3].Value.y, 0.1f);
            AssertUtils.InDelta(1, child3PosCol.Captures[3].Value.z, 0.1f);

            var child3EventCol = recDecoded.Recordings[2].CustomEventCollection();
            Assert.NotNull(child3EventCol);
            Assert.AreEqual(1, child3EventCol.Captures.Length);
            Assert.AreEqual("event 1", child3EventCol.Captures[0].Value.Name);
            Assert.AreEqual(69f, child3EventCol.Captures[0].Value.Contents.AsFloat("dude its still going"));
        }

        [Test]
        [TestCase(CompressionLevel.NoCompression, TimeStorageTechnique.BST16)]
        [TestCase(CompressionLevel.Optimal, TimeStorageTechnique.BST16)]
        [TestCase(CompressionLevel.Fastest, TimeStorageTechnique.BST16)]
        [TestCase(CompressionLevel.NoCompression, TimeStorageTechnique.Raw32)]
        [TestCase(CompressionLevel.Optimal, TimeStorageTechnique.Raw32)]
        [TestCase(CompressionLevel.Fastest, TimeStorageTechnique.Raw32)]
        [TestCase(CompressionLevel.NoCompression, TimeStorageTechnique.Raw64)]
        [TestCase(CompressionLevel.Optimal, TimeStorageTechnique.Raw64)]
        [TestCase(CompressionLevel.Fastest, TimeStorageTechnique.Raw64)]
        public void WriteRead_StressTest(CompressionLevel level,
            TimeStorageTechnique timeStorageTechnique)
        {
            // *************************** ARRANGE ****************************
            int numCaptures = 1000;
            var posCaptures = Encoders.EncoderTestUtils.RandomPositionCaptures(numCaptures);
            var rotCaptures = Encoders.EncoderTestUtils.RandomRotationCaptures(numCaptures);

            int numChildRecordings = 100;
            var childRecordings = new IRecording[numChildRecordings];

            for (int i = 0; i < childRecordings.Length; i++)
            {
                childRecordings[i] = new Recording(
                    i.ToString(),
                    string.Format("Child {0}", i),
                    null,
                    new ICaptureCollection<ICapture>[]
                    {
                        new PositionCollection(posCaptures),
                        new EulerRotationCollection(rotCaptures)
                    },
                    new Metadata(new Dictionary<string, IProperty>()
                    {
                        { "cur-child", new IntProperty(i) }
                    })
                );
            }

            var rec = new Recording(
                "p",
                "parent",
                childRecordings,
                new ICaptureCollection<ICapture>[]
                {
                    new PositionCollection(posCaptures),
                    // new EulerRotationCollection(rotCaptures)
                },
                new Metadata()
            );

            // ***************************** ACT ******************************
            IRecording recDecoded = SerializeAndDeserialize(rec, level, timeStorageTechnique);

            // **************************** ASSERT ****************************
            // AssertRecordingsEqual(rec, recDecoded, 0.0001f);

            Assert.AreEqual("p", recDecoded.ID);
            Assert.AreEqual("parent", recDecoded.Name);
            Assert.AreEqual(numChildRecordings, recDecoded.Recordings.Length);
            Encoders.EncoderTestUtils.AssertPositionCollectionsEqual(new PositionCollection(posCaptures),
                recDecoded.PositionCollection(), 0.1f, 0.001f);
            // Tests.IO.Encoders.EncoderTestUtils.AssertRotationCollectionsEqual(new EulerRotationCollection(rotCaptures), recDecoded.RotationCollection(), 0.01f, 0.001f);


            for (int i = 0; i < recDecoded.Recordings.Length; i++)
            {
                var childDecoded = recDecoded.Recordings[i];
                Assert.AreEqual(i.ToString(), childDecoded.ID);
                Assert.AreEqual(string.Format("Child {0}", i), childDecoded.Name);
                Encoders.EncoderTestUtils.AssertPositionCollectionsEqual(new PositionCollection(posCaptures),
                    childDecoded.PositionCollection(), 0.1f, 0.001f);
                Encoders.EncoderTestUtils.AssertRotationCollectionsEqual(
                    new EulerRotationCollection(rotCaptures), childDecoded.RotationCollection(), 0.01f, 0.001f);
                Assert.AreEqual(i, childDecoded.Metadata.AsInt("cur-child"));
            }
        }
    }
}