using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System.IO;
using System.Text;

using Recolude.Core.IO;

namespace Tests.IO
{
    public class InMemoryBinaryStorageTests
    {

        [Test]
        public void Exists_FalseWhenNotExists()
        {
            // *************************** ARRANGE ***************************
            var storage = new InMemoryBinaryStorage();

            // ***************************** ACT *****************************
            var result = storage.Exists("something");

            // **************************** ASSERT ****************************
            Assert.IsFalse(result);
        }

        [Test]
        public void Exists_TrueWhenExists()
        {
            // *************************** ARRANGE ***************************
            var storage = new InMemoryBinaryStorage();
            storage.Write(new MemoryStream(new byte[0]), "something");

            // ***************************** ACT *****************************
            var result = storage.Exists("something");

            // **************************** ASSERT ****************************
            Assert.IsTrue(result);
        }

        [Test]
        public void Delete_RemovesEntry()
        {
            // *************************** ARRANGE ***************************
            var storage = new InMemoryBinaryStorage();
            storage.Write(new MemoryStream(new byte[0]), "something");
            storage.Delete("something");

            // ***************************** ACT *****************************
            var result = storage.Exists("something");

            // **************************** ASSERT ****************************
            Assert.IsFalse(result);
        }

        [Test]
        public void Read_ReadsBackProper()
        {
            // *************************** ARRANGE ***************************
            var storage = new InMemoryBinaryStorage();
            storage.Write(new MemoryStream(Encoding.UTF8.GetBytes("Test !!!")), "something");

            // ***************************** ACT *****************************
            var resultStream = storage.Read("something");

            // **************************** ASSERT ****************************
            Assert.NotNull(resultStream);
            using (var memoryStream = new MemoryStream())
            {
                resultStream.CopyTo(memoryStream);
                Assert.AreEqual("Test !!!", Encoding.UTF8.GetString(memoryStream.ToArray()));
            }
        }

    }

}