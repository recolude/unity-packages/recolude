﻿using System.IO.Compression;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

using Recolude.Core.IO;
using Recolude.Core;

namespace Tests.IO
{
    public class RAPBinaryTests
    {
        [Test]
        public void WriteReadUVarInt()
        {
            // *************************** ARRANGE ***************************
            var numsToSeralize = new int[10000];
            var numsDeserialized = new int[10000];
            for (int i = 0; i < numsToSeralize.Length; i++)
            {
                numsToSeralize[i] = i;
            }

            var mem = new MemoryStream();

            // ***************************** ACT *****************************
            foreach (var num in numsToSeralize)
            {
                RAPBinary.WriteUVarInt(mem, num);
            }
            mem.Seek(0, SeekOrigin.Begin);
            for (int i = 0; i < numsDeserialized.Length; i++)
            {
                numsDeserialized[i] = RAPBinary.ReadUVarInt(mem);
            }

            // **************************** ASSERT ****************************
            for (int i = 0; i < numsToSeralize.Length; i++)
            {
                Assert.AreEqual(numsToSeralize[i], numsDeserialized[i]);
            }
        }

        [Test]
        public void WriteReadUVarInt_Compressed()
        {
            // *************************** ARRANGE ***************************
            var numsToSeralize = new int[10000];
            var numsDeserialized = new int[10000];
            var memStrm = new MemoryStream();
            for (int i = 0; i < numsToSeralize.Length; i++)
            {
                numsToSeralize[i] = i;
            }

            // ***************************** ACT *****************************
            using (var compressor = new DeflateStream(memStrm, CompressionMode.Compress, true))
            {
                foreach (var num in numsToSeralize)
                {
                    RAPBinary.WriteUVarInt(compressor, num);
                }
            }

            memStrm.Seek(0, SeekOrigin.Begin);

            using (var decompressor = new DeflateStream(memStrm, CompressionMode.Decompress))
            {
                for (int i = 0; i < numsDeserialized.Length; i++)
                {
                    numsDeserialized[i] = RAPBinary.ReadUVarInt(decompressor);
                }
            }

            // **************************** ASSERT ****************************
            for (int i = 0; i < numsToSeralize.Length; i++)
            {
                Assert.AreEqual(numsToSeralize[i], numsDeserialized[i]);
            }
        }
    }
}