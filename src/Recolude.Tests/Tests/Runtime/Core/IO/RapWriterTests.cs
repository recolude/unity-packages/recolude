using System.IO.Compression;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

using Recolude.Core.IO;
using Recolude.Core;

namespace Tests.IO
{
    public class RapWriterTests
    {
        [Test]
        public void WriteReadUVarInt()
        {
            // *************************** ARRANGE ***************************
            int numsToRun = 1000;
            var numsToSeralize = new int[numsToRun];
            var numsDeserialized = new int[numsToRun];
            for (int i = 0; i < numsToSeralize.Length; i++)
            {
                numsToSeralize[i] = i;
            }

            var mem = new MemoryStream();

            // ***************************** ACT *****************************
            using (var writer = new RAPWriter(mem, true))
            {
                foreach (var num in numsToSeralize)
                {
                    writer.WriteAsUVarInt(num);
                }
            }

            mem.Seek(0, SeekOrigin.Begin);
            for (int i = 0; i < numsDeserialized.Length; i++)
            {
                numsDeserialized[i] = RAPBinary.ReadUVarInt(mem);
            }

            mem.Seek(0, SeekOrigin.Begin);
            // **************************** ASSERT ****************************
            for (int i = 0; i < numsToSeralize.Length; i++)
            {
                Assert.AreEqual(numsToSeralize[i], numsDeserialized[i]);
            }
        }

    }

}