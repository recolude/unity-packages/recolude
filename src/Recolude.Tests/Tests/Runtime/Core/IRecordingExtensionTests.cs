using NUnit.Framework;
using UnityEngine;

using Recolude.Core;

namespace Tests
{
    public class IRecordingExtensionTests
    {
        [Test]
        public void CastCollectionCorrectly()
        {
            var posCollection = new PositionCollection("Test Name", new Capture<Vector3>[0]);

            var rec = new Recording(
                "1",
                "name",
                null,
                new ICaptureCollection<ICapture>[] {
                    posCollection,
                },
                null,
                null,
                null
            );

            var collection = rec.GetCollection<Capture<Vector3>>("Test Name");

            Assert.NotNull(collection);
            Assert.AreEqual(collection, posCollection);
        }
    }
}