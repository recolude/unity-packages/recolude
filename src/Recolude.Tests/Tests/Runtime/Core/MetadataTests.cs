using NUnit.Framework;
using UnityEngine;
using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using Recolude.Core;
using Recolude.Core.IO;
using Recolude.Core.IO.PropertyWriters;
using Recolude.Core.Properties;
using PropertyAttribute = NUnit.Framework.PropertyAttribute;

namespace Tests
{
    public class MetadataTests
    {
        [Test]
        public void ConvertsNormalStringStringDict()
        {
            // ARRANGE ========================================================
            var inDict = new Dictionary<string, string>()
            {
                { "a", "b" },
                { "c", "d" },
            };

            // ACT ============================================================
            var block = new Metadata(inDict);

            // ASSERT =========================================================
            Assert.AreEqual(inDict.Count, block.Count);
            Assert.AreEqual(inDict["a"], block.AsString("a"));
            Assert.AreEqual(inDict["c"], block.AsString("c"));
        }

        [Test]
        public void ToJSON_Empty()
        {
            // ARRANGE ========================================================
            var block = new Metadata();

            // ACT ============================================================
            var json = PropertyWriterCollection.Instance.JsonValue(block);

            // ASSERT =========================================================
            Assert.AreEqual("{}", json);
        }

        [Test]
        public void ToJSON_SingleStringEntry()
        {
            // ARRANGE ========================================================
            var block = new Metadata();
            block["test"] = new Property<string>("Value!!");

            // ACT ============================================================
            var json = PropertyWriterCollection.Instance.JsonValue(block);

            // ASSERT =========================================================
            Assert.AreEqual("{\"test\": \"Value!!\"}", json);
        }

        [Test]
        public void ToJSON_SingleIntEntry()
        {
            // ARRANGE ========================================================
            var block = new Metadata();
            block["int test"] = new IntProperty(18);

            // ACT ============================================================
            var json = PropertyWriterCollection.Instance.JsonValue(block);

            // ASSERT =========================================================
            Assert.AreEqual("{\"int test\": 18}", json);
        }

        [Test]
        public void ToJSON_SingleFloatEntry()
        {
            // ARRANGE ========================================================
            var block = new Metadata();
            block["f test"] = new FloatProperty(1.1f);

            // ACT ============================================================
            var json = PropertyWriterCollection.Instance.JsonValue(block);

            // ASSERT =========================================================
            Assert.AreEqual("{\"f test\": 1.1}", json);
        }

        [Test]
        public void ToJSON_BoolEntries()
        {
            // ARRANGE ========================================================
            var block = new Metadata();
            block["t test"] = new Property<bool>(true);
            block["f test"] = new Property<bool>(false);

            // ACT ============================================================
            var json = PropertyWriterCollection.Instance.JsonValue(block);

            // ASSERT =========================================================
            Assert.AreEqual("{\"t test\": true,\"f test\": false}", json);
        }

        [Test]
        public void ToJSON_IntArrayEntries()
        {
            // ARRANGE ========================================================
            var block = new Metadata();
            block["i arr"] = new ArrayProperty<int>(1, 2, 3);

            // ACT ============================================================
            var json = PropertyWriterCollection.Instance.JsonValue(block);

            // ASSERT =========================================================
            Assert.AreEqual("{\"i arr\": [1,2,3]}", json);
        }

        [Test]
        public void ToJSON_SubObjEntries()
        {
            // ARRANGE ========================================================
            var sub = new Metadata();
            sub["b"] = new Property<byte>(12);

            var block = new Metadata();
            block["m val"] = new MetadataProperty(sub);

            // ACT ============================================================
            var json = PropertyWriterCollection.Instance.JsonValue(block);

            // ASSERT =========================================================
            Assert.AreEqual("{\"m val\": {\"b\": 12}}", json);
        }

        [Test]
        public void Casting()
        {
            // ARRANGE ========================================================
            var block = new Metadata();
            block["string"] = new Property<string>("property");
            block["integer"] = new IntProperty(5);

            // ACT/ASSERT =====================================================
            Assert.AreEqual(2, block.Count);

            Assert.AreEqual("property", block.AsString("string"));
            Assert.AreEqual("property", block.AsType<string>("string"));
            Assert.IsTrue(block.IsString("string"));
            Assert.IsFalse(block.IsInt("string"));

            Assert.AreEqual(5, block.AsInt("integer"));
            Assert.AreEqual(5, block.AsType<int>("integer"));
            Assert.IsFalse(block.IsString("integer"));
            Assert.IsTrue(block.IsInt("integer"));
        }

        [Test]
        public void Reset()
        {
            // ARRANGE ========================================================
            var block = new Metadata();
            block["string"] = new Property<string>("property");
            block["integer"] = new IntProperty(5);

            // ACT/ASSERT =====================================================
            Assert.AreEqual(2, block.Count);
            Assert.IsTrue(block.ContainsKey("string"));
            Assert.IsTrue(block.ContainsKey("integer"));

            block.Reset();

            Assert.AreEqual(0, block.Count);
            Assert.IsFalse(block.ContainsKey("string"));
            Assert.IsFalse(block.ContainsKey("integer"));
        }

        [Test]
        public void Write_Read_EmptyBlock()
        {
            // ARRANGE ========================================================
            var block = new Metadata();

            // ACT ============================================================
            Metadata blockBack;

            using (var memory = new MemoryStream())
            {
                using (var writer = new BinaryWriter(memory, new UTF8Encoding(false, true), true))
                {
                    block.Write(writer);
                }

                memory.Seek(0, SeekOrigin.Begin);
                using (var binReader = new BinaryUtilReader(memory))
                {
                    blockBack = Metadata.Read(binReader);
                }
            }

            // ASSERT =========================================================
            Assert.AreEqual(block.Count, blockBack.Count);
        }

        [Test]
        public void Read_New_Date()
        {
            // ACT ============================================================
            Metadata blockBack;

            using (var memory = new MemoryStream())
            {
                using (var writer = new BinaryWriter(memory, new UTF8Encoding(false, true), true))
                {
                    long bdayInUnixNano = 817551324000000; // 1995, time.November, 28, 9, 35, 24, 0, time.UTC
                    writer.Write((byte)1); // number of entries

                    writer.Write((byte)4); // length of first entries name
                    writer.Write(Encoding.UTF8.GetBytes("bday")); // the name

                    writer.Write((byte)12); // DateTime's property type
                    writer.Write(bdayInUnixNano);
                }

                memory.Seek(0, SeekOrigin.Begin);
                using (var binReader = new BinaryUtilReader(memory))
                {
                    blockBack = Metadata.Read(binReader);
                }
            }

            // ASSERT =========================================================
            Assert.AreEqual(1, blockBack.Count);

            Assert.AreEqual(1995, blockBack.AsTime("bday").Year);
            Assert.AreEqual(11, blockBack.AsTime("bday").Month);
            Assert.AreEqual(28, blockBack.AsTime("bday").Day);
            Assert.AreEqual(9, blockBack.AsTime("bday").Hour);
            Assert.AreEqual(35, blockBack.AsTime("bday").Minute);
            Assert.AreEqual(24, blockBack.AsTime("bday").Second);
            Assert.AreEqual(0, blockBack.AsTime("bday").Millisecond);
        }

        [Test]
        public void Read_Old_Date()
        {
            // ACT ============================================================
            Metadata blockBack;

            using (var memory = new MemoryStream())
            {
                using (var writer = new BinaryWriter(memory, new UTF8Encoding(false, true), true))
                {
                    long oldDayInUnixNano = -32185104833000000; // 950, time.February, 4, 5, 6, 7, 0, time.UTC
                    writer.Write((byte)1); // number of entries

                    writer.Write((byte)3); // length of 2nd entries name
                    writer.Write(Encoding.UTF8.GetBytes("old")); // the name

                    writer.Write((byte)12); // DateTime's property type
                    writer.Write(oldDayInUnixNano);
                }

                memory.Seek(0, SeekOrigin.Begin);
                using (var binReader = new BinaryUtilReader(memory))
                {
                    blockBack = Metadata.Read(binReader);
                }
            }

            // ASSERT =========================================================
            Assert.AreEqual(1, blockBack.Count);

            Assert.AreEqual(950, blockBack.AsTime("old").Year);
            Assert.AreEqual(2, blockBack.AsTime("old").Month);
            Assert.AreEqual(4, blockBack.AsTime("old").Day);
            Assert.AreEqual(5, blockBack.AsTime("old").Hour);
            Assert.AreEqual(6, blockBack.AsTime("old").Minute);
            Assert.AreEqual(7, blockBack.AsTime("old").Second);
            Assert.AreEqual(0, blockBack.AsTime("old").Millisecond);
        }

        [Test]
        public void Write_Read_MetadataArray()
        {
            // ARRANGE ========================================================
            var block = new Metadata(new Dictionary<string, IProperty>()
            {
                { "sub", new ArrayProperty<Metadata>(new Metadata()) }
            });

            // ACT ============================================================
            Metadata blockBack;

            using (var memory = new MemoryStream())
            {
                using (var writer = new BinaryWriter(memory, new UTF8Encoding(false, true), true))
                {
                    block.Write(writer);
                }

                memory.Seek(0, SeekOrigin.Begin);
                using (var binReader = new BinaryUtilReader(memory))
                {
                    blockBack = Metadata.Read(binReader);
                }
            }

            // ASSERT =========================================================
            Assert.AreEqual(block.Count, blockBack.Count);
        }

        [Test]
        public void Write_Read()
        {
            // ARRANGE ========================================================
            var vec2 = new Vector2(-99.99991f, 10021.12f);
            var vec3 = new Vector3(-77222.1f, 8891.21f, 91051.151f);
            var time = new DateTime(1995, 11, 28, 9, 30, 59, 234);

            var nested = new Metadata(new Dictionary<string, IProperty>()
            {
                { "another-one-x", new FloatProperty(-111f) },
                { "another-one-y", new FloatProperty(22f) },
                { "another-one-z", new FloatProperty(6831.212f) },
                {
                    "norm",
                    new MetadataProperty(new Dictionary<string, IProperty>()
                    {
                        { "col-x", new IntProperty(55) },
                        { "col-y", new IntProperty(-110002) }
                    })
                }
            });

            var block = new Metadata
            {
                ["string"] = new Property<string>("property"),
                ["integer"] = new IntProperty(5),
                ["float"] = new FloatProperty(12.34f),
                ["bool-false"] = new Property<bool>(false),
                ["bool-true"] = new Property<bool>(true),
                ["byte"] = new Property<byte>(17),
                ["vec2"] = new Vector2Property(vec2),
                ["vec3"] = new Vector3Property(vec3),
                ["child-metadata"] = new MetadataProperty(nested),
                ["time"] = new TimeProperty(time),
                ["empty-string-array"] = new ArrayProperty<string>(new string[] { }),
                ["one-string-array"] = new ArrayProperty<string>(new string[] { "a" }),
                ["two-string-array"] = new ArrayProperty<string>(new string[] { "9", "10" }),
                ["empty-int-array"] = new ArrayProperty<int>(new int[] { }),
                ["one-int-array"] = new ArrayProperty<int>(new int[] { 9 }),
                ["two-int-array"] = new ArrayProperty<int>(new int[] { 13, -999 }),
                ["empty-float-array"] = new ArrayProperty<float>(new float[] { }),
                ["one-float-array"] = new ArrayProperty<float>(new float[] { 9 }),
                ["two-float-array"] = new ArrayProperty<float>(new float[] { 13, -999 }),
                ["empty-bool-array"] = new ArrayProperty<bool>(new bool[] { }),
                ["one-bool-array"] = new ArrayProperty<bool>(new bool[] { true }),
                ["two-bool-array"] = new ArrayProperty<bool>(new bool[] { true, false }),
                ["empty-byte-array"] = new ArrayProperty<byte>(new byte[] { }),
                ["one-byte-array"] = new ArrayProperty<byte>(new byte[] { 112 }),
                ["two-byte-array"] = new ArrayProperty<byte>(new byte[] { 99, 250 }),
                ["empty-vec2-array"] = new ArrayProperty<Vector2>(new Vector2[] { }),
                ["one-vec2-array"] = new ArrayProperty<Vector2>(new Vector2[] { new Vector2(1, 2) }),
                ["two-vec2-array"] = new ArrayProperty<Vector2>(new Vector2[] { new Vector2(3, 4), new Vector2(5, 6) }),
                ["empty-vec3-array"] = new ArrayProperty<Vector3>(new Vector3[] { }),
                ["one-vec3-array"] = new ArrayProperty<Vector3>(new Vector3[] { new Vector3(1, 2, 3) }),
                ["two-vec3-array"] =
                    new ArrayProperty<Vector3>(new Vector3[] { new Vector3(4, 5, 6), new Vector3(7, 8, 9) }),
                ["empty-time-array"] = new ArrayProperty<DateTime>(new DateTime[] { }),
                ["one-time-array"] = new ArrayProperty<DateTime>(new DateTime(1, 2, 3)),
                ["two-time-array"] = new ArrayProperty<DateTime>(new DateTime(4, 5, 6), new DateTime(7, 8, 9)),
                ["empty-metadata-array"] = new ArrayProperty<Metadata>(),
                ["one-metadata-array"] = new ArrayProperty<Metadata>(new Metadata()),
                ["two-metadata-array"] = new ArrayProperty<Metadata>(new Metadata[]
                {
                    nested,
                    nested
                })
            };

            // ACT ============================================================
            Metadata blockBack;

            using (var memory = new MemoryStream())
            {
                using (var writer = new BinaryWriter(memory, new UTF8Encoding(false, true), true))
                {
                    block.Write(writer);
                }

                memory.Seek(0, SeekOrigin.Begin);
                using (var binReader = new BinaryUtilReader(memory))
                {
                    blockBack = Metadata.Read(binReader);
                }
            }

            // ASSERT =========================================================
            Assert.AreEqual(block.Count, blockBack.Count);
            Assert.AreEqual("property", blockBack.AsString("string"));
            Assert.AreEqual(5, blockBack.AsInt("integer"));
            Assert.AreEqual(12.34f, blockBack.AsFloat("float"));
            Assert.AreEqual(false, blockBack.AsBool("bool-false"));
            Assert.AreEqual(true, blockBack.AsBool("bool-true"));
            Assert.AreEqual(17, blockBack.AsByte("byte"));
            Assert.AreEqual(vec2, blockBack.AsVector2("vec2"));
            Assert.AreEqual(vec3, blockBack.AsVector3("vec3"));
            Assert.AreEqual(-111f, blockBack.AsMetadata("child-metadata").AsFloat("another-one-x"));
            Assert.AreEqual(22f, blockBack.AsMetadata("child-metadata").AsFloat("another-one-y"));
            Assert.AreEqual(6831.212f, blockBack.AsMetadata("child-metadata").AsFloat("another-one-z"));
            Assert.AreEqual(55, blockBack.AsMetadata("child-metadata").AsMetadata("norm").AsInt("col-x"));
            Assert.AreEqual(-110002, blockBack.AsMetadata("child-metadata").AsMetadata("norm").AsInt("col-y"));
            Assert.AreEqual(time, blockBack.AsTime("time"));

            Assert.AreEqual(0, blockBack.AsStringArray("empty-string-array").Length);
            Assert.AreEqual(1, blockBack.AsStringArray("one-string-array").Length);
            Assert.AreEqual("a", blockBack.AsStringArray("one-string-array")[0]);
            Assert.AreEqual(2, blockBack.AsStringArray("two-string-array").Length);
            Assert.AreEqual("9", blockBack.AsStringArray("two-string-array")[0]);
            Assert.AreEqual("10", blockBack.AsStringArray("two-string-array")[1]);

            Assert.AreEqual(0, blockBack.AsIntArray("empty-int-array").Length);
            Assert.AreEqual(1, blockBack.AsIntArray("one-int-array").Length);
            Assert.AreEqual(9, blockBack.AsIntArray("one-int-array")[0]);
            Assert.AreEqual(2, blockBack.AsIntArray("two-int-array").Length);
            Assert.AreEqual(13, blockBack.AsIntArray("two-int-array")[0]);
            Assert.AreEqual(-999, blockBack.AsIntArray("two-int-array")[1]);

            Assert.AreEqual(0, blockBack.AsFloatArray("empty-float-array").Length);
            Assert.AreEqual(1, blockBack.AsFloatArray("one-float-array").Length);
            Assert.AreEqual(9, blockBack.AsFloatArray("one-float-array")[0]);
            Assert.AreEqual(2, blockBack.AsFloatArray("two-float-array").Length);
            Assert.AreEqual(13, blockBack.AsFloatArray("two-float-array")[0]);
            Assert.AreEqual(-999, blockBack.AsFloatArray("two-float-array")[1]);

            Assert.AreEqual(0, blockBack.AsBoolArray("empty-bool-array").Length);
            Assert.AreEqual(1, blockBack.AsBoolArray("one-bool-array").Length);
            Assert.AreEqual(true, blockBack.AsBoolArray("one-bool-array")[0]);
            Assert.AreEqual(2, blockBack.AsBoolArray("two-bool-array").Length);
            Assert.AreEqual(true, blockBack.AsBoolArray("two-bool-array")[0]);
            Assert.AreEqual(false, blockBack.AsBoolArray("two-bool-array")[1]);

            Assert.AreEqual(0, blockBack.AsByteArray("empty-byte-array").Length);
            Assert.AreEqual(1, blockBack.AsByteArray("one-byte-array").Length);
            Assert.AreEqual(112, blockBack.AsByteArray("one-byte-array")[0]);
            Assert.AreEqual(2, blockBack.AsByteArray("two-byte-array").Length);
            Assert.AreEqual(99, blockBack.AsByteArray("two-byte-array")[0]);
            Assert.AreEqual(250, blockBack.AsByteArray("two-byte-array")[1]);

            Assert.AreEqual(0, blockBack.AsVector2Array("empty-vec2-array").Length);
            Assert.AreEqual(1, blockBack.AsVector2Array("one-vec2-array").Length);
            Assert.AreEqual(new Vector2(1, 2), blockBack.AsVector2Array("one-vec2-array")[0]);
            Assert.AreEqual(2, blockBack.AsVector2Array("two-vec2-array").Length);
            Assert.AreEqual(new Vector2(3, 4), blockBack.AsVector2Array("two-vec2-array")[0]);
            Assert.AreEqual(new Vector2(5, 6), blockBack.AsVector2Array("two-vec2-array")[1]);

            Assert.AreEqual(0, blockBack.AsVector3Array("empty-vec3-array").Length);
            Assert.AreEqual(1, blockBack.AsVector3Array("one-vec3-array").Length);
            Assert.AreEqual(new Vector3(1, 2, 3), blockBack.AsVector3Array("one-vec3-array")[0]);
            Assert.AreEqual(2, blockBack.AsVector3Array("two-vec3-array").Length);
            Assert.AreEqual(new Vector3(4, 5, 6), blockBack.AsVector3Array("two-vec3-array")[0]);
            Assert.AreEqual(new Vector3(7, 8, 9), blockBack.AsVector3Array("two-vec3-array")[1]);

            Assert.AreEqual(0, blockBack.AsTimeArray("empty-time-array").Length);
            Assert.AreEqual(1, blockBack.AsTimeArray("one-time-array").Length);
            Assert.AreEqual(new DateTime(1, 2, 3), blockBack.AsTimeArray("one-time-array")[0]);
            Assert.AreEqual(2, blockBack.AsTimeArray("two-time-array").Length);
            Assert.AreEqual(new DateTime(4, 5, 6), blockBack.AsTimeArray("two-time-array")[0]);
            Assert.AreEqual(new DateTime(7, 8, 9), blockBack.AsTimeArray("two-time-array")[1]);

            Assert.AreEqual(0, blockBack.AsMetadataArray("empty-metadata-array").Length);
            Assert.AreEqual(1, blockBack.AsMetadataArray("one-metadata-array").Length);
            Assert.AreEqual(new Metadata(), blockBack.AsMetadataArray("one-metadata-array")[0]);
            Assert.AreEqual(2, blockBack.AsMetadataArray("two-metadata-array").Length);
            Assert.AreEqual(nested, blockBack.AsMetadataArray("two-metadata-array")[0]);
            Assert.AreEqual(nested, blockBack.AsMetadataArray("two-metadata-array")[1]);

            Assert.AreEqual(block, blockBack);
        }
    }
}