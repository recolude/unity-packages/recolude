using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System.IO;

using Recolude.Core;
using Recolude.Core.Playback;
using Recolude.Core.Playback.ActorBuilderStrategies;

namespace Tests.Playback.ActorBuilderStrategies
{
    public class AssetBundleActorBuilderTests
    {

        AssetBundle bundle = null;

        private AssetBundle GetAssetBundle()
        {
            if (bundle == null)
            {
                bundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "actorbuilder-tests"));
            }
            return bundle;
        }

        [Test]
        public void AssetBundleActorBuilder_BasicLoad()
        {
            // *************************** ARRANGE ***************************
            var actorBuilder = new AssetBundleActorBuilder(GetAssetBundle());

            // ***************************** ACT *****************************
            IActor[] actors = actorBuilder.Build(new Recording(new Recording("1", "Basic Actor")));

            // **************************** ASSERT ****************************
            Assert.NotNull(actors);
            Assert.AreEqual(1, actors.Length);
            Assert.IsTrue(actors[0] is GameObjectActor);

            var basicActor = actors[0] as GameObjectActor;
            Assert.NotNull(basicActor.Representation);
            Assert.True(basicActor.Representation.activeInHierarchy);
        }

        [Test]
        public void AssetBundleActorBuilder_BasicLoad_ReturnsNullWhenDoesntExist()
        {
            // *************************** ARRANGE ***************************
            var actorBuilder = new AssetBundleActorBuilder(GetAssetBundle());

            // ***************************** ACT *****************************
            IActor[] actors = actorBuilder.Build(new Recording(new Recording("1", "Doesn't Exist")));

            // **************************** ASSERT ****************************
            Assert.NotNull(actors);
            Assert.AreEqual(0, actors.Length);
        }

        [Test]
        public void AssetBundleActorBuilder_BasicLoad_ReturnsFallbackWhenDoesntExist()
        {
            // *************************** ARRANGE ***************************
            var actorBuilder = new AssetBundleActorBuilder(GetAssetBundle(), GameObject.CreatePrimitive(PrimitiveType.Sphere), null);

            // ***************************** ACT *****************************
            IActor[] actors = actorBuilder.Build(new Recording(new Recording("1", "Doesn't Exist")));

            // **************************** ASSERT ****************************
            Assert.NotNull(actors);
            Assert.AreEqual(1, actors.Length);
            Assert.IsTrue(actors[0] is GameObjectActor);

            var basicActor = actors[0] as GameObjectActor;
            Assert.NotNull(basicActor.Representation);
            Assert.AreEqual("Sphere(Clone)", basicActor.Representation.name);
            Assert.True(basicActor.Representation.activeInHierarchy);
        }

    }

}