using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;

using Recolude.Core;
using Recolude.Core.Playback;
using Recolude.Core.Playback.ActorBuilderStrategies;

namespace Tests.Playback.ActorBuilderStrategies
{
    public class DictionaryAssetBuilderTests
    {
        [Test]
        public void DictionaryAssetBuilder_BasicLoad()
        {
            // *************************** ARRANGE ***************************
            var actorBuilder = new DictionaryActorBuilder(new Dictionary<string, GameObject>(){
                ["a"] = GameObject.CreatePrimitive(PrimitiveType.Sphere)
            });

            // ***************************** ACT *****************************
            IActor[] actors = actorBuilder.Build(new Recording(new Recording("1", "a")));

            // **************************** ASSERT ****************************
            Assert.NotNull(actors);
            Assert.AreEqual(1, actors.Length);
            Assert.IsTrue(actors[0] is GameObjectActor);

            var basicActor = actors[0] as GameObjectActor;
            Assert.NotNull(basicActor.Representation);
            Assert.True(basicActor.Representation.activeInHierarchy);
        }

        [Test]
        public void DictionaryAssetBuilder_BasicLoad_ReturnsEmptyWhenDoesntExist()
        {
            // *************************** ARRANGE ***************************
            var actorBuilder = new DictionaryActorBuilder(new Dictionary<string, GameObject>(){
                ["a"] = GameObject.CreatePrimitive(PrimitiveType.Sphere)
            });

            // ***************************** ACT *****************************
            IActor[] actors = actorBuilder.Build(new Recording(new Recording("1", "DoesntExist")));

            // **************************** ASSERT ****************************
            Assert.NotNull(actors);
            Assert.AreEqual(0, actors.Length);
        }

        [Test]
        public void DictionaryAssetBuilder_BasicLoad_ReturnsFallbackWhenDoesntExist()
        {
            // *************************** ARRANGE ***************************
            var actorBuilder = new DictionaryActorBuilder(new Dictionary<string, GameObject>(){
                ["a"] = GameObject.CreatePrimitive(PrimitiveType.Cube)
            }, GameObject.CreatePrimitive(PrimitiveType.Sphere), null);

            // ***************************** ACT *****************************
            IActor[] actors = actorBuilder.Build(new Recording(new Recording("1", "DoesntExist")));

            // **************************** ASSERT ****************************
            Assert.NotNull(actors);
            Assert.AreEqual(1, actors.Length);
            Assert.IsTrue(actors[0] is GameObjectActor);

            var basicActor = actors[0] as GameObjectActor;
            Assert.NotNull(basicActor.Representation);
            Assert.AreEqual("Sphere(Clone)", basicActor.Representation.name);
            Assert.True(basicActor.Representation.activeInHierarchy);
        }


    }
}
