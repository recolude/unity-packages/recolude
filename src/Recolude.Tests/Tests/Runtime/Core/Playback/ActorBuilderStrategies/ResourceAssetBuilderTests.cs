﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

using Recolude.Core;
using Recolude.Core.Playback;
using Recolude.Core.Playback.ActorBuilderStrategies;

namespace Tests.Playback.ActorBuilderStrategies
{
    public class ResourceAssetBuilderTests
    {
        // A Test behaves as an ordinary method
        [Test]
        public void ResourceAssetBuilder_BasicLoad()
        {
            // *************************** ARRANGE ***************************
            var actorBuilder = new ResourceActorBuilder();

            // ***************************** ACT *****************************
            IActor[] actors = actorBuilder.Build(new Recording(new Recording("1", "ResoureActorBuilderBasicTest")));

            // **************************** ASSERT ****************************
            Assert.NotNull(actors);
            Assert.AreEqual(1, actors.Length);
        }

        [Test]
        public void ResourceAssetBuilder_BasicLoad_ReturnsNullWhenDoesntExist()
        {
            // *************************** ARRANGE ***************************
            var actorBuilder = new ResourceActorBuilder();

            // ***************************** ACT *****************************
            IActor[] actors = actorBuilder.Build(new Recording(new Recording("1", "DoesntExist")));

            // **************************** ASSERT ****************************
            Assert.NotNull(actors);
            Assert.AreEqual(0, actors.Length);
        }

        [Test]
        public void ResourceAssetBuilder_BasicLoad_ReturnsFallbackWhenDoesntExist()
        {
            // *************************** ARRANGE ***************************
            var actorBuilder = new ResourceActorBuilder(GameObject.CreatePrimitive(PrimitiveType.Sphere));

            // ***************************** ACT *****************************
            IActor[] actors = actorBuilder.Build(new Recording(new Recording("1", "DoesntExist")));

            // **************************** ASSERT ****************************
            Assert.NotNull(actors);
            Assert.AreEqual(1, actors.Length);
            Assert.IsTrue(actors[0] is GameObjectActor);

            var basicActor = actors[0] as GameObjectActor;
            Assert.NotNull(basicActor.Representation);
            Assert.AreEqual("Sphere(Clone)", basicActor.Representation.name);
        }

        [Test]
        public void ResourceAssetBuilder_LoadsWithSubpath()
        {
            // *************************** ARRANGE ***************************
            var actorBuilder = new ResourceActorBuilder("MoreTestData");

            // ***************************** ACT *****************************
            IActor[] actors = actorBuilder.Build(new Recording(new Recording("1", "InSubfolder")));

            // **************************** ASSERT ****************************
            Assert.NotNull(actors);
            Assert.AreEqual(1, actors.Length);
            Assert.IsTrue(actors[0] is GameObjectActor);

            var basicActor = actors[0] as GameObjectActor;
            Assert.NotNull(basicActor.Representation);
        }

        [Test]
        public void ResourceAssetBuilder_LoadsWithSubpath_ReturnsNullWhenDoesntExist()
        {
            // *************************** ARRANGE ***************************
            var actorBuilder = new ResourceActorBuilder("MoreTestData");

            // ***************************** ACT *****************************
            IActor[] actors = actorBuilder.Build(new Recording(new Recording("1", "Doesnt Exist")));

            // **************************** ASSERT ****************************
            Assert.NotNull(actors);
            Assert.AreEqual(0, actors.Length);
        }

        [Test]
        public void ResourceAssetBuilder_LoadsWithSubpath_ReturnsWithFallbackWhenDoesntExist()
        {
            // *************************** ARRANGE ***************************
            var actorBuilder = new ResourceActorBuilder("MoreTestData", GameObject.CreatePrimitive(PrimitiveType.Sphere), null);

            // ***************************** ACT *****************************
            IActor[] actors = actorBuilder.Build(new Recording(new Recording("1", "Doesnt Exist")));

            // **************************** ASSERT ****************************
            Assert.NotNull(actors);
            Assert.AreEqual(1, actors.Length);
            Assert.IsTrue(actors[0] is GameObjectActor);

            var basicActor = actors[0] as GameObjectActor;
            Assert.NotNull(basicActor.Representation);
            Assert.AreEqual("Sphere(Clone)", basicActor.Representation.name);
        }

    }
}
