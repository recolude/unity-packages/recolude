// using System.Collections;
// using System.Collections.Generic;
// using NUnit.Framework;
// using UnityEngine;
// using UnityEngine.TestTools;
// using Recolude.Core.Playback;
// using Recolude.Core;
// using Recolude.Core.Time;

// namespace Tests.Playback
// {
//     public class ActorPlaybackControlTests
//     {

//         [Test]
//         public void MovesActorToPointInTime()
//         {
//             // ARRANGE ========================================================
//             var parent = new GameObject();
//             var go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
//             go.transform.SetParent(parent.transform);

//             var subjectRecording = new SubjectRecording(
//                 "0",
//                 "example",
//                 null,
//                 new[] {
//                     new VectorCapture(0, Vector3.zero),
//                     new VectorCapture(1, Vector3.one),
//                 },
//                 new[] {
//                     new VectorCapture(0, Vector3.zero),
//                     new VectorCapture(1, Vector3.zero),
//                 },
//                 new UnityLifeCycleEventCapture[] { },
//                 new CustomEventCapture[] { }
//             );
//             var recording = RecordingScriptableObject.CreateInstance(
//                 new[] { subjectRecording },
//                 new CustomEventCapture[] { },
//                 new Metadata()
//             );
//             var actor = new ActorPlaybackControl(go, null, recording, subjectRecording);

//             // ACT / ASSERT ===================================================
//             var t = 0.5f;
//             actor.MoveTo(t);
//             Assert.AreEqual(t, go.transform.position.x);
//             Assert.AreEqual(t, go.transform.position.y);
//             Assert.AreEqual(t, go.transform.position.z);

//             // This one moves us backwards in time :D
//             t = 0.25f;
//             actor.MoveTo(t);
//             Assert.AreEqual(t, go.transform.position.x);
//             Assert.AreEqual(t, go.transform.position.y);
//             Assert.AreEqual(t, go.transform.position.z);

//             t = 0.75f;
//             actor.MoveTo(t);
//             Assert.AreEqual(t, go.transform.position.x);
//             Assert.AreEqual(t, go.transform.position.y);
//             Assert.AreEqual(t, go.transform.position.z);
//         }

//         [Test]
//         public void MovesActorToPointInTime_CanHandleEventsOnSameTimestamp()
//         {
//             // ARRANGE ========================================================
//             var parent = new GameObject();
//             var go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
//             go.transform.SetParent(parent.transform);

//             var subjectRecording = new SubjectRecording(
//                 "0",
//                 "example",
//                 null,
//                 new[] {
//                     new VectorCapture(0, Vector3.zero),
//                     new VectorCapture(0, Vector3.zero),
//                     new VectorCapture(0.5f, Vector3.one * 0.5f),
//                     new VectorCapture(0.75f, Vector3.one * 0.5f),
//                     new VectorCapture(1, Vector3.one),
//                     new VectorCapture(1, Vector3.one),
//                 },
//                 new[] {
//                     new VectorCapture(0, Vector3.zero),
//                     new VectorCapture(1, Vector3.zero),
//                 },
//                 new UnityLifeCycleEventCapture[] { },
//                 new CustomEventCapture[] { }
//             );
//             var recording = RecordingScriptableObject.CreateInstance(
//                 new[] { subjectRecording },
//                 new CustomEventCapture[] { },
//                 new Metadata()
//             );
//             var actor = new ActorPlaybackControl(go, null, recording, subjectRecording);

//             // ACT / ASSERT ===================================================
//             var t = 0f;
//             actor.MoveTo(t);
//             Assert.AreEqual(t, go.transform.position.x);
//             Assert.AreEqual(t, go.transform.position.y);
//             Assert.AreEqual(t, go.transform.position.z);

//             t = 0.25f;
//             actor.MoveTo(t);
//             Assert.AreEqual(t, go.transform.position.x);
//             Assert.AreEqual(t, go.transform.position.y);
//             Assert.AreEqual(t, go.transform.position.z);

//             t = 0.5f;
//             actor.MoveTo(t);
//             Assert.AreEqual(0.5f, go.transform.position.x);
//             Assert.AreEqual(0.5f, go.transform.position.y);
//             Assert.AreEqual(0.5f, go.transform.position.z);
            
//             t = 0.5f;
//             actor.MoveTo(t);
//             Assert.AreEqual(0.5f, go.transform.position.x);
//             Assert.AreEqual(0.5f, go.transform.position.y);
//             Assert.AreEqual(0.5f, go.transform.position.z);
            
//             t = 1f;
//             actor.MoveTo(t);
//             Assert.AreEqual(t, go.transform.position.x);
//             Assert.AreEqual(t, go.transform.position.y);
//             Assert.AreEqual(t, go.transform.position.z);
//         }

//     }

// }