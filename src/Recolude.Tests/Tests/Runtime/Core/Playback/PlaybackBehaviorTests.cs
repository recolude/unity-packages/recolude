﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Recolude.Core.Playback;
using Recolude.Core;
using Recolude.Core.Time;

namespace Tests.Playback
{
    public class PlaybackBehaviorTests
    {

        class Builder : IActorBuilder
        {
            private EventRecorder globalRecorder;

            private EventRecorder actorEventRecorder;

            public Builder(EventRecorder globalRecorder, EventRecorder actorEventRecorder)
            {
                this.globalRecorder = globalRecorder;
                this.actorEventRecorder = actorEventRecorder;
            }

            public IActor[] Build(IRecording recording)
            {
                List<IActor> actors = new List<IActor>();
                actors.Add(new GameObjectActor(recording, null, globalRecorder));
                foreach (var item in recording.Recordings)
                {
                    actors.Add(new GameObjectActor(item, new GameObject(), actorEventRecorder));
                }
                return actors.ToArray();
            }
        }

        class TimeTester : ITimeProvider
        {
            float curTime;

            public TimeTester()
            {
                this.curTime = 0f;
            }

            public float CurrentTime()
            {
                return curTime;
            }

            public void SetTime(float newTime)
            {
                curTime = newTime;
            }
        }

        class EventRecorder : IPlaybackCustomEventHandler
        {
            List<IRecording> calledSubjects;

            List<Capture<CustomEvent>> calledCustomEvents;

            public EventRecorder()
            {
                calledSubjects = new List<IRecording>();
                calledCustomEvents = new List<Capture<CustomEvent>>();
            }

            public void OnCustomEvent(IRecording subject, TimeMovementDetails details, Capture<CustomEvent> customEvent)
            {
                Debug.LogFormat("CurTime: {0}; Delta: {1}", details.CurrentTime, details.DeltaTime);
                calledSubjects.Add(subject);
                calledCustomEvents.Add(customEvent);
            }

            public int EventsExperiencedCount()
            {
                return calledCustomEvents.Count;
            }

            public List<IRecording> SubjectsSeen()
            {
                return this.calledSubjects;
            }

            public List<Capture<CustomEvent>> EventsSeen()
            {
                return this.calledCustomEvents;
            }
        }

        [Test]
        public void PlaybackEmitsGlobalEvents()
        {
            // ARRANGE ========================================================
            var rec = new Recording(
                "",
                "global",
                new Recording[0],
                new ICaptureCollection<ICapture>[] {
                    new CustomEventCollection(new CustomEventCapture[] {
                        new CustomEventCapture(0, "a", "a contents"),
                        new CustomEventCapture(0.5f, "c", "c contents"),
                    })
                },
                new Metadata()
            );

            var eventHandlerTester = new EventRecorder();
            var mockTime = new TimeTester();

            var playback = Recolude.Core.Playback.PlaybackBehavior.Build(
                rec,
                new Builder(eventHandlerTester, null),
                false,
                mockTime
            );

            // ACT ============================================================
            Debug.Log("Play");
            playback.Play();

            Debug.Log("UpdateState");
            playback.UpdateState();

            Debug.Log("SetTime");
            mockTime.SetTime(0.1f);

            Debug.Log("UpdateState");
            playback.UpdateState();

            // ASSERT =========================================================
            Assert.AreEqual(1, eventHandlerTester.EventsExperiencedCount());
            Assert.NotNull(eventHandlerTester.SubjectsSeen()[0]);
            Assert.AreEqual(rec, eventHandlerTester.SubjectsSeen()[0]);
            Assert.AreEqual("a", eventHandlerTester.EventsSeen()[0].Value.Name);
            Assert.AreEqual("a contents", eventHandlerTester.EventsSeen()[0].Value.Contents.AsString("value"));
            Assert.AreEqual(0, eventHandlerTester.EventsSeen()[0].Time);
        }

        [Test]
        public void PlaybackEmitsSubjectCustomEvents()
        {
            // ARRANGE ========================================================
            var rec = new Recording(
                "",
                "parent",
                new Recording[] {
                    new Recording(
                        "0",
                        "Alex",
                        null,
                        new ICaptureCollection<ICapture>[] {
                            new PositionCollection("", new VectorCapture[0]),
                            new EulerRotationCollection("", new VectorCapture[0]),
                            new LifeCycleCollection(new UnityLifeCycleEventCapture[0]),
                            new CustomEventCollection(new CustomEventCapture[] {
                                new CustomEventCapture(0, "alex a", "a contents"),
                                new CustomEventCapture(0.5f, "alex c", "c contents"),
                            })
                        },
                        new Metadata(),
                        null,
                        null
                    ),
                    new Recording(
                        "1",
                        "other",
                        null,
                        new ICaptureCollection<ICapture>[] {
                            new PositionCollection("", new VectorCapture[0]),
                            new EulerRotationCollection("", new VectorCapture[0]),
                            new LifeCycleCollection(new UnityLifeCycleEventCapture[0]),
                            new CustomEventCollection(new CustomEventCapture[] {
                                new CustomEventCapture(0.2f, "other a", "a contents"),
                            })
                        },
                        new Metadata(),
                        null,
                        null
                    )
                },
                new ICaptureCollection<ICapture>[]{
                    new CustomEventCollection(new CustomEventCapture[] {
                        new CustomEventCapture(0, "global a", "a contents"),
                        new CustomEventCapture(0.5f, "global c", "c contents"),
                    }),
                },
                new Metadata()
            );

            var globalEventRecorder = new EventRecorder();
            var actorEventRecorder = new EventRecorder();
            var mockTime = new TimeTester();
            var builder = new Builder(globalEventRecorder, actorEventRecorder);

            var playback = Recolude.Core.Playback.PlaybackBehavior.Build(
                rec,
                builder,
                false,
                mockTime
            );

            // ACT ============================================================
            playback.Play();
            playback.UpdateState();
            mockTime.SetTime(0.1f);
            playback.UpdateState();
            playback.Stop();

            // ASSERT =========================================================
            Assert.AreEqual(1, globalEventRecorder.EventsExperiencedCount());
            Assert.NotNull(globalEventRecorder.SubjectsSeen()[0]);
            Assert.AreEqual(rec, globalEventRecorder.SubjectsSeen()[0]);
            Assert.AreEqual("global a", globalEventRecorder.EventsSeen()[0].Value.Name);
            Assert.AreEqual("a contents", globalEventRecorder.EventsSeen()[0].Value.Contents.AsString("value"));
            Assert.AreEqual(0, globalEventRecorder.EventsSeen()[0].Time);

            Assert.AreEqual(1, actorEventRecorder.EventsExperiencedCount());
            Assert.NotNull(actorEventRecorder.SubjectsSeen()[0]);
            Assert.AreEqual(rec.Recordings[0], actorEventRecorder.SubjectsSeen()[0]);
            Assert.AreEqual("Alex", actorEventRecorder.SubjectsSeen()[0].Name);
            Assert.AreEqual("0", actorEventRecorder.SubjectsSeen()[0].ID);
            Assert.AreEqual("alex a", actorEventRecorder.EventsSeen()[0].Value.Name);
            Assert.AreEqual("a contents", actorEventRecorder.EventsSeen()[0].Value.Contents.AsString("value"));
            Assert.AreEqual(0, actorEventRecorder.EventsSeen()[0].Time);
        }

    }

}
