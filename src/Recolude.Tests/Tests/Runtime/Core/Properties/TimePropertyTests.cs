using System;
using System.IO.Compression;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Recolude.Core.IO;
using Recolude.Core.Properties;
using Recolude.Core;
using Recolude.Core.IO.PropertyWriters;

namespace Tests.PropertyTests
{
    public class TimePropertyTests
    {
        [Test]
        public void Write_Value()
        {
            // ARRANGE ========================================================
            TimeProperty prop = new TimeProperty(new DateTime(1995, 11, 28, 9, 35, 24));
            byte[] writtenBytes;

            // ACT ============================================================
            using (var memory = new MemoryStream())
            {
                using (var writer = new BinaryWriter(memory, new System.Text.UTF8Encoding(false, true)))
                {
                    PropertyWriterCollection.Instance.Write<DateTime>(writer, prop);
                }

                writtenBytes = memory.ToArray();
            }

            // ASSERT =========================================================
            Assert.AreEqual(9, writtenBytes.Length);

            // Property type byte
            Assert.AreEqual(12, writtenBytes[0]);

            // [0 175 64 0 143 231 2 0]
            Assert.AreEqual(0, writtenBytes[1]);
            Assert.AreEqual(175, writtenBytes[2]);
            Assert.AreEqual(64, writtenBytes[3]);
            Assert.AreEqual(0, writtenBytes[4]);
            Assert.AreEqual(143, writtenBytes[5]);
            Assert.AreEqual(231, writtenBytes[6]);
            Assert.AreEqual(2, writtenBytes[7]);
            Assert.AreEqual(0, writtenBytes[8]);
        }
    }
}