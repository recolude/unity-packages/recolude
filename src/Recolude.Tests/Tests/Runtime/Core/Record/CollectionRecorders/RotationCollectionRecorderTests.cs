using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System.IO;

using Recolude.Core;
using Recolude.Core.Util;
using Recolude.Core.Record.CollectionRecorders;

namespace Tests.Record.CollectionRecorders
{
    public class RotationCollectionRecorderTests
    {

        [Test]
        public void BasicCapture()
        {
            // *************************** ARRANGE ***************************
            var rotationThing = new RotationRecorder(0);

            // ***************************** ACT *****************************
            rotationThing.Record(new VectorCapture(0, new Vector3(0, 0, 0)));
            rotationThing.Record(new VectorCapture(0.4f, new Vector3(0, 1, 0)));
            var collection = rotationThing.ToCollection(0, 1, null);

            // **************************** ASSERT ****************************
            Assert.AreEqual(2, collection.Captures.Length);
            Assert.AreEqual("Rotation", collection.Name);

            Assert.AreEqual(new Vector3(0, 0, 0), collection.Captures[0].Value);
            Assert.AreEqual(0, collection.Captures[0].Time, .00001f);

            Assert.AreEqual(new Vector3(0, 1, 0), collection.Captures[1].Value);
            Assert.AreEqual(.4f, collection.Captures[1].Time, .00001f);
        }

        [Test]
        public void TranslateBetweenUncaughtPositions()
        {
            // *************************** ARRANGE ***************************
            var rotationThing = new RotationRecorder(0);

            // ***************************** ACT *****************************
            rotationThing.Record(new VectorCapture(0.0f, new Vector3(0, 1, 0)));
            rotationThing.Record(new VectorCapture(0.5f, new Vector3(0, 0, 0)));
            rotationThing.Record(new VectorCapture(1.0f, new Vector3(0, 1, 0)));
            rotationThing.Record(new VectorCapture(1.5f, new Vector3(0, 0, 0)));
            var collection = rotationThing.ToCollection(0.25f, 1.25f, null);

            // **************************** ASSERT ****************************
            Assert.AreEqual(4, collection.Captures.Length);

            Assert.AreEqual(new Vector3(0, 0.5f, 0), collection.Captures[0].Value);
            Assert.AreEqual(new Vector3(0, 0, 0), collection.Captures[1].Value);
            Assert.AreEqual(new Vector3(0, 1, 0), collection.Captures[2].Value);
            Assert.AreEqual(new Vector3(0, 0.5f, 0), collection.Captures[3].Value);

            Assert.AreEqual(.25f, collection.Captures[0].Time, .00001f);
            Assert.AreEqual(.5f, collection.Captures[1].Time, .00001f);
            Assert.AreEqual(1.0f, collection.Captures[2].Time, .00001f);
            Assert.AreEqual(1.25f, collection.Captures[3].Time, .00001f);
        }

        [Test]
        public void HandlesPauses()
        {
            // *************************** ARRANGE ***************************
            var rotationThing = new RotationRecorder(0);

            // ***************************** ACT *****************************
            rotationThing.Record(new VectorCapture(0.0f, new Vector3(0, 1, 0)));
            rotationThing.Record(new VectorCapture(0.5f, new Vector3(0, 0, 0)));
            rotationThing.Record(new VectorCapture(1.0f, new Vector3(0, 1, 0)));
            rotationThing.Record(new VectorCapture(1.5f, new Vector3(0, 0, 0)));
            var collection = rotationThing.ToCollection(0.25f, 1.25f, new Vector2[] { new Vector2(.5f, 1.0f) });

            // **************************** ASSERT ****************************
            Assert.AreEqual(4, collection.Captures.Length);

            Assert.AreEqual(new Vector3(0, 0.5f, 0), collection.Captures[0].Value);
            Assert.AreEqual(new Vector3(0, 0, 0), collection.Captures[1].Value);
            Assert.AreEqual(new Vector3(0, 1, 0), collection.Captures[2].Value);
            Assert.AreEqual(new Vector3(0, 0.5f, 0), collection.Captures[3].Value);

            Assert.AreEqual(.25f, collection.Captures[0].Time, .00001f);
            Assert.AreEqual(.5f, collection.Captures[1].Time, .00001f);
            Assert.AreEqual(.5f, collection.Captures[2].Time, .00001f);
            Assert.AreEqual(0.75f, collection.Captures[3].Time, .00001f);
        }

        [Test]
        public void HandleNoCapture()
        {
            // *************************** ARRANGE ***************************
            var rotationThing = new RotationRecorder(0);

            // ***************************** ACT *****************************
            var collection = rotationThing.ToCollection(0, 1, null);

            // **************************** ASSERT ****************************
            Assert.AreEqual(0, collection.Captures.Length);
        }

        [Test]
        public void HandleSingleCapture()
        {
            // *************************** ARRANGE ***************************
            var rotationThing = new RotationRecorder(0);

            // ***************************** ACT *****************************
            rotationThing.Record(new VectorCapture(0, new Vector3(0, 0, 0)));
            var collection = rotationThing.ToCollection(0, 1, null);

            // **************************** ASSERT ****************************
            Assert.AreEqual(1, collection.Captures.Length);
            Assert.AreEqual(new Vector3(0, 0.0f, 0), collection.Captures[0].Value);
            Assert.AreEqual(0.0f, collection.Captures[0].Time, .00001f);
        }

        [Test]
        public void HandleTwoCapturesAtSameTimestamp()
        {
            // *************************** ARRANGE ***************************
            var rotationThing = new RotationRecorder(0);

            // ***************************** ACT *****************************
            rotationThing.Record(new VectorCapture(0, new Vector3(0, 0, 0)));
            rotationThing.Record(new VectorCapture(1, new Vector3(0, 1, 0)));
            rotationThing.Record(new VectorCapture(1, new Vector3(0, 1, 0)));
            rotationThing.Record(new VectorCapture(1, new Vector3(0, 1, 0)));
            var collection = rotationThing.ToCollection(0, 1, null);

            // **************************** ASSERT ****************************
            Assert.AreEqual(2, collection.Captures.Length);
            Assert.AreEqual(new Vector3(0, 0.0f, 0), collection.Captures[0].Value);
            Assert.AreEqual(new Vector3(0, 1.0f, 0), collection.Captures[1].Value);
            Assert.AreEqual(0.0f, collection.Captures[0].Time, .00001f);
            Assert.AreEqual(1.0f, collection.Captures[1].Time, .00001f);
        }

    }

}