using NUnit.Framework;
using UnityEngine;

using Recolude.Core;
using Recolude.Core.Record;

namespace Tests.Record
{
    public class RecorderTests
    {

        [Test]
        public void EmitsEventOnFinishRecording()
        {
            // *************************** ARRANGE ***************************
            IRecording calledBackRec = null;
            IRecording returnedRec = null;

            var recorder = new Recorder();
            recorder.AddOnRecordingCompleteCallback(delegate (IRecording rec)
            {
                calledBackRec = rec;
            });

            // ***************************** ACT *****************************
            recorder.Start();
            returnedRec = recorder.Finish();

            // **************************** ASSERT ****************************
            Assert.NotNull(returnedRec);
            Assert.AreEqual(returnedRec, calledBackRec);
        }

        [Test]
        public void RemovesCallbackForOnRecordingFinish()
        {
            // *************************** ARRANGE ***************************
            IRecording calledBackRec = null;
            IRecording returnedRec = null;

            var recorder = new Recorder();

            UnityEngine.Events.UnityAction<IRecording> cb = delegate (IRecording rec)
            {
                calledBackRec = rec;
            };

            // ***************************** ACT *****************************
            recorder.AddOnRecordingCompleteCallback(cb);
            recorder.Start();
            recorder.RemoveOnRecordingCompleteCallback(cb);
            returnedRec = recorder.Finish();

            // **************************** ASSERT ****************************
            Assert.NotNull(returnedRec);
            Assert.IsNull(calledBackRec);
        }

        [Test]
        public void RemovesAllCallbackForOnRecordingFinish()
        {
            // *************************** ARRANGE ***************************
            IRecording calledBackRec = null;
            IRecording returnedRec = null;

            var recorder = new Recorder();

            UnityEngine.Events.UnityAction<IRecording> cb = delegate (IRecording rec)
            {
                calledBackRec = rec;
            };

            // ***************************** ACT *****************************
            recorder.AddOnRecordingCompleteCallback(cb);
            recorder.Start();
            recorder.ClearOnRecordingCompleteListeners();
            returnedRec = recorder.Finish();

            // **************************** ASSERT ****************************
            Assert.NotNull(returnedRec);
            Assert.IsNull(calledBackRec);
        }

    }

}