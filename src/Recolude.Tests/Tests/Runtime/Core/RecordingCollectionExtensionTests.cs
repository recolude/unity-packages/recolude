using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System.IO;

using Recolude.Core;

namespace Tests
{
    public class RecordingCollectionExtensionTests
    {

        [Test]
        public void ToCSV_Basic()
        {
            // *************************** ARRANGE ****************************
            var rec = new Recording(
                "",
                "Rec111",
                new Recording[] {
                    new Recording(
                        "1",
                        "Mommy",
                        null,
                        new ICaptureCollection<ICapture>[] {
                            new PositionCollection(
                                new VectorCapture[]{
                                    new VectorCapture(0, new Vector3(5, 6, 7)),
                                    new VectorCapture(3, new Vector3(8, 9, 10)),
                                }
                            ),
                            new EulerRotationCollection(
                                new VectorCapture[]{
                                    new VectorCapture(4, Vector3.right),
                                    new VectorCapture(4, Vector3.left),
                                    new VectorCapture(4, Vector3.down),
                                }
                            ),
                            new LifeCycleCollection(
                            new UnityLifeCycleEventCapture[]{
                                new UnityLifeCycleEventCapture(2, UnityLifeCycleEvent.Start),
                            }
                            ),
                            new CustomEventCollection(
                                new CustomEventCapture[] {
                                    new CustomEventCapture(0.5f, "Mommy-kick", "what"),
                                    new CustomEventCapture(
                                        0.75f,
                                        "Scold",
                                        new Dictionary<string,string>()
                                        {
                                            {"intensity","1000 suns"},
                                        }
                                    )
                                }
                            )
                        },
                        new Metadata(new Dictionary<string, string>()
                        {
                            {"a", "b"},
                        }),
                        null,
                        null
                    ),
                },
                new ICaptureCollection<ICapture>[]{
                    new CustomEventCollection(new CustomEventCapture[] {
                        new CustomEventCapture(0.25f, "the hell", "is this recording"),
                        new CustomEventCapture(
                            0.75f,
                            "um",
                            new Dictionary<string,string>()
                            {
                                {"uhh","yup"},
                            }
                        )
                    })
                },
                new Metadata(new Dictionary<string, string>(){
                    {"nonsubject", "metadata"},
                })
            );

            var rec2 = new Recording(
                "", 
                "Rec222",
                new Recording[] {
                    new Recording(
                        "0",
                        "Soccer",
                        null,
                        new ICaptureCollection<ICapture>[] {
                            new PositionCollection(
                                new VectorCapture[]{
                                    new VectorCapture(1, new Vector3(2, 3, 4)),
                                }
                            ),
                            new EulerRotationCollection(
                                new VectorCapture[]{
                                    new VectorCapture(2, Vector3.up),
                                }
                            ),
                            new LifeCycleCollection(new UnityLifeCycleEventCapture[]{
                                new UnityLifeCycleEventCapture(1, UnityLifeCycleEvent.Enable),
                                new UnityLifeCycleEventCapture(2, UnityLifeCycleEvent.Destroy),
                            }),
                            new CustomEventCollection(
                                new CustomEventCapture[] {
                                    new CustomEventCapture(0, "Soccer-kick", "went far"),
                                }
                            )
                        },
                        new Metadata(new Dictionary<string, string>()
                        {
                            {"1", "2"},
                        }),
                        null,
                        null
                    )
                },
                new ICaptureCollection<ICapture>[]{
                    new CustomEventCollection(new Capture<CustomEvent>[0])
                },
                new Metadata(new Dictionary<string, string>(){
                    {"nonsubject", "metadata"},
                })
            );

            // ***************************** ACT ******************************
            var csvPages = new IRecording[] { rec, rec2 }.ToCSV();

            // **************************** ASSERT ****************************
            Assert.NotNull(csvPages);
            Assert.AreEqual(7, csvPages.Length);
            Assert.AreEqual("Recordings", csvPages[0].GetName());
            Assert.AreEqual("Subjects", csvPages[1].GetName());
            Assert.AreEqual("MetaData", csvPages[2].GetName());
            Assert.AreEqual("CustomEvents", csvPages[3].GetName());
            Assert.AreEqual("PositionData", csvPages[4].GetName());
            Assert.AreEqual("RotationData", csvPages[5].GetName());
            Assert.AreEqual("LifeCycleEvents", csvPages[6].GetName());

            Assert.AreEqual(
                string.Join(
                    "\n",
                    "\"ID\",\"Name\"",
                    "\"0\",\"Rec111\"",
                    "\"1\",\"Rec222\""
                ),
                csvPages[0].ToString()
            );

            // Check Subjects
            Assert.AreEqual(
                string.Join(
                    "\n",
                    "\"Recording\",\"ID\",\"Name\"",
                    "\"0\",\"1\",\"Mommy\"",
                    "\"1\",\"0\",\"Soccer\""
                ),
                csvPages[1].ToString()
            );

            // Check Metadata
            Assert.AreEqual(
                string.Join(
                    "\n",
                    @"""Recording"",""SubjectID"",""Key"",""Value""",
                    @"""0"","""",""nonsubject"",""metadata""",
                    @"""0"",""1"",""a"",""b""",
                    @"""1"","""",""nonsubject"",""metadata""",
                    @"""1"",""0"",""1"",""2"""
                ),
                csvPages[2].ToString()
            );

            // Custom Events
            Assert.AreEqual(
                string.Join(
                    "\n",
                    @"""Recording"",""SubjectID"",""Index"",""Time"",""Name"",""Key"",""Value""",
                    @"""0"","""",""1"",""0.25"",""the hell"",""value"",""is this recording""",
                    @"""0"","""",""2"",""0.75"",""um"",""uhh"",""yup""",
                    @"""0"",""1"",""3"",""0.5"",""Mommy-kick"",""value"",""what""",
                    @"""0"",""1"",""4"",""0.75"",""Scold"",""intensity"",""1000 suns""",
                    @"""1"",""0"",""1"",""0"",""Soccer-kick"",""value"",""went far"""
                ),
                csvPages[3].ToString()
            );

            // Positions
            Assert.AreEqual(
                string.Join(
                    "\n",
                    @"""Recording"",""SubjectID"",""Time"",""X"",""Y"",""Z""",
                    @"""0"",""1"",""0"",""5"",""6"",""7""",
                    @"""0"",""1"",""3"",""8"",""9"",""10""",
                    @"""1"",""0"",""1"",""2"",""3"",""4"""
                ),
                csvPages[4].ToString()
            );

            // Rotations
            Assert.AreEqual(
                string.Join(
                    "\n",
                    @"""Recording"",""SubjectID"",""Time"",""X"",""Y"",""Z""",
                    @"""0"",""1"",""4"",""1"",""0"",""0""",
                    @"""0"",""1"",""4"",""-1"",""0"",""0""",
                    @"""0"",""1"",""4"",""0"",""-1"",""0""",
                    @"""1"",""0"",""2"",""0"",""1"",""0"""
                ),
                csvPages[5].ToString()
            );

            // Life Events
            Assert.AreEqual(
                string.Join(
                    "\n",
                    @"""Recording"",""SubjectID"",""Time"",""Event""",
                    @"""0"",""1"",""2"",""Start""",
                    @"""1"",""0"",""1"",""Enable""",
                    @"""1"",""0"",""2"",""Destroy"""
                ),
                csvPages[6].ToString()
            );

        }

    }

}