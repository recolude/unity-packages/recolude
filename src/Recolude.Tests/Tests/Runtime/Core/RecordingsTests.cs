using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System.IO;

using Recolude.Core;

namespace Tests
{
    public class RecordingTests
    {

        [Test]
        public void ToCSV_Basic()
        {
            // *************************** ARRANGE ***************************
            var rec = new Recording(
                "",
                "",
                new Recording[] {
                    new Recording(
                        "0",
                        "Soccer",
                        null,
                        new ICaptureCollection<ICapture>[] {
                            new PositionCollection(
                                new VectorCapture[]{
                                    new VectorCapture(1, new Vector3(2, 3, 4)),
                                }
                            ),
                            new EulerRotationCollection(
                                new VectorCapture[]{
                                    new VectorCapture(2, Vector3.up),
                                }
                            ),
                            new LifeCycleCollection(
                            new UnityLifeCycleEventCapture[]{
                                new UnityLifeCycleEventCapture(1, UnityLifeCycleEvent.Enable),
                                new UnityLifeCycleEventCapture(2, UnityLifeCycleEvent.Destroy),
                            }
                            ),
                            new CustomEventCollection(
                                new CustomEventCapture[] {
                                    new CustomEventCapture(0, "Soccer-kick", "went far"),
                                }
                            )
                        },
                        new Metadata(new Dictionary<string, string>()
                        {
                            {"1", "2"},
                        }),
                        null,
                        null
                    ),
                    new Recording(
                        "1",
                        "Mommy",
                        null,
                        new ICaptureCollection<ICapture>[] {
                        new PositionCollection(
                            new VectorCapture[]{
                                new VectorCapture(0, new Vector3(5, 6, 7)),
                                new VectorCapture(3, new Vector3(8, 9, 10)),
                            }
                        ),
                        new EulerRotationCollection(
                            new VectorCapture[]{
                                new VectorCapture(4, Vector3.right),
                                new VectorCapture(4, Vector3.left),
                                new VectorCapture(4, Vector3.down),
                        }
                        ),
                        new LifeCycleCollection(
                        new UnityLifeCycleEventCapture[]{
                            new UnityLifeCycleEventCapture(2, UnityLifeCycleEvent.Start),
                        }
                        ),
                        new CustomEventCollection(
                            new CustomEventCapture[] {
                                new CustomEventCapture(0.5f, "Mommy-kick", "what"),
                                new CustomEventCapture(
                                    0.75f,
                                    "Scold",
                                    new Dictionary<string,string>()
                                    {
                                        {"intensity","1000 suns"},
                                    }
                                )
                            }
                        )
                        },
                        new Metadata(new Dictionary<string, string>()
                        {
                            {"a", "b"},
                        }),
                        null,
                        null
                    ),
                },
                new ICaptureCollection<ICapture>[]{
                    new CustomEventCollection(new CustomEventCapture[] {
                        new CustomEventCapture(0.25f, "the hell", "is this recording"),
                        new CustomEventCapture(
                            0.75f,
                            "um",
                            new Dictionary<string,string>()
                            {
                                {"uhh","yup"},
                            }
                        )
                    })
                },
                new Metadata(new Dictionary<string, string>(){
                    {"nonsubject", "metadata"},
                })
            );

            // ***************************** ACT *****************************
            var csvPages = rec.ToCSV();

            // **************************** ASSERT ****************************
            Assert.NotNull(csvPages);
            Assert.AreEqual(6, csvPages.Length);
            Assert.AreEqual("Subjects", csvPages[0].GetName());
            Assert.AreEqual("MetaData", csvPages[1].GetName());
            Assert.AreEqual("CustomEvents", csvPages[2].GetName());
            Assert.AreEqual("PositionData", csvPages[3].GetName());
            Assert.AreEqual("RotationData", csvPages[4].GetName());
            Assert.AreEqual("LifeCycleEvents", csvPages[5].GetName());

            // Check Subjects
            Assert.AreEqual(
                string.Join(
                    "\n",
                    "\"ID\",\"Name\"",
                    "\"0\",\"Soccer\"",
                    "\"1\",\"Mommy\""
                ),
                csvPages[0].ToString()
            );

            // Check Metadata
            Assert.AreEqual(
                string.Join(
                    "\n",
                    @"""SubjectID"",""Key"",""Value""",
                    @""""",""nonsubject"",""metadata""",
                    @"""0"",""1"",""2""",
                    @"""1"",""a"",""b"""
                ),
                csvPages[1].ToString()
            );

            // Custom Events
            Assert.AreEqual(
                string.Join(
                    "\n",
                    @"""SubjectID"",""Index"",""Time"",""Name"",""Key"",""Value""",
                    @""""",""1"",""0.25"",""the hell"",""value"",""is this recording""",
                    @""""",""2"",""0.75"",""um"",""uhh"",""yup""",
                    @"""0"",""3"",""0"",""Soccer-kick"",""value"",""went far""",
                    @"""1"",""4"",""0.5"",""Mommy-kick"",""value"",""what""",
                    @"""1"",""5"",""0.75"",""Scold"",""intensity"",""1000 suns"""
                ),
                csvPages[2].ToString()
            );


            // Positions
            Assert.AreEqual(
                string.Join(
                    "\n",
                    @"""SubjectID"",""Time"",""X"",""Y"",""Z""",
                    @"""0"",""1"",""2"",""3"",""4""",
                    @"""1"",""0"",""5"",""6"",""7""",
                    @"""1"",""3"",""8"",""9"",""10"""
                ),
                csvPages[3].ToString()
            );

            // Rotations
            Assert.AreEqual(
                string.Join(
                    "\n",
                    @"""SubjectID"",""Time"",""X"",""Y"",""Z""",
                    @"""0"",""2"",""0"",""1"",""0""",
                    @"""1"",""4"",""1"",""0"",""0""",
                    @"""1"",""4"",""-1"",""0"",""0""",
                    @"""1"",""4"",""0"",""-1"",""0"""
                ),
                csvPages[4].ToString()
            );

            // Life Events
            Assert.AreEqual(
                string.Join(
                    "\n",
                    @"""SubjectID"",""Time"",""Event""",
                    @"""0"",""1"",""Enable""",
                    @"""0"",""2"",""Destroy""",
                    @"""1"",""2"",""Start"""
                ),
                csvPages[5].ToString()
            );
        }

        [Test]
        public void EmptyRecordingCalculatesDuration()
        {
            // *************************** ARRANGE ***************************
            var rec = new Recording(
                "",
                "",
                new IRecording[] {

                },
                new ICaptureCollection<ICapture>[] {
                    new CustomEventCollection(
                        new CustomEventCapture[] { }
                    )
                },
                new Metadata(new Dictionary<string, string>(){
                    {"nonsubject", "metadata"},
                }),
                null,
                null
            );

            // ***************************** ACT *****************************
            var duration = rec.Duration;

            // **************************** ASSERT ****************************
            Assert.AreEqual(0f, duration);
        }


        [Test]
        public void SingleCaptureCollectionCalculatesDuration()
        {
            // *************************** ARRANGE ***************************
            var rec = new Recording(
                "",
                "",
                new IRecording[] {

                },
                new ICaptureCollection<ICapture>[] {
                    new CustomEventCollection(
                        new CustomEventCapture[] {
                            new CustomEventCapture(1, "a", "b"),
                            new CustomEventCapture(2, "a", "b"),
                    })
                },
                new Metadata(new Dictionary<string, string>(){
                    {"nonsubject", "metadata"},
                }),
                null,
                null
            );

            // ***************************** ACT *****************************
            var duration = rec.Duration;

            // **************************** ASSERT ****************************
            Assert.AreEqual(1f, duration);
        }

        [Test]
        public void SingleSubRecordingCalculatesDuration()
        {
            // *************************** ARRANGE ***************************
            var rec = new Recording(
                "",
                "",
                new IRecording[] {
                    new Recording(
                    "",
                    "",
                    new IRecording[] {

                    },
                    new ICaptureCollection<ICapture>[] {
                        new CustomEventCollection(
                            new CustomEventCapture[] {
                                new CustomEventCapture(1, "a", "b"),
                                new CustomEventCapture(2, "a", "b"),
                        })
                    },
                    new Metadata(new Dictionary<string, string>(){
                        {"nonsubject", "metadata"},
                    }),
                    null,
                    null
                )
                },
                new ICaptureCollection<ICapture>[] {
                    new CustomEventCollection(
                        new CustomEventCapture[] {
                    })
                },
                new Metadata(new Dictionary<string, string>(){
                    {"nonsubject", "metadata"},
                }),
                null,
                null
            );

            // ***************************** ACT *****************************
            var duration = rec.Duration;

            // **************************** ASSERT ****************************
            Assert.AreEqual(1f, duration);
        }

    }

}