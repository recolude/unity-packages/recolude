using System;
using NUnit.Framework;
using Recolude.Core.Time;

namespace Tests.TimeTests
{
    class ControllableTimeProviderTests
    {
        [Test]
        [TestCase(float.MinValue)]
        [TestCase(float.MaxValue)]
        [TestCase(0)]
        [TestCase(10.1f)]
        [TestCase(-10.1f)]
        public void CanSetTime(float timeToSet)
        {
            // *************************** ARRANGE ****************************
            var timeProvider = new ControllableTimeProvider();

            // ***************************** ACT ******************************
            timeProvider.SetTime(timeToSet);

            // **************************** ASSERT ****************************
            Assert.AreEqual(timeToSet, timeProvider.CurrentTime());
        }

        [Test]
        [TestCase(float.MinValue)]
        [TestCase(float.MaxValue)]
        [TestCase(0)]
        [TestCase(10.1f)]
        [TestCase(-10.1f)]
        public void CanInitializeTime(float timeToSet)
        {
            // *************************** ARRANGE ****************************
            var timeProvider = new ControllableTimeProvider(timeToSet);

            // **************************** ASSERT ****************************
            Assert.AreEqual(timeToSet, timeProvider.CurrentTime());
        }

        [Test]
        public void SettingNaNTime_Throws()
        {
            // *************************** ARRANGE ****************************
            var timeProvider = new ControllableTimeProvider();

            // ************************* ACT/ASSERT ***************************
            Assert.Throws<ArgumentException>(() => timeProvider.SetTime(float.NaN));
        }

        [Test]
        public void InitializingNaNTime_Throws()
        {
            // ************************* ACT/ASSERT ***************************
            Assert.Throws<ArgumentException>(() => new ControllableTimeProvider(float.NaN));
        }

        [Test]
        public void CanSetTimeMultipleTimes()
        {
            // *************************** ARRANGE ****************************
            var timeProvider = new ControllableTimeProvider();
            var timesToSet = new float[] {
                1,
                2,
                10,
                12.1f,
                300.99999f,
                1000.313131f
            };

            // ************************* ACT/ASSERT ***************************
            foreach (var item in timesToSet)
            {
                timeProvider.SetTime(item);
                Assert.AreEqual(item, timeProvider.CurrentTime());

            }
        }
    }
}