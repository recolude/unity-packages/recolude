using System;
using NUnit.Framework;
using Recolude.Core.Time;

namespace Tests.TimeTests
{
    class PausableTimeProviderTests
    {

        [Test]
        public void Initializing_WithNullProvider_Throws()
        {
            // ************************* ACT/ASSERT ***************************
            Assert.Throws<ArgumentNullException>(() => new PausableTimeProvider(null));
        }

        [Test]
        public void Pausing_WhilePaused_Throws()
        {
            // *************************** ARRANGE ****************************
            var underlyingTimeProvider = new ControllableTimeProvider();
            var timeProvider = new PausableTimeProvider(underlyingTimeProvider);

            // ***************************** ACT ******************************
            timeProvider.Pause();

            // **************************** ASSERT ****************************
            Assert.Throws<InvalidOperationException>(() => timeProvider.Pause());
        }

        [Test]
        public void Unpausing_WhileUnpaused_Throws()
        {
            // *************************** ARRANGE ****************************
            var underlyingTimeProvider = new ControllableTimeProvider();
            var timeProvider = new PausableTimeProvider(underlyingTimeProvider);

            // ************************* ACT/ASSERT ***************************
            Assert.Throws<InvalidOperationException>(() => timeProvider.Unpause());
        }

        [Test]
        public void PausingHaltsNewReadings()
        {
            // *************************** ARRANGE ****************************
            var underlyingTimeProvider = new ControllableTimeProvider(10f);
            var timeProvider = new PausableTimeProvider(underlyingTimeProvider);

            // ************************* ACT/ASSERT ***************************
            Assert.AreEqual(false, timeProvider.Paused);
            timeProvider.Pause();
            Assert.AreEqual(true, timeProvider.Paused);
            Assert.AreEqual(10f, timeProvider.CurrentTime());

            underlyingTimeProvider.SetTime(25f);
            Assert.AreEqual(10f, timeProvider.CurrentTime());

            float timeTranspired = timeProvider.Unpause();
            Assert.AreEqual(false, timeProvider.Paused);
            Assert.AreEqual(10f, timeProvider.CurrentTime());
            Assert.AreEqual(15f, timeTranspired);

            underlyingTimeProvider.SetTime(30f);
            Assert.AreEqual(15f, timeProvider.CurrentTime());
        }

    }

}