using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Recolude.Util.TerrainUtils.Editor
{

    public class TerrainExaminer : EditorWindow
    {

        [SerializeField]
        Terrain terrainToExport;


        [MenuItem("Window/Recolude/Terrain/Examiner")]
        public static TerrainExaminer Init()
        {
            TerrainExaminer window = (TerrainExaminer)GetWindow(typeof(TerrainExaminer));
            window.Show();
            window.Repaint();
            return window;
        }

        void OnEnable()
        {
            titleContent = new GUIContent("Examine Terrain");
        }


        void OnGUI()
        {
            Terrain newTerrainToExport = EditorGUILayout.ObjectField(terrainToExport, typeof(Terrain), true) as Terrain;
            if (newTerrainToExport != terrainToExport)
            {
                terrainToExport = newTerrainToExport;
            }

            if (terrainToExport == null)
            {
                return;
            }

            var td = terrainToExport.terrainData;
            if (GUILayout.Button("Build Tree Collection"))
            {
                TreeInstance[] treeInstances = td.treeInstances;
                TreeCollection.Tree[] trees = new TreeCollection.Tree[treeInstances.Length];

                var terrainSize = new Vector3(
                    td.heightmapScale.x * td.heightmapResolution,
                    td.heightmapScale.y,
                    td.heightmapScale.z * td.heightmapResolution
                );

                for (int i = 0; i < trees.Length; i++)
                {
                    trees[i] = new TreeCollection.Tree
                    {
                        Position = new Vector3(
                            treeInstances[i].position.x * terrainSize.x,
                            treeInstances[i].position.y * terrainSize.y,
                            treeInstances[i].position.z * terrainSize.z
                        ),
                        Scale = new Vector3(treeInstances[i].widthScale, treeInstances[i].heightScale, treeInstances[i].widthScale)
                    };
                }

                TreeCollection asset = TreeCollection.Build(trees);
                AssetDatabase.CreateAsset(asset, "Assets/Tree Collection.asset");
                AssetDatabase.SaveAssets();

                EditorUtility.FocusProjectWindow();

                Selection.activeObject = asset;
            }
        }
    }
}