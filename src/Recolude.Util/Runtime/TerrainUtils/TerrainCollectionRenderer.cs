using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Recolude.Util.TerrainUtils
{
    public class TerrainCollectionRenderer : MonoBehaviour
    {
        [SerializeField]
        TreeCollection[] collectionsToRender;

        private void Start()
        {
            foreach (var collection in collectionsToRender)
            {
                collection.Setup();
            }
        }

        private void Update()
        {
            foreach (var collection in collectionsToRender)
            {
                collection.Render();
            }
        }
    }
}