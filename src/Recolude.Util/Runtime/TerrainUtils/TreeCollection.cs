using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Recolude.Util.TerrainUtils
{

    [CreateAssetMenu(fileName = "Tree Collection", menuName = "Recolude/Terrain/Tree Collection", order = 1)]
    public class TreeCollection : ScriptableObject
    {
        [System.Serializable]
        public struct Tree
        {
            public Vector3 Position;

            public Vector3 Scale;
        }

        [SerializeField]
        private Color color1;

        [SerializeField]
        private Color color2;

        [SerializeField]
        private Tree[] trees;

        [SerializeField]
        private Material material;

        [SerializeField]
        private Mesh mesh;

        [SerializeField]
        private string materialColorPropertyName;

        private Matrix4x4[] matrices;

        private MaterialPropertyBlock block;

        public static TreeCollection Build(Tree[] trees)
        {
            TreeCollection asset = ScriptableObject.CreateInstance<TreeCollection>();
            asset.trees = trees;
            return asset;
        }

        public void Setup()
        {
            matrices = new Matrix4x4[trees.Length];
            Vector4[] colors = new Vector4[trees.Length];

            block = new MaterialPropertyBlock();

            for (int i = 0; i < trees.Length; i++)
            {
                Quaternion rotation = Quaternion.Euler(Random.Range(-4, 4), Random.Range(-180, 180), Random.Range(-4, 4));

                var mat = Matrix4x4.TRS(
                    trees[i].Position,
                    rotation,
                    trees[i].Scale
                );

                matrices[i] = mat;
                colors[i] = Color.Lerp(color1, color2, Random.value);
            }

            // Custom shader needed to read these!!
            block.SetVectorArray(materialColorPropertyName, colors);
        }


        public void Render()
        {
            Graphics.DrawMeshInstanced(mesh, 0, material, matrices, trees.Length, block);
        }

    }

}
